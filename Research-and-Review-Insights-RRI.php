<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Research and Review Insights (RRI)</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Research and Review Insights (RRI)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Research and Review Insights (RRI)</h2>
                <!-- <h4>Online ISSN: 2059-0393</h4>  -->
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!-- <span class="black-text"><i class="fa fa-university"></i>    University of Lincoln<br><br></span> -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
              
             
                 
                 

<p><a href="img/covers-RRI.jpg" target="_blank"><img src="img/covers-RRI.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Research and Review Insights (RRI) is a bimonthly, open access, peer-reviewed journal which aims to publish only Research and Review articles that present significant advances in all fields of medical, clinical, engineering, environmental, pharmaceutical and life sciences disciplines.</p>

<p>RRI operate a system of rapid review process and publication which aims to increase the visibility to provide freely accessible research information worldwide.</p>

<p>RRI welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.rri@oatext.com">editor.rri@oatext.com</a></p>

<p>Please, follow the&nbsp;<a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>

<p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by RRI, is properly cited.</p>



               
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Editor in chief</h2>
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Editorial_Board" class="content">
              
              <h2 class="mb30 blue-bg white-text p10">Executive Editors</h2>
                <div class="row">
                  <div class="medium-4 columns">
                    <h4>Vincenzo Naddeo</h4>
                    <p>Associate Professor in Environmental Engineering<br>
                      Sanitary Environmental Engineering Division (SEED)<br>
                      Department of Civil Engineering<br>
                      University of Salerno <br>
                      Italy</p>
                  </div>
                  <div class="medium-4 columns">
                  	<h4>Gang Li</h4>
                  	<p>Division of Cardiology<br>
					Department of Geriatrics<br>
					The First Affiliated Hospital of Chongqing Medical University<br>
					The First Youyi Road, Yuzhong District<br>
					Chongqing<br>
					China</p>
                  </div>
                  <div class="medium-4 columns"></div>
                </div>



              <h2 class="mb30 blue-bg white-text p10">Editorial Board</h2>
              
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Salem Bouomrani</h4>
                  <p>Department of Internal Medicine<br>
                    Military Hospital of Gabes. Beb Bhar<br>
                    Tunisia</p>
                </div>
                <div class="medium-4 columns">
                	<h4>Liu Jia</h4>
                	<p>Surgeon<br>
					Jilin University<br>
					China</p>
                </div>
                <div class="medium-4 columns">
                	<h4>Sana Sharifian</h4>
                	<p>Faculty of Marine Science and Technology<br>
          					Hormozgan University<br>
          					Iran</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                	<h4>Norbert Nwankwo</h4>
                	<p>Department of Clinical Pharmacy<br>
          					University of Port Harcourt<br>
          					Nigeria</p>
                </div>
                <div class="medium-4 columns">
                	<h4>Taha Roodbar Shojaei</h4>
                	<p>Department of Nano-materials and Nanotechnology<br>
        					Institute of Advanced Technology (ITMA)<br>
        					University Putra Malaysia<br>
        					Malaysia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alexander G. Tonevitsky</h4>
                  <p>Head of Department for translational oncology<br>
                    P.A. Hertsen Moscow Research Oncology Institute<br>
                    Russia</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Oriana Trubiani</h4>
                  <p>Department of Medical<br>
                   oral and Biotechnological Sciences<br>
                  "G. D'Annunzio" University of Chieti-Pescara<br>
                  Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Marietha Nel</h4>
                  <p>Senior Research Scientist<br>
                    Department of Surgery<br>
                    Faculty of Health Sciences<br>
                    University of the Witwatersrand<br>
                    South Africa</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Paul Edward Kaloostian</h4>
                  <p>Department of Neurosurgery<br>
                    University of California at Riverside<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Shan Gao</h4>
                  <p>Laboratory of Protozoology<br>
                    Institute of Evolution & Marine Biodiversity<br>
                    Ocean University of China<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pratibha Mishra nee Vyas</h4>
                  <p>Microbiology Domain<br>
                    School of Biotechnology and Biosciences<br>
                    Lovely Professional University<br>
                    India</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Michael Trimmel</h4>
                  <p>Professor<br>
                      Institute of Environmental Health<br>
                      Medical University of Vienna<br>
                      Austria</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Rina Chakrabarti</h4>
                  <p>Aqua Research Lab
                    Department of Zoology<br>
                    University of Delhi<br>
                    India</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ali F. Alghamdi</h4>
                  <p>Chemistry department
                  Faculty of science<br>
                  Taibah University<br>
                  Saudi Arabia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Maria Pina Serra</h4>
                  <p>Department of Biomedical Sciences<br>
                  University of Cagliari<br>
                  Italy</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Li-Pin Kao</h4>
                  <p>Australian Institute for Bioengineering and Nanotechnology<br>
                      Th e University of Queensland<br>
                      Australia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mahmoud G Soliman</h4>
                  <p>Faculty of Physics<br>
                    Philipps-Universität Marburg<br>
                    Deutschland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Lucia Musumeci</h4>
                  <p>Department of Cardiovascular Sciences<br>
                    University of Liège<br>
                    Belgium</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Laura Mihai</h4>
                  <p>National Institute for Laser<br>
                  Plasma and Radiation Physics<br>
                  (INFLPR) Romania</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mohammad Alghoul</h4>
                  <p>Senior Research Scientist<br>
                    Renewable Energy Program<br>
                    Energy and Building Research Center<br>
                    Kuwait Institute for Scientiﬁc Research<br>
                    Kuwait</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mohammed Alasel</h4>
                  <p>Philipps University<br>
                      Institute of Pharmaceutical chemistry<br>
                      Marburg, Germany</p>
                </div>
              </div>

              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>

              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>





            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <!--<h4>Volume 1, Issue 6</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <!--<h4>Volume 1, Issue 5</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <!--<h4>Volume 1, Issue 4</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <p>Will be updated soon.</p>
             <!-- <div class="lee">
            <dl data-accordion="" class="accordion" >
              <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
              <div class="content" id="panel1b">
                  <div class="hr-tabs">
                  <ul class="tabs" data-tab>
                      <li class="tab-title active"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                    <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                  </ul>
                  <div class="tabs-content">
                    <div class="content  " id="panela">
                      <div class="row pt10">
                      <div class="small-12 columns">
                    AAA
                        </div>
                        </div>
                        </div>

          <div class="content" id="panelb">B</div>

            <div class="content" id="panelc">C</div> 

        </div>

          </div>
          
        </dd>
      </dl>
    </div> -->
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              
              <p>RRI welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.rri@oatext.com">editor.rri@oatext.com</a></p>



            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
