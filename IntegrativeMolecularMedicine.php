<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="google-site-verification" content="GRZVwJKdrXMzh98mqJZ25yBoTeCLw2OA05Pj4hC8o4s" />
<title>Molecular Medicine | Molecular Medicine Reports | Molecular Medicine US</title>
<meta name="description" content="Molecular medicine strives to promote the understanding of normal body functioning and disease pathogenesis at the molecular level, molecular medicine is an interdisciplinary approach to human biology." />
<meta name="keywords" content="molecular medicine, molecular medicine reports, molecular medicine US, molecular medicine impact factor, molecular medicine ppt, molecular medicine presentation, molecular medicine jobs, molecular medicine careers, molecular medicine definition, molecular medicine journal." />
<meta name="robots" content="index,follow"/>
<link rel="canonical" href="http://www.oatext.com/IntegrativeMolecularMedicine.php" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,600' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css"/>
<link rel="stylesheet" href="css/custom.css"/>
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Integrative Molecular Medicine (IMM)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes" class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges</a></dd>
            <dd><a  name="AnchorName" href="#Special_Issues"    class="anchor">Special Issues</a></dd>
          </dl>
          <!--<h4 class="mt10">In Collaboration With</h4> -->
          <div class="text-center"> </div>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Integrative Molecular Medicine (IMM)</h2>
                <h4>Online ISSN: 2056-6360</h4>
                <h2 class="mb5"><a href="#Editor-in-Chief">(Editor in Chief)</a></h2>
                <div class="row mt12">
                  <div class="medium-4 columns">
                    <h2 class="mb5"><a href="#a-1">Ivan Gout</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> University College London</span> </div>
                  
                  <div class="medium-4 columns">
                    <h2 class="mb5"><a href="#a-2">Ricardo H. Alvarez </a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> Cancer Treatment Centers of America</span> </div>

                  <div class="medium-4 columns">
                    <h2 class="mb5"><a href="#a-3">Koji Wakame</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> Hokkaido Pharmaceutical University school of Pharmacy</span></div>
                  
                  
                </div>
                <hr/>
                <p class="text-justify"><a href="img/unnamed-2.jpg"  target="_blank"><img src="img/unnamed-2.jpg" class="pl20" alt="" align="right" width="20%"></a>Integrative Molecular Medicine (IMM) is a peer-reviewed, online open access journal dedicated to a new research discipline at the interface between clinical research and basic biology. We aim to publish articles that broadly enlighten the biomedicine research community by providing an insight on breakthrough discoveries in basic and clinical medicinal research, thereby lending a strong impetus to this important and rapidly developing field and helping to forge new links between clinicians and molecular biologists. Integrative Molecular Medicine highlights ongoing integrative basic and clinical biomedicines covering fields of biology and medicine.As such the following qualities are essential in any article published by the journal: scientific credibility and rigour, and coherence and clarity in the writing. Contributions do not need to be novel as confirmatory and replication studies will be considered, however the article must present new findings, which could include the reporting of negative findings.</p>
                <p class="text-justify">IMM provides a new platform for all researchers, scientists, scholars, students to publish their research work and update the latest research information.Integrative Molecular Medicine publishes research articles as full-length research papers and short reports. In addition, the journal publishes editorials and review articles in innovative formats that target a broad and non-specialized audience.</p>
                <p class="text-justify">Journal will accept article from the following topics are integrative medical fields including biochemistry, molecular and cell biology, biotechnology, genetics, physiology, endocrinology, signal transduction, cell proliferation, differentiation and development, stem cells and regenerative medicine apoptosis, gene expression, pathology, metabolic disease, genomics, transcriptomics, proteomics, metabolomics of disease, systems medicine, brain disease, cardiovascular biology, diabetes, obesity, osteoporosis, nutrition, pharmacology, toxicology, cancer biology and oncology, epidemiology, genetic medicine, and other medicine-related topics.</p>
                <p class="text-justify">IMM welcomes direct submissions of manuscripts from authors. You can submit your manuscript to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>
                  <!--; alternatively to: <a href="mailto:yamaguchi@oatext.com">yamaguchi@oatext.com</a>-->
                </p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Ivan Gout</h2>
              <hr>
              <p><img align="right" class="profile-pic" alt="" src="img/Ivan-Gout.jpg">I graduated as an MD at Lviv State Medical University (Ukraine) in 1983 with a great passion to become a surgeon in oncology. Thinking that a PhD in experimental oncology would help to realize my dream, I obtained my doctorate at the Institute of Experimental Oncology, National Academy of Sciences of Ukraine in 1987. A fellowship from the International Agency for Research on Cancer (IARC) took me even further from clinical oncology and also from Ukraine. I arrived in London on the first wave of perestroika and began my post-doctoral training in Jim Woodgett's laboratory at the Ludwig Institute for Cancer Research (UCL Branch). Subsequently, I worked in Mike Waterfield's laboratory at the same Institute studying signal transduction via the PI3 kinase pathway. In 1996, I started my own group at the Ludwig Institute for Cancer Research, focusing on the regulation of growth via the S6 kinase pathway. Since 2003, I have been a Professor in the Department of Biochemistry at UCL – now renamed Structural and Molecular Biology, where I have an active research group working on signal transduction and cellular metabolism in health and disease. I have a strong research (113 papers in peer-reviewed journals, H-Index 52) and patent (10 world-wide patents) portfolio and run two drug discovery programs aimed at developing small molecule inhibitors, targeting ribosomal S6 kinase and Aurora A kinase.</p>
              <h2>Ricardo H. Alvarez</h2>
              <hr/>
              <p><img align="right" class="profile-pic" alt="" src="img/Dr. Alvarez.jpg">Dr. Alvarez, a native of Argentina, joined CTCA in October 2014 to lead the company&rsquo;s global focus on cancer research. He is internationally known for his contribution to breast cancer multidisciplinary management, inflammatory breast cancer, and discovery and monitoring of minimal residual disease in solid tumors. As a medical oncologist, he has focused his clinical career the past two decades on treating patients with the most difficult cases of breast cancer. Prior to joining CTCA, Dr. Alvarez served for five years as an Assistant Professor in the University of Texas MD Anderson Cancer Center Department of Breast Medical Oncology, Division of Cancer Medicine in Houston, Tx. He also served as Assistant Professor at The Morgan Welch Inflammatory Breast Cancer Research Program. His additional experience includes teaching at the Department of Pathology and the Department of Internal Medicine, Universidad Nacional de La Plata, Argentina. He served as Chief, Oncology Section, Hospital Ramon Carrillo - San Carlos de Bariloche, Rio Negro, Argentina, and Instructor, Oncology Consultants P.A., Houston, Tx. He was Secretary of Clinical Oncology Section, School of Oncology, Fundacion Jose Maria Mainetti, Centro Oncologico de Excelencia, Gonnet, Argentina; and Member, Centro Oncologico de Excelencia, Gonnet.</p>
              <p>Dr. Alvarez received his medical degree from Universidad Nacional de La Plata - Facultad de Medicina, Argentina. He completed his residency in internal medicine and medical oncology at Centro Oncologico de Excelencia - Gonnet, Argentina. Following completion of his medical oncology training, he pursued additional training in Clinical Pathology to further study molecular diagnostic and serve as Instructor at the Pathology Department with Dr. Pedro Laguens at the Universidad Nacional de La Plata. He also participated in multiple research activities and attended multiple grand rounds with Professors Jose Maria Mainetti and Alberto Luchina, and with several others became leaders in Argentinean oncology. Following his training in Argentina, he moved to Houston to pursue a medical residency in internal medicine at The University of Texas at Houston, and completed a fellowship in hematology and oncology at The University of Texas MD Anderson Cancer Center, Houston, TX. He earned a master&rsquo;s degree in cancer biology from the Graduate School of Biomedical Sciences at Houston. Dr. Alvarez has received numerous professional awards for his work in cancer research, including T-32 NIH CA009666 in 2006, Award of Excellence in Cancer Research by Texas Medical Society of Oncology in 2008, and the Susan Papizan Dolan Fellowship in Breast Cancer Research in 2008. He has participated in nearly $5 million in research grants and contracts with multiple clinical studies, including several Phase II Investigator Initiated Studies, since 2009 alone.</p>
              <p>In 2014, he was Principal Investigator in a study related to monitoring minimal residual disease in locally advanced breast cancer and inflammatory breast cancer by detecting circulating tumor cells. The goal of this study, funded by Sister Institution Fund Network as part of the Global Academy Program Department at MD Anderson, is to identify molecular markers of breast cancer tumor recurrence. This clinical trial is currently active in the U.S., as well as in five academic Institutions across the world, including: INCan in Mexico, INEM in Peru, Cl&Iacute;nica Alemana in Santiago de Chile, Barretos Hosptial in Brasil, and University of Oslo in Norway. Dr. Alvarez has authored more than 50 articles, abstracts and book chapters and is a frequent presenter at international oncology conferences. His professional memberships include: American Society of Clinical Oncology (ASCO,) American College of Physicians (ACP), American Society of Hematology (ASH), American Association of Cancer Research (AACR), and the Society of Clinical Research Associates (SOCRA). He was a founding member of Grupo de Estudio Tratamiento e Investigacion del Cancer del Sur (GETICS) in Argentina in 1996.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10"> Regional Editor-in-Chief</h2>
                </div>
              </div>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Hans-Peter Deigner (Europe)</h4>
                  <p>Dean, Faculty of Medical & Life Sciences<br>
                    Professor of Pharmacology <br>
                    Furtwangen University <br>
                    Germany </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Susumu Minamisawa (Asia Pacific)</h4>
                  <p>Professor<br>
                    Department of Cell Physiology<br>
                    The Jikei University School of Medicine
                    
                    Japan </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Arnon Blum (Middle East)</h4>
                  <p>Professor of Medicine<br>
                    Faculty of Medicine<br>
                    Bar Ilan University<br>
                    Israel</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Tetsuya Mine </h4>
                  <p>Department of Medicine<br>
                    Graduate University of Tokyo School of Medicine<br>
                    Japan</p>
                </div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
              <div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10">Editorial Board</h2>
                </div>
              </div>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>George Perry</h4>
                  <p>Professor and Dean of College of Sciences<br>
                    University of Texas at San Antonio</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Anupam Bishayee</h4>
                  <p>Associate Professor & Chair<br>
                    Department of Pharmaceutical Sciences<br>
                    School of Pharmacy<br>
                    American University of Health Sciences</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ali Cyrus Banan</h4>
                  <p>Professor of Medicine & Physiology<br>
                    Executive Director of  Translational Medicine Research University of South Carolina School  of Medicine</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Rafat A. Siddiqui</h4>
                  <p>Senior Investigator/Director<br>
                    Cellular Biochemistry Laboratory<br>
                    Methodist Research Institute<br>
                    Indiana University Health<br>
                    Adjunct Professor of Medicine<br>
                    Department of Medicine<br>
                    Indiana University School of Medicine</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jennifer Li</h4>
                  <p>Assistant Professor of Histology<br>
                    Department of Biomedical Sciences<br>
                    Mercer University School of Medicine</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Bruno Sobral</h4>
                  <p>Adjunct Professor<br>
                    Virginia Bioinformatics Institute at Virginia Tech<br>
                    Adjunct Professor, Department of Cancer Biology<br>
                    Wake Forest School of Medicine<br>
                  </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Cheng-Jeng Tai</h4>
                  <p>Director/Associate Professor<br>
                    Division of Hematology and Oncology<br>
                    Taipei Medical Univesity Hospital</p>
                </div>
                <div class="medium-4 columns">
                  <h4>David W. Moskowitz</h4>
                  <p>CEO & Chief Medical Officer<br>
                    GenoMed, Inc.<br>
                    ADD IN imm</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sotirios G. Stergiopoulos</h4>
                  <p>Executive Director US Medical Affairs Oncology,<br>
                    Breast Disease Lead Celgene</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Basel al-Ramadi</h4>
                  <p>Professor and Chair<br>
                    Department of Medical Microbiology & Immunology<br>
                    College of Medicine and Health Sciences<br>
                    United Arab Emirates University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mawieh Hamad </h4>
                  <p>Associate Professor<br>
                    Department of Medical Laboratory  Sciences<br>
                    University of Sharjah</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Zoran M Pavlovic</h4>
                  <p>Medical Director CNS<br>
                    PRA International, Germany</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Khalid Siddiqui</h4>
                  <p>Head Biochemistry & Molecular Genetics Lab, <br>
                    Strategic Center for Diabates Research<br>
                    King Saud University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Russell Weiner</h4>
                  <p>Executive Director & Head<br>
                    Merck Clinical Biomarker and Diagnostics Lab</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Shahid Siddiqui</h4>
                  <p>Professor of Genetics, Director<br>
                    Program in Human Genetics & Development <br>
                    Head, Research Strategy Unit <br>
                    Faculty of Dentistry and Medicine<br>
                    Umm-Al-Qura University </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Mohamed A. Sabry</h4>
                  <p>Associate Professor<br>
                    Department of Medical Biochemistry <br>
                    College of Medicine & Medical Sciences<br>
                    Arabian Gulf University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Glen J. Weiss</h4>
                  <p>Clinical Associate Professor<br>
                    Translational Genomics Research Institute (TGen)<br>
                    Clinical Assistant Professor of Medicine<br>
                    The University of Arizona College of Medicine-Phoenix<br>
                    Director of Clinical Research<br>
                    Cancer Treatment Centers of America<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Lilach Soreq</h4>
                  <p>Marie Curie Research Associate <br>
                    Department of Molecular Neuroscience<br>
                    UCL Institute of Neurology<br>
                    United Kingdom </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Derek A. Wainwright</h4>
                  <p>Assistant Professor<br>
                    Department of Neurological Surgery<br>
                    Northwestern University<br>
                    Feinberg School of Medicine</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Kulvinder Singh Saini</h4>
                  <p>Senior Consultant <br>
                    Professor of Biotechnology and Genomics<br>
                    King Abdulaziz University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Tosson Alymorsy</h4>
                  <p>Professor of Parasitology<br>
                    Faculty of Medicine<br>
                    Ain Shams University</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Tomiyasu Murata</h4>
                  <p>Associate Professor<br>
                    Departments of Analytical Neurobiology<br>
                    Faculty of Pharmacy<br>
                    Meijo University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vural Fidan </h4>
                  <p>Deputy Director of Otorhinolaryngology Department<br>
                    Yunus Emre Government Hospital</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Faris Q. Alenzi</h4>
                  <p>Professor of Immunology <br>
                    College of Applied Medical Sciences<br>
                    Salman bin Abdulaziz University </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ahmed Malki</h4>
                  <p>Associate Professor<br>
                    Coordinator of Biomedical Science Graduate Program<br>
                    Department of Health Sciences<br>
                    College of Arts and Sciences<br>
                    Qatar University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Omar Bagasra</h4>
                  <p>Professor, Department of Biology<br>
                    Director <br>
                    South Carolina Center for Biotechnology<br>
                    Claflin University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Renee Dufault</h4>
                  <p>Founding Director<br>
                    Food Ingredient and Health Research Institute </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Asim Ahmed Elnour</h4>
                  <p>Adjunct Clinical Associate Professor<br>
                    Pharmacology Department<br>
                    Faculty of Medicine and Health Sciences (FMHS)<br>
                    United Arab Emirates University (UAEU) </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pinar Erkekoglu</h4>
                  <p>Associate Professor of Toxicology <br>
                    Faculty of Pharmacy <br>
                    Department of Toxicology<br>
                    Hacettepe University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ben Sghaier Mohamed</h4>
                  <p>Laboratory for Forest Ecology<br>
                  National Institute for Research in Rural Engineering, Water and Forests<br>
                  University of carthage<br>
                  Tunisie</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Satoru Osuka</h4>
                  <p>Department of Neurosurgery<br>
                    Winship Cancer Institute<br>
                    Emory University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Tadaaki Miyazaki</h4>
                  <p>Professor<br>
                    Department of Probiotics Immunology<br>
                    Institute for Genetic Medicine<br>
                    Hokkaido University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>William CS Cho</h4>
                  <p>Department of Clinical Oncology<br>
                  Queen Elizabeth Hospital<br>
                  Hong Kong</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Bin Yang</h4>
                  <p>Associate Professor and Staff Pathologist<br>
                    Director of Molecular Cytopathology<br>
                    Director of International Business Development<br>
                    Pathology & Laboratory Medicine Institute<br>
                    Cleveland Clinic Lerner College of Medicine</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jon Ver Halen</h4>
                  <p>Associate Professor<br>
                    Department of Plastic Surgery <br>
                    Texas A&M School of Medicine </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Marco Weiergräber (MD,PhD)</h4>
                  <p>Specialist in Physiology, Director and Professor<br>
                    Federal Institute for Drugs and Medical Devices (Bundesinstitut für Arzneimittel und Medizinprodukte (BfArM))<br>
                    53175 Bonn, Germany</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ali Gholamrezanezhad, MD, ABR, FEBNM </h4>
                  <p>Department of Radiology<br>
                    University Hospitals of Cleveland<br>
                    Case Western Reserve University<br>
                    Cleveland, Ohio, USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Andrea Ottonello</h4>
                  <p>Adjunct Professor<br>
                    Department of Surgical and Diagnostic Integrated Sciences<br>
                    University of Genoa, Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alessandra Lucchese</h4>
                  <p>Department of Orthodontics<br>
                    Vita Salute San Raffaele University<br>
                    Milan, Italy</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Antonella Fioravanti</h4>
                  <p>Assistant Professor<br>
                    Department of Medicine, Surgery and Neuroscience, Rheumatology Unit<br>
                    University of Siena<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mohamed Numan</h4>
                  <p>Department of Biochemistry, Microbiology and Bioinformatics<br>
                    Faculty of Medicine<br>
                    Université Laval – Quebec City<br>
                    Canada</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Tamer A Gheita</h4>
                  <p>Professor of Rheumatology<br>
                    Cairo University<br>
                    Egypt</p>
                  </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Randa G.Naffa</h4>
                  <p>Department of Molecular Biology Research Laboratory<br>
                    Faculty of medicine<br>
                    The University of Jordan<br>
                    Jordan</p>
                </div>
                <div class="medium-4 columns">
                  <h4>C. Gopi Mohan</h4>
                  <p>Associate Professor<br>
                    Amrita Centre for Nanosciences and Molecular Medicine<br>
                    Amrita Institute of Medical Sciences and Research Centre<br>
                    India</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sanjun Shi</h4>
                  <p>Department of Pharmacy,<br>
                    Daping Hospital, <br>
                    Third Military Medical University, <br>
                    Chongqing<br>
                    China </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Vilda Purutcuoglu</h4>
                  <p>Department of Statistics<br>
                  Middle East Technical University<br>
                  Turkey</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yasuhito Shimada, MD, PhD.</h4>
                  <p>Department of Integrative Pharmacology<br>
                    Mie University Graduate School of Medicine<br>
                    Japan</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Veena N Rao</h4>
                  <p>Professor<br>
                    Department of OB/GYN<br>
                    Grady Memorial Hospital<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Akikazu Sakudo</h4>
                  <p>Associate Professor<br>
                  Laboratory of Biometabolic Chemistry<br>
                  School of Health Sciences<br>
                  University of the Ryukyus<br>
                  Japan</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vincent van Ginneken</h4>
                  <p>Chairman & Scientific Director<br> 
                    Blue Green Technologies<br>
                    The Netherlands</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>
              
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>  

            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 4, Issue 3</h4>
              <hr/>
            <!------IMM.1000283------>
              <div class="article">
                <h4><a href="Brain-steatosis-in-an-obese-mouse-model-during-cycles-of-Famine-and-Feast-the-underestimated-role-of-fat-(WAT)-in-brain-volume-formation.php">“Brain steatosis” in an obese mouse model during cycles of Famine and Feast the underestimated role of fat (WAT) in brain volume formation</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vincent van Ginneken <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Evert de Vries <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elwin Verheij <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan van der Greef</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 24, 2017</p>
                </em>
                <hr/>
              </div>
            <!------IMM.1000283 x------>
            <!------IMM.1000285------>
              <div class="article">
                <h4><a href="Moxa-Tar-an-important-source-for-developing-new-anti-inflammatory-drug.php">Moxa Tar, an important source for developing new anti-inflammatory drug</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Qian Zeng</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 28, 2017</p>
                </em>
                <hr/>
              </div>
            <!------IMM.1000285 x------>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4> Volume 4, Issue 2</h4>
              <hr/>
              <!------IMM.1000273------>
              <div class="article">
                <h4><a href="Delayed-anaphylactic-reaction-to-intravenous-infusion-of-ondansetron-report-of-3-cases.php">Delayed anaphylactic reaction to intravenous infusion of ondansetron report of 3 cases</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nesrine BS
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Haifa R
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bouraoui EO
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bechir A
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emna B
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rabiaa A
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yosra BY
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rawdha S
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Salem CB
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Houcem H
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abederrahim K
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zorgati O</p>
                <p class="mb5">Case Report-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 13, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000273 x------>
             <!------IMM.1000274------>
              <div class="article">
                <h4><a href="Acute-exposure-to-an-electric-field-induces-changes-in-human-plasma-lysophosphatidylcholine-(lysoPC)-224-levels-Molecular-insight-into-the-docking-of-lysoPC-224-interaction-with-TRPV2.php">Acute exposure to an electric field induces changes in human plasma lysophosphatidylcholine (lysoPC)-22:4 levels: Molecular insight into the docking of lysoPC-22:4 interaction with TRPV2</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakagawa-Yagi Y
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hara H
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakanishi H
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tasaka T
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hara A</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 18, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000274 x------>
             <!------IMM.1000275------>
              <div class="article">
                <h4><a href="Luteolin-rich-chrysanthemum-flower-extract-suppresses-baseline-serum-uric-acid-in-Japanese-subjects-with-mild-hyperuricemia.php">Luteolin-rich chrysanthemum flower extract suppresses baseline serum uric acid in Japanese subjects with mild hyperuricemia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marina Hirano
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shogo Takeda
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shoketsu Hitoe
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiroshi Shimoda</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 18, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000275 x------>
              <!------IMM.1000276------>
              <div class="article">
                <h4><a href="Comparison-between-developments-of-emergency-medicine-services-in-Saudi-Arabia,-compare-to-USA-and-Canada.php">Comparison between developments of emergency medicine services in Saudi Arabia, compare to USA and Canada</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zohair Al Aseri</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 06, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000276 x------>
             
             <!------IMM.1000277------>
              <div class="article">
                <h4><a href="Cell-death-offers-exceptional-cellular-and-molecular-windows-for-pharmacological-interventions-in-protozoan-parasites.php">Cell death offers exceptional cellular and molecular windows for pharmacological interventions in protozoan parasites</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saeed El-Ashram
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rashid mehmood
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gungor Cagdas Dincel
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ibrahim Al Nasr
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xun Suo</p>
                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 08, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000277 x------>
              <!------IMM.1000278------>
              <div class="article">
                <h4><a href="Characterization-of-the-lipid-profile-post-mortem-for-Type-2-diabetes-in-human-brain-and-plasma-of-the-elderly-with-LCMS-techniques-a-descriptive-approach-of-diabetic-encephalopathy.php">Characterization of the lipid profile post mortem for Type-2 diabetes in human brain and plasma of the elderly with LCMS-techniques: a descriptive approach of diabetic encephalopathy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>V van Ginneken
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>E Verheij
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>M Hekman
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>J van der Greef</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 10, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000278 x------>
              <!------IMM.1000279------>
              <div class="article">
                <h4><a href="Volumetric-overload-shocks-in-the-patho-etiology-of-the-transurethral-resection-prostatectomy-syndrome-and-acute-dilution-hyponatraemia.php">Volumetric overload shocks in the patho-etiology of the transurethral resection prostatectomy syndrome and acute dilution hyponatraemia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nisha Pindoria
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Salma A Ghanem
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khalid A Ghanem
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ahmed N Ghanem</p>
                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 15, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000279 x------>
              <!------IMM.1000280------>
              <div class="article">
                <h4><a href="Computational-prediction-of-novel-human-miRNAs-miRNA-target-sites-in-correlation-with-SNP-rs678653-at-3’UTR-of-cyclin-D1-gene.php">Computational prediction of novel human miRNAs/miRNA target sites in correlation with SNP (rs678653) at 3’UTR of cyclin D1 gene</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nisha Thakur
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Seemi Farhat Basir
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mausumi Bharadwaj
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ravi Mehrotra</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 17, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000280 x------>
              <!------IMM.1000281------>
              <div class="article">
                <h4><a href="Crime-re-offence-and-substance-abuse-of-patients-with-severe-mental-disorder.php">Crime, re-offence, and substance abuse of patients with severe mental disorder</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fernando Almeida
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Diana Moreira</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 20, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000281 x------>
              <!------IMM.1000282------>
              <div class="article">
                <h4><a href="The-plastinated-porcine-heart-specimen-for-learning-echocardiography.php">The plastinated porcine heart specimen for learning echocardiography</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiromi Suenaga</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 21, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000282 x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4> Volume 4, Issue 1</h4>
              <hr/>
              <!------IMM.1000265. x---->
              <div class="article">
                <h4><a href="Joint-Accreditation-Committee-of-the-International-Society-for-Cellular-Therapy-and-the-European-Group-for-Blood-and-Marrow-Transplantation-accreditation-for-autologous-hematopoietic-stem-cell-transplantation-in-non-le.php">Joint Accreditation Committee of the International Society for Cellular Therapy and the European Group for Blood and Marrow Transplantation accreditation for autologous hematopoietic stem cell transplantation in non-leukemic malignancies: burden or benefit?</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Huijberts S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Baars JW <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tinteren HV <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Holtkamp MJ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rodenhuis S</p>
                <p class="mb5">Commentary-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 29, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000265. x------>
              <!------IMM.1000266. x---->
              <div class="article">
                <h4><a href="Fermented-vegetable-and-fruit-extract-(OM-X®)-stimulates-murine-gastrointestinal-tract-cells-and-RAW264.7-cells-in-vitro-and-regulates-liver-gene-expression-in-vivo.php">Fermented vegetable and fruit extract (OM-X®) stimulates murine gastrointestinal tract cells and RAW264.7 cells in vitro and regulates liver gene expression in vivo</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Wakame <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akifumi Nakata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keisuke Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshihiro Mihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muneaki Takahata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasuyoshi Miyake <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masashi Okada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshie Shimomiya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ken-ichi Komatsu</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 18, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000266. x------>
              <!------IMM.1000267. x---->
              <div class="article">
                <h4><a href="Proliferation-without-nuclei-suggests-mitochondrial-cells-MitoCells-to-be-prokaryotic-nature.php">Proliferation without nuclei suggests mitochondrial cells (MitoCells) to be prokaryotic nature</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakano K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakayama T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tachikawa E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mizoguchi E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sasaki K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tarashima M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakayama N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saito K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Osawa M</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 26, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000267. x------>
              <!------IMM.1000268 ------>
              <div class="article">
                <h4><a href="Approaching-non-canonical-STAT3-signaling-to-redefine-cancer-therapeutic-strategy.php">Approaching non-canonical STAT3 signaling to redefine cancer therapeutic strategy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shalini Dimri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sukanya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abhijit De</p>
                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000268  x------>
              <!------IMM.1000269 ------>
              <div class="article">
                <h4><a href="Digital-pathology-An-overview.php">Digital pathology: An overview</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saini ML <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bouzin C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saini K</p>
                <p class="mb5">Mini review-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000269  x------>
              <!------IMM.1000270 ------>
              <div class="article">
                <h4><a href="A-literature-review-of-issues-affecting-PrEP-Education-and-Implementation-in-racism-mistrust-in-black-communities-HIVAIDS.php">A literature review of issues affecting PrEP Education and Implementation in racism mistrust in black communities (HIV/AIDS)</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Cazeau P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Jackson R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Theodore L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Balatien B</p>
                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 14, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000270  x------>
              <!------IMM.1000271 ------>
              <div class="article">
                <h4><a href="Detection-of-serum-hepatitis-B-Virus-antigen-and-hepatitis-C-virus-antibody-from-prostate-cancer-patients-in-Japan.php">Detection of serum hepatitis B Virus antigen and hepatitis C virus antibody from prostate cancer patients in Japan</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ishiguro H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Furuya K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Izumi K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nagai T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kawahara T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kubota Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yao M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Uemura H</p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 14, 2017</p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000271  x------>
              <!------IMM.1000272------>
              <div class="article">
                <h4><a href="Correlation-of-duration-hypertension-and-glycemic-control-with-microvascular-complications-of-diabetes-mellitus-at-a-tertiary-care-hospital.php"> Correlation of duration, hypertension and glycemic control with microvascular complications of diabetes mellitus at a tertiary care hospital </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ramnath Santosh Ramanathan, MD </p>
                <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 21, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------IMM.1000272 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion" >
                  <dd> <a href="#panel1e" class="accordian-active">Volume 4<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1e">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>April 2016</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Feb 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns"> BBB </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000265. x---->
                                <div class="article">
                                  <h4><a href="Joint-Accreditation-Committee-of-the-International-Society-for-Cellular-Therapy-and-the-European-Group-for-Blood-and-Marrow-Transplantation-accreditation-for-autologous-hematopoietic-stem-cell-transplantation-in-non-le.php">Joint Accreditation Committee of the International Society for Cellular Therapy and the European Group for Blood and Marrow Transplantation accreditation for autologous hematopoietic stem cell transplantation in non-leukemic malignancies: burden or benefit?</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Huijberts S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Baars JW <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tinteren HV <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Holtkamp MJ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rodenhuis S</p>
                                  <p class="mb5">Commentary-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000265. x------>
                                <!------IMM.1000266. x---->
                                <div class="article">
                                  <h4><a href="Fermented-vegetable-and-fruit-extract-(OM-X®)-stimulates-murine-gastrointestinal-tract-cells-and-RAW264.7-cells-in-vitro-and-regulates-liver-gene-expression-in-vivo.php">Fermented vegetable and fruit extract (OM-X®) stimulates murine gastrointestinal tract cells and RAW264.7 cells in vitro and regulates liver gene expression in vivo</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Wakame <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akifumi Nakata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keisuke Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshihiro Mihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muneaki Takahata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasuyoshi Miyake <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masashi Okada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshie Shimomiya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ken-ichi Komatsu</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 18, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000266. x------>
                                <!------IMM.1000267. x---->
                                <div class="article">
                                  <h4><a href="Proliferation-without-nuclei-suggests-mitochondrial-cells-MitoCells-to-be-prokaryotic-nature.php">Proliferation without nuclei suggests mitochondrial cells (MitoCells) to be prokaryotic nature</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakano K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakayama T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tachikawa E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mizoguchi E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sasaki K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tarashima M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakayama N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saito K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Osawa M</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 26, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000267. x------>
                                <!------IMM.1000268 ------>
                                <div class="article">
                                  <h4><a href="Approaching-non-canonical-STAT3-signaling-to-redefine-cancer-therapeutic-strategy.php">Approaching non-canonical STAT3 signaling to redefine cancer therapeutic strategy</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shalini Dimri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sukanya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abhijit De</p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000268  x------>
                                <!------IMM.1000269 ------>
                                <div class="article">
                                  <h4><a href="Digital-pathology-An-overview.php">Digital pathology: An overview</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saini ML <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bouzin C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saini K</p>
                                  <p class="mb5">Mini review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000269  x------>
                                <!------IMM.1000270 ------>
                                <div class="article">
                                  <h4><a href="A-literature-review-of-issues-affecting-PrEP-Education-and-Implementation-in-racism-mistrust-in-black-communities-HIVAIDS.php">A literature review of issues affecting PrEP Education and Implementation in racism mistrust in black communities (HIV/AIDS)</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Cazeau P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Jackson R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Theodore L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Balatien B</p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 14, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000270  x------>
                                <!------IMM.1000271 ------>
                                <div class="article">
                                  <h4><a href="Detection-of-serum-hepatitis-B-Virus-antigen-and-hepatitis-C-virus-antibody-from-prostate-cancer-patients-in-Japan.php">Detection of serum hepatitis B Virus antigen and hepatitis C virus antibody from prostate cancer patients in Japan</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ishiguro H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Furuya K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Izumi K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nagai T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kawahara T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kubota Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yao M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Uemura H</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 14, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000271  x------>
                                <!------IMM.1000272------>
                                <div class="article">
                                  <h4><a href="Correlation-of-duration-hypertension-and-glycemic-control-with-microvascular-complications-of-diabetes-mellitus-at-a-tertiary-care-hospital.php"> Correlation of duration, hypertension and glycemic control with microvascular complications of diabetes mellitus at a tertiary care hospital </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ramnath Santosh Ramanathan, MD </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 21, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000272 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1d" class="accordian-active">Volume 3 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1d">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#v2-panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec  2016</span> <span class="mt3 grey-text">Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#v2-panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Oct  2016</span> <span class="mt3 grey-text">Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#v2-paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Aug  2016</span> <span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>June  2016</span> <span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Apr  2016</span> <span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#v2-panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Feb  2016</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="v2-panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000253. x---->
                                <div class="article">
                                  <h4><a href="Effects-of-serotonin-transporter-gene-polymorphism-on-mood-during-the-period-before-the-competition-in-Japanese-ballet-dancers.php">Effects of serotonin transporter gene polymorphism on mood during the period before the competition in Japanese ballet dancers</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kanaka Yatabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshio Kumai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiroto Fujiya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Naoko Yui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Satomi Kasuya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuka Murofushi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keisuke Tateishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fumiko Terawaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hajime Kobayashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Aya Uchino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takaaki Kudo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mahiro Ohno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hisao Miyano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tadasu Oyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Haruki Musha
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000253. x------>
                                <!------IMM.1000254. x---->
                                <div class="article">
                                  <h4><a href="Is-Emilin-1-a-molecular-link-contributing-to-the-extension-of-thoracic-aortic-aneurysm-dissection-and-increasing-the-magnitude-of-the-associated-hypertension.php">Is Emilin-1 a molecular link contributing to the extension of thoracic aortic aneurysm dissection and increasing the magnitude of the associated hypertension</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Simon W Rabkin</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000254. x------>
                                <!------IMM.1000255. x---->
                                <div class="article">
                                  <h4><a href="Computational-tools-and-interdisciplinary-ingenuity-for-accelerating-the-development-of-new-medicine.php">Computational tools and interdisciplinary ingenuity for accelerating the development of new medicine</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Matthew E. Welsch <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sotirios G. Stergiopoulos</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000255. x------>
                                <!------IMM.1000256. x---->
                                <div class="article">
                                  <h4><a href="Aggregation-of-Sphingosine-DNA-and-cell-construction-using-components-from-egg-white.php">Aggregation of Sphingosine-DNA and cell construction using components from egg white</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shoshi Inooka</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000256. x------>
                                <!------IMM.1000257. x---->
                                <div class="article">
                                  <h4><a href="IgE-class-switch-recombination-is-regulated-by-CaMKII-via-NF-B-alternative-pathway.php">IgE class switch recombination is regulated by CaMKII via NF-κB alternative pathway</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kano Tanabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akihiro Goto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akane Maeda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hitomi Sakamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryutaro Kajihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kohji Fukunaga <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Seiji Inui</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000257. x------>
                                <!------IMM.1000258. x---->
                                <div class="article">
                                  <h4><a href="Science-of-omics-Perspectives-and-Prospects-for-human-health-care.php">Science of omics: Perspectives and Prospects for human health care</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nawin Mishra</p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000258. x------>
                                <!------IMM.1000259. x---->
                                <div class="article">
                                  <h4><a href="Identification-of-rasraf-binding-site-and-design-of-interfering-peptide-with-potential-clinical-application.php">Identification of ras/raf binding site and design of interfering peptide with potential clinical application</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tian L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zhang X <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nemati F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vallerand D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Raimonide C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Decaudin D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Brossas JY <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zini JM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sylvain Choquet S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Scoazec MF <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Feillant M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Le Ster K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Loisel S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rebollo A</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 06, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000259. x------>
                                <!------IMM.1000260. x---->
                                <div class="article">
                                  <h4><a href="Comprehensive-analysis-of-surface-proteins-of-peripheral-mononuclear-blood-cells-in-patients-with-systemic-lupus-erythematosus.php">Comprehensive analysis of surface proteins of peripheral mononuclear blood cells in patients with systemic lupus erythematosus</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nozawa Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Arito M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Omoteyama K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sato M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takakuwa Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ooka S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kurokawa MS <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomohiro Kato</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000260. x------>
                                <!------IMM.1000261. x---->
                                <div class="article">
                                  <h4><a href="cAMP-dependent-protein-kinase-A-acts-as-a-negative-regulator-for-nontypeable-Haemophilus-influenzae-induced-GM-CSF-expression-via-inhibition-of-MEK-ERK-signaling-pathway.php">cAMP-dependent protein kinase A acts as a negative regulator for nontypeable Haemophilus influenzae-induced GM-CSF expression via inhibition of MEK-ERK signaling pathway</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukihiro Tasaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wenzhuo Y. Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anuhya S. Konduru <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kensei Komatsu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shingo Matsuyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hirofumi Kai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jian-Dong Li</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000261. x------>
                                <!------IMM.1000262------>
                                <div class="article">
                                  <h4><a href="The-evidence-of-genetic-polymorphisms-of-genes-involved-in-the-P2RX7-signaling-pathway-as-predictive-biomarkers-for-response-and-loss-of-response-to-infliximab-against-Crohns-disease.php"> The evidence of genetic polymorphisms of genes involved in the <em>P2RX7</em> signaling pathway as predictive biomarkers for response and loss of response to infliximab against Crohn's disease </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Araki C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshimura M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fukumitsu Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ma S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ishida T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Urabe S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Matsushima K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Honda T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Uehara R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fukuda Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshima F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Higuchi N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Isomoto H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nakao K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tsukamoto K </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000262 x------>
                                <!------IMM.1000263. x---->
                                <div class="article">
                                  <h4><a href="Study-on-immune-changes-and-correlation-of-T-regulatory-cells-and-IL-35-in-the-early-phase-of-rat-model-of-acute-pancreatitis.php">Study on immune changes and correlation of T regulatory cells and IL-35 in the early phase of rat model of acute pancreatitis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zhuo Yue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>You Zhao Yang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abu Taiub Mohammed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mohiuddin Chowdhury</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000263. x------>
                                <!------IMM.1000264. x---->
                                <div class="article">
                                  <h4><a href="Enhancing-mood-relaxation-and-memories-through-the-TTAP®-approach-in-residents-with-MCI.php">Enhancing mood, relaxation and memories through the TTAP® approach in residents with MCI</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Madori LL <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Balacky C</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000264. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000241.------>
                                <div class="article">
                                  <h4><a href="Effect-of-3-hydroxybutyrate-an-endogenous-histone-deacetylase-inhibitor-on-FOXO3A-mRNA-expression-in-human-epithelial-colorectal-Caco-2-cells-Insight-into-the-epigenetic-mechanisms-of-electric-field-therapy.php">Effect of 3-hydroxybutyrate, an endogenous histone deacetylase inhibitor, on FOXO3A mRNA expression in human epithelial colorectal Caco-2 cells: Insight into the epigenetic mechanisms of electric field therapy</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuzo Nakagawa-Yagi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Hara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hidenori Tsuboi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junji Abe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akikuni Hara</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 13, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000241. x------>
                                <!------IMM.1000242.------>
                                <div class="article">
                                  <h4><a href="New-scaffolds-of-inhibitors-targeting-the-DNA-binding-of-NF-κB.php">New scaffolds of inhibitors targeting the DNA binding of NF-κB</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takanobu Kobayashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sei-ich Tanuma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Miyazawa</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000242. x------>
                                <!------IMM.1000243.------>
                                <div class="article">
                                  <h4><a href="Hb-Crete-in-a-Turkish-Family-with-Crete-Island-origin.php">Hb Crete in a Turkish Family with Crete Island origin</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Duran Canatan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>İbrahim Keser <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Türker Bilgen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Serpil Delibaş</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000243. x------>
                                <!------IMM.1000244.------>
                                <div class="article">
                                  <h4><a href="Hypothalamus-pituitary-adrenal-HPA-axis-chronic-stress-hair-cortisol-metabolic-syndrome-and-mindfulness.php">Hypothalamus-pituitary-adrenal (HPA) axis, chronic stress, hair cortisol, metabolic syndrome and mindfulness</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Helen Patricia Gaete</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000244. x------>
                                <!------IMM.1000245.------>
                                <div class="article">
                                  <h4><a href="Metabolic-comfort-in-oncology-and-free-amino-acids-perspectives-for-the-use-of-their-regulatory-actions-in-physiological-concentrations.php">Metabolic comfort in oncology and free amino acids: perspectives for the use of their regulatory actions in physiological concentrations</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karavay A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karavay P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kaliada T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nefyodov L</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000245. x------>
                                <!------IMM.1000246. x---->
                                <div class="article">
                                  <h4><a href="The-effect-of-trans-resveratrol-on-the-expression-of-the-human-DNA-repair-associated-genes.php">The effect of trans-resveratrol on the expression of the human DNA-repair associated genes</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fumiaki Uchiumi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Arakawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yutaka Takihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Motohiro Akui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sayaka Ishibashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sei-ichi Tanuma</p>
                                  <p class="mb5">Review article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 20, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000246. x------>
                                <!------IMM.1000247. x---->
                                <div class="article">
                                  <h4><a href="Ruptured-intracranial-dermoid-cyst-a-case-report.php">Ruptured intracranial dermoid cyst: a case report</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Domenico Murrone <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bruno Romanelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Aldo Ierardi</p>
                                  <p class="mb5">Case Report-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000247. x------>
                                <!------IMM.1000248. x---->
                                <div class="article">
                                  <h4><a href="Characterization-of-small-leucine-rich-proteoglycans-in-aortic-valves-of-patients-with-aortic-valve-stenosis.php">Characterization of small leucine-rich proteoglycans in aortic valves of patients with aortic valve stenosis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiroshi Furukawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masahide Chikada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michiyo K. Yokoyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mitsumi Arito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Manae S. Kurokawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshiyuki Sato <br>
                                    <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masaaki Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuki Omoteyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Naoya Suematsu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshiya Kobayashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masahiro Sagane <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hirotoshi Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takashi Ando <br>
                                    <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomohiro Kato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tsuyoshi Miyairi</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000248. x------>
                                <!------IMM.1000249. x---->
                                <div class="article">
                                  <h4><a href="Exercise-interleukins-and-bone-homeostasis.php">Exercise, interleukins and bone homeostasis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>John Kelly Smith</p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000249. x------>
                                <!------IMM.1000250. x---->
                                <div class="article">
                                  <h4><a href="The-benefits-of-incorporating-basic-sciences-and-translational-research-into-a-pharmacy-school-curriculum.php">The benefits of incorporating basic sciences and translational research into a pharmacy school curriculum</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rachel R. Miller <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gregory S. Gorman</p>
                                  <p class="mb5">Editorial-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000250. x------>
                                <!------IMM.1000251. x---->
                                <div class="article">
                                  <h4><a href="The-Re-Emerging-Role-of-Iron-in-Infection-and-Immunity.php">The Re-Emerging Role of Iron in Infection and Immunity</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mawieh Hamad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khuloud Bajbouj</p>
                                  <p class="mb5">Commentary Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000251. x------>
                                <!------IMM.1000252. x---->
                                <div class="article">
                                  <h4><a href="Investigation-of-Mutations-in-Exon-7-8-and-Exon-9-of-FHIT-Gene-in-Laryngeal-Squamous-Cell-Carcinoma.php">Investigation of Mutations in Exon 7,8 and Exon 9 of FHIT Gene in Laryngeal Squamous Cell Carcinoma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nesrin Turaçlar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hasibe Cingilli Vural <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Şahande Elagöz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emine Elif Altuntaş <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fikriye Polat</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000252. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000231.------>
                                <div class="article">
                                  <h4><a href="Repetitive-stretching-enhances-the-formation-of-neurite-swellings-in-cultured-neuronal-cells.php"> Repetitive stretching enhances the formation of neurite swellings in cultured neuronal cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiromichi Nakadate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Evrim Kurtoglu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shota Shirasaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shigeru Aomura </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 06, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000231. x------>
                                <!------IMM.1000232.------>
                                <div class="article">
                                  <h4><a href="Anticholinergic-activity-disappears-soon-after-the-prescription-of-cholinesterase-inhibitor.php"> Anticholinergic activity disappears soon after the prescription of cholinesterase inhibitor </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Taishi Koganemaru <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koji Hori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Misa Hosoi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimiko Konishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsugu Hachisu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroi Tomioka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masayuki Tani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuka Kitajima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsuko Inamoto </p>
                                  <p class="mb5">Case Report-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000232. x------>
                                <!------IMM.1000233.------>
                                <div class="article">
                                  <h4><a href="Proteomics-and-molecular-medicine.php"> Proteomics and molecular medicine </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Margaret Simonian </p>
                                  <p class="mb5">Editorial-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000233. x------>
                                <!------IMM.1000234.------>
                                <div class="article">
                                  <h4><a href="Distinguishing-malignant-from-benign-prostate-using-Br-Ca-K-Mg-Mn-and-Na-content-in-prostatic-tissue.php"> Distinguishing malignant from benign prostate using Br, Ca, K, Mg, Mn, and Na content in prostatic tissue </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vladimir Zaichick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sofia Zaichick </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000234. x------>
                                <!------IMM.1000235. ------>
                                <div class="article">
                                  <h4><a href="DNA-microarray-analysis-of-gene-expression-changes-in-ICR-mouse-liver-following-treatment-with-active-hexose-correlated-compound.php"> DNA microarray analysis of gene expression changes in ICR mouse liver following treatment with active hexose correlated compound </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Wakame <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akifumi Nakata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keisuke Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takehito Miura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anil.D.Kulkarni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marie-Francoise Doursout <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alamelu Sundersan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ken-Ich Komatsu </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000235.  x------>
                                <!------IMM.1000236.------>
                                <div class="article">
                                  <h4><a href="A-bright-future-for-breast-and-colon-cancer-patients.php ">A bright future for breast and colon cancer patients</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Veronica J. James <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark McGovern <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Peihong Wu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Boyang Chang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yankeng Wu</p>
                                  <p class="mb5">Mini Review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 18, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000236. x------>
                                <!------IMM.1000237.------>
                                <div class="article">
                                  <h4><a href="Cancer-incidence-and-the-biology-of-extreme-old-age.php">Cancer incidence and the biology of extreme old age</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Josh Hiller <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Celeste Vallejo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leo Betthauser <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James Keesling</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 19, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000237. x------>
                                <!------IMM.1000238.------>
                                <div class="article">
                                  <h4><a href="Distinct-functional-roles-of-cancerous-immunoglobulins-in-cancer-immunology.php">Distinct functional roles of cancerous immunoglobulins in cancer immunology</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gregory Lee</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000238. x------>
                                <!------IMM.1000239.------>
                                <div class="article">
                                  <h4><a href="Antitumor-activities-of-BIBF-1120-BI-860585-and-BI-836845-in-preclinical-models-of-sarcoma.php">Antitumor activities of BIBF 1120, BI 860585, and BI 836845 in preclinical models of sarcoma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoko Takai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Asuka Matsuo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhiwei Qiao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tadashi Kondo</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 26, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000239. x------>
                                <!------IMM.1000240.------>
                                <div class="article">
                                  <h4><a href="Kynurenic_acid_an_aryl_hydrocarbon_receptor_ligand_is_elevated_in_serum_of_Zucker_fatty_rats.php">Kynurenic acid, an aryl hydrocarbon receptor ligand, is elevated in serum of Zucker fatty rats</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Oxenkrug G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Cornicelli J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>van der Hart M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roeser J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Summergrad P</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 29, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000240. x------>
                                <!------IMM.1000 S1001.------>
                                <div class="article">
                                  <h4><a href="Regulatory-role-of-heat-shock-proteins-in-autoimmune-and-inflammatory-diseases.php"> Regulatory role of heat-shock proteins in autoimmune and inflammatory diseases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cristina Daneri-Becerra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mario D. Galigniana </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000 S1001. x------>
                                <!------IMM.1000 S1002. ------>
                                <div class="article">
                                  <h4><a href="Immunotherapy-in-basal-cell-carcinoma.php"> Immunotherapy in basal cell carcinoma </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nepton Sheik Khoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>GhazalehShoja E Razavi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Himakshi Sharma </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000 S1002.  x------>
                                <!------IMM.1000 S1003.------>
                                <div class="article">
                                  <h4><a href="Suppressor-of-cytokine-signaling-and-rheumatoid-arthritis.php"> Suppressor of cytokine signaling and rheumatoid arthritis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Charles J. Malemud </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000 S1003. x------>
                                <!------IMM.1000 S1004.------>
                                <div class="article">
                                  <h4><a href="Immunotherapy-for-cancers-of-the-biliary-tract.php"> Immunotherapy for cancers of the biliary tract </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Nepton Sheik Khoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abdul Rahman El Kinge <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abdul Rahman El Kinge </p>
                                  <p class="mb5">Perspective-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000 S1004. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000216.------>
                                <div class="article">
                                  <h4><a href="Clinical-potential-of-DAC-70-sol.php"> Clinical potential of DAC-70 sol </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akio Sugitachi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Naoko Takahashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshimori Takamori </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000216. x------>
                                <!------IMM.1000217.------>
                                <div class="article">
                                  <h4><a href="Characterization-of-osteoarthritis-in-patients-with-diabetes-mellitus-type-2.php"> Characterization of osteoarthritis in patients with diabetes mellitus type 2 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander P Lykov <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena P Trifonova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Olga V Sazonova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena V Zonova </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 14, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000217. x------>
                                <!------IMM.1000218. ------>
                                <div class="article">
                                  <h4><a href="Stretch-induced-functional-disorder-of-axonal-transport-in-the-cultured-rat-cortex-neuron.php"> Stretch-induced functional disorder of axonal transport in the cultured rat cortex neuron </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shigeru Aomura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiromichi Nakadate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuma Kaneko <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akiyoshi Nishimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Remy Willinger </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000218.  x------>
                                <!------IMM.1000219.------>
                                <div class="article">
                                  <h4><a href="Effects-of-soy-peptides-on-IL-1-induced-matrix-degrading-enzymes-in-human-articular-chondrocytes.php"> Effects of soy peptides on IL-1β-induced matrix-degrading enzymes in human articular chondrocytes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mitsumi Arito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Mitsui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Manae S Kurokawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuo Yudoh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshikazu Kamada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisateru Niki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomohiro Kato </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000219. x------>
                                <!------IMM.1000220. ------>
                                <div class="article">
                                  <h4><a href="Cadherin-catenin-signaling-in-developmental-biology-and-pathology.php"> Cadherin/catenin signaling in developmental biology and pathology </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zen Kouchi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 27, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000220.  x------>
                                <!------IMM.1000221. ------>
                                <div class="article">
                                  <h4><a href="The-canonical-Wnt-signal-paradoxically-regulates-osteoarthritis-development-through-the-endochondral-ossification-process.php"> The canonical Wnt signal paradoxically regulates osteoarthritis development through the endochondral ossification process </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Hiroshi Kawaguchi </p>
                                  <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 27, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000221.  x------>
                                <!------IMM.1000222. ------>
                                <div class="article">
                                  <h4><a href="Induction-of-the-expression-of-GABARAPL1-by-hydrogen-peroxide-in-C6-glioma-cells.php"> Induction of the expression of GABARAPL1 by hydrogen peroxide in C6 glioma cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshimitsu Kiriyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kunihiko Kasai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiromi Nochi </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000222.  x------>
                                <!------IMM.1000222. ------>
                                <div class="article">
                                  <h4><a href="Induction-of-the-expression-of-GABARAPL1-by-hydrogen-peroxide-in-C6-glioma-cells.php"> Induction of the expression of GABARAPL1 by hydrogen peroxide in C6 glioma cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshimitsu Kiriyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kunihiko Kasai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiromi Nochi </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000222.  x------>
                                <!------IMM.1000223.------>
                                <div class="article">
                                  <h4><a href="Anti-aging-effects-of-Lactobacilli.php"> Anti-aging effects of Lactobacilli </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hisako Nakagawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tadaaki Miyazaki </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 13, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000223. x------>
                                <!------IMM.1000224. ------>
                                <div class="article">
                                  <h4><a href="Intranasal-insulin-administration-does-not-modulate-incretin-secretion-gastric-emptying-and-appetite-in-healthy-young-adults.php"> Intranasal insulin administration does not modulate incretin secretion, gastric emptying, and appetite in healthy young adults </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshikazu Hirasawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisayo Yokoyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nooshin Naghavi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihiro Yamashina <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryosuke Takeda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akemi Ota <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daiki Imai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomoaki Morioka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masanori Emoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazunobu Okazaki </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000224.  x------>
                                <!------IMM.1000225.------>
                                <div class="article">
                                  <h4><a href="Nrf2-gene-as-a-double-edged-sword-Clinical-relevance-of-its-genetic-polymorphisms.php"> <em>Nrf2 </em>gene as a double-edged sword: Clinical relevance of its genetic polymorphisms </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshihisa Ishikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaharu Shinkai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshi Kaneko </p>
                                  <p class="mb5">Mini Review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000225. x------>
                                <!------IMM.1000226. ------>
                                <div class="article">
                                  <h4><a href="Acute-appendicitis-revisited-computed-tomography-and-ultrasound-scan-are-they-necessary-to-establish-a-diagnosis.php"> Acute appendicitis revisited: computed tomography and ultrasound scan are they necessary to establish a diagnosis? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Osman A Hamour <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eman M.Fallatah <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rawan O. Alshehri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zain A. Alshareef <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Halah F.AL-Enizi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000226.  x------>
                                <!------IMM.1000227.------>
                                <div class="article">
                                  <h4><a href="Primary-cilia-mediated-intercellular-signaling-in-hair-follicles.php"> Primary cilia-mediated intercellular signaling in hair follicles </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuyuki Matsushima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mika Suematsu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chie Mifude <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kuniyoshi Kaseda </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000227. x------>
                                <!------IMM.1000228. ------>
                                <div class="article">
                                  <h4><a href="Trophic-factors-intervention-regenerates-the-nestin-expressing-cell-population-in-a-model-of-perinatal-excitotoxicity-Implications-for-perinatal-brain-injury-and-prematurity.php"> Trophic factors intervention regenerates the nestin-expressing cell population in a model of perinatal excitotoxicity: Implications for perinatal brain injury and prematurity </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>A. Espinosa-Jeffrey <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R. A. Arrazola <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>B. Chu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Taniguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>S. M. Barajas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>P. Bokhoor <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>J. Garcia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Feria-Velasco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>J. de Vellis </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000228.  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000201. ------>
                                <div class="article">
                                  <h4><a href="The-influence-of-a-passion-flower-extract-on-free-testosterone-in-healthy-men-a-two-part-investigation-involving-younger-and-older-men.php"> The influence of a passion flower extract on free testosterone in healthy men: a two part investigation involving younger and older men </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Richard J. Bloomer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John J. MacDonnchadh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Trint A. Gunnels <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John Henry M. Schriefer </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000201.  x------>
                                <!------IMM.1000202.------>
                                <div class="article">
                                  <h4><a href="Different-associations-of-premorbid-intelligence-vs-current-cognition-with-BMI-insulin-and-diabetes-in-the-homebound-elderly.php"> Different associations of premorbid intelligence <em>vs</em>. current cognition with BMI, insulin and diabetes in the homebound elderly </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mkaya Mwamburi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wei Qiao Qiu </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000202. x------>
                                <!------IMM.1000203. ------>
                                <div class="article">
                                  <h4><a href="Comparative-effect-of-ultrasound-therapy-with-conventional-therapy-on-breast-engorgement-in-immediate-post-partum-mothers-A-randomized-controlled-trial.php"> Comparative effect of ultrasound therapy with conventional therapy on breast engorgement in immediate post-partum mothers: A randomized controlled trial </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Powar Priyanka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Basavaraj C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ramannavar A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Geeta Kurhade <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arvind Kurhade <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Angel Justiz-Vaillant <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rajarm Powar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sehlule Vuma </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 02, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000203.  x------>
                                <!------IMM.1000204. ------>
                                <div class="article">
                                  <h4><a href="Surgical-resection-and-long-term-disease-free-survival-in-stage-IIIB-non-small-cell-lung-cancer-after-gefitinib-down-staging-a-case-report.php"> Surgical resection and long-term disease-free survival in stage IIIB non-small cell lung cancer after gefitinib down-staging: a case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Natella Jafarova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gary Dewar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R Petter Tonseth <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David L. Saltman </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 08, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000204.  x------>
                                <!------IMM.1000205. ------>
                                <div class="article">
                                  <h4><a href="Important-high-light-on-diabetes-in-acute-coronary-syndrome-cases-Saudi-Arabia.php"> Important high light on diabetes in acute coronary syndrome cases: Saudi Arabia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khalid A. AlNemer </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000205.  x------>
                                <!------IMM.1000206. ------>
                                <div class="article">
                                  <h4><a href="Adipose-derived-adult-stem-cells-and-new-technique-for-intracavernosal-injection-for-erectile-dysfunction-novel-approach-to-treat-diabetes-mellitus-Erectus-Shot-Technique.php"> Adipose-derived adult stem cells and new technique for intracavernosal injection for erectile dysfunction: novel approach to treat diabetes mellitus. Erectus Shot Technique </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carlos Mercado,MD </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 11, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000206.  x------>
                                <!------IMM.1000207. ------>
                                <div class="article">
                                  <h4><a href="Blockade-of-the-neuropeptide-Y-Y2-receptor-with-the-potent-antagonist-BIIE0246-regulates-gene-expression-levels-in-the-lipid-metabolic-pathways-in-human-hepatoma-cell-line-HepG2.php"> Blockade of the neuropeptide Y Y2 receptor with the potent antagonist BIIE0246 regulates gene expression levels in the lipid metabolic pathways in human hepatoma cell line HepG2 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kaji H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Okada M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hamaue A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mori M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nagai M </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000207.  x------>
                                <!------IMM.1000208.------>
                                <div class="article">
                                  <h4><a href="Gestational-diabetes-in-New-Zealand-ethnic-groups.php"> Gestational-diabetes-in-New-Zealand-ethnic-groups </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ljiljana M Jowitt </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000208. x------>
                                <!------IMM.1000209. ------>
                                <div class="article">
                                  <h4><a href="Hypothalamic-mitochondria-in-energy-homeostasis-and-obesity.php"> Hypothalamic mitochondria in energy homeostasis and obesity </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xin Guo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lei Wu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weiqun Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Denis M Medeiros <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Stephen Clarke <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Edralin Lucas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Brenda Smith <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Winyoo Chowanadisai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Winyoo Chowanadisai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dingbo Lin </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000209.  x------>
                                <!------IMM.1000210. ------>
                                <div class="article">
                                  <h4><a href="Acute-exposure-to-an-electric-field-induces-changes-in-human-plasma-9-HODE-13-HODE-and-immunoreactive-substance-P-levels-Insight-into-the-molecular-mechanisms-of-electric-field-therapy.php"> Acute exposure to an electric field induces changes in human plasma 9-HODE, 13-HODE, and immunoreactive substance P levels: Insight into the molecular mechanisms of electric field therapy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuzo Nakagawa-Yagi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Hara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumiyuki Nakagawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masashi Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akikuni Hara </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000210.  x------>
                                <!------IMM.1000211. ------>
                                <div class="article">
                                  <h4><a href="Incidence-of-gastric-carcinoma-at-King-Faisal-Specialist-Hospital--Jeddah-Saudi-Arabia-a-hospital-based-study.php"> Incidence of gastric carcinoma at King Faisal Specialist Hospital- Jeddah Saudi Arabia: a hospital-based study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Raha Alahmadi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Osman Hamour <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Al-Enizi H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tashkandi A </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 01, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000211.  x------>
                                <!------IMM.1000212. ------>
                                <div class="article">
                                  <h4><a href="Diabetes-and-periodontitis-multidisciplinary-management.php"> Diabetes and periodontitis: multidisciplinary management</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Harinder S. Sandhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jaffer A </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000212.  x------>
                                <!------IMM.1000213. ------>
                                <div class="article">
                                  <h4><a href="Growth-hormone-releasing-peptide-6-GHRP-6-and-other-related-secretagogue-synthetic-peptides-A-mine-of-medical-potentialities-for-unmet-medical-needs.php"> Growth hormone releasing peptide-6 (GHRP-6) and other related secretagogue synthetic peptides: A mine of medical potentialities for unmet medical needs </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jorge Berlanga-Acosta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerardo Guillen Nieto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ernesto Lopez-Mola <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luis Herrera-Martinez </p>
                                  <p class="mb5">Review  Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000213.  x------>
                                <!------IMM.1000214.------>
                                <div class="article">
                                  <h4><a href="Single-subanesthesia-dose-of-ketamine-enhances-GluR1-Ser845-phosphorylation-in-prefrontal-cortex.php"> Single subanesthesia dose of ketamine enhances GluR1 Ser845 phosphorylation in prefrontal cortex </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mingfa Huang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ke Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiang Cai </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000214. x------>
                                <!------IMM.1000215.------>
                                <div class="article">
                                  <h4><a href="Enhancement-of-physical-fitness-by-black-ginger-extract-rich-in-polymethoxyflavones-a-double-blind-randomized-crossover-trial.php"> Enhancement of physical fitness by black ginger extract rich in polymethoxyflavones: a double-blind randomized crossover trial </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuya Toda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marina Kohatsu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shogo Takeda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Norihito Shimizu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Shimoda </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000215. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000185. ------>
                                <div class="article">
                                  <h4><a href="Interferon-response-and-virus-host-interaction-in-aspect-of-micrornas-regulation.php">Interferon response and virus-host interaction in aspect of microRNAs regulation</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Thananya Jinato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kritsada Khongnomnan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emily Johnson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Witthaya Poomipak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Jarika Makkoch<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Sunchai Payungporn</p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000185.  x------>
                                <!------IMM.1000186. ------>
                                <div class="article">
                                  <h4><a href="The-effects-of-herbal-teas-on-drug-permeability.php"> The effects of herbal teas on drug permeability </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akira Nakatsuma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoshi Wada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junji Kamano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshimitsu Kiriyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaki Ninomiya </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 07, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000186.  x------>
                                <!------IMM.1000187.------>
                                <div class="article">
                                  <h4><a href="Pharmacological-regulation-of-bladder-cancer-by-miR-130-family-seed-targeting-LNA.php"> Pharmacological regulation of bladder cancer by miR-130 family seed-targeting LNA </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hiroshi Egawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kentaro Jingushi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takayuki Hirono <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryo Hirose <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshino Nakatsuji <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuko Ueda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kaori Kitae <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazutake Tsujikawa </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000187. x------>
                                <!------IMM.1000188.------>
                                <div class="article">
                                  <h4><a href="For-the-comments-on-DNA-replication,-telomere-function-and-cancer-action.php"> For the comments on DNA replication, telomere function and cancer action </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adnan Y. Rojeab </p>
                                  <p class="mb5">Commentary-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 11, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000188. x------>
                                <!------IMM.1000189. ------>
                                <div class="article">
                                  <h4><a href="Diabetes-drugs-that-protect-pancreatic-cells.php"> Diabetes drugs that protect pancreatic &beta; cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akira Nakatsuma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshimitsu Kiriyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaki Ninomiya </p>
                                  <p class="mb5">Review Article -Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000189.  x------>
                                <!------IMM.1000190------>
                                <div class="article">
                                  <h4><a href="The-effect-of-mild-electrical-stimulation-with-heat-shock-on-diabetic-KKAy-mice.php"> The effect of mild electrical stimulation with heat shock on diabetic KKAy mice </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukari Kai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yu Tsurekawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryosuke Fukuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Omachi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mariam Piruzyan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuka Okamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keishi Motomura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mary Ann Suico <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tsuyoshi Shuto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatsuya Kondo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiichi Araki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hirofumi Kai </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000190 x------>
                                <!------IMM.1000191. ------>
                                <div class="article">
                                  <h4><a href="Effect-of-fasting-and-refeeding-on-the-consequences-of-myocardial-infarction-in-rats.php"> Effect of fasting and refeeding on the consequences of myocardial infarction in rats </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alcione Lescano de Souza Junior <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Christiane Malfitano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Diego Figueroa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leandro Eziquiel de Souza <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eliane Ignotti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Cláudia Irigoyen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rui Curi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000191.  x------>
                                <!------IMM.1000192. ------>
                                <div class="article">
                                  <h4><a href="Pathogenesis-of-idiopathic-nephrotic-syndrome-in-children-molecular-mechanisms-and-therapeutic-implications.php"> Pathogenesis of idiopathic nephrotic syndrome in children: molecular mechanisms and therapeutic implications </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Samuel N Uwaezuoke </p>
                                  <p class="mb5">Mini Review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000192.  x------>
                                <!------IMM.1000193------>
                                <div class="article">
                                  <h4><a href="Cav2-2-mediated-signaling-in-the-neural-circuits-underlying-anxiety.php"> Cav2.2-mediated signaling in the neural circuits underlying anxiety </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000193. x------>
                                <!------IMM.1000194. ------>
                                <div class="article">
                                  <h4><a href="Mechanical-triggering-of-Wnt3a-and-beta-catenin-in-3D-bioreactors-The-role-of-frequency-and-rhythm.php"> Mechanical triggering of Wnt3a and beta-catenin in 3D bioreactors: The role of frequency and rhythm </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Máximo-Alberto Díez-Ulloa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ramiro Couceiro-Otero </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 07, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000194.  x------>
                                <!------IMM.1000195.------>
                                <div class="article">
                                  <h4><a href="A-new-strategy-for-mitigation-of-the-allergenic-activity-of-ovomucoid-in-hen-eggs-and-beta-lactoglobulin-in-cow-milk.php"> A new strategy for mitigation of the allergenic activity of ovomucoid in hen eggs and beta-lactoglobulin in cow milk </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Kido <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Natsuko Nishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sachiko Misumi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomoaki Matsumoto </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000195. x------>
                                <!------IMM.1000196. ------>
                                <div class="article">
                                  <h4><a href="Significance-of-Crosslaps-and-the-tumour-markers-in-diseases-of-the-prostate-gland.php"> Significance of β-Crosslaps and the tumour markers in diseases of the prostate gland </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lukas J. Becker <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerhard M. Oremek </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000196.  x------>
                                <!------IMM.1000197. ------>
                                <div class="article">
                                  <h4><a href="The-protection-effects-of-different-dosages-of-ulinastatin-on-myocardial.php"> The protection effects of different dosages of ulinastatin on myocardial </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yanbo Ling <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jianxin Sun </p>
                                  <p class="mb5">Short Communication -Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 5, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000197.  x------>
                                <!------IMM.1000198. ------>
                                <div class="article">
                                  <h4><a href="Distribution-of-plexin-and-neuropilin-mRNAs-in-the-cranium-and-brain-stem-of-chick-embryos.php"> Distribution of <em>plexin</em> and <em>neuropilin mRNAs </em>in the cranium and brain stem of chick embryos </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ziaul Haque <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qin Pu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruijin Huang </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000198.  x------>
                                <!------IMM.1000199. ------>
                                <div class="article">
                                  <h4><a href="Angiotensin-II-type-I-receptor-blocker-Losartan-inhibits-fibrosis-in-liver-by-suppressing-TGF-beta1-production.php"> Angiotensin II type I receptor blocker, Losartan, inhibits fibrosis in liver by suppressing TGF-beta1 production </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hisanobu Ogata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideko Noguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshio Ohtsubo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiyuan Liao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Kohara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazunari Yamada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mutsunori Murahashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuki Hijikata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Suetsugu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert M Hoffman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kenzaburo Tani </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000199.  x------>
                                <!------IMM.1000200.------>
                                <div class="article">
                                  <h4><a href="A-quantum-theory-of-disease-including-cancer-and-aging.php"> A quantum theory of disease, including cancer and aging </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jerry I. Jacobson </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000200. x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1c" class="accordian-active">Volume 2 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#v2-panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec  2015</span> <span class="mt3 grey-text">Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#v2-panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Oct  2015</span> <span class="mt3 grey-text">Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#v2-paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Aug 2015</span> <span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2015</span> <span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Apr 2015</span> <span class="mt3 grey-text"> Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#v2-panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Feb 2015</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content  " id="v2-panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="Inhibition-of-Cav2-2-mediated-signaling-disrupts-conditioned-fear-extinction.php">Inhibition of Cav2.2-mediated signaling disrupts conditioned fear extinction</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5"> Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 07, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Effect-of-24-25-dihydroxyvitamin-D3-on-localization-of-catalase-in-chick-enterocytes.php"> Effect of 24,25-dihydroxyvitamin D<sub>3</sub> on localization of catalase in chick enterocytes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yang Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ilka Nemere </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 09, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Lack-of-association-between-CGRP-related-gene-polymorphisms-and-medication-overuse-headache-in-migraine-patients.php">Lack of association between CGRP-related gene polymorphisms and medication overuse headache in migraine patients</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masakazu Ishii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hirotaka Katoh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatsuya Kurihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ken-ichi Saguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shunichi Shimizu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuru Kawamura </p>
                                  <p class="mb5"> Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 14, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Effects-of-Cav2-2-inhibitor-on-hippocampal-spatial-short-term-cognition.php">Effects of Cav2.2 inhibitor on hippocampal spatial short-term cognition</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5"> Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 19, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Flavonoid-glucuronides-isolated-from-spinach-inhibit-IgE-mediated-degranulation-in-basophilic-leukemia-RBL-2H3-cells-and-passive-cutaneous-anaphylaxis-reaction-in-mice.php">Flavonoid glucuronides isolated from spinach inhibit IgE-mediated degranulation in basophilic leukemia RBL-2H3 cells and passive cutaneous anaphylaxis reaction in mice</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuta Morishita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emi Saito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eri Takemura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryoma Fujikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryohei Yamamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masanori Kuroyanagi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Osamu Shirota <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Norio Muto </p>
                                  <p class="mb5"> Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="The-nutritional-status-of-women-of-the-coastal-region-of-the-Great-East-Japan-Earthquake-disaster-area-Three-years-after.php">The nutritional status of women of the coastal region of the Great East Japan Earthquake disaster area: Three years after </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuka Kotozaki </p>
                                  <p class="mb5">Short Report-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 02, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Disruption-of-spatial-cognition-by-intra-accumbens-injection-of-Cav2-2-inhibitor.php">Disruption of spatial cognition by intra-accumbens injection of Cav2.2 inhibitor </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 07, 2015 </p>
                                  </em> </div>
                                <div class="article">
                                  <h4><a href="review-of-genomics-of-barrett-s-esophagus-and-esophageal-adenocarcinoma.php">Review of genomics of Barrett’s esophagus and esophageal adenocarcinoma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Khaled Assim Karkout <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amgad El Sherif <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammed Yaman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dongfeng Tan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sadir Al Rawi </p>
                                  <p class="mb5">Mini Review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 18, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="A-fine-structure-study-on-osteogenesis-of-mesenchymal-stem-cells-cultured-with-a-3D-collagen-scaffold.php"> A fine structure study on osteogenesis of mesenchymal stem cells cultured with a 3D collagen scaffold </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Shunji Kumabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michiko Nakatsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rie Iwai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsura Ueda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshifumi Matsuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shosuke Morita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasutomo Iwai </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 05, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Mental-health-as-a-clinical-hallmark-of-cancer.php"> Mental health as a clinical hallmark of cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Manuela Malaguti-Boyle </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 12, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Prognostic-significance-of-insulin-like-growth-factor-II-mRNA-binding-protein-3-in-urological-cancers-a-systemic-review-and-meta-analysis.php"> Prognostic significance of insulin-like growth factor-II mRNA-binding protein 3 in urological cancers: a systemic review and meta-analysis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Da-wei Sun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ying-yi Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yue Qi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yu-guo Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jian Ma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gui-qi Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guo-yue Lv </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 16, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Cav2-2-mediated-NMDA-receptor-signaling-in-short-term-memory.php">Cav2.2-mediated NMDA receptor signaling in short-term memory </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 16, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Marine-algae-Sargassum-horneri-bioactive-factor-suppresses-proliferation-and-stimulates-apoptotic-cell-death-in-human-breast-cancer-MDA-MB-231-cells-in-vitro.php"> Marine algae <em>Sargassum</em> horneri bioactive factor suppresses proliferation and stimulates apoptotic cell death in human breast cancer MDA-MB-231 cells <em>in vitro </em> </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Masayoshi Yamaguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toru Matsumoto
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Synergy-between-depression-and-Alzheimers-disease-A-spectrum-model-of-genomic-vulnerability-with-therapeutic-implications.php"> Synergy between depression and Alzheimer’s disease: A spectrum model of genomic vulnerability with therapeutic implications </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Roberto Rodrigues <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farhan Ahmad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>George Perry <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert Petersen </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 17, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Association-analysis-of-CYP2A6-polymorphism-to-sudden-and-unexpected-death-of-infants.php"> Association analysis of CYP2A6 polymorphism to sudden and unexpected death of infants </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Motoki Osawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihiko Inaoka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masato Nakatome <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keiko Miyashita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eriko Ochiai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yu Kakimoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumiko Satoh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryoji Matoba</p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 23, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Non-invasive-scoring-systems-for-predicting-NASH-in-Japan-evidences-from-Japan-Study-Group-of-NAFLD.php"> Non-invasive scoring systems for predicting NASH in Japan: evidences from Japan Study Group of NAFLD </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Yoshio Sumida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshio Sumida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Nakajima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideyuki Hyogo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saiyu Tanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masafumi Ono <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideki Fujii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuichiro Eguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masato Yoneda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshi Okanoue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshito Itoh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Japan Study Group of NAFLD (JSG-NAFLD)
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 06, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="content " id="v2-panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM-15-132------>
                                <div class="article">
                                  <h4><a href="Evidence-based-medicine-climbing-a-mountain-for-a-better-decision-making.php"> Evidence-based medicine; climbing a mountain for a better decision-making </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mona O. Mohsen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmed M. Malki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hassan Abel Aziz </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-15-132 x------>
                                <!------IMM-15-133------>
                                <div class="article">
                                  <h4><a href="Effects-of-CCCP-on-the-expression-of-GABARAPL2-in-C6-glioma-cells.php"> Effects of CCCP on the expression of GABARAPL2 in C6 glioma cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshimitsu Kiriyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Airi Ozaki, Katsuhito Kino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiromi Nochi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-15-133 x------>
                                <!------IMM-15-134------>
                                <div class="article">
                                  <h4><a href="Blood-inorganic-mercury-is-directly-associated-with-glucose-levels-in-the-human-population-and-may-be-linked-to-processed-food-intake.php"> Blood inorganic mercury is directly associated with glucose levels in the human population and may be linked to processed food intake </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Renee Dufault <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zara Berg <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raquel Crider <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roseanne Schnoll <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Larry Wetsit <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wayne Two Bulls <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Steven G. Gilbert <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>H.M. “Skip” Kingston <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mesay Mulugeta Wolle <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>G.M. Mizanur Rahman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dan R. Laks </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 02, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-15-134 x------>
                                <!------IMM-2-135------>
                                <div class="article">
                                  <h4><a href="Shoulder-dystocia-among-parous-parturients-Birth-weight-differences-of-the-largest-prior-vaginal-delivery-and-index-pregnancy.php"> Shoulder dystocia among parous parturients:  Birth weight differences of the largest prior vaginal delivery and index pregnancy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Suneet P. Chauhan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kenny Choi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Everett F. Magann <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sharon D. Keiser <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jonathan F. Rehberg <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John C. Morrison </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 06, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-135 x------>
                                <!------IMM-2-136------>
                                <div class="article">
                                  <h4><a href="Generation-of-inner-ear-hair-cells-in-vitro-from-mouse-embryonic-stem-cells.php"> Generation of inner ear hair cells <em>in vitro </em>from mouse embryonic stem cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Geping Wu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ling Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guangyin Xu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yan Yu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hongyan Zhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yan Lv </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-136 x------>
                                <!------IMM-2-137------>
                                <div class="article">
                                  <h4><a href="A-critical-evaluation-of-serum-lipase-and-amylase-as-diagnostic-tests-for-acute-pancreatitis.php"> A critical evaluation of serum lipase and amylase as diagnostic tests for acute pancreatitis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sarfaraz Jasdanwala <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark Babyatsky </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-137 x------>
                                <!------IMM-2-138------>
                                <div class="article">
                                  <h4><a href="Age-related-difference-in-nociceptive-behavior-of-Cav2-1-1-mutant-mice-rolling-Nagoya.php"> Age-related difference in nociceptive behavior of Cav2.1&alpha;1 mutant mice, <em>rolling Nagoya</em> </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-138 x------>
                                <!------IMM-2-139------>
                                <div class="article">
                                  <h4><a href="Discovery-of-a-novel-effect-of-electric-field-exposure-on-human-plasma-beta-endorphin-and-interleukin-12-levels-Insight-into-mechanisms-of-pain-alleviation-and-defense-against-infection-by-electric-field-therapy.php"> Discovery of a novel effect of electric field exposure on human plasma <em>beta</em>-endorphin and interleukin-12 levels: Insight into mechanisms of pain alleviation and defense against infection by electric field therapy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuzo Nakagawa-Yagi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Hara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yusuke Yoshida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Midorikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akikuni Hara </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-139 x------>
                                <!------IMM-2-140------>
                                <div class="article">
                                  <h4><a href="Diagnosis-of-amyloid-positive-mild-cognitive-impairment-using-structural-magnetic-resonance-imaging-The-worth-of-multiple-regions-of-interest.php"> Diagnosis of amyloid-positive mild cognitive impairment using structural magnetic resonance imaging: The worth of multiple regions of interest </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Piers Vigers <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akihiko Shiino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ikuo Tooyama </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 26, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-140 x------>
                                <!------IMM-2-141------>
                                <div class="article">
                                  <h4><a href="The-effect-of-an-alkaline-buffered-creatine-Kre-Alkalyn-on-cell-membrane-behavior-protein-synthesis-and-cisplatin-mediated-cellular-toxicity.php"> The effect of an alkaline buffered creatine (Kre-Alkalyn&reg;), on cell membrane behavior, protein synthesis, and cisplatin-mediated cellular toxicity </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jeff Golini </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-141 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000142------>
                                <div class="article">
                                  <h4><a href="Loss-of-FAM20A-protein-induces-Amelogenesis-Imperfecta-in-mice.php"> Loss of FAM20A protein induces <em>Amelogenesis Imperfecta</em> in mice </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Chunying An <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shunji Kumabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michiko Nakatsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsura Ueda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshifumi Matsuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kentaro Ueno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasutomo Iwai </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000142 x------>
                                <!------IMM.1000143 ------>
                                <div class="article">
                                  <h4><a href="Lipoprotein-a-a-principle-culprit-Relevance-to-pathobiology-of-hypertension-ischemic-strokes-and-implications-for-novel-therapeutic-targets-a-new-puzzle-found.php"> Lipoprotein (a), a principle culprit: Relevance to pathobiology of hypertension, ischemic strokes and implications for novel therapeutic targets, a new puzzle found </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Okom Nkili F. C. OFODILE </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000143  x------>
                                <!------IMM.1000144------>
                                <div class="article">
                                  <h4><a href="Progesterone-therapy-attenuates-fetal-brain-cytokine-levels-and-improves-survival-birth-weights-in-infected-rat-pups.php"> Progesterone therapy attenuates fetal brain cytokine levels and improves survival/birth weights in infected rat pups </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Christian M. Briery <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carl H. Rose <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kedra Wallace <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William A. Bennett <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rick W. Martin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John C. Morrison </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 13, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000144 x------>
                                <!------IMM-2-145 ------>
                                <div class="article">
                                  <h4><a href="Color-imaging-histodiagnostic-approach-for-cancer.php"> Color-imaging histodiagnostic approach for cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akio Sugitachi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koki Otsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshimoto Kimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masanori Hakozaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mizunori Yaegashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Megumu Kamishima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Kume <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yukimi Ohmori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoshi Nishizuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akira Sasaki </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 14, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM-2-145 x------>
                                <!------IMM.1000146------>
                                <div class="article">
                                  <h4><a href="Cellular-stress-after-transferring-human-cornea-from-in-vivo-to-in-vitro-milieu-A-metabolomic-approach.php"> Cellular stress after transferring human cornea from <em>in vivo </em>to <em>in vitro </em>milieu: A metabolomic approach </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomasz Kryczka </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000146 x------>
                                <!------IMM.1000147 ------>
                                <div class="article">
                                  <h4><a href="Metabolic-changes-in-donor-corneas-during-cold-storage-challenging-stereotypes.php"> Metabolic changes in donor corneas during cold storage–challenging stereotypes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomasz Kryczka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jacek P. Szaflik <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jerzy Szaflik <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Midelfart </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 15, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000147  x------>
                                <!------IMM.1000148------>
                                <div class="article">
                                  <h4><a href="Characterization-and-analysis-of-claudin-1-expression-in-colorectal-cancer-and-its-metastases-A-pilot-study.php"> Characterization and analysis of claudin-1 expression in colorectal cancer and its metastases: A pilot study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wael Al Kattan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abderrahman Ouban <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rehana Nawab <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Omar Al-Obaid </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000148 x------>
                                <!------IMM.1000149------>
                                <div class="article">
                                  <h4><a href="Clinical-applicability-of-glycated-hemoglobin-in-the-evolution-of-patients-with-hospital-hyperglycemia.php"> Clinical applicability of glycated hemoglobin in the evolution of patients with hospital hyperglycemia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beatriz Dal Santo Francisco Bonamichi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>João Eduardo Nunes Salles <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carolina Ferraz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adriano Namo Cury <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rubens Aldo Sargaço </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 23, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000149 x------>
                                <!------IMM.1000150------>
                                <div class="article">
                                  <h4><a href="Single-nucleotide-polymorphism-and-cell-type-dependent-gene-expression-of-neuropeptide-Y2-receptor.php"> Single-nucleotide-polymorphism-and-cell-type-dependent-gene-expression-of-neuropeptide-Y2-receptor </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mio Okada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masayo Nagai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akiko Hamaue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maiko Mori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hidesuke Kaji </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 26, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000150 x------>
                                <!------IMM.1000151. ------>
                                <div class="article">
                                  <h4><a href="Inhibition-of-Cav2.2-mediated-signaling-induces-sensorimotor-gating-deficits.php"> Inhibition of Cav2.2-mediated signaling induces sensorimotor gating deficits </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 30, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000151.  x------>
                                <!------IMM.1000152------>
                                <div class="article">
                                  <h4><a href="Proteomic-network-analysis-of-human-uterine-smooth-muscle-in-pregnancy-labor-and-preterm-labor.php"> Proteomic network analysis of human uterine smooth muscle in pregnancy, labor, and preterm labor </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Craig Ulrich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David R. Quilici <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Karen A. Schlauch <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Iain L. O. Buxton </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000152 x------>
                                <!------IMM.1000153------>
                                <div class="article">
                                  <h4><a href="Screening-pre-diabetes-and-obese-women-in-an-early-stage-of-renal-dysfucntion-from-serum-25-hydroxy-vitamin-D-and-serum-parathormone-levels-with-age-body-mass-index-and-fast-plasma-glucose.php"> Screening pre-diabetes and obese women in an early stage of renal dysfucntion from  serum 25- hydroxy vitamin D and serum parathormone levels with age, body mass index and fast plasma glucose </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ernest Emilion <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Richard Emilion </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000153 x------>
                                <!------IMM.1000154.------>
                                <div class="article">
                                  <h4><a href="Genome-wide-analysis-of-differentially-expressed-long-noncoding-RNAs-induced-by-low-shear-stress-in-human-umbilical-vein-endothelial-cells.php"> Genome-wide analysis of differentially expressed long noncoding RNAs induced by low shear stress in human umbilical vein endothelial cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shao-Liang Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhi-Mei Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zuo-Ying Hu and Bing Li </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 29, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000154. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------IMM.1000155. ------>
                                <div class="article">
                                  <h4><a href="Human-umbilical-cord-blood-stem-cells-applied-in-therapy-for-ionizing-radiation-injury.php"> Human umbilical cord blood stem cells applied in therapy for ionizing radiation injury </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xinwen Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yujun Xia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chenglong JI </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000155.  x------>
                                <!------IMM.1000156. ------>
                                <div class="article">
                                  <h4><a href="Clinical-pathways-of-metastatic-spinal-cord-compression-Orthopedics-experience-based-on-hospital-admissions.php"> Clinical pathways of metastatic spinal cord compression: Orthopedics experience based on hospital admissions </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sharifa Ezat Wan Puteh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Noraziani Khamis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sabarul Afian Mokhtar </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 05, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000156.  x------>
                                <!------IMM.1000157 ------>
                                <div class="article">
                                  <h4><a href="Antioxidant-antimicrobial-and-antitumor-activity-of-bacteria-of-the-genus-Bifidobacterium-selected-from-the-gastrointestinal-tract-of-human.php"> Antioxidant, antimicrobial and antitumor activity of bacteria of the genus <em>Bifidobacterium</em>, selected from the gastrointestinal tract of human </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Prosekov A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dyshlyuk L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Milentyeva I <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sukhih S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Babich O <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ivanova S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pavskyi V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shishin M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Matskova L </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 06, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000157  x------>
                                <!------IMM.1000158------>
                                <div class="article">
                                  <h4><a href="Blockade-of-recombinant-human-IL-6-by-tocilizumab-suppresses-matrix-metalloproteinase-9-production-in-the-C28-I2-immortalized-human-chondrocyte-cell-line.php"> Blockade of recombinant human IL-6 by tocilizumab suppresses matrix metalloproteinase-9 production in the C28/I2 immortalized human chondrocyte cell line </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Evan C. Meszaros <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wissam Dahoud <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sam Mesiano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Charles J. Malemud </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000158 x------>
                                <!------IMM.1000159.------>
                                <div class="article">
                                  <h4><a href="Aspects-of-interferons-IFNs-interfering-with-HCV-G1-response.php"> Aspects of interferons (IFNs) interfering with HCV-G1 response </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ilham T Qattan </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 11, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000159. x------>
                                <!------IMM.1000160------>
                                <div class="article">
                                  <h4><a href="Purification-of-IFN-secreting-effector-T-lymphocytes-that-induce-apoptosis-in-cancer-cells.php"> Purification of IFNγ-secreting, effector T lymphocytes that induce apoptosis in cancer cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adeeb AlZoubi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rahaf AlZoubi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fawzy AlSheyab <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farah Khalifeh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammed El-Khateeb </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000160 x------>
                                <!------IMM.1000161------>
                                <div class="article">
                                  <h4><a href="Effects-of-a-Cav2-2-inhibitor-on-spatial-and-non-spatial-short-term-memory.php"> Effects of a Cav2.2 inhibitor on spatial and non-spatial short-term memory </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000161 x------>
                                <!------IMM.1000162. ------>
                                <div class="article">
                                  <h4><a href="Relationships-of-clinical-response-to-relevant-molecular-signal-during-Phase-I-testing-of-Aurora-Kinase-A-inhibitor-Retrospective-assessment.php"> Relationships of clinical response to relevant molecular signal during Phase I testing of Aurora Kinase A inhibitor: Retrospective assessment </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>John Nemunaitis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Christopher Blend <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriel Bien-Willner <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Meghan Degele <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alyssa Roth <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Stacey Hayes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leah Plato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew Guo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James Nemunaitis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David B. Jackson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Neil Senzer </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000162.  x------>
                                <!------IMM.1000163.------>
                                <div class="article">
                                  <h4><a href="Impact-of-weight-loss-in-metabolic-profile-with-reeducation-diet-in-obesity-group.php"> Impact of weight loss in metabolic profile with reeducation diet in obesity group </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beatriz Dal Santo Francisco Bonamichi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sheila Patricia Lopes Rocha <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rafael Bonamichi dos Santos <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nanci Merce <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nilza Maria Scalissi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>João Eduardo Nunes Salles </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 26, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000163. x------>
                                <!------IMM.1000164.------>
                                <div class="article">
                                  <h4><a href="Association-between-miR-146a-SNP-rs2910164-and-ischemic-stroke-in-Asian-population-a-meta-analysis.php"> Association between miR-146a SNP rs2910164 and ischemic stroke in Asian population: a meta-analysis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xue-qi YANG <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiao ZHANG </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000164. x------>
                                <!------IMM.1000165------>
                                <div class="article">
                                  <h4><a href="Xenoantibodies-to-porcine-non-galactose-1-3-galactose-antigens-in-non-human-primates-cross-react-and-cause-apoptosis-to-human-endothelial-cells.php"> Xenoantibodies to porcine non-galactose &alpha; 1,3 galactose antigens in non-human primates cross-react and cause apoptosis to human endothelial cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rafael Mañez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Isabel Moscoso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cristina Costa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alberto Centeno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eduardo Lopez-Pelaez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Natalia Suarez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nieves Domenech </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000165 x------>
                                <!------IMM.1000166------>
                                <div class="article">
                                  <h4><a href="Cation-interaction-of-N-N-Dimethyltryptamine-in-hydrochloric-acid-solution-characteristic-to-gastric-acid.php"> Cation-π interaction of N,N-Dimethyltryptamine in hydrochloric acid solution characteristic to gastric acid </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrej Vidak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Iva Movre Šapić <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vladimir Dananić </p>
                                  <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 10, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000166 x------>
                                <!------IMM.1000167.------>
                                <div class="article">
                                  <h4><a href="The-role-of-sulforaphane-on-duchenne-muscular-dystrophy-by-activation-of-Nrf2.php"> The role of sulforaphane on duchenne muscular dystrophy by activation of Nrf2 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cheng-Cao Sun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jing-Yu Pan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shu-Jun Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>De-Jia Li </p>
                                  <p class="mb5">Mini Review-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 14, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000167. x------>
                                <!------IMM.1000168------>
                                <div class="article">
                                  <h4><a href="Hypothesis-on-a-signalling-system-based-on-molecular-vibrations-of-structure-forming-macromolecules-in-cells-and-tissues.php"> Hypothesis on a signalling system based on molecular vibrations of structure forming macromolecules in cells and tissues </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Werner Jaross </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 14, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000168 x------>
                                <!------IMM.1000169------>
                                <div class="article">
                                  <h4><a href="Elevated-anthranilic-acid-plasma-concentrations-in-type-1-but-not-type-2-diabetes-mellitus.php"> Elevated  anthranilic acid plasma concentrations in type 1 but not type 2 diabetes mellitus </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gregory Oxenkrug <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marieke van der Hart <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Summergrad </p>
                                  <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 25, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------IMM.1000169 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content active" id="v2-panelf">
                            <!------IMM.1000170. ------>
                            <div class="article">
                              <h4><a href="Role-of-Cav2-2-mediated-signaling-in-depressive-behaviors.php"> Role of Cav2.2-mediated signaling in depressive behaviors </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kimie Niimi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weidong Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eiki Takahashi </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 05, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000170.  x------>
                            <!------IMM.1000171------>
                            <div class="article">
                              <h4><a href="Establishment-of-2-microglobulin-deficient-human-iPS-cells-using-CRISPR-Cas9-system.php"> Establishment of β-2 microglobulin deficient human iPS cells using CRISPR/Cas9 system </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takehito Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisako Akatsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshiki Yamaguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keiko Miyashita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masafumi Tanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tetsuro Tamaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masato Ohtsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Minoru Kimura </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 12, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000171 x------>
                            <!------IMM.1000172------>
                            <div class="article">
                              <h4><a href="Microarray-analysis-of-long-non-coding-RNA-expression-in-Ankylosing-Spondylitis.php"> Microarray analysis of long non-coding RNA expression in Ankylosing Spondylitis </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Weiguo Sui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huan Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huiyan He <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wen Xue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xin Zhao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yong Dai </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 15, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000172 x------>
                            <!------IMM.1000173------>
                            <div class="article">
                              <h4><a href="Beyond-bone-remodeling-emerging-functions-of-osteoprotegerin-in-host-defense-and-microbial-infection.php"> Beyond bone remodeling–emerging functions of osteoprotegerin in host defense and microbial infection </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michiyo Kobayashi-Sakamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Riyoko Tamai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yusuke Kiyoura </p>
                              <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 16, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000173 x------>
                            <!------IMM.1000174.------>
                            <div class="article">
                              <h4><a href="Clinical-validity-of-our-color-imaging-histo-diagnosis-for-cancer.php"> Clinical validity of our color-imaging histo-diagnosis for cancer </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Akio Sugitachi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshimoto Kimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koki Otsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masanori Hakozaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mizunori Yaegashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Megumu Kamishima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Kume <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoshi Nishizuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Noriyuki Uesugi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tamotsu Sugai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akira Sasaki </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 30, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000174. x------>
                            <!------IMM.1000175.------>
                            <div class="article">
                              <h4><a href="Enhanced-recellularization-of-renal-extracellular-matrix-scaffold-under-negative-pressure.php"> Enhanced recellularization of renal extracellular matrix scaffold under negative pressure </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Satoshi Hachisuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuichi Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miki Yoshiike <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryuto Nakazawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideo Sasaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatsuya Chikaraishi </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000175. x------>
                            <!------IMM.1000176.------>
                            <div class="article">
                              <h4><a href="A-paediatric-patient-with-AML-M1-and-a-t11-19q23;p13-1-rearrangement.php"> A paediatric patient with AML M1 and a t(11;19)(q23;p13.1) rearrangement </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lisa Duffy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rong Gong <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nyree Cole <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Donald R. Love <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alice M. George </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 02, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000176. x------>
                            <!------IMM.1000177------>
                            <div class="article">
                              <h4><a href="Stilbene-derivatives-from-melinjo-extract-have-antioxidant-and-immune-modulatory-effects-in-healthy-individuals.php"> Stilbene derivatives from melinjo extract have antioxidant and immune modulatory effects in healthy individuals </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>J. Luis Espinoza <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dao Thi An1 <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ly Quoc Trung <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kayoko Yamada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shinji Nakao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takami </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 05, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000177 x------>
                            <!------IMM.1000178------>
                            <div class="article">
                              <h4><a href="Combined-therapy-using-fetal-stem-cells-and-a-complex-of-physical-exercises-in-treatment-of-patients-with-amyotrophic-lateral-sclerosis.php"> Combined therapy using fetal stem cells and a complex of physical exercises in treatment of patients with amyotrophic lateral sclerosis </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>A.A. Sinelnyk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>M.O. Klunnyk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>M.P. Demchuk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>N.S. Sych <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>O.V. Ivankova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>I.G. Matiyashchuk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>M.V. Skalozub <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A.V. Novytska <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>K.I. Sorochynska </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 13, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000178 x------>
                            <!------IMM.1000179------>
                            <div class="article">
                              <h4><a href="Integration-of-major-histocompatibility-complex-methylation-and-transcribed-ultra-conserved-regions-analyses-in-uremia.php"> Integration of major histocompatibility complex, methylation, and transcribed ultra-conserved regions analyses in uremia </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jinjun Qiu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huiyan He <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weiguo Sui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dong’e Tang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yong Dai </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 16, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000179 x------>
                            <!------IMM.1000180------>
                            <div class="article">
                              <h4><a href="Prognostication-of-heart-failure-development-and-advance-the-role-of-high-sensitive-ST2.php"> Prognostication of heart failure development and advance: the role of high-sensitive ST2 </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander E. Berezin </p>
                              <p class="mb5">Short Communication-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 16, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000180 x------>
                            <!------IMM.1000181------>
                            <div class="article">
                              <h4><a href="Nasal-hygiene-in-patients-with-end-stage-renal-disease.php"> Nasal hygiene in patients with end-stage renal disease </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zhao Fan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Baiya Li </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 20, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000181 x------>
                            <!------IMM.1000182------>
                            <div class="article">
                              <h4><a href="EOLA1-inhibits-lipopolysaccharide-induced-vascular-cell-adhesion-molecule-1-expression-by-association-with-MT2A-in-endothelial-cells.php"> EOLA1 inhibits lipopolysaccharide-induced vascular cell adhesion molecule-1 expression by association with MT2A in endothelial cells </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Min Luo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weiling Len <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiaotian Lei <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hao Meng <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xinshou Ouyang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ziwen Liang </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000182 x------>
                            <!------IMM.1000183------>
                            <div class="article">
                              <h4><a href="Current-situation-of-newborn-screening-for-congenital-hypothyroidism-in-China.php"> Current situation of newborn screening for congenital hypothyroidism in China </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Juan Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yonglin Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xia Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fuyong Jiao </p>
                              <p class="mb5">Editorial-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000183 x------>
                            <!------IMM.1000184. ------>
                            <div class="article">
                              <h4><a href="The-genetic-variant-of-COX-2-acts-as-a-potential-risk-factor-of-colorectal-cancer-evidence-from-a-meta-analysis-based-on-13461-individuals.php"> The genetic variant of COX-2 acts as a potential risk factor of colorectal cancer: evidence from a meta-analysis based on 13461 individuals </a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yan Pan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Song Gao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qi Qi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luming Liu </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 30, 2015 </p>
                              </em>
                              <hr/>
                            </div>
                            <!------IMM.1000184.  x------>
                            <div class="row pt10">
                              <div class="small-12 columns"> </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>December 2014</span><span class="mt3 grey-text">Issue 3</span></a></li>
                          <li class="tab-title"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>October 2014</span><span class="mt3 grey-text">Issue 2</span></a></li>
                          <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>August 2014</span><span class="mt3 grey-text">Issue 1</span></a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content  " id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <h4><a href="imm-articles.php">Inaugural Editorial for Integrative Molecular Medicine</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr10 small-font text-lite-green"></i>Masayoshi Yamaguchi</p>
                                <p class="mb5">Editorial-Integrative Molecular Medicine (IMM)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr10 fa-calendar lite-text"></i>Jul 17, 2014</p>
                                </em>
                                <hr>
                                <h4><a href="role-of-nutritional-factor-article.php">Role of nutritional factor menaquinone-7 in bone homeostasis and osteoporosis prevention</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr10 small-font text-lite-green"></i>Masayoshi Yamaguchi</p>
                                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr10 fa-calendar lite-text"></i>Jul 23, 2014</p>
                                </em>
                                <hr>
                                <h4><a href="Regucalcin-a-novel-regulatory-protein-implicated-in-obesity-and-diabetes.php">Regucalcin, a novel regulatory protein implicated in obesity and diabetes</a></h4>
                                <p class="pt5 mb10"><i class="fa fa-dot-circle-o pr10 small-font text-lite-green"></i>Masayoshi Yamaguchi</p>
                                <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr10 fa-calendar lite-text"></i>Jul 23, 2014</p>
                                </em> </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <h4><a href="High-waist-circumference-A-potential-risk-factor-for-premature-metabolic-syndrome-in-women-irrespective-of-menopausal-status.php">High waist circumference - A potential risk factor for premature metabolic syndrome in women irrespective of menopausal status</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Namrata Chhabra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kuldip Sodhi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sahiba Kukreja <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sahil Chhabra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sarah Chhabra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Kavish Ramessur </p>
                            <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>September 20, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Detection-of-DNA-methylation-of-gastric-juice-derived-exosomes-in-gastric-cancer.php">Detection of DNA methylation of gastric juice-derived exosomes in gastric cancer</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshida Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yamamoto H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Morita R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>OikawaR <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Matsuo Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Maehata T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Nosho K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Watanabe Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuda H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Itoh F </p>
                            <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>September 29, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Quest-for-the-development-of-tooth-root-periodontal-ligament-complex-by-tissue-engineering.php">Quest for the development of tooth root/periodontal ligament complex by tissue engineering</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Hidefumi Maeda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Akifumi Akamine </p>
                            <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 01, 2014 </p>
                            </em>
                            <hr/>
                            <h4><a href="Serum-concentration-of-bone-morphogenetic-proteins-(BMPs)-is-not-linked-to-serum-anti-mullerian-hormone-(AMH)-level.php">Serum concentration of bone morphogenetic proteins (BMPs) is not linked to serum anti-mullerian hormone (AMH) level</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sayaka Ogura-Nose <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Osamu Yoshino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Ikumi Akiyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasushi Hirota <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tetsuya Hirata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Miyuki Harada M<br>
                              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisahiko Hiroi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>KaoriKoga <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shigeru Saito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yutaka Osuga <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Tomoyuki Fujii </p>
                            <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 03, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Non-targeted-human-plasma-metabolomics-reveals-the-changes-in-oleoylethanolamide-a-lipid-derived-signaling-molecule-by-acute-exposure-of-electric-field.php#IMM_Figures_Data.php">Non-targeted human plasma metabolomics reveals the changes in oleoylethanolamide, a lipid-derived signaling molecule, by acute exposure of electric field</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nakagawa-Yagi Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hara H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Fujimori T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yamaguchi T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Midorikawa A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Hara A </p>
                            <p class="mb5">Research-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 14, 2014</p>
                            </em> </div>
                          <div class="content active" id="panelc">
                            <h4><a href="Disulfide-bridged-proteins-with-potential-for-medical-applications-therapeutic-relevance-sample-preparation-and-structure-function-relationships.php">Disulfide-bridged proteins with potential for medical applications: therapeutic relevance, sample preparation and structure – function relationships</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Michiro Muraki </p>
                            <p class="mb5">Review Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 05, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Suppressive-effects-of-EB-virus-infection-on-HER2-expression-in-gastric-cancer-cells.php">Suppressive effects of EB virus infection on HER2 expression in gastric cancer cells</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Ryo Morita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Yasutaka Sukawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Yamamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihito Yoshida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ritsuko Oikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasumasa Matsuo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tadateru Maehata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Katsuhiko Nosho <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshiyuki Watanabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Yasuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumio Itoh </p>
                            <p class="mb5">Research  Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 05, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Development-of-a-novel-approach-to-enhance-the-solubility-of-ftibamzone-formulation.php">Development of a novel approach to enhance the solubility of ftibamzone formulation</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Ofonime Udofot <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kristen Jaruszewski <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shawn Spencer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Edward Agyare </p>
                            <p class="mb5">Research  Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 07, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="A-peptide-profile-of-amniotic-fluid-in-a-fetal-lamb-model-of-gastroschisis.php">A peptide profile of amniotic fluid in a fetal lamb model of gastroschisis</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Kei Ohyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshiyuki Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kunihide Tanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shutaro Manabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideki Nagae <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsumi Arito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nobuko Iizuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shigeki Kojima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuki Omoteyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuji Seki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Manae S. Kurokawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Naoya Suematsu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuki Okamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kevin C. Pring <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomohiro Kato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroaki Kitagawa </p>
                            <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 17, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Diabetes-mellitus-is-important-as-a-risk-factor-of-atrial-fibrillation.php">Diabetes mellitus is important as a risk factor of atrial fibrillation</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenichi Aizawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Fujii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshinori Seko </p>
                            <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 27, 2014</p>
                            </em>
                            <hr/>
                            <div class="article">
                              <h4><a href="The-effects-of-anesthesia-and-fetal-surgery-on-the-early-ovine-fetus.php">The effects of anesthesia and fetal surgery on the early ovine fetus</a></h4>
                              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shutaro Manabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroaki Kitagawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kei Ooyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuji Seki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hideki Nagae <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masayuki Takagi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junki Koike <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jane Zuccollo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kevin C Pringle </p>
                              <p class="mb5">Research Article-Integrative Molecular Medicine (IMM)</p>
                              <em>
                              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>December 27, 2014</p>
                              </em> </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr/>
              <table cellspacing="0" border="0" cellpadding="0" align="center" style="border:0px;">
                <tr>
                  <td align="center" style="border:0;"><a href="http://www.cas.org" target="_blank"><img src="img/caslogopms.jpg"></a></td>
                  <td align="center" style="border:0;"><a href="https://scholar.google.com/scholar?hl=en&amp;q=2056-6360&amp;btnG=" target="_blank"><img src="img/IMM_Google.JPG"></a></td>
                  <td align="center" style="border:0;"><a href="http://rzblx1.uni-regensburg.de/ezeit/searchres.phtml?bibid=AAAAA&colors=7&lang=en&jq_type1=QS&jq_term1=Integrative+Molecular+Medicine+" target="_blank"><img src="img/IMM_Electronic.JPG"></a></td>
                </tr>
                <tr>
                  <td align="center" style="background-color:#FFF; border:0;"><a href="http://www.citefactor.org/journal/index/12347/integrative-molecular-medicine#.V7SoE_mSziU" target="_blank"><img src="img/IMM_CiteFactor.JPG"></a></td>
                  <td align="center" style="background-color:#FFF; border:0;"><a href="http://road.issn.org/issn/2056-6360-integrative-molecular-medicine-#.V7SoFvmSziU" target="_blank"><img src="img/IMM_ROAD.JPG"></a></td>
                  <td align="center" style="background-color:#FFF; border:0;"><a href="http://scicurve.com/journal/2056-6360" target="_blank"><img src="img/IMM_SCICURVE.JPG"></a></td>
                </tr>
                <tr>
                  <td align="center" style="border:0;"><a href="http://www.worldcat.org/title/integrative-molecular-medicine-imm/oclc/904149363" target="_blank"><img src="img/IMM_WorldCat.JPG"></a></td>
                  <td align="center" style="border:0;"><a href="http://dispatch.opac.d-nb.de/DB=1.1/SET=2/TTL=1/CMD?ACT=SRCHA&IKT=8&SRT=LST_ty&TRM=2056-6360" target="_blank"><img src="img/IMM_ZDB.JPG"></a></td>
                  <td align="center" style="border:0;"><a href="http://www.refseek.com/search?q=Integrative+Molecular+Medicine" target="_blank"><img src="img/IMM_refseek.JPG"></a></td>
                </tr>
                <tr>
                  <td align="center" style="background-color:#FFF; border:0;"><a href="http://as-fhj.c17.es/index.php/opac/action/listFondos/revista[id]/53882/query[anio]//" target="_blank"><img src="img/IMM_AS.JPG"></a></td>
                  <td align="center" style="background-color:#FFF; border:0;"><a href="https://dkfzsearch.kobv.de/simpleSearch.do?plv=2&hitsPerPage=&query=2056-6360&formsearch=%E2%9C%934" target="_blank"><img src="img/IMM_DKFZ.JPG"></a></td>
                  <td align="center" style="background-color:#FFF; border:0;"></td>
                </tr>
              </table>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>IMM welcomes direct submissions of manuscripts from authors. You can submit your manuscript to:<a href="mailto:submissions@oatext.com">submissions@oatext.com</a>; alternatively to:<a href="mailto:yamaguchi@oatext.com">yamaguchi@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
            <div id="Special_Issues" class="content">
              <h2>Special Issues</h2>
              <hr/>
              <h4>Breast Cancer </h4>
              <h4>Editor Affiliation:</h4>
              <p class="mb10">Ricardo H. Alvarez<br>
                Cancer Treatment Centers of America </p>
              <p>Submission date: November 30, 2016<br>
                Publication date: January 31, 2017</p>
              <ul class="hide">
                <li>
                  <div class="">
                    <h3 class="mb10 blue-text" style="top:-5px; position:relative;">Obstacles to the Medical Revolution</h3>
                    <p class="mb10"><span class="f15 blue-text">Editor Affiliation:</span><br>
                      David W. Moskowitz<br>
                      MD FACP, Founder and CEO, GenoMed, Inc. (www.genomed.com) </p>
                    <p class="mb10">Submission date: June 30, 2016 <br>
                      Publication date: August 03, 2016</p>
                    <p>Description of the Special Issue:  It's widely agreed that molecular medicine will revolutionize the practice of medicine. Few people realize that it's long overdue. Linus Pauling and colleagues published "Sickle Cell Anemia, a Molecular Disease," in the November 1949 issue of Science, but the treatment for sickle cell disease remains unsatisfactory over 65 years later.</p>
                    <p>What has been holding up the medical revolution? Papers are solicited on this general topic. They may address any of the following topics, or another that the author feels is equally important:</p>
                    <p>1. Problems solving polygenic diseases<br>
                      2. Is there a better approach to oncology?<br>
                      3. Drug discovery and development in the 21st century<br>
                      4. Shifting the healthcare industry to prevention: economic and legal challenges<br>
                      5. The role of public health authorities<br>
                      6. The role of the media<br>
                      7. The role of investors </p>
                  </div>
                  <hr/>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
