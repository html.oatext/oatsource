<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Nephrology and Renal Diseases (NRD) - OATEXT</title>
<meta name="description" content="Nephrology and Renal Diseases (NRD) is a bimonthly open access, peer-reviewed, online journal that encompasses all basic, clinical, experimental and translational aspects of nephrology, dialysis and transplantation and variety of kidney diseases, including both acute and chronic kidney disorders on oatext." />
<meta name="keywords" content="scientific journals, medical journals, journals, oatext" />
<meta name="robots" content=" ALL, index, follow"/>
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For All" />
<meta name="language" content="English" />
<meta http-equiv="window-target" content="_top"/>
<meta http-equiv="pics-label" content="for all ages"/>
<meta name="rating" content="general"/>
<meta content="All, FOLLOW" name="GOOGLEBOTS"/>
<meta content="All, FOLLOW" name="YAHOOBOTS"/>
<meta content="All, FOLLOW" name="MSNBOTS"/>
<meta content="All, FOLLOW" name="BINGBOTS"/>
<meta content="all" name="Googlebot-Image"/>
<meta content="all" name="Slurp"/>
<meta content="all" name="Scooter"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Nephrology and Renal Diseases (NRD)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Nephrology and Renal Diseases (NRD)</h2>
                <h4>Online ISSN: 2399-908X</h4>
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!--       <span class="black-text"><i class="fa fa-university"></i>        University of Lincoln<br>
<br>
</span>  
 -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
                <p> <a href="img/cover-nrd.jpg" target="_blank"><img src="img/cover-nrd.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Nephrology and Renal Diseases (NRD) is a bimonthly open access, peer-reviewed, online journal that encompasses all basic, clinical, experimental and translational aspects of nephrology, dialysis and transplantation and variety of kidney diseases, including both acute and chronic kidney disorders.</p>
                <p>Journal emphasizes on the pathophysiology of the kidney and vascular supply, epidemiology, screening, diagnosis, treatment and findings relevant to the development of new therapies.</p>
                <p>NRD will feature original empirical research, critical reviews of the literature, brief commentaries, meeting reports, case reports, innovations in clinical practice, letters and drug clinical trial studies.</p>
                <p>NRD welcomes direct submissions from authors: Attach your word file with e- mail and send it to&nbsp;<a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to&nbsp;<a href="mailto:editor.nrd@oatext.com">editor.nrd@oatext.com</a></p>
                <p>Please, follow the&nbsp;<a href="http://www.oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">http://creativecommons.org/licenses/by/4.0/</a>&nbsp;), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by NRD, is properly cited.</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Editor in chief</h2>
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Laith Al-Rabadi</h4>
                  <p>Assistant Professor<br>
                    University of Utah Hospital<br>
                    Salt Lake City</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yohei Miyamoto, Ph.D.</h4>
                  <p>General Manager<br>
                    Pharmaceutical Clinical Research Department  Toray Industries, Inc</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sorin Fedeles PhD, MBA</h4>
                  <p>Associate Research Scientist<br>
                    Yale University School of Medicine<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Konstantinos A. Fourtounas</h4>
                  <p>Consultant Nephrologist  PSKC,
                    King Salman Armed Forces North Western Region
                    Tabuk, KSA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Xian Wu Cheng, MD, PhD, FAHA</h4>
                  <p>Professor and Chairman for Cardiology<br>
                    Yanbian University Hospital<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Anirban Ganguli, MD</h4>
                  <p>Attending Nephrologist<br>
                    Georgetown University<br>
                    Washington Hospital Center<br>
                    Washington</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Borja Quiroga</h4>
                  <p>Nephrology Department<br>
                    Hospital Gregorio Maranon<br>
                    Madrid ,Spain</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Wenzheng Zhang</h4>
                  <p>Department of Regenerative and<br>
                    Cancer Cell Biology<br>
                    Albany Medical College<br>
                    New York</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jian-hua Ma</h4>
                  <p> Department of Endocrinology<br>
                    Nanjing First Hospital<br>
                    Nanjing Medical University<br>
                    China</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Wanqing Chen</h4>
                  <p>National Central Cancer Registry<br>
                    National Cancer Center<br>
                    Beijing<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Rodolfo Rivera</h4>
                  <p>Division of Clinical Nephrology<br>
                    San Gerardo Hospital<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>EIRINI I. GRAPSA</h4>
                  <p>Director of Nephrology Dept<br> 
                    Aretaieion Hospital University of Athens<br>
                    Greece</p>
                </div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 2, Issue 2</h4>
              <hr/>
            <!------NRD.1000118------>
              <div class="article">
                <h4><a href="Prospective-comparative-multicenter-study-of-suctioning-MPCNL-with-the-aid-of-a-patented-system,-traditional-MPCNL-and-ureteroscopy-in-treating-impacted-upper-ureteral-stones.php">Prospective comparative multicenter study of suctioning MPCNL with the aid of a patented system, traditional MPCNL and ureteroscopy in treating impacted upper ureteral stones</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zhongsheng Yang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kok-Hooi Yap <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chunxiang Luo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Donghua Xie <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiaohui Liao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xuanxi Qiu
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiaolin Deng
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lunfeng Zhu
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Difu Fan
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zuofeng Peng
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wen Qin
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qigui Liu
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tiejun Pan
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhiqiang Chen
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhangqun Ye
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leming Song</p>
                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 30, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000118 x------>
            <!------NRD.1000119------>
              <div class="article">
                <h4><a href="Hypertension-and-IgA-nephropathy-Role-of-clinical-and-familial-factors-in-progression-to-renal-failure.php">Hypertension and IgA nephropathy: Role of clinical and familial factors in progression to renal failure</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lucia Del Vecchio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Simona Curioni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paolo Grillo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>PierGiorgio Messa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giorgio Slaviero <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dario Roccatello
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carmelita Marcantoni
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daniele Cusi</p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 03, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000119 x------>
            <!------NRD.1000120------>
              <div class="article">
                <h4><a href="Hematuria-is-a-risk-factor-towards-end-stage-renal-disease-A-propensity-score-analysis.php">Hematuria is a risk factor towards end-stage renal disease - A propensity score analysis</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoko Shima</p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 09, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000120 x------>
            <!------NRD.1000121------>
              <div class="article">
                <h4><a href="A-mini-review-highlights-on-the-application-of-nano-materials-for-Kidney-disease-A-key-development-in-Medicinal-therapy.php">A mini review highlights on the application of nano-materials for Kidney disease: A key development in Medicinal therapy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ateeq Rahman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Daniel Likius <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Veikko Uahengo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Syed Iqbaluddin</p>
                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 23, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000121 x------>
            <!------NRD.1000122------>
              <div class="article">
                <h4><a href="A-validated-age-and-comorbidity-prognostic-index-for-survival-in-dialysis-patients.php">A validated age and comorbidity prognostic index for survival in dialysis patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rodríguez- Mendiola N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fernández Lucas M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Teruel JL <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zamora J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Liaño F</p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 06, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000122 x------>
            <!------NRD.1000123------>
              <div class="article">
                <h4><a href="Effects-of-different-types-of-protein-supplementation-on-serum-albumin-levels-in-hemodialysis-patients.php">Effects of different types of protein supplementation on serum albumin levels in hemodialysis patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Izabela Spereta Moscardini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ana Carolina Finzetto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fabíola Pansani Maniglia</p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 27, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000123 x------>
            <!------NRD.1000124------>
              <div class="article">
                <h4><a href="Therapeutic-apheresis-Technical-modalities-and-therapeutic-applications.php">Therapeutic apheresis: Technical modalities and therapeutic applications</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Grapsa Eirini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Panokostas Dimitrios</p>
                <p class="mb5">Short Communication-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 31, 2017</p>
                </em>
                <hr/>
              </div>
            <!------NRD.1000124 x------>

            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 2, Issue 1</h4>
              <hr/>
               <!------NRD.1000113------>
              <div class="article">
                <h4><a href="Hematocrit-and-mortality-in-hemodialysis-patients-A-registry-analysis-from-the-KiHeart-Cohort.php">Hematocrit and mortality in hemodialysis patients: A registry analysis from the KiHeart Cohort</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jose Jayme G De Lima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luis Henrique W Gowdak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Flavio J de Paula <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Henrique Cotshi S Muela <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elias David-Neto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luiz A Bortolotto</p>
                <p class="mb5">Original Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000113 x------>
              <!------NRD.1000114------>
              <div class="article">
                <h4><a href="Hyperkalemia.php">Hyperkalemia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Al-Rabadi L
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Singhania G
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marji C
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ahmed F
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cho M
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beck L
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hall I</p>

                <p class="mb5">Editorial-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 24, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000114 x------>
              <!------NRD.1000115------>
              <div class="article">
                <h4><a href="ANG-II-ANG-(1-7)-ALDO-and-AVP-biphasic-effects-on-Na+H+-transport-the-role-of-cellular-calcium.php">ANG II, ANG-(1-7), ALDO and AVP biphasic effects on Na+/H+ transport: the role of cellular calcium</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Margarida de Mello-Aires
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deise CA Leite-Dellova
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Regiane C Castelo-Branco
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gerhard Malnic
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria Oliveira-Souza</p>

                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 05, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000115 x------>
              <!------NRD.1000116------>
              <div class="article">
                <h4><a href="Crescentic-IgA-nephropathy-following-bone-fracture.php">Crescentic IgA nephropathy following bone fracture</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ayman Karkar
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Reda Ghacha
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mohammed Abdelrahman</p>

                <p class="mb5">Case Report-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 12, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000116 x------>
              <!------NRD.1000117------>
              <div class="article">
                <h4><a href="Knockdown-of-Glypican-1-and-5-isoforms-causes-proteinuria-and-glomerular-injury-in-zebrafish-Danio-rerio.php">Knockdown of Glypican-1 and 5 isoforms causes proteinuria and glomerular injury in zebrafish (Danio rerio)</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Klaus Stahl
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan Hegermann
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Florence Njau
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Patricia Schroder
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lynne B. Staggs
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nils Hanke
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michaela Beese
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mario Schiffer
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hermann Haller</p>


                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 19, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000117 x------>
              
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 1, Issue 2</h4>
              <hr/>
              <!------NRD.1000107------>
              <div class="article">
                <h4><a href="Perceived-Risk-of-Kidney-Disease-and-its-Risk-Factors-Results-of-A-Community-Health-Survey.php"> Perceived Risk of Kidney Disease and its Risk Factors: Results of A Community Health Survey </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deborah Rosenthal-Asher <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lakia Maxwell <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wendy Dover <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shavaun Sutton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daniel Cukor </p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 15, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000107 x------>
              <!------NRD.1000108------>
              <div class="article">
                <h4><a href="Prediction-of-ESRD-and-long-term-remission-by-proteinuric-markers-in-Primary-Focal-Segmental-Glomerulosclerosis-with-Nephrotic-Syndrome-a-review.php"> Prediction of ESRD and long-term remission by proteinuric markers in Primary Focal Segmental Glomerulosclerosis with Nephrotic Syndrome : a review </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Claudio Bazzi </p>
                <p class="mb5">Review  Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000108 x------>
              <!------NRD.1000109------>
              <div class="article">
                <h4><a href="Blood-pressure-goals-in-CKD-SPRINTing-away-from-JNC-8.php">Blood pressure goals in CKD-SPRINTing away from JNC-8 ?</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Anirban Ganguli </p>
                <p class="mb5">Short Communication-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000109 x------>
              <!------NRD.1000110------>
              <div class="article">
                <h4><a href="A-kidney-tumor-to-know-mucinous-tubular-and-spindle-cell-carcinoma-of-the-kidney-mtscc-k.php"> A kidney tumor to know: mucinous tubular and spindle cell carcinoma of the kidney (mtscc-k) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Y. Dehayni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Y. Elabiad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>B. Balla <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Y. Boukhlifi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>H. Benomar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Ammani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Qarro </p>
                <p class="mb5">Case Report-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000110 x------>
              <!------NRD.1000112------>
              <div class="article">
                <h4><a href="Daytime-Variation-Of-Copeptin-A-Pilot-Study.php">Daytime Variation Of Copeptin: A Pilot Study</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Elke Bruneel </p>
                <p class="mb5">Original Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 30, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000112 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              
              <div class="lee">
            <dl data-accordion="" class="accordion">
              <dd> <a href="#panel1c" class="accordian-active">Volume 2<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          
                          <!--<li class="tab-title"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2016</span><span class="mt3 grey-text">Issue 2</span> </a></li>-->
                          <li class="tab-title active"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>January 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">

                          <!--<div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                BBB
                              </div>
                            </div>
                          </div>-->

                          <div class="content active" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                               <!------NRD.1000113------>
              <div class="article">
                <h4><a href="Hematocrit-and-mortality-in-hemodialysis-patients-A-registry-analysis-from-the-KiHeart-Cohort.php">Hematocrit and mortality in hemodialysis patients: A registry analysis from the KiHeart Cohort</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jose Jayme G De Lima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luis Henrique W Gowdak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Flavio J de Paula <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Henrique Cotshi S Muela <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elias David-Neto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luiz A Bortolotto</p>
                <p class="mb5">Original Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000113 x------>
              <!------NRD.1000114------>
              <div class="article">
                <h4><a href="Hyperkalemia.php">Hyperkalemia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Al-Rabadi L
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Singhania G
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marji C
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ahmed F
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cho M
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beck L
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hall I</p>

                <p class="mb5">Editorial-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 24, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000114 x------>
              <!------NRD.1000115------>
              <div class="article">
                <h4><a href="ANG-II-ANG-(1-7)-ALDO-and-AVP-biphasic-effects-on-Na+H+-transport-the-role-of-cellular-calcium.php">ANG II, ANG-(1-7), ALDO and AVP biphasic effects on Na+/H+ transport: the role of cellular calcium</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Margarida de Mello-Aires
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deise CA Leite-Dellova
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Regiane C Castelo-Branco
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gerhard Malnic
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria Oliveira-Souza</p>

                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 05, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000115 x------>
              <!------NRD.1000116------>
              <div class="article">
                <h4><a href="Crescentic-IgA-nephropathy-following-bone-fracture.php">Crescentic IgA nephropathy following bone fracture</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ayman Karkar
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Reda Ghacha
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mohammed Abdelrahman</p>

                <p class="mb5">Case Report-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 12, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000116 x------>
              <!------NRD.1000117------>
              <div class="article">
                <h4><a href="Knockdown-of-Glypican-1-and-5-isoforms-causes-proteinuria-and-glomerular-injury-in-zebrafish-Danio-rerio.php">Knockdown of Glypican-1 and 5 isoforms causes proteinuria and glomerular injury in zebrafish (Danio rerio)</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Klaus Stahl
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan Hegermann
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Florence Njau
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Patricia Schroder
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lynne B. Staggs
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nils Hanke
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michaela Beese
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mario Schiffer
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hermann Haller</p>


                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 19, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000117 x------>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </dd>





              <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
              <div class="content" id="panel1b">
                  <div class="hr-tabs">
                  <ul class="tabs" data-tab>
                      
                    <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Oct 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                  </ul>
                  <div class="tabs-content">
                    <div class="content active" id="panelb">
                      <div class="row pt10">
                        <div class="small-12 columns">
                           <!------NRD.1000107------>
              <div class="article">
                <h4><a href="Perceived-Risk-of-Kidney-Disease-and-its-Risk-Factors-Results-of-A-Community-Health-Survey.php"> Perceived Risk of Kidney Disease and its Risk Factors: Results of A Community Health Survey </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deborah Rosenthal-Asher <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lakia Maxwell <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wendy Dover <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shavaun Sutton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daniel Cukor </p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 15, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000107 x------>
              <!------NRD.1000108------>
              <div class="article">
                <h4><a href="Prediction-of-ESRD-and-long-term-remission-by-proteinuric-markers-in-Primary-Focal-Segmental-Glomerulosclerosis-with-Nephrotic-Syndrome-a-review.php"> Prediction of ESRD and long-term remission by proteinuric markers in Primary Focal Segmental Glomerulosclerosis with Nephrotic Syndrome : a review </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Claudio Bazzi </p>
                <p class="mb5">Review  Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000108 x------>
              <!------NRD.1000109------>
              <div class="article">
                <h4><a href="Blood-pressure-goals-in-CKD-SPRINTing-away-from-JNC-8.php">Blood pressure goals in CKD-SPRINTing away from JNC-8 ?</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Anirban Ganguli </p>
                <p class="mb5">Short Communication-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000109 x------>
              <!------NRD.1000110------>
              <div class="article">
                <h4><a href="A-kidney-tumor-to-know-mucinous-tubular-and-spindle-cell-carcinoma-of-the-kidney-mtscc-k.php"> A kidney tumor to know: mucinous tubular and spindle cell carcinoma of the kidney (mtscc-k) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Y. Dehayni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Y. Elabiad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>B. Balla <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Y. Boukhlifi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>H. Benomar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Ammani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A. Qarro </p>
                <p class="mb5">Case Report-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000110 x------>
              <!------NRD.1000112------>
              <div class="article">
                <h4><a href="Daytime-Variation-Of-Copeptin-A-Pilot-Study.php">Daytime Variation Of Copeptin: A Pilot Study</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Elke Bruneel </p>
                <p class="mb5">Original Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 30, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000112 x------>
              
                        </div>
                      </div>
                    </div>

                    <div class="content" id="panela">
                      <div class="row pt10">
                        <div class="small-12 columns">
                          <!------NRD.1000101------>
              <div class="article">
                <h4><a href="Anemia-in-Patients-with-Chronic-Kidney-Disease-Current-Screening-and-management-Approaches.php"> Anemia in Patients with Chronic Kidney Disease: Current Screening and management Approaches </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rodolfo Fernando Rivera <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luca Di Lullo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antonio De Pascalis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fulvio Floccari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giancarlo Joli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena Pezzini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena Brioni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Teresa Sciarrone Alibrandi </p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 20, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000101 x------>
              <!------NRD.1000102------>
              <div class="article">
                <h4><a href="Atypical-presentation-of-IgG4-related-autoimmune-pancreatitis-be-awake-to-early-detecting-subsequent-renal-involvement-case-report-and-literature-review.php">Atypical presentation of IgG4-related autoimmune pancreatitis - be awake to early detecting subsequent renal involvement: case report and literature review</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Agnieszka AP <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Isabelle Brochériou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pieter Demetter <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joëlle Nortier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Celso Matos <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Myriam Delhaye</p>
                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 29, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000102 x------>
              <!------NRD.1000103------>
              <div class="article">
                <h4><a href="Prospective-multicenter-study-of-suctioning-MPCNL-with-the-aid-of-a-patented-system-in-treating-renal-staghorn-calculi.php"> Prospective multicenter study of suctioning MPCNL with the aid of a patented system in treating renal staghorn calculi </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xuanxi Qiu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leming Song <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>DonghuaXie <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chuance Du <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiaolin Deng <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Difu Fan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lunfeng Zhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhonsheng Yang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shengfeng Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wen Qin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tairong Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zuofeng Peng <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Min Hu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qigui Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tiejun Pan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhiqiang Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhangqun Ye </p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 03, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000103 x------>
              <!------NRD.1000104------>
              <div class="article">
                <h4><a href="A-rare-manifestation-of-a-common-disease-Very-early-severe-preeclampsia-in-a-low-risk-patient.php">A rare manifestation of a common disease: Very early severe preeclampsia in a low-risk patient</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keren Rotshenker-Olshinka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Samueloff Arnon <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ezra Gabbay <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hen Y. Sela</p>
                <p class="mb5">Review Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 24, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000104 x------>
              <!------NRD.1000105  ------>
              <div class="article">
                <h4><a href="Blood-glycaemic-variations-in-patients-with-type-2-diabetes-mellitus-treated-with-different-premixed-insulin-analogues-therapy.php">Blood glycaemic variations in patients with type 2 diabetes mellitus treated with different premixed insulin analogues therapy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Feng-fei Li, PhD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ting Li, MD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying Zhang, PhD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wen-li Zhang, MD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xiao-fei Su, MD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jin-dan Wu, MD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lei Ye, MD, PhD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jian-hua Ma, MD, PhD</p>
                <p class="mb5">Research Article-Nephrology and Renal Diseases (NRD)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------NRD.1000105 x------>
                        </div>
                      </div>
                    </div>

                  </div>

          </div>
          
        </dd>
      </dl>
    </div> 

            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>NRD welcomes direct submissions from authors: Attach your word file with e- mail and send it to&nbsp;<a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to&nbsp;<a href="mailto:editor.nrd@oatext.com">editor.nrd@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
