<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="OA Text is an independent open-access scientific publisher showcases innovative research and ideas aimed at improving health by linking research and practice to the benefit of society." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT oatext, oat, open access text" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<title>Pulmonary Vein Isolation by the Helical Approach</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
<div class="small-12 columns">
<hr class="mt0 green-hr-line"/>
</div></div>
<!--banner -->
  <div class="row">
    <div class="medium-12 text-center columns">
<div class="inner-banner">
      <h1>Take a look at the Recent articles</h1>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="row">
    <div class="large-8 columns">
    

    
    
      <div class="inner-left">
        <h2 class="w600">Pulmonary Vein Isolation by the Helical Approach</h2>
        
        
<div data-dropdown="a1" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi</p>
</div>
<div id="a1" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Artificial Cardiac Stimulation Division, Department of Medicine, Hospital e Clínica São Gonçalo, São Gonçalo, RJ, Brazil</p>
<p><i class="fa  fa-envelope-o"></i> E-mail : <a href="mailto:bhuvaneswari.bibleraaj@uhsm.nhs.uk">bhuvaneswari.bibleraaj@uhsm.nhs.uk</a></p>
</div>

<div data-dropdown="a2" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Guilherme Miglioli Lobato</p>
</div>
<div id="a2" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Anesthesiology Division, Department of Medicine, Hospital e Clínica São Gonçalo, São Gonçalo, RJ, Brazil</p>
</div>
<div data-dropdown="a3" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Shaojie Chen</p>
</div>
<div id="a3" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Cardiology, Shanghai First People’s Hospital, Shanghai Jiao Tong University School of Medicine, Shanghai, China</p>
</div>




 
      
      <p class="mt10">DOI: 10.15761/JIC.1000217</p>
      <div class="mt25"></div>
      <div class="custom-horizontal">
        <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs">
          <dd class="active"><a  name="AnchorName"  href="#Article"   class="anchor" >Article</a></dd>
          <dd><a  name="AnchorName"  href="#Article_Info"   class="anchor">Article Info</a></dd>
          <dd><a   name="AnchorName" href="#Author_Info"    class="anchor">Author Info</a></dd>
          <dd><a   name="AnchorName" href="#Figures_Data"   class="anchor">Figures & Data</a></dd>
        </dl>
        <div class="tabs-content">
          <div id="Article" class="content active">
          

<div class="text-justify">

<h2 id="jumpmenu1">Case report</h2>
                  <p>The perfect line of attack for the management of AF is rhythm control, but this is sometimes very hard to accomplish [1]. For such actions, complete isolation of all pulmonary veins (PVI) is currently widely accepted as the best endpoint. The recommendations of the European Society of Cardiology (ESC) (Guidelines for the management of atrial fibrillation: the Task Force for the Management of Atrial Fibrillation of the European Society of Cardiology (ESC) [1]. For the individual patient with symptomatic AF, there must be sufficient potential benefit to justify a complex ablation procedure associated with possibly severe complications. Operator experience is an important consideration when considering ablation as a treatment option. The studies cited in support of the recommendations have been almost exclusively performed by highly experienced operators and expert staff working in specialized institutions, but in clinical practice, more junior and less experienced operators may be involved in many institutions. Catheter ablation is usually undertaken in patients with symptomatic paroxysmal AF that is resistant to at least one antiarrhythmic drug. Catheter ablation for paroxysmal AF should be considered in symptomatic patients who have previously failed a trial of antiarrhythmic medication (Class IIa, Level A). Ablation of persistent symptomatic AF that is refractory to antiarrhythmic therapy should be considered a treatment option (Class IIa, Level B).</p>

                  <p>In this case, we presented a case of a male patient, 69 years old, with hypertension, type 2 Diabetes <em>Mellitus</em>, having already been previously stroke, cholecystectomy previously, and treated for prostate cancer, without coronary artery disease, currently presenting the following symptoms: pre-syncope episodes associated with sustained irregular palpitation tachycardia and dyspnea on the usual efforts. The Duplex scan of carotid and vertebral was normal. The transthoracic &nbsp;echocardiogram presented normal systolic function with left ventricular ejection fraction measured by Simpson&rsquo;s method = 80%, and left atrium diameter = 44 mm, with the rest of the measurements normal, as well as, the resting myocardial scintigraphy and with drug use did not demonstrate ischemia and/or fibrosis. Angiography coronary arteries presented normally. The 24-hour ambulatory blood pressure measurements (ABPM) showed: mean ABPM = 136/79 mmHg, wakeful ABPM = 138/90 mmHg, sleep ABPM = 131/78 mmHg and maximum systolic awake ABPM = 194 mmHg. The 24-hour-Holter monitoring demonstrated minimum &ndash; average &ndash; maximum heart rate (HR): 49 &ndash; 69 &ndash; 117 bpm, presenting sinus rhythm alternated with paroxysmal atrial fibrillation (AF), 13,136 atrial ectopic beats, and the major PR interval measuring 280ms. The patient was in use of losartan 100 mg daily, amlodipine 10 mg daily, hydrochlorothiazide 25 mg daily, propafenone hydrochloride 300 mg three times daily and dabigatran 150 mg two times daily, as well as, the other oral hypoglycemic drugs (the ethics committee composed by Paola Baars Gomes Moises, Luis Marcelo Rodrigues Paz, Humberto Cesar Tinoco e Jonny Shogo Takahashi, approved the execution of the case). Informed consent was signed by the patient.</p>

                  <p>The electrophysiological study demonstrated a distal conduction block (HV interval = 96ms), already having dual chamber pacing implantation indicator in a second time. The patient was submitted to general anesthesia by an anesthesiologist, and 2 g of cefazolin was administered intravenously. He presented sinus rhythm before the procedure begins. Under fluoroscopic vision, three right femoral vein punctures were performed, with 1 sheath 6F, one 8F and another 12F. Through the sheath 12F, the Agilis&trade; NxT Steerable Introducer (St. Jude Medical, USA) was inserted, and the sheath 8F was replaced by the long sheath Fast-Cath &trade; Transseptal Guiding Introducers SL-0 (St. Jude Medical, USA). Through the sheath 6F a diagnostic decapolar catheter was inserted into the coronary sinus (CS). We performed two transeptal punctures using a 71 and 98 cm BRK&trade; Transeptal Needles (St. Jude Medical, USA), respectively, and the right femoral artery was punctured, being inserted an 8F sheath to monitor invasive blood pressure. Electro-surgical plate and EnSite&trade; Velocity&trade; Cardiac Mapping System (St. Jude Medical, USA) electrode for electro-anatomical mapping were positioned on the patient. Right heart chambers catheterization were performed, and left chambers catheterization were also performed after double transeptal punctures, with the following catheter positions:</p>

                  <ul>
                    <li>A decapolar dirigible catheter was positioned within the CS;</li>
                    <li>A decapolar dirigible circular catheter, in pulmonary veins and left atrium, alternately;</li>
                    <li>Four EnligHTN&trade; catheters (St. Jude Medical, USA) into the pulmonary veins, alternately (Figure 1 A, B, C, and D);</li>
                    <li>After the withdrawal of the EnligHTN&trade; (St. Jude Medical, USA), we replaced it for the TactiCath&trade; Quartz Contact Force Ablation Catheter (St. Jude Medical, USA), in the left atrium and pulmonary veins, alternately.</li>
                  </ul>

                  <img src="img/JIC-3-217-g001.gif">
                  <p><b>Figure 1. </b> (A) EnligHTN™ catheter open into the LSPV, (B) pulled back for new helical ablation spots, (C) into the LIPV, and (D) into the RSPV. Circular, decapolar dirigible circular catheter; CS, coronary sinus; Decapolar, decapolar dirigible catheter; EnligHTN™ catheter poles demonstrated in blue, green, red and yellow; LA, left atrium; LIPV, left inferior pulmonary vein; LSPV, left superior pulmonary vein; RS, right superior pulmonary vein.</p>
 
                  <p>The long sheaths in the left atrium were maintained continuously irrigated with heparinized saline. Infusion of heparin was performed by an infusion pump, keeping the coagulation activated time &gt; 300 seconds. The esophagus remained in the center retro-atrial midline. The electro-anatomical map reconstruction of the left atrium and pulmonary veins by the EnSite&trade; Velocity&trade; Cardiac Mapping System (St. Jude Medical, USA) was performed, and helical applications were done in system manner &quot;quartet&quot; with the construction of three low-power propellers inside each pulmonary vein by the catheter EnligHTN&trade; being used one unit for each pulmonary vein, according to the size of the vein (total of four pulmonary veins), with a power of 5 W and maximum temperature of 70&deg; C, during 60 seconds, with reduction and modification of the pulmonary vein electrical signals displayed in the decapolar circular catheter. To eliminate the few remaining electrical potentials we used the TactiCath&trade; Quartz Contact Force Ablation Catheter (St. Jude Medical, USA), as shown in Figure. Radiofrequency applications were made in the antrum of the pulmonary veins of the left atrium, using the power of 35 W and maximum temperature of 42&deg; C. Atrial and pulmonary intra-vein stimulation to check pulmonary vein inlet and outlet blockade confirmed the isolation of pulmonary veins (Figure 2). New decrement atrial electrical stimulation at continuous cycles of 350 to 200 ppm did not induce sustained new arrhythmias, maintaining sinus rhythm. The total procedure time was approximately 30 minutes.</p>

                  <img src="img/JIC-3-217-g002.gif">
                  <p><b>Figure 2. </b>Left superior pulmonary vein isolated. The electrophysiological study demonstrated a distal conduction block (HV interval = 96ms).</p>

                  <p>After 48 hours the patient was discharged, using the same medications; the symptoms were no longer present; no AF episodes appeared during this period. Until the present time of follow up (1 month), the patient remained asymptomatic, without AF.</p>



<h2 id="jumpmenu2">References</h2>
                  <ol>
                    <li>1.  Camm AJ, Kirchhof P, Lip GY, Schotten U, Savelieva I, et al. (2010). Guidelines for the management of atrial fibrillation: the Task Force for the Management of Atrial Fibrillation of the European Society of Cardiology (ESC) Eur Heart J 31: 2369-429. </li>
                  </ol>




</div>

         
          </div>
          <div id="Article_Info" class="content">
            <h4 class="mb10"><span class="">Editorial Information</span></h4>
            <h4 class="mb5"><span class="black-text">Editor-in-Chief</span></h4>
   
<p>Massimo Fioranelli<br>
Guglielmo Marconi University</p>

<h4>Article Type</h4>
<p>Short Communication</p>

<h4>Publication history</h4>
<p>Received date: March 26, 2017<br>
Accepted date: April 26, 2017<br>
Published date: April 29, 2017</p>

<h4>Copyright</h4>
<p>&copy; 2017 Márcio Galindo Kiuch. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</p>

<h4>Citation</h4>
<p>Kiuchi MG, Lobatand GM and Chen S (2016) Pulmonary vein isolation by the helical approach. J Integr Cardiol 3: DOI: 10.15761/JIC.1000217</p>

          </div>
          <div id="Author_Info" class="content">
            <h4 class="mb10"><span class="">Corresponding author</span></h4>
            
            
            <h4 class="mb10"><span class="black-text">Márcio Galindo Kiuchi</span></h4>
            <p>Rua Cel. Moreira César, 138 - Centro, São Gonçalo – Rio de Janeiro – Brazil. ZIP-CODE: 24440-400.Tel and Fax: +55 (21) 26047744</p>





          </div>
          <div id="Figures_Data" class="content">
             
             


 <img src="img/JIC-3-217-g001.gif">
                  <p><b>Figure 1. </b> (A) EnligHTN™ catheter open into the LSPV, (B) pulled back for new helical ablation spots, (C) into the LIPV, and (D) into the RSPV. Circular, decapolar dirigible circular catheter; CS, coronary sinus; Decapolar, decapolar dirigible catheter; EnligHTN™ catheter poles demonstrated in blue, green, red and yellow; LA, left atrium; LIPV, left inferior pulmonary vein; LSPV, left superior pulmonary vein; RS, right superior pulmonary vein.</p>
 
 <img src="img/JIC-3-217-g002.gif">
                  <p><b>Figure 2. </b>Left superior pulmonary vein isolated. The electrophysiological study demonstrated a distal conduction block (HV interval = 96ms).</p>







  
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="inner-right">
      <div class="row">
        <div class="small-12 columns">
          <h4 class="left mr20 mt5">Articles</h4>
          <a href="pdf/JIC-3-217.pdf" target="_blank" class="left mr5"><img src="img/icon-pdf.png" alt=""></a><a href="#" class="left mr5"><img src="img/icon-xml.png" alt=""></a></div>
      </div>
      <hr/>
           <h4><a href="Journal-of-Integrative-Cardiology-JIC.php">Journal Home</a></h4>

      <hr/>
      <div id="sticky-anchor"></div>
      <div id="sticky">
        <h2>Jump to</h2>
        <hr/>
        <div class="row collapse">
          <div class="large-6 columns">
            <div class="right-set">
              <ul class="main-bar-list">
                <li><a href="#Article" class="active">Article</a></li>
                <li><a href="#Article_Info" >Article Info</a></li>
                <li><a href="#Author_Info" >Author Info</a></li>
                <li><a href="#Figures_Data" >Figures & Data</a></li>
              </ul>
            </div>
          </div>
          <div class="large-6 columns">
            <ul class="right-bar-list">
        		<li><a href="#jumpmenu1">Case report</a></li>
            <li><a href="#jumpmenu2">References</a></li>
            
            





            
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>

	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script><script>
      $(document).foundation();
    </script>
<!--
<script>
	$('a[href="#"]').click(function(event){

    event.preventDefault();

});
	</script> -->
</body>
</html>
