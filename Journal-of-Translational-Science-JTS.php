<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="google-site-verification" content="GRZVwJKdrXMzh98mqJZ25yBoTeCLw2OA05Pj4hC8o4s" />
<title>Translational Science | Translational Science Journal | Translational Science Group</title>
<meta name="description" content="Translational science is a multidisciplinary form of science that bridges the recalcitrant gaps that sometimes exist between fundamental science and applied science." />
<meta name="keywords" content="translational science, translational science journal, translational science group, translational science jobs, translational science journal, translational science 2015, translational science institute, translational science 2016, translational science definition, translational science awards." />
<meta name="robots" content="index,follow"/>
<link rel="canonical" href="http://www.oatext.com/Journal-of-Translational-Science-JTS.php" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Journal of Translational Science (JTS)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early VIew</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600"> Journal of Translational Science (JTS) </h2>
                <h4>Online ISSN: 2059-268X</h4>
                <h2 class="mb5"><a href="#Editor-in-Chief">Terry Lichtor </a><span class="f14">(Founding Editor in Chief)</span></h2>
                        <span class="black-text"><i class="fa fa-university"></i> Arkansas State University</span> 
                <hr/>
                <div class="text-justify">
                  <p><a href="img/jts.jpg" target="_blank"><img src="img/jts.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Journal of Translational Science (JTS) is an open access journal with comprehensive peer review policy and a rapid publication process.&nbsp; JTS is a novel journal that will focus upon the translation of cellular, molecular, and genetic pathways into clinical strategies for multiple medical disciplines that can impact a broad spectrum of disorders that involve stem cells, degenerative diseases, aging, immune function, tumorigenesis, epigenetics, musculoskeletal function, cognition, behavior, neuronal, cardiac, pulmonary, gastrointestinal, and vascular targets, and metabolic function overseeing all aspects of translational research and medicine.&nbsp; JTS will provide a platform in today&#39;s scientific and medical literature to serve as an international forum for the healthcare and scientific communities worldwide to translate novel &quot;bench to bedside&quot; science into clinical therapies as well as report upon prognostics, novel therapeutic strategies, and biomarker development.</p>
                  <p>JTS will feature original research, review papers, clinical studies, editorials, expert opinion and perspective papers, commentaries, and book reviews.</p>
                  <p>JTS welcomes direct submissions from authors: Attach your word file with e- mail and send it to&nbsp;<a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to <a href="mailto:editor.jts@oatext.com">editor.jts@oatext.com</a></p>
                  <p>Please, follow the&nbsp;<a href="http://www.oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                  <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (</span><a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">http://creativecommons.org/<wbr />licenses/by/4.0/</a> ), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by JTS, is properly cited</p>
                </div>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Terry Lichtor</h2>
              <hr/>
              <p><img src="img/Terry Lichtor.jpg" class="profile-pic" align="right">Terry Lichtor, MD, PhD is a practicing neurosurgeon.&nbsp; He has a number of research interests, and his brain tumor work is largely focused on the development of a DNA vaccine for treatment of primary and metastatic intracerebral tumors.&nbsp; In particular Dr. Lichtor has shown that vaccines prepared by transfer of DNA from the tumor into a highly immunogenic cell line stimulates an antitumor immune response which may be efficacious in the treatment of patients with brain tumors.&nbsp; Other research interests include developing a passive immunotherapeutic strategy for treatment of Alzheimer&rsquo;s Disease.&nbsp; In addition, he has been involved in the development of a non-invasive MRI technique to measure intracranial pressure and brain compliance, and has been exploring the efficacy of this modality in managing a variety of patients with neurologic problems where intracranial pressure is an issue.&nbsp; Dr. Lichtor is a member of the neurosurgery faculty at Rush University Medical Center in Chicago, Illinois.</p>

             
            </div>

            
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Brent T. Harris</h4>
                  <p class="mb5"> Director of Neuropathology<br>
                    Associate Professor of Pathology and Neurology<br>
                    Georgetown University Medical Center </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Craig S. Atwood</h4>
                  <p class="mb5"> Associate Professor of Medicine,<br>
                    University of Wisconsin-Madison School of Medicine and Public Health<br>
                    Director, Laboratory for Endocrinology, Aging and Disease,<br>
                    William S. Middleton Memorial Veterans Hospital </p>
                </div>
           		<div class="medium-4 columns">
                  <h4>C. Cameron Yin</h4>
                  <p class="mb5"> Associate Professor<br>
                    Department of Hematopathololgy<br>
                    UT MD Anderson Cancer Center<br>
                  </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Arnon Blum</h4>
                  <p class="mb5"> Professor of Medicine<br>
                    Director of Medicine Department <br>
                    Bar-Ilan University <br>
                    Visiting professor<br>
                    University of Miami </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Michael Danquah</h4>
                  <p class="mb5"> Assistant Professor, Pharmaceutical Sciences<br>
                    College of Pharmacy<br>
                    Chicago State University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alfio Ferlito</h4>
                  <p class="mb5"> Professor of Otorhinolaryngology-Head and Neck Surgery<br>
                    University of Udine School of Medicine <br>
                  </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Mutlu Özcan</h4>
                  <p> Professor<br>
                    University of Zurich<br>
                    Head of Dental Materials Unit<br>
                    Center for Dental and Oral Medicine<br>
                    Clinic for Fixed and Removable Prosthodontics<br>
                    and Dental Materials Science </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Terry Lichtor</h4>
                  <p>Clinical Professor<br>
                    Department of Surgery<br>
                    New York Institute of Technology<br>
                    Arkansas State University</p>
                </div>
            	<div class="medium-4 columns">
                  <h4>Ken H. Young</h4>
                  <p>Associate Professor<br>
                    Department of Hematopathology<br>
                    The University of Texas<br>
                    MD Anderson Cancer Center </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Huai l. Feng</h4>
                  <p>Clinical Professor<br>
                    Department of Obstetrics and Gynecology<br>
                    The University of Texas<br>
                    Southwestern Medical Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Eric R. Braverman</h4>
                  <p>Assistant Professor of Integrative Medicine<br>
                    Weill Cornell Medical College<br>
                    Assistant Professor<br>
                    Department of Psychiatry<br>
                    University of Florida College of Medicine &<br>
                    McKnight Brain Institute, Courtesy </p>
                </div>
            	<div class="medium-4 columns">
                  <h4>Antonio Cortese</h4>
                  <p>Department of Medicine and Surgery<br>
                    Unit of Maxillofacial Surgery<br>
                    University of Salerno<br>
                    Italy</p>
                </div>
            </div>
            <hr/>
            <div class="row">    
                <div class="medium-4 columns">
                  <h4>Zhiqun Tan</h4>
                  <p>Associate Research Professor<br>
                    Institute for Memory Impairments &<br>
                    Neurological Disorders<br>
                    University of California Irvine </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Russell H. Swerdlow</h4>
                  <p>Gene and Marge Sweeney Professor<br>
                    Professor of Neurology, Molecular & Integrative Physiology,<br>
                    Biochemistry & Molecular Biology<br>
                    Director, University of Kansas Alzheimer's Disease Center<br>
                    Director, KUMC Neurodegenerative Disorders Program<br>
                    University of Kansas School of Medicine </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Corrado Angelini</h4>
                  <p>Director<br>
                    Master Neuromuscular Disorders<br>
                    University of Padova</p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Masafumi Takahashi</h4>
                  <p>Professor<br>
                    Division of Inflammation Research<br>
                    Center for Molecular Medicine<br>
                    Jichi Medical University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Geoffrey Burnstock</h4>
                  <p>President<br>
                    Autonomic Neuroscience Centre<br>
                    Royal Free & University College Medical School<br>
                    Emeritus Professor<br>
                    University College London<br>
                    Emeritus Professor<br>
                    Department of Pharmacology and Therapeutics<br>
                    The University of Melbourne, Australia </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Leonard L. LaPointe</h4>
                  <p>Francis Eppes Distinguished Professor<br>
                    Communication Science and Disorders<br>
                    Program in Neuroscience<br>
                    College of Medicine<br>
                    Florida State University </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Pasquale Calabrese</h4>
                  <p>Department of Psychology<br>
                    and Interdisciplinary Platform<br>
                    Psychiatry and Psychology<br>
                    Division of Molecular and Cognitive Neuroscience<br>
                    University of Basel </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Frank C  Barone</h4>
                  <p>Professor of Neurology and Physiology & Pharmacology<br>
                    Director of Basic Research, Cerebrovascular Division<br>
                    Medical Student Research Adviser<br>
                    SUNY Downstate Medical Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Wilbert S Aronow</h4>
                  <p>Professor of Medicine <br>
                    Divisions of Cardiology, <br>
                    Geriatrics, and Pulmonary/Critical Medicine <br>
                    Westchester Medical Center and<br>
                    New York Medical College </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4> Wutian Wu</h4>
                  <p>Professor<br>
                    Department of Anatomy<br>
                    LKS Faculty of Medicine<br>
                    The University of Hong Kong</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jie Wu</h4>
                  <p>Senior Member & Professor<br>
                    Department of Molecular Oncology, SRB-3<br>
                    Cancer Biology and Evolution Program<br>
                    H. Lee Moffitt Cancer Center and Research Institute</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mirko Diksic</h4>
                  <p>Professor Emeritus,<br>
                    Department of Neurology and Neurosurgery<br>
                    McGill University</p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Lakshmyya Kesavalu</h4>
                  <p>Associate Professor<br>
                    Department of Periodontology and Oral Biology<br>
                    College of Dentistry</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Subhash Banerjee</h4>
                  <p>Chief, Division of Cardiology <br>
                    Co-director Cardiac Catheterization Laboratory<br>
                    VA North Texas Health Care System<br>
                    Associate Professor of Medicine<br>
                    University of Texas Southwestern Medical Center</p>
                </div>
            	<div class="medium-4 columns">
                  <h4>Tugan Tezcaner</h4>
                  <p>Assistant Professor <br>
                    Department of General Surgery<br>
                    Baskent University School of Medicine </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Jan Cerny</h4>
                  <p>Assistant Professor of Medicine<br>
                    Division of Hematology/Oncology<br>
                    Department of Medicine<br>
                    Director, Leukemia Program<br>
                    University of Massachusetts Medical School<br>
                    Associate Director, Cancer Research Office <br>
                    UMass Memorial Cancer Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ben Warner</h4>
                  <p> Clinical Associate Professor<br>
                    Department of General Practice and Dental Public Health<br>
                    The University of Texas</p>
                </div>
  				<div class="medium-4 columns">
                  <h4>Richard Allsopp</h4>
                  <p>Associate Professor<br>
                    Institute for Biogenesis Research<br>
                    Department of Anatomy & Reproductive Biology<br>
                    John A. Burns School of Medicine <br>
                    University of Hawaii </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Peiwen Fei</h4>
                  <p>Associate Professor<br>
                    University of Hawaii Cancer Center<br>
                    University of Hawaii<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vicente Notario</h4>
                  <p>Professor of Radiation Medicine, <br>
                    Biochemistry and Molecular & Cellular Biology<br>
                    Chief, Laboratory of Experimental Carcinogenesis<br>
                    Director, Division of Radiation Research<br>
                    Department of Radiation Medicine<br>
                    Molecular Oncology Program<br>
                    Lombardi Comprehensive Cancer Center<br>
                    Georgetown University Medical Center </p>
                </div>
            	<div class="medium-4 columns">
                  <h4>Xianhua Piao</h4>
                  <p>Assistant Professor<br>
                    Department of Pediatrics<br>
                    Harvard Medical School<br>
                    Assistant Professor, Medicine<br>
                    Boston Children's Hospital </p>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="medium-4 columns">
                  <h4>Robert Striker</h4>
                  <p>Associate Professor <br>
                    Medicine and Medical Microbiology & Immunology<br>
                    University of Wisconsin-Madison </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Olujimi A. Ajijola</h4>
                  <p>Assistant Professor <br>
                    UCLA Cardiac Arrhythmia Center<br>
                    Assistant Director<br>
                    UCLA Specialty Training and <br>
                    Advanced Research (STAR) Program<br>
                    David Geffen School of Medicine at UCLA<br>
                    UCLA Health System</p>
                </div>
            	<div class="medium-4 columns">
                  <h4>Vineet Punia, MD, MS</h4>
                  <p>Chief Epilepsy Fellow, PGY 6<br>
                    Epilepsy Center, Cleveland Clinic,<br>
                    USA</p>
                </div>
            </div>
            <hr/>
            <div class="row">    
                <div class="medium-4 columns">
                  <h4>Dr. William CS CHO</h4>
                  <p>Consultant of Registered Chinese Medicine Practitioner Association<br>
                    Registered Chinese Medicine Practitioner<br>
                    Department of Clinical Oncology, Queen Elizabeth Hospital<br> Hong Kong </p>
                </div>
                
            	<div class="medium-4 columns"></div>
              <div class="medium-4 columns"></div>
            </div>
            
            <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>

              </div>




            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 3, Issue 3</h4>
              <hr/>               
              <!------JTS.1000180------>
              <div class="article">
              <h4><a href="Translational-research-into-vascular-wall-function-regulatory-effects-of-systemic-and-specific-factors.php">Translational research into vascular wall function: regulatory effects of systemic and specific factors</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander V Maksimenko</p>
              <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 23, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000180 x------>
              <!------JTS.1000181------>
              <div class="article">
              <h4><a href="An-updated-review-of-interactions-of-statins-with-antibacterial-and-antifungal-agents.php">An updated review of interactions of statins with antibacterial and antifungal agents</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khalid Eljaaly
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Samah Alshehri</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  March 03, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000181 x------>
              <!------JTS.1000182------>
              <div class="article">
              <h4><a href="Hemostasis-bleeding-and-thrombosis-in-liver-disease.php">Hemostasis, bleeding and thrombosis in liver disease</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Brisas Flores
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hirsh D Trivedi
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Simon C Robson
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alan Bonder</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 04, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000182 x------>
              <!------JTS.1000183------>
              <div class="article">
              <h4><a href="Higher-risk-for-carcinogenesis-for-residents-populating-the-isotope-contaminated-territories-as-assessed-by-NanoString-gene-expression-profiling.php">Higher risk for carcinogenesis for residents populating the isotope-contaminated territories as assessed by NanoString gene expression profiling</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>LS Baleva
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>VS Sukhorukov
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>T Marshall
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>AE Sipyagina
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>H Abe
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>AS Voronkova
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>NM Karakhan
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>P Barach
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>AR Sadykov
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>NI Egorova
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>SV Suchkov</p>

              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000183 x------>
              <!------JTS.1000184------>
              <div class="article">
              <h4><a href="Infection-prediction-for-aneurysmal-subarachnoid-hemorrhage-patients-at-hospital-admission-combined-panel-of-serum-amyloid-A-and-clinical-parameters.php">Infection prediction for aneurysmal subarachnoid hemorrhage patients at hospital admission: combined panel of serum amyloid A and clinical parameters</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Leire Azurmendi
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Asita Sarrafzadeh
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Natalia Tiberti
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Natacha Kapandji
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paola Sanchez-Peña
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vincent Degos
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Louis Puybasset
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sébastien Richard
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Natacha Turck
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jean-Charles Sanchez</p>

              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 17, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000184 x------>
              <!------JTS.1000185------>
              <div class="article">
              <h4><a href="Phytochemical-hematologic-and-anti-tumor-activity-evaluations-of-Carica-papaya-leaf-extract.php">Phytochemical, hematologic and anti-tumor activity evaluations of Carica papaya leaf extract</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hoang Nguyen
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Joseph P Kitzmiller
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pooja Khungar
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Narendra Dabbade
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kieu Tho Nguyen
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elizabeth Lynn Flynn</p>

              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 20, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000185 x------>
         


            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 3, Issue 2</h4>
              <hr/>
                <!------JTS.1000172 ------>
              <div class="article">
              <h4><a href="The-BRCA1-tumor-suppressor-potential-long-range-interactions-of-the-BRCA1-promoter-and-the-risk-of-breast-cancer.php">The BRCA1 tumor suppressor: potential long-range interactions of the BRCA1 promoter and the risk of breast cancer</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beata Anna Bielinska</p>
              <p class="mb5">Review  Article- Journal of Translational Science (JTS) </p>
              <em>
              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  December 12, 2016   </p>
              </em>
              <hr/>
              </div>
              <!------JTS.1000172  x------>
              <!------JTS.1000173------>
              <div class="article">
              <h4><a href="Basic-foundations-for-building-cognitive-skills-and-expertise.php">Basic foundations for building cognitive skills and expertise</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marcel Martin
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Ouellet
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zeesham Aslam</p>
              <p class="mb5">Short Communication-Journal of Translational Science (JTS) </p>
              <em>
              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>December 26, 2016   </p>
              </em>
              <hr/>
              </div>
              <!------JTS.1000173 x------>
              <!------JTS.1000174------>
              <div class="article">
              <h4><a href="Investigation-of-humoral-and-cellular-immunity-hydatid-in-pathology-in-people.php">Investigation of humoral and cellular immunity hydatid in pathology in people</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hasmik Zanginyan
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Laura Hovsepyan
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gayane Ghazaryan </p>
              <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>   January 03, 2017   </p></em>
              <hr/>
              </div>
              <!------JTS.1000174 x------>
              <!------JTS.1000175------>
              <div class="article">
              <h4><a href="Does-electromagnetic-therapy-meet-an-equivalent-counterpart-within-the-organism.php">Does electromagnetic therapy meet an equivalent counterpart within the organism?</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Richard HW Funk</p>
              <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 13, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000175 x------>
              <!------JTS.1000176------>
              <div class="article">
              <h4><a href="Caring-for-the-caregivers-perception-of-pharmacists-as-care-providers.php">Caring for the caregivers: perception of pharmacists as care providers</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Florence Osisanya
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Angela C Riley </p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 06, 2017   </p></em>
              <hr/>
              </div>
              <!------JTS.1000176 x------>
              <!------JTS.1000177------>
              <div class="article">
              <h4><a href="Flow-enhances-the-agonist-induced-responses-of-the-bradykinin-receptor-(B2)-because-of-its-lectinic-nature-Role-of-its-oligosaccharide-environment.php">Flow enhances the agonist-induced responses of the bradykinin receptor (B2) because of its lectinic nature: Role of its oligosaccharide environment</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Azucena E Jiménez-Corona
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ricardo Espinosa-Tanguma
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maureen Knabb
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rafael Rubio</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 10, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000177 x------>
              <!------JTS.1000178------>
              <div class="article">
              <h4><a href="To-study-serum-zinc-levels-in-ischemic-stroke-patients.php">To study serum zinc levels in ischemic stroke patients</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sushree S Rautaray
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Purnima Dey Sarkar</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 13, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000178 x------>
              <!------JTS.1000179------>
              <div class="article">
              <h4><a href="Therapeutic-management-of-an-extensive-pressure-ulcer-affecting-occipital-and-parietal-bone-stem-cell-therapy.php">Therapeutic management of an extensive pressure ulcer affecting occipital and parietal bone-stem cell therapy</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hana Zelenková
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiří Adler
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomáš Sopuch</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 06, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000179 x------>  
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 3, Issue 1</h4>
              <hr/>
              <!------JTS.1000164------>
                  <div class="article">
                  <h4><a href="Exploring-the-global-animal-biodiversity-in-the-search-for-new-drugs-insects.php">Exploring the global animal biodiversity in the search for new drugs - insects</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis RA Mans
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shellice Sairras
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Deeksha Ganga
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joëlle Kartopawiro </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 31, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000164 x------>
                  <!------JTS.1000165------>
                  <div class="article">
                  <h4><a href="Thiamine-and-dystonia.php">Thiamine and dystonia</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Antonio Costantini
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Immacolata Pala
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Erika Trevi
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roberto Fancellu </p>
                  <p class="mb5">Case Series-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 02, 2016</p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000165 x------>
                  <!------JTS.1000166------>
                  <div class="article">
                  <h4><a href="The-modifiers-of-amyotrophic-lateral-sclerosis-survival-and-clinical-trial-design.php">The modifiers of amyotrophic lateral sclerosis survival and clinical trial design</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bin Zhou</p>
                  <p class="mb5">Mini Review-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 07, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000166 x------>
                  <!------JTS.1000167------>
                  <div class="article">
                  <h4><a href="Mitochondrial-aberrations-and-ophthalmic-diseases.php">Mitochondrial aberrations and ophthalmic diseases</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khaled K Abu-Amero
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kakarla V Chalam </p>
                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 18, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000167 x------>
                  <!------JTS.1000168------>
                  <div class="article">
                  <h4><a href="Microscopic-imaging-of-living-cells-automatic-tracking-and-the-description-of-the-kinetics-of-the-cells-for-image-mining.php">Microscopic imaging of living cells, automatic tracking and the description of the kinetics of the cells for image- mining</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Petra Perner</p>
                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 26, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000168 x------>  
                  <!------JTS.1000169------>
                  <div class="article">
                  <h4><a href="Aqueous-humor-microRNA-profiling-in-primary-congenital-glaucoma.php">Aqueous humor microRNA profiling in primary congenital glaucoma</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deepak P Edward
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alka Mahale
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Haiping Hao
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arif Khan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sami Hameed
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ibrahim Al Jadaan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ohoud Owaidhah
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A. Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammed Al Shamrani
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sami Al Shahwan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khaled K Abu-Amero </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 28, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000169 x------>
                  <!------JTS.1000170------>
                  <div class="article">
                  <h4><a href="Two-cases-of-multiple-myeloma-presenting-with-light-chain-tubulopathy.php">Two cases of multiple myeloma, presenting with light-chain tubulopathy</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elena V Zakharova
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alina M Anilina
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena V Zvonova
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatyana R Jylinskaya
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ekaterina S Stolyarevich </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 30, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000170 x------>
                  <!------JTS.1000171------>
                  <div class="article">
                  <h4><a href="SNP-rs2157719-in-the-CDKN2B-AS1-gene-may-influence-cup-to-disc-ratio-in-patients-with-primary-open-angle-glaucoma.php">SNP rs2157719 in the<em> CDKN2B-AS1 </em>gene 
                  gene may influence cup-to-disc ratio in patients with primary open angle glaucoma </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khaled K. Abu-Amero
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A. Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmed Mousa
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Taif A. Azad
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Faisal A. Almobarak
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tahira Sultan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saleh Altuwaijri
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saleh A. Al-Obeidan </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  December 05, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000171 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion" >
                <dd> <a href="#panel1D" class="accordian-active">Volume 3<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1D">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          
                          <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2017</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>January 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">

                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000172 ------>
              <div class="article">
              <h4><a href="The-BRCA1-tumor-suppressor-potential-long-range-interactions-of-the-BRCA1-promoter-and-the-risk-of-breast-cancer.php">The BRCA1 tumor suppressor: potential long-range interactions of the BRCA1 promoter and the risk of breast cancer</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beata Anna Bielinska</p>
              <p class="mb5">Review  Article- Journal of Translational Science (JTS) </p>
              <em>
              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  December 12, 2016   </p>
              </em>
              <hr/>
              </div>
              <!------JTS.1000172  x------>
              <!------JTS.1000173------>
              <div class="article">
              <h4><a href="Basic-foundations-for-building-cognitive-skills-and-expertise.php">Basic foundations for building cognitive skills and expertise</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marcel Martin
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Ouellet
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zeesham Aslam</p>
              <p class="mb5">Short Communication-Journal of Translational Science (JTS) </p>
              <em>
              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>December 26, 2016   </p>
              </em>
              <hr/>
              </div>
              <!------JTS.1000173 x------>
              <!------JTS.1000174------>
              <div class="article">
              <h4><a href="Investigation-of-humoral-and-cellular-immunity-hydatid-in-pathology-in-people.php">Investigation of humoral and cellular immunity hydatid in pathology in people</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hasmik Zanginyan
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Laura Hovsepyan
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gayane Ghazaryan </p>
              <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>   January 03, 2017   </p></em>
              <hr/>
              </div>
              <!------JTS.1000174 x------>
              <!------JTS.1000175------>
              <div class="article">
              <h4><a href="Does-electromagnetic-therapy-meet-an-equivalent-counterpart-within-the-organism.php">Does electromagnetic therapy meet an equivalent counterpart within the organism?</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Richard HW Funk</p>
              <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 13, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000175 x------>
              <!------JTS.1000176------>
              <div class="article">
              <h4><a href="Caring-for-the-caregivers-perception-of-pharmacists-as-care-providers.php">Caring for the caregivers: perception of pharmacists as care providers</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Florence Osisanya
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Angela C Riley </p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 06, 2017   </p></em>
              <hr/>
              </div>
              <!------JTS.1000176 x------>
              <!------JTS.1000177------>
              <div class="article">
              <h4><a href="Flow-enhances-the-agonist-induced-responses-of-the-bradykinin-receptor-(B2)-because-of-its-lectinic-nature-Role-of-its-oligosaccharide-environment.php">Flow enhances the agonist-induced responses of the bradykinin receptor (B2) because of its lectinic nature: Role of its oligosaccharide environment</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Azucena E Jiménez-Corona
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ricardo Espinosa-Tanguma
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maureen Knabb
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rafael Rubio</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 10, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000177 x------>
              <!------JTS.1000178------>
              <div class="article">
              <h4><a href="To-study-serum-zinc-levels-in-ischemic-stroke-patients.php">To study serum zinc levels in ischemic stroke patients</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sushree S Rautaray
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Purnima Dey Sarkar</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 13, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000178 x------>
              <!------JTS.1000179------>
              <div class="article">
              <h4><a href="Therapeutic-management-of-an-extensive-pressure-ulcer-affecting-occipital-and-parietal-bone-stem-cell-therapy.php">Therapeutic management of an extensive pressure ulcer affecting occipital and parietal bone-stem cell therapy</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hana Zelenková
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiří Adler
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomáš Sopuch</p>
              <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
              <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  February 06, 2017</p></em>
              <hr/>
              </div>
              <!------JTS.1000179 x------>
                              </div>
                            </div>
                          </div>

                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                               <!------JTS.1000164------>
                  <div class="article">
                  <h4><a href="Exploring-the-global-animal-biodiversity-in-the-search-for-new-drugs-insects.php">Exploring the global animal biodiversity in the search for new drugs - insects</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis RA Mans
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shellice Sairras
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Deeksha Ganga
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joëlle Kartopawiro </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 31, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000164 x------>
                  <!------JTS.1000165------>
                  <div class="article">
                  <h4><a href="Thiamine-and-dystonia.php">Thiamine and dystonia</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Antonio Costantini
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Immacolata Pala
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Erika Trevi
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roberto Fancellu </p>
                  <p class="mb5">Case Series-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 02, 2016</p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000165 x------>
                  <!------JTS.1000166------>
                  <div class="article">
                  <h4><a href="The-modifiers-of-amyotrophic-lateral-sclerosis-survival-and-clinical-trial-design.php">The modifiers of amyotrophic lateral sclerosis survival and clinical trial design</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bin Zhou</p>
                  <p class="mb5">Mini Review-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 07, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000166 x------>
                  <!------JTS.1000167------>
                  <div class="article">
                  <h4><a href="Mitochondrial-aberrations-and-ophthalmic-diseases.php">Mitochondrial aberrations and ophthalmic diseases</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khaled K Abu-Amero
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kakarla V Chalam </p>
                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 18, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000167 x------>
                  <!------JTS.1000168------>
                  <div class="article">
                  <h4><a href="Microscopic-imaging-of-living-cells-automatic-tracking-and-the-description-of-the-kinetics-of-the-cells-for-image-mining.php">Microscopic imaging of living cells, automatic tracking and the description of the kinetics of the cells for image- mining</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Petra Perner</p>
                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 26, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000168 x------>  
                  <!------JTS.1000169------>
                  <div class="article">
                  <h4><a href="Aqueous-humor-microRNA-profiling-in-primary-congenital-glaucoma.php">Aqueous humor microRNA profiling in primary congenital glaucoma</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deepak P Edward
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alka Mahale
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Haiping Hao
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arif Khan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sami Hameed
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ibrahim Al Jadaan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ohoud Owaidhah
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A. Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammed Al Shamrani
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sami Al Shahwan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khaled K Abu-Amero </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 28, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000169 x------>
                  <!------JTS.1000170------>
                  <div class="article">
                  <h4><a href="Two-cases-of-multiple-myeloma-presenting-with-light-chain-tubulopathy.php">Two cases of multiple myeloma, presenting with light-chain tubulopathy</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elena V Zakharova
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alina M Anilina
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena V Zvonova
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatyana R Jylinskaya
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ekaterina S Stolyarevich </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 30, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000170 x------>
                  <!------JTS.1000171------>
                  <div class="article">
                  <h4><a href="SNP-rs2157719-in-the-CDKN2B-AS1-gene-may-influence-cup-to-disc-ratio-in-patients-with-primary-open-angle-glaucoma.php">SNP rs2157719 in the<em> CDKN2B-AS1 </em>gene 
                  gene may influence cup-to-disc ratio in patients with primary open angle glaucoma </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khaled K. Abu-Amero
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Altaf A. Kondkar
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmed Mousa
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Taif A. Azad
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Faisal A. Almobarak
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tahira Sultan
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saleh Altuwaijri
                  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saleh A. Al-Obeidan </p>
                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  December 05, 2016   </p>
                  </em>
                  <hr/>
                  </div>
                  <!------JTS.1000171 x------>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </dd>


                  <dd> <a href="#panel1c" class="accordian-active">Volume 2 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                        <li class="tab-title active"><a href="#panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Nov 2016</span> <span class="mt3 grey-text"> Issue 6</span> </a></li>
                        <li class="tab-title"><a href="#panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Sep 2016</span> <span class="mt3 grey-text"> Issue 5</span> </a></li>
                        <li class="tab-title"><a href="#paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2016</span> <span class="mt3 grey-text"> Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text"> Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Mar 2016</span> <span class="mt3 grey-text"> Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Jan 2016</span> <span class="mt3 grey-text"> Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000155 ------>
                    <div class="article">
                    <h4><a href="Perilipin-2-A-new-prospective-biomarker-for-the-diagnosis-of-acute-myocardial-ischemia.php">Perilipin-2: A new prospective biomarker for the diagnosis of acute myocardial ischemia</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Stejskal D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karpíšek M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Krejčí G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Václavík J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Švesták M</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 15, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000155  x------>
                    <!------JTS.1000156 ------>
                    <div class="article">
                    <h4><a href="Patients-with-metastatic-hormone-receptor-positive-breast-cancer-express-PIK3CA-oncogene-mutational-heterogeneity-in-circulating-tumor-cells.php">Patients with metastatic hormone receptor-positive breast cancer express PIK3CA oncogene mutational heterogeneity in circulating tumor cells</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bram De Laere <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dieter JE Peeters <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anja Brouwer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Roberto Salgado <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Peter A van Dam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Annemie Rutten <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gert Van den Eynden <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carsten Denkert <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Laure-Anne Teuwen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Peter B Vermeulen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Steven J Van Laere <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Luc Y Dirix</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 15, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000156  x------>
                    <!------JTS.1000157 ------>
                    <div class="article">
                    <h4><a href="Exosomes-Extracellular-communicators-of-health-and-disease.php">Exosomes: Extracellular communicators of health and disease</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cindy E McKinney</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 23, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000157  x------>
                    <!------JTS.1000158 ------>
                    <div class="article">
                    <h4><a href="Disease-onset-and-aging-in-the-world-of-circular-RNAs.php">Disease onset and aging in the world of circular RNAs</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese</p>
                    <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 31, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000158  x------>
                    <!------JTS.1000159 ------>
                    <div class="article">
                    <h4><a href="Molecular-gene-expression-following-blunt-and-rotational-models-of-traumatic-brain-injury-parallel-injuries-associated-with-stroke-and-depression.php">Molecular gene expression following blunt and rotational models of traumatic brain injury parallel injuries associated with stroke and depression</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Véronique Paban <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michael Ogier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Caroline Chambon <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nicolas Fernandez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Johan Davidsson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marten Risling <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Béatrice Alescio-Lautier</p> 
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 11, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000159  x------>
                    <!------JTS.1000160 ------>
                    <div class="article">
                    <h4><a href="Relationship-between-non-alcoholic-fatty-liver-disease-and-periodontal-disease-a-review-and-study-protocol-on-the-effect-of-periodontal-treatment-on-non-alcoholic-fatty-liver-disease.php">Relationship between non-alcoholic fatty liver disease and periodontal disease: a review and study protocol on the effect of periodontal treatment on non-alcoholic fatty liver disease</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Chieko Kudo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takaomi Kessoku <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yohei Kamata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koichi Hidaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takeo Kurihashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoyuki Iwasaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shogo Takashiba <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshiro Kodama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Toshiyuki Tamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Atsushi Nakajima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masato Minabe</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 20, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000160  x------>
                     <!------JTS.1000161 
              <div class="article">
                <h4><a href="Osteodistraction-with-dental-implant-borne-devices-for-bone-regeneration-in-atrophied-premaxilla.php">Osteodistraction with dental implant - borne devices for bone regeneration in atrophied premaxilla</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Prof. Antonio Cortese M.D. D.M.D,</p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 23, 2016 </p>
                </em>
                <hr/>
              </div>
              ---JTS.1000161  x------>
                    <!------JTS.1000162 ------>
                    <div class="article">
                    <h4><a href="Magnetite-nanoparticles-yield-a-significant-bacteriostatic-effect-on-microorganisms-in-relation-to-free-radical-lipid-peroxidation.php">Magnetite nanoparticles yield a significant bacteriostatic effect on microorganisms in relation to free radical lipid peroxidation</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Belousov A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yu Voyda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Belousova E</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 23, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000162  x------>
                    <!------JTS.1000163 ------>
                    <div class="article">
                    <h4><a href="Extracellular-mitochondria-as-mediators-of-dysregulation-and-cell-death-in-intestinal-epithelium.php">Extracellular mitochondria as mediators of dysregulation and cell death in intestinal epithelium</a></h4>
                    <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mandar S Joshi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sheria Wilson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Brandon L Schanbacher <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>John A Bauer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Peter J Giannone</p>
                    <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                    <em>
                    <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 26, 2016 </p>
                    </em>
                    <hr/>
                    </div>
                    <!------JTS.1000163  x------>
                              </div>
                            </div>
                          </div>    



                          <div class="content" id="panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                              <!------JTS.1000147 ------>
              <div class="article">
                <h4><a href="The-essential-role-of-autophagy-with-oncogenes-carcinogenesis-and-immune-function.php">The essential role of autophagy with oncogenes, carcinogenesis, and immune function</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lawrence M Agius</p>
                <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 16, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000147  x------>
              <!------JTS.1000148 ------>
              <div class="article">
                <h4><a href="Use-of-orthodontic-extrusion-as-aid-for-restoring-extensively-destroyed-teeth-a-case-series.php">Use of orthodontic extrusion as aid for restoring extensively destroyed teeth: a case series</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Giuseppe Troiano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bruno Parente <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Luigi Laino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mario Dioguardi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gabriele Cervino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marco Cicciu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Domenico Ciavarella <br><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lorenzo Lo Muzio </p>
                <p class="mb5">Case Series- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 20, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000148  x------>
              <!------JTS.1000149 ------>
              <div class="article">
                <h4><a href="Analysis-of-cancer-specific-isoforms-across-the-cancer-genome-atlas-tumor-types-potential-disease-linkages.php">Analysis of cancer-specific isoforms across the cancer genome atlas tumor types: potential disease linkages</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yan Ji <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yunqin Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ming Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jia Wei </p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000149  x------>
              <!------JTS.1000150 ------>
              <div class="article">
                <h4><a href="Improved-spatial-learning-and-memory-performance-following-Tualang-honey-treatment-during-cerebral-hypoperfusion-induced-neurodegeneration.php">Improved spatial learning and memory performance following Tualang honey treatment during cerebral hypoperfusion-induced neurodegeneration</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anil Kumar Saxena <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hnin P Phyu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Imad M Al-Ani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Oothuman P </p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000150  x------>
              <!------JTS.1000151 ------>
              <div class="article">
                <h4><a href="The-clinical-application-of-medical-science-research-investment-and-duration.php">The clinical application of medical science research: investment and duration</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nao Hanaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shintaro Sengoku <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuichi Imanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Kawakami </p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 30, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000151  x------>
              <!------JTS.1000152 ------>
              <div class="article">
                <h4><a href="Evaluation-of-various-bioreactor-process-systems-for-the-production-of-induced-pluripotent-stem-cells.php">Evaluation of various bioreactor process systems for the production of induced pluripotent stem cells</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Antje Appelt-Menzel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ivo Schwedhelm <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fabian Kühn <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alevtina Cubukova   <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Frank Edenhofer  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Heike Wallesl  <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan Hansmann </p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 04, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000152  x------>
              <!------JTS.1000153 ------>
              <div class="article">
                <h4><a href="Application-of-optogenetics-mediated-motor-cortex-stimulation-in-the-treatment-of-chronic-neuropathic-pain.php">Application of optogenetics-mediated motor cortex stimulation in the treatment of chronic neuropathic pain</a></h4>
                <p class="pt5 mb10"> <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Sufang Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Feng Tao </p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 11, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000153  x------>
              <!------JTS.1000154 ------>
              <div class="article">
                <h4><a href="The-minimotif-synthesis-hypothesis-for-the-origin-of-life.php">The minimotif synthesis hypothesis for the origin of life</a></h4>
                <p class="pt5 mb10"> <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Martin R Schiller</p>
                <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 19, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JTS.1000154  x------></div>
                            </div>
                          </div>    


                            <div class="content" id="paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000140 ------>
                                      <div class="article">
                                        <h4><a href="Intravenous-high-dose-ascorbic-acid-reduces-the-expression-of-inflammatory-markers-in-peripheral-mononuclear-cells-of-subjects-with-metabolic-syndrome.php"> Intravenous high-dose ascorbic acid reduces the expression of inflammatory markers in peripheral mononuclear cells of subjects with metabolic syndrome </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nina Mikirova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruth C Scimeca </p>
                                        <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 30, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                <!------JTS.1000140  x------>
                                <!------JTS.1000141------>
                                      <div class="article">
                                        <h4><a href="Fighting-cancer-with-growing-complexity.php"> Fighting cancer with growing complexity </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Robert Skopec </p>
                                        <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 06, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                <!------JTS.1000141 x------>
                                <!------JTS.1000142------>
                                      <div class="article">
                                        <h4><a href="The-physical-physiological-and-biological-effects-of-qigong-therapy.php"> The physical, physiological, and biological effects of qigong therapy </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>XinQi Dong <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>E-Shien Chang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kevin Chen </p>
                                        <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 16, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                      <!------JTS.1000142 x------>
                                      <!------JTS.1000143------>
                                      <div class="article">
                                        <h4><a href="Glycogen-persistence-in-ultramarathon-athletes-after-six-hours-of-exercise.php"> Glycogen persistence in ultramarathon athletes after six hours of exercise </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tarini VAF <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kelencz CA <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zanuto R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lotufo R </p>
                                        <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 16, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                      <!------JTS.1000143 x------>
                                      <!------JTS.1000144------>
                                      <div class="article">
                                        <h4><a href="Molecular-profiling-and-clinico-pathological-characteristics-of-vulvar-and-vaginal-melanomas.php"> Molecular profiling and clinico-pathological characteristics of vulvar and vaginal melanomas </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Caterina Fumagalli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giulio Tosti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sara Gandini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marco Turina <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena Guerini-Rocco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Barberis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giovanni Mazzarol </p>
                                        <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 23, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                      <!------JTS.1000144 x------>
                                      <!------JTS.1000145------>
                                      <div class="article">
                                        <h4><a href="The-hope-of-personalized-medicine.php"> The hope of personalized medicine </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Peter Kapitein </p>
                                        <p class="mb5">Opinion Article-Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 23, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                      <!------JTS.1000145 x------>
                                      <!------JTS.1000146------>
                                      <div class="article">
                                        <h4><a href="Forkhead-transcription-factors-new-considerations-for-alzheimer-s-disease-and-dementia.php"> Forkhead transcription factors: new considerations for alzheimer’s disease and dementia </a></h4>
                                        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                        <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                        <em>
                                        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 14, 2016 </p>
                                        </em>
                                        <hr/>
                                      </div>
                                      <!------JTS.1000146 x------>
                                    



                                
                              </div>
                            </div>
                          </div>



                          <div class="content" id="panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000132.------>
                                <div class="article">
                                  <h4><a href="Gene-expression-response-to-ascorbic-acid-in-mice-implanted-with-sarcoma-S180-cells.php"> Gene expression response to ascorbic acid in mice implanted with sarcoma S180 cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nina Mikirova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruth C. Scimeca </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000132. x------>
                                <!------JTS.1000133.------>
                                <div class="article">
                                  <h4><a href="A-role-for-the-liver-in-parturition-and-preterm-birth.php"> A role for the liver in parturition and preterm birth </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anthony R Mawson </p>
                                  <p class="mb5">Review Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000133. x------>
                                <!------JTS.1000134.------>
                                <div class="article">
                                  <h4><a href="Vitamin-D-and-environmental-factors-in-multiple-sclerosis.php"> Vitamin D and environmental factors in multiple sclerosis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Georgi S Slavov <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anastasiya G Trenova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mariya G Manova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ivanka I Kostadinova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tonka V Vasileva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zahari I Zahariev </p>
                                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000134. x------>
                                <!------JTS.1000135.------>
                                <div class="article">
                                  <h4><a href="Clinical-benefits-for-the-monitoring-and-modulating-of-subconjunctival-tissue-following-glaucoma-filtration-surgery.php"> Clinical benefits for the monitoring and modulating of subconjunctival tissue following glaucoma filtration surgery </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dao-Yi Yu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William Morgan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Er-Ning Su <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Stephen Cringle </p>
                                  <p class="mb5">Review  Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000135. x------>
                                <!------JTS.1000136.------>
                                <div class="article">
                                  <h4><a href="Exploring-the-global-animal-biodiversity-in-the-search-for-new-drugs-marine-invertebrates.php"> Exploring the global animal biodiversity in the search for new drugs -marine invertebrates </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis R.A. Mans </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000136. x------>
                                <!------JTS.1000138.------>
                                <div class="article">
                                  <h4><a href="The-bright-side-of-reactive-oxygen-species-lifespan-extension-without-cellular-demise.php"> The bright side of reactive oxygen species: lifespan extension without cellular demise </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000138. x------>
                                <!------JTS.1000139------>
                                <div class="article">
                                  <h4><a href="High-human-cytomegalovirus-pp65-level-in-cardiac-allografts-precedes-acute-rejection-episodes.php"> High human cytomegalovirus pp65 level in cardiac allografts precedes acute rejection episodes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mensur Dzabic <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Afsar Rahbar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Monika Grudzinska <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Piotr Religa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cecilia Söderberg-Nauclér </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000139 x------>
                                <!------JTS.1000140 ------>
                                <div class="article">
                                  <h4><a href="Intravenous-high-dose-ascorbic-acid-reduces-the-expression-of-inflammatory-markers-in-peripheral-mononuclear-cells-of-subjects-with-metabolic-syndrome.php"> Intravenous high-dose ascorbic acid reduces the expression of inflammatory markers in peripheral mononuclear cells of subjects with metabolic syndrome </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nina Mikirova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruth C Scimeca </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000140  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000124------>
                                <div class="article">
                                  <h4><a href="Endothelial-Progenitor-Cells-EPCs-clinical-implications.php"> Endothelial Progenitor Cells (EPCs) – clinical implications </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Arnon Blum <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nava Blum </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000124 x------>
                                <!------JTS.1000125------>
                                <div class="article">
                                  <h4><a href="Neurological-disorders-genetic-correlations-and-the-role-of-exome-sequencing.php"> Neurological disorders, genetic correlations, and the role of exome sequencing </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tony L Brown <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Theresa M Meloche </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000125 x------>
                                <!------JTS.1000126------>
                                <div class="article">
                                  <h4><a href="Chest-compression-during-sustained-inflation-hypoxic-and-oxidative-stress.php"> Chest compression during sustained inflation: hypoxic and oxidative stress </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shaun Cowan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Georg M Schmölzer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tze-Fun Lee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Po-Yin Cheung </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000126 x------>
                                <!------JTS.1000127 ------>
                                <div class="article">
                                  <h4><a href="Catalogingantineoplastic-agents-according-to-their-effectiveness-against-platinum-resistant-and-platinum-sensitive-ovarian-carcinoma-cell-lines.php"> Cataloging antineoplastic agents according to their effectiveness against platinum-resistant and platinum-sensitive ovarian carcinoma cell lines </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kimiko Ishiguro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yong-Lian Zhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Z. Ping Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Philip GPenketh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Krishnamurthy Shyam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rui Zhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raymond P. Baumann <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alan CSartorelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thomas J Rutherford <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena S Ratner </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000127  x------>
                                <!------JTS.1000128------>
                                <div class="article">
                                  <h4><a href="Periosteum-derived-micro-grafts-for-tissue-regeneration-of-human-maxillary-bone.php"> Periosteum-derived micro-grafts for tissue regeneration of human maxillary bone </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Riccardo d’Aquino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Letizia Trovato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antonio Graziano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriele Ceccarelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriella Cusella de Angelis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Angelo Marangini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alessandro Nisio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Galli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Pasi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marco Finotti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saturnino M Lupi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Silvana Rizzo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruggero Rodriguez Y Baena </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 07, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000128 x------>
                                <!------JTS.1000129------>
                                <div class="article">
                                  <h4><a href="Association-of-Vitamin-D-deficiency-cellular-hypoxia-and-caspase-3-with-renal-disease-in-pediatric-diabetic-nephropathy.php"> Association of Vitamin D deficiency, cellular hypoxia, and caspase-3 with renal disease in pediatric diabetic nephropathy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vitaliy Maidannyk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ievgeniia Burlaka </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000129 x------>
                                <!------JTS.1000130 ------>
                                <div class="article">
                                  <h4><a href="Comparisons-between-myeloperoxidase-lactoferrin-calprotectin-and-lipocalin-2-as-fecal-biomarkers-of-intestinal-inflammation-in-malnourished-children.php"> Comparisons between myeloperoxidase, lactoferrin, calprotectin and lipocalin-2, as fecal biomarkers of intestinal inflammation in malnourished children </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mara de Moura Gondim Prata <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>HavtA <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bolick DT <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pinkerton, R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lima AAM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guerrant RL </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000130  x------>
                                <!------JTS.1000131------>
                                <div class="article">
                                  <h4><a href="Charting-a-course-for-erythropoietin-in-traumatic-brain-injury.php"> Charting a course for erythropoietin in traumatic brain injury </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Review  Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000131 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000116------>
                                <div class="article">
                                  <h4><a href="Low-dose-tenecteplase-restores-vessel-patency-in-a-clinically-relevant-porcine-animal-model.php"> Low dose tenecteplase restores vessel patency in a clinically relevant porcine animal model </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tseliou E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ntalianis A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Diakos N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pozios I <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Repasos V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katsaros F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koudoumas D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dimopoulos S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tsagalou E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tsamatsoulis M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kontopidi V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Margari Z <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Terrovitis J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nanas JN </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January, 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000116 x------>
                                <!------JTS.1000117------>
                                <div class="article">
                                  <h4><a href="Making-of-a-highly-useful-multipurpose-vaccine.php"> Making of a highly useful multipurpose vaccine </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>GP Talwar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Priyanka Singh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nishu Atrey <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jagdish C Gupta </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000117 x------>
                                <!------JTS.1000118 ------>
                                <div class="article">
                                  <h4><a href="Serum-cystatin-C-is-a-useful-biomarker-but-not-superior-to-serum-creatinine-assessment-for-the-diagnosis-of-acute-kidney-injury-in-septic-children-a-prospective-cohort-study.php"> Serum cystatin C is a useful biomarker but not superior to serum creatinine assessment for the diagnosis of acute kidney injury in septic children: a prospective cohort study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Osama Y Safdar </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 20, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000118  x------>
                                <!------JTS.1000119 ------>
                                <div class="article">
                                  <h4><a href="Comparative-study-of-intravenous-and-topical-administration-of-mesenchymal-stem-cells-in-experimental-colitis.php"> Comparative study of intravenous and topical administration of mesenchymal stem cells in experimental colitis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Didia Bismara Cury <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rogério Antonio de Oliveira <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Aparecida Dalboni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luciana Aparecida Reis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Clara Vesolato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Edson Pereira <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nestor Schor </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000119  x------>
                                <!------JTS.1000120------>
                                <div class="article">
                                  <h4><a href="Picking-a-bone-with-WISP1-CCN4-new-strategies-against-degenerative-joint-disease.php"> Picking a bone with WISP1 (CCN4): new strategies against degenerative joint disease </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000120 x------>
                                <!------JTS.1000121------>
                                <div class="article">
                                  <h4><a href="Determination-of-early-tumoricidal-drug-induced-cardiotoxicity-with-biological-markers.php"> Determination of early tumoricidal drug-induced cardiotoxicity with biological markers </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander Berezin </p>
                                  <p class="mb5">Mini Review-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000121 x------>
                                <!------JTS.1000122 ------>
                                <div class="article">
                                  <h4><a href="Pancreatic-acinar-cell-carcinoma-presenting-as-a-third-metachronous-primary-tumor-with-unconventional-features.php"> Pancreatic acinar cell carcinoma presenting as a third metachronous primary tumor with unconventional features </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jennifer Ford <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ghassan Tranesh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Mazzara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammed Barawi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jimmy Haouilou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Richard Berri </p>
                                  <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000122  x------>
                                <!------JTS.1000123------>
                                <div class="article">
                                  <h4><a href="Glutamate-transporters-the-regulatory-proteins-for-excitatory-excitotoxic-glutamate-in-brain.php"> Glutamate transporters: the regulatory proteins for excitatory/excitotoxic glutamate in brain </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Josep J. Centelles </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000123 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>November 2015</span> <span class="mt3 grey-text"> Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>September 2015</span> <span class="mt3 grey-text"> Issue 2</span> </a></li>
                          <li class="tab-title "><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2015</span> <span class="mt3 grey-text"> Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelc">
                            <div class="row">
                              <div class="small-12 columns">
                                <!------JTS.1000111------>
                                <div class="article">
                                  <h4><a href="Long-term-follow-up-of-patients-with-neurocysticercosis-and-the-development-of-seazures.php"> Long-term follow-up of patients with neurocysticercosis and the development of seazures </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fandiño-Franky Jaime <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guerra-Olivares Randy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Piña-Cabrales Sandra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fandiño-MerzJavier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mayor Liliana <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tellez Carlos Alfonso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Merlano-Zabaleta Alfonso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vitola Bacter <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>García CE <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rodríguez Y Tomás </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 30, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000111 x------>
                                <!------JTS.1000112------>
                                <div class="article">
                                  <h4><a href="MicroRNAs-and-SIRT1-A-Strategy-for-Stem-Cell-Renewal-and-Clinical-Development.php"> MicroRNAs and SIRT1: A Strategy for Stem Cell Renewal and Clinical Development? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Perspective-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 08, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000112 x------>
                                <!------JTS.1000113------>
                                <div class="article">
                                  <h4><a href="Ectopic-manifestation-of-a-cholesterol-granuloma-in-the-nasopalatine-duct.php"> Ectopic manifestation of a cholesterol granuloma in the nasopalatine duct </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Giuseppe Troiano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Placido Carlo Guidone <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lelio Adelchi Fabrocini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Francesca Sanguedolce <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mario Dioguardi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giovanni Giannatempo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lorenzo Lo Muzio </p>
                                  <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 10, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000113 x------>
                                <!------JTS.1000114------>
                                <div class="article">
                                  <h4><a href="Convergence-a-transformative-approach-to-advanced-research-at-the-intersection-of-life-physical-sciences-and-engineering-and-enhanced-university-industry-partnerships.php"> Convergence: a transformative approach to advanced research at the intersection of life, physical sciences and engineering and enhanced university-industry partnerships </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Amanda Arnold <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Melvin Greer </p>
                                  <p class="mb5">Opinion-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 19, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000114 x------>
                                <!------JTS.1000115------>
                                <div class="article">
                                  <h4><a href="Real-time-ultrasound-assessment-of-sternal-stability-in-adhesive-enhanced-sternal-closure.php"> Real-time ultrasound assessment of sternal stability in adhesive-enhanced sternal closure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Doa El-Ansary <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sulakshana Balachandran <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alistair Royse <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kathryn King-Shier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adam Bryant <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Linda Denehy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Fedak </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000115 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JTS.1000105 ------>
                                <!------JSIN.1000101------>
                                <div class="article">
                                  <h4><a href="Inaugural-Message-from-the-Founding-Editor-in-Chief.php"> Inaugural Message from the Founding Editor-in-Chief </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Editorial-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JSIN.1000101 x------>
                                <!------JTS.1000102------>
                                <div class="article">
                                  <h4><a href="Neuronal-Activity-Mitogens-and-mTOR-Overcoming-the-Hurdles-for-the-Treatment-of-Glioblastoma-Multiforme.php"> Neuronal Activity, Mitogens, and mTOR:  Overcoming the Hurdles for the Treatment of Glioblastoma Multiforme </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenneth Maiese </p>
                                  <p class="mb5">Perspective- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 01, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000102 x------>
                                <!------JTS.1000103------>
                                <div class="article">
                                  <h4><a href="Polysaccharides-in-colon-specific-drug-delivery.php"> Polysaccharides in colon specific drug delivery </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vikas Jain <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Neha Shukla <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>SC Mahajan </p>
                                  <p class="mb5">Review Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000103 x------>
                                <!------JTS.1000104 ------>
                                <div class="article">
                                  <h4><a href="Molecular-analysis-of-CD133-positive-circulating-tumor-cells-from-patients-with-metastatic-castration-resistant-prostate-cancer.php"> Molecular analysis of CD133-positive circulating tumor cells from patients with metastatic castration-resistant prostate cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Edwin E Reyes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marc Gillard <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryan Duggan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kristen Wroblewski <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Steven Kregel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masis Isikbay <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jacob Kach <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hannah Brechka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David J Vander Weele <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Russell Z Szmulewitz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Donald J Vander Griend </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 30, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000104  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="Role-of-short-term-lipoic-acid-therapy-in-patients-with-hypercholesterolemia-to-reverse-oxidative-stress-and-endothelial-dysfunction.php"> Role of short-term α-lipoic acid therapy in patients with hypercholesterolemia to reverse oxidative stress and endothelial dysfunction </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anup K Sabharwal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daniel Kurnik <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jason D Morrow <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James M May </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 31, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000105  x------>
                                <!------JSIN.1000106------>
                                <div class="article">
                                  <h4><a href="Regulatory-processes-for-sponsor-investigators.php"> Regulatory processes for sponsor-investigators </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gordon D. MacFarlane </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 01, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JSIN.1000106 x------>
                                <!------JTS.1000107------>
                                <div class="article">
                                  <h4><a href="Occult-co-infection-in-the-oral-cavity-with-cytomegalovirus-during-immuno-suppression.php"> Occult co-infection in the oral cavity with cytomegalovirus during immuno-suppression </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mario Dioguardi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giuseppe Troiano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lucio Lo Russo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giovanni Giannatempo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Corrado Rubini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dario Bertossi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roberto Cocchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lorenzo Lo Muzio </p>
                                  <p class="mb5">Case Report-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000107 x------>
                                <!------JTS.1000108------>
                                <div class="article">
                                  <h4><a href="Direct-regional-microvascular-monitoring-and-assessment-of-blood-brain-barrier-function-following-cerebral-ischemia-reperfusion-injury.php"> Direct regional microvascular monitoring and assessment of blood brain barrier function following cerebral ischemia-reperfusion injury </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tavarekere¬ N Nagaraja <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kelly A Keenan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James R Ewing <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sukruth Shashikumar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Varun S Nadig <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raveena Munnangi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert A Knight </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000108 x------>
                                <!------JTS.1000109------>
                                <div class="article">
                                  <h4><a href="Practical-guidelines-for-Argatroban-and-Bivalirudin-in-patients-with-Heparin-induced-Thrombocytopenia
.php"> Practical guidelines for Argatroban and Bivalirudin in patients with Heparin-induced Thrombocytopenia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Giuseppe Colucci <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michael Nagler <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nicole Klaus <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tiziana Conte <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Evelyne Giabbani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lorenzo Alberio </p>
                                  <p class="mb5">Research Article- Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000109 x------>
                                <!------JTS.1000110 ------>
                                <div class="article">
                                  <h4><a href="Continuous-positive-airway-pressure-CPAP-consequences-on-right-heart-function-and-serum-biomarkers-in-obstructive-sleep-apnea-assessed-by-tricuspid-annular-plane-systolic-excursion-TAPSE-and-color-tissue-doppler-imaging.php"> Continuous positive airway pressure (CPAP) consequences on right heart function and serum biomarkers in obstructive sleep apnea assessed by tricuspid annular plane systolic excursion (TAPSE) and color tissue doppler imaging </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rose Franco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Aniko Szabo M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Glenn Krakower <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Timothy Woods </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000110  x------>
                                <!------JTS.1000111------>
                                <div class="article">
                                  <h4><a href="Long-term-follow-up-of-patients-with-neurocysticercosis-and-the-development-of-seazures.php"> Long-term follow-up of patients with neurocysticercosis and the development of seazures </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fandiño-Franky Jaime <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guerra-Olivares Randy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Piña-Cabrales Sandra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fandiño-MerzJavier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mayor Liliana <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tellez Carlos Alfonso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Merlano-Zabaleta Alfonso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vitola Bacter <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiménez JD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rodríguez Y Tomás </p>
                                  <p class="mb5">Research Article-Journal of Translational Science (JTS) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 30, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JTS.1000111 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>JTS welcomes direct submissions from authors: Attach your word file with e- mail and send it to submissions@oatext.com alternatively to <a href="mailto:editor.jts@oatext.com">editor.jts@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
