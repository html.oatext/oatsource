<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Journal of Pregnancy and Reproduction (JPR)</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php");?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Journal of Pregnancy and Reproduction (JPR)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Journal of Pregnancy and Reproduction (JPR)</h2>
                <!-- <h4>Online ISSN: 2059-0393</h4>  -->
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!-- <span class="black-text"><i class="fa fa-university"></i>    University of Lincoln<br><br></span> -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
              
             
                 
                 

<p><a href="img/covers-JPR.jpg" target="_blank"><img src="img/covers-JPR.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Journal of Pregnancy and Reproduction (JPR) is an open access and peer reviewed journal with a rapid publication process. JPR will feature original research, reviews, clinical studies, editorials, expert opinion and perspective papers, commentaries, short communications, hypothesis, mini-reviews, conference proceedings, meeting-reports, book reviews etc on all clinical and medical aspects related to pregnancy and reproduction.</p>

<p>JPR will accept articles from the following topic areas including complications and diseases during pregnancy, hypothalamic pituitary dysfunction, advances in in-vitro fertilization (IVF), assisted reproduction, endometriosis, polycystic ovarian syndrome (PCOS), contraception, reproductive diseases, fertility and infertility, reproductive medicine, ovulation induction, fertility advancements and other aspects.</p>

<p>JPR welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.jpr@oatext.com">editor.jpr@oatext.com</a></p>

<p>Please, follow the&nbsp;<a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>

<p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by JPR, is properly cited.</p>



               
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Editor in chief</h2>
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Reecha Sharma</h4>
                  <p>Assistant professor<br>
                    Department of Health services, Interdisciplinary Health services<br>
                    St. Joseph’s University<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Suha Al-Oballi Kridli</h4>
                  <p>Professor<br>
                    Department of Nursing<br>
                    Oakland University<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ricardo Francalacci Savaris</h4>
                  <p>Professor<br>
                    Department of Obstetrics and Gynecology<br>
                    Universidade Federal do Rio Grande do Sul<br>
                    Brazil</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Giovanni Monni</h4>
                  <p>Director<br>
                    Department Obstetrics/Gynecology<br>
                    Prenatal and Preimplantation Genetic Diagnosis<br>
                    Fetal Therapy<br>
                    Microcitemico Hospital<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>J. David Wininger</h4>
                  <p>Assistant Professor and Scientific Director<br>
                    Center for Reproductive Medicine<br>
                    Department of Obstetrics and Gynecology<br>
                    Wake Forest School of Medicine<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hua-chuan Zheng</h4>
                  <p>Professor<br>
                      Cancer Research Center<br>
                      The First Affiliated Hospital of Liaoning Medical University<br>
                      China</p>
                </div>
              </div>  
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Shoichiro Ozaki </h4>
                  <p>Professor Emeritus<br>
                      Ehime University<br>
                      Japan</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Georgios Androutsopoulos </h4>
                  <p>Assistant Professor & Consultant<br>
                    Department of Obstetrics & Gynecology<br>
                    School of Medicine, University of Patras<br> 
                    Greece</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Kelly Rabah</h4>
                  <p>Assistant Professor<br>
                    Department of Obstetrics/Gynecology<br>
                    Wright State University<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Chong Qiao</h4>
                  <p>Professor<br>
                    Department of Obstetrics & Gynaecology<br>
                    Shengjing Hospital<br>
                    China Medical University<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                    <h4>Akmal Nabil Ahmad El-Mazny</h4>
                    <p>Professor <br>
                    Department of Obstetrics and Gynecology<br>
                    Cairo University<br>
                    Egypt</p>
                </div>
                <div class="medium-4 columns">
                    <h4>DARIO GALANTE</h4>
                    <p>Responsible for Pediatric and Neonatal Anesthesia<br>
                      University Department of Anesthesia and Intensive Care<br>
                      University Hospital Ospedali Riuniti of Foggia<br>
                      Italy</p>
                </div>
              </div> 
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>József Gábor Joó</h4>
                  <p>Assistant Professor<br>
                      Department of Obstetrics and Gynecology<br>
                      Semmelweis University<br>
                      Hungary</p>
                </div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>

              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>  





            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <!--<h4>Volume 1, Issue 6</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <!--<h4>Volume 1, Issue 5</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <!--<h4>Volume 1, Issue 4</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <p>Will be updated soon.</p>
             <!-- <div class="lee">
            <dl data-accordion="" class="accordion" >
              <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
              <div class="content" id="panel1b">
                  <div class="hr-tabs">
                  <ul class="tabs" data-tab>
                      <li class="tab-title active"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                    <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                  </ul>
                  <div class="tabs-content">
                    <div class="content  " id="panela">
                      <div class="row pt10">
                      <div class="small-12 columns">
                    AAA
                        </div>
                        </div>
                        </div>

          <div class="content" id="panelb">B</div>

            <div class="content" id="panelc">C</div> 

        </div>

          </div>
          
        </dd>
      </dl>
    </div> -->
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              
              <p>JPR welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.jpr@oatext.com">editor.jpr@oatext.com</a></p>

            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
