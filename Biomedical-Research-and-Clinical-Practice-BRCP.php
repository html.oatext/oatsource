<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Biomedical Research and Clinical Practice</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Biomedical Research and Clinical Practice (BRCP)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd class="no-visible no-height no-border" ><a  name="AnchorName"  href="#a-1"   class="anchor "  >xxx</a></dd>
            <dd  class="no-visible no-height no-border"><a  name="AnchorName"  href="#a-2"   class="anchor" >yyy</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes"    class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
            <dd><a  name="AnchorName" href="#Special_Issues"    class="anchor">Special Issues </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Biomedical Research and Clinical Practice (BRCP)</h2>
                <h4>Online ISSN: 2397-9631</h4>
                <h2 class="mb5"><a href="#Editor-in-Chief">(Founding Editors)</a></h2>
                <div class="row mt10">
                  <div class="medium-6 columns">
                    <h2 class="mb5"><a href="#a-1">Kazuhisa Nishizawa</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> Teikyo University</span> </div>
                  <div class="medium-6 columns">
                    <h2 class="mb5"><a href="#a-2">Cory J. Xian</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> University of South Australia</span> </div>
                </div>
                <hr/>
                <div class="text-justify">
                  <p><a href="img/BRCP-COVER.jpg" target="_blank"><img src="img/BRCP-COVER.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Biomedical Research and Clinical Practice (BRCP) is an open access journal with comprehensive peer review policy and a rapid publication process. BRCP is a novel journal that will focus upon the latest developments in medical research and associated disciplines. This journal will provide an extensive coverage on physiology, biochemistry, microbiology, clinical and medical engineering and computational aspects related to biomedical sciences. This journal aims to disseminate information on physiology, biochemistry, clinical chemistry, biomedical engineering, biomedical informatics, genetics, biophysics, system biology, mathematical medicine, cell biology, molecular and structural biology, developmental biology, pathology, geriatrics, hepatology, hematology, microbiology, immunology, infectious diseases, dermatology, preventive medicine, nephrology, neurology, endocrinology, oncology, ophthalmology, orthopaedics, personalized medicine, pharmacology, radiology, nuclear medicine, regenerative medicine, rehabilitation, toxicology, dentistry and oral surgery with an emphasis on medical/clinical significance. BRCP renders a global platform for academicians and researchers to share their work in relevant fields of biomedicine. </p>
                  <p>BRCP will feature original research, review papers, clinical studies, editorials, expert opinion and perspective papers, commentaries, and book reviews.</p>
                  <p> Biomedical Research and Clinical Practice (BRCP) is composed of the following Specialty Sections</p>
                  <p>
                  <table cellspacing="0" width="100%" cellpadding="0">
                    <tr>
                      <td width="24%"><p>Allergy</p></td>
                      <td width="18%"><p>Endocrinology</p></td>
                      <td width="29%"><p>Molecular & Cellular Biomedicine</p></td>
                      <td><p>Pharmacology</p></td>
                    </tr>
                    <tr>
                      <td><p>Anesthesiology</p></td>
                      <td><p>Epidemiology</p></td>
                      <td><p>Nephrology</p></td>
                      <td><p>Physiology</p></td>
                    </tr>
                    <tr>
                      <td><p>Biochemistry</p></td>
                      <td><p>Forensic Medicine</p></td>
                      <td><p>Neurology</p></td>
                      <td><p>Plastic Surgery</p></td>
                    </tr>
                    <tr>
                      <td><p>Biomaterials</p></td>
                      <td><p>Gastroenterology</p></td>
                      <td><p>Neurosurgery</p></td>
                      <td><p>Psychiatry</p></td>
                    </tr>
                    <tr>
                      <td><p>Biophysics</p></td>
                      <td><p>Genetics</p></td>
                      <td><p>Obstetrics & Gynecology</p></td>
                      <td><p>Public Health</p></td>
                    </tr>
                    <tr>
                      <td><p>Biotechnology</p></td>
                      <td><p>Genomics</p></td>
                      <td><p>Ophthalmology</p></td>
                      <td><p>Pulmonary Diseases</p></td>
                    </tr>
                    <tr>
                      <td><p>Cancer & Clinical Oncology</p></td>
                      <td><p>Hematology</p></td>
                      <td><p>Oral & Maxillofacial Surgery</p></td>
                      <td><p>Radiology</p></td>
                    </tr>
                    <tr>
                      <td><p>Cardiovascular Diseases</p></td>
                      <td><p>Hepatology</p></td>
                      <td><p>Orthopedics</p></td>
                      <td><p>Rehabilitation</p></td>
                    </tr>
                    <tr>
                      <td><p>Clinical Chemistry</p></td>
                      <td><p>Immunology</p></td>
                      <td><p>Otolaryngology</p></td>
                      <td><p>Sports Medicine</p></td>
                    </tr>
                    <tr>
                      <td><p>Computational Biology</p></td>
                      <td><p>Infectious Diseases</p></td>
                      <td><p>Palliative Care</p></td>
                      <td><p>Surgery</p></td>
                    </tr>
                    <tr>
                      <td><p>Critical Care</p></td>
                      <td><p>Medical Economics</p></td>
                      <td><p>Parasitology</p></td>
                      <td><p>Tissue Engineering & Regenerative Medicine</p></td>
                    </tr>
                    <tr>
                      <td><p>Dentistry</p></td>
                      <td><p>Medical Engineering</p></td>
                      <td><p>Pathology</p></td>
                      <td><p>Toxicology</p></td>
                    </tr>
                    <tr>
                      <td><p>Dermatology</p></td>
                      <td><p>Metabolic Diseases</p></td>
                      <td><p>Pediatrics</p></td>
                      <td><p>Urology</p></td>
                    </tr>
                    <tr>
                      <td><p>Emergency Medicine</p></td>
                      <td><p>Microbiology</p></td>
                      <td><p>Pharmaceutics</p></td>
                      <td><p>Virology</p></td>
                    </tr>
                  </table>
                  </p>
                  <p>BRCP welcomes direct submissions from authors: Attach your word file with e- mail and send it to <a href="mailto:submissions@oatext.com">submissions@oatext.com</a> alternatively to <a href="mailto:editor.brcp@oatext.com">editor.brcp@oatext.com</a></p>
                  <p>Please, follow the <a href="http://oatext.com/Manuscript-Preparation.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                  <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by BRCP, is properly cited.</p>
                  <div class="shadow-widget same-height-widget">
                    <div class="news-widget">
                      <h2 class="blue-text">Biomedical Research and Clinical Practice Editor in Chief Message</h2>
                      <hr>
                      <div class="flex-video">
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/qsMmWbxs8EY" frameborder="0" allowfullscreen></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Kazuhisa Nishizawa </h2>
              <hr/>
              <p><img class="profile-pic" alt="" src="img/Kazuhisa-Nishizawa.jpg" align="right">Dr Nishizawa is serving as Professor at the Department of Clinical Laboratory Science of the Teikyo University School of Medical Technology, Japan. After graduating from the University of Tokyo School of Medicine, Tokyo, and being qualified as a medical doctor in 1989, He started the research at the Division of Medicine, Department of Neurobiology, Institute of Brain Research, University of Tokyo Graduate School, and received the Ph.D. degree (medicine and physiology) in 1994. Although his qualification is medical, his current research focuses on computational biochemistry: biologically relevant processes of interactions between lipid membrane and proteins, ion channel dynamics, transporter dynamics, membrane fusion (viral and intracellular), and other pathological processes. His recent focus is on improving several force fields for molecular dynamics with an emphasis on membrane protein folding, which are of high relevance of current molecular medicine.</p>
              <p>Prior to these chemical studies, Dr Nishizawa has wet-laboratory experiences in molecular biology of mRNA stability regulation in neuronal cells and T-lymphocyte immunobiology from 1993 to 1998, including his postdoc year<a name="_GoBack"></a>s at Dana-Farber Cancer Institute, Boston. He also conducted studies in molecular evolutionary genetics using numerical simulations from 1998 to 2002. In 2006, he served as the founding Chief of the Dept of Clinical Laboratory Science at Teikyo University.</p>
              <p>With a publication record covering computational physical chemistry, neurobiology, immunobiology and molecular evolution, since 2010 he has been serving as an editorial board member of several journals including Journal of Biophysical Chemistry, BioMed Research International, and Computational and Mathematical Methods in Medicine, the latter providing a unique forum for interdisciplinary research between clinicians/experimentalists and theoreticians, focusing on collaborations across biomedical, clinical, and translational research areas.&nbsp; Teaching many students in his courses of biochemistry, clinical chemistry and analytical chemistry, he emphasizes training the next generation of scientists who have ambitions to bridge across diverse medical disciplines.</p>
              <br>
              <h2>Cory J. Xian</h2>
              <hr/>
              <p><img src="img/Cory J Xian.JPG" align="right" class="profile-pic"> Dr Xian is a Research Professor and NHMRC Senior Research Fellow at University of South Australia. He has more than 25 years of experience in research on tissue growth and repair, growth plate biology and bone growth, bone repair and regeneration, cancer chemotherapy-induced bone defects, and osteoporosis. Dr Xian obtained his PhD in 1993 from Murdoch University; and his earlier research positions include those at Child Health Research Institute, University of Adelaide, Flinders University, and Women’s and Children’s Hospital. Dr Xian has over 140 peer-reviewed publications.</p>
            </div>
            <div id="a-1" class="content">
              <h2>Kazuhisa Nishizawa </h2>
              <hr/>
              <p><img class="profile-pic" alt="" src="img/Kazuhisa-Nishizawa.jpg" align="right">Dr Nishizawa is serving as Professor at the Department of Clinical Laboratory Science of the Teikyo University School of Medical Technology, Japan. After graduating from the University of Tokyo School of Medicine, Tokyo, and being qualified as a medical doctor in 1989, He started the research at the Division of Medicine, Department of Neurobiology, Institute of Brain Research, University of Tokyo Graduate School, and received the Ph.D. degree (medicine and physiology) in 1994. Although his qualification is medical, his current research focuses on computational biochemistry: biologically relevant processes of interactions between lipid membrane and proteins, ion channel dynamics, transporter dynamics, membrane fusion (viral and intracellular), and other pathological processes. His recent focus is on improving several force fields for molecular dynamics with an emphasis on membrane protein folding, which are of high relevance of current molecular medicine.</p>
              <p>Prior to these chemical studies, Dr Nishizawa has wet-laboratory experiences in molecular biology of mRNA stability regulation in neuronal cells and T-lymphocyte immunobiology from 1993 to 1998, including his postdoc year<a name="_GoBack"></a>s at Dana-Farber Cancer Institute, Boston. He also conducted studies in molecular evolutionary genetics using numerical simulations from 1998 to 2002. In 2006, he served as the founding Chief of the Dept of Clinical Laboratory Science at Teikyo University.</p>
              <p>With a publication record covering computational physical chemistry, neurobiology, immunobiology and molecular evolution, since 2010 he has been serving as an editorial board member of several journals including Journal of Biophysical Chemistry, BioMed Research International, and Computational and Mathematical Methods in Medicine, the latter providing a unique forum for interdisciplinary research between clinicians/experimentalists and theoreticians, focusing on collaborations across biomedical, clinical, and translational research areas.&nbsp; Teaching many students in his courses of biochemistry, clinical chemistry and analytical chemistry, he emphasizes training the next generation of scientists who have ambitions to bridge across diverse medical disciplines.</p>
            </div>
            <div id="a-2" class="content">
              <h2>Cory J. Xian</h2>
              <hr/>
              <p><img src="img/Cory J Xian.JPG" align="right" class="profile-pic"> <b>Biography</b> Dr Xian is a Research Professor and NHMRC Senior Research Fellow at University of South Australia. He has more than 25 years of experience in research on tissue growth and repair, growth plate biology and bone growth, bone repair and regeneration, cancer chemotherapy-induced bone defects, and osteoporosis. Dr Xian obtained his PhD in 1993 from Murdoch University; and his earlier research positions include those at Child Health Research Institute, University of Adelaide, Flinders University, and Women’s and Children’s Hospital. Dr Xian has over 140 peer-reviewed publications.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Section Editor in Chiefs</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Kevin D. Brown (Genetics)</h4>
                  <p>Associate Professor<br>
                    Department of Biochemistry and Molecular Biology<br>
                    University of Florida<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Carmen Cuffari (Gastroenterology)</h4>
                  <p>Associate Professor<br>
                    Department of Pediatrics<br>
                    The Johns Hopkins Children’s Center<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hironobu Ihn (Dermatology)</h4>
                  <p>Professor & Chairman<br>
                    Department of Dermatology & Plastic Surgery<br>
                    Kumamoto University<br>
                    Japan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>CHAN Sun On (Neurology)</h4>
                  <p>Professor<br>
                    School of Biomedical Sciences<br>
                    Assistant Dean, Faculty of Medicine<br>
                    The Chinese University of Hong Kong<br>
                    Hong Kong </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Rajeev Aurora (Computational Biology)</h4>
                  <p>Associate Professor<br>
                    Molecular Microbiology & Immunology<br>
                    Saint Louis University School of Medicine<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Tetsuro Sasada (Cancer and Clinical Oncology)</h4>
                  <p>Director<br>
                    Cancer Vaccine Center<br>
                    Kanagawa Cancer Center<br>
                    Japan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Noah Isakov (Molecular & Cellular Biomedicine)</h4>
                  <p>Chair<br>
                    The Shraga Segal Department of Microbiology and Immunology<br>
                    Ben Gurion University of the Negev <br>
                    Israel</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Luigi Iuliano (Clinical Chemistry)</h4>
                  <p>Senior Associate Professor <br>
                    Department of Internal Medicine<br>
                    Sapienza University of Rome<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Cory Xian (Tissue Engineering and Regenerative medicine)</h4>
                  <p>Professor<br>
                    School of Pharmacy & Medical Sciences<br>
                    University of South Australia<br>
                    Australia</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Alessandro Mauriello (Cardiovascular Diseases)</h4>
                  <p>Associate Professor<br>
                    Department of Biomedicine and Prevention<br>
                    University of Rome Tor Vergata<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Thomas Müller (Psychiatry)</h4>
                  <p>Professor<br>
                    Department of Neurology<br>
                    St. Joseph Hospital<br>
                    Germany</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vahit Ozmen (Surgery)</h4>
                  <p>Professor<br>
                    Department of Surgery<br>
                    Istanbul University<br>
                    Turkey</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Sanjib Das Adhikary (Anesthesiology)</h4>
                  <p>Associate Professor
                    Division Director of Regional Anesthesia  and Acute Pain Medicine<br>
                    Penn State College of Medicine<br>
                    USA</p>
                </div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Robert Geoffrey Cooper</h4>
                  <p>Professor of Medicine<br>
                    Faculty of Health &amp; Life Sciences<br>
                    University of Liverpool<br>
                    UK</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jiann-Der Lee</h4>
                  <p>Professor<br>
                    FIET<br>
                    Department of Electrical Engineering<br>
                    Chang Gung University<br>
                    Taiwan </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Kazuhisa Bessho</h4>
                  <p>Professor and Chairman<br>
                    Department of Oral and Maxillofacial Surgery<br>
                    Graduate School of Medicine, Kyoto University<br>
                    Japan </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ewa Szczepanska-Sadowska</h4>
                  <p>Professor<br>
                    Department of Experimental and Clinical Physiology<br>
                    Medical University of Warsaw<br>
                    Poland </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Philip D. Cotter</h4>
                  <p>Principal and Co-Founder of ResearchDx<br>
                    Laboratory Director<br>
                    Pacific Diagnostics Clinical Laboratory<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Howard A. Palley</h4>
                  <p>Professor Emeritus and<br>
                    Distinguished Fellow,<br>
                    Institute for Human Services Policy<br>
                    School of Social Work<br>
                    University of Maryland<br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Zhixiang Wang</h4>
                  <p>Professor<br>
                    Department of Medical Genetics<br>
                    Faculty of Medicine and Dentistry<br>
                    University of Alberta<br>
                    Canada </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Gianluca Serafini</h4>
                  <p>Professor of Psychiatry<br>
                    Faculty of Medicine and Psychology<br>
                    Sapienza University<br>
                    Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alvin A Holder</h4>
                  <p>Department of Chemistry and Biochemistry                    
                    Old Dominion University<br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Yoram Barak</h4>
                  <p>Professor<br>
                    Department of Psychiatry<br>
                    Sackler School of Medicine<br>
                    Tel-Aviv University<br>
                    Israel </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yasuo Iwasaki</h4>
                  <p>Professor <br>
                    Department of Neurology<br>
                    Toho University<br>
                    Omori Hospital<br>
                    Japan </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Tzi Bun NG</h4>
                  <p>Professor of Biochemistry<br>
                    School of Biomedical Sciences, <br>
                    Faculty of Medicine,<br>
                    The Chinese University of Hong Kong<br>
                    Hong Kong </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Anvarhusein A. Isab</h4>
                  <p>Professor<br>
                    Department of Chemistry<br>
                    King Fahd University of Petroleum &amp; Minerals<br>
                    Saudi Arabia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Serap YALIN</h4>
                  <p>Associate Professor<br>
                    Department of Biochemistry<br>
                    Mersin University<br>
                    Turkey</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Natalia V. Gulyaeva</h4>
                  <p>Professor and Head<br>
                    Dept. of Functional Biochemistry of the Nervous System,<br>
                    Russian Academy of Sciences<br>
                    Russia</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Krzysztof Roszkowski</h4>
                  <p>Professor<br>
                    Department of Oncology, Radiotherapy and Gynecologic Oncology<br>
                    Nicolaus Copernicus University<br>
                    Poland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Osmar Antonio Centurión</h4>
                  <p>Professor of Medicine<br>
                    Department of Health Sciences’ Investigation<br>
                    Asuncion National University<br>
                    Paraguay</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Wen Qiu</h4>
                  <p>Associate professor<br>
                    Department of Immunology<br>
                    Nanjing Medical University<br>
                    P.R. China</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ben Chung-Lap Chan</h4>
                  <p>Senior Research Coordinator<br>
                    Institute of Chinese Medicine<br>
                    Chinese University of Hong Kong<br>
                    Hong Kong</p>
                </div>
                <div class="medium-4 columns">
                  <h4>David I.Mostofsky</h4>
                  <p>Professor<br>
                    Department of Psychological & Brain Sciences<br>
                    Boston University<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Lawrence C. Perlmuter</h4>
                  <p>Professor<br>
                    Department of Psychology<br>
                    Rosalind Franklin University of Medicine<br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Lanchun Lu</h4>
                  <p>Assistant Professor<br>
                    Department of Radiation Oncology<br>
                    The OHIO State University<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Esther Priel</h4>
                  <p>Professor<br>
                    Department of Microbiology , Immunology & Genetics<br>
                    Ben-Gurion university of the Negev<br>
                    Israel </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sivagnanam Thamilselvan</h4>
                  <p>Associate Scientist<br>
                    Department of Urology<br>
                    Henry Ford Health System<br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4> Harry Schroeder</h4>
                  <p>Professor<br>
                    Division of Clinical Immunology and Rheumatology<br>
                    University of Alabama<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Annemarie Shibata </h4>
                  <p>Associate Professor<br>
                    Department of Biology<br>
                    Creighton University
                    
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Oleg Nadashkevich</h4>
                  <p>Professor<br>
                    Department of Family Medicine<br>
                    Lviv National Medical University<br>
                    Ukraine </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>José Francisco Navarro</h4>
                  <p>Professor<br>
                    Department of Psychobiology and Methodology of Behavioural Science<br>
                    University of Málaga<br>
                    Spain </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Maria Passafaro</h4>
                  <p>Department BIOMETRA<br>
                    CNR Institute of Neuroscience<br>
                    Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Heung-Il Suk</h4>
                  <p>Assistant Professor<br>
                    Department of Brain and Cognitive Engineering<br>
                    Korea University<br>
                    South Korea </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Tamer A Gheita </h4>
                  <p>Professor <br>
                    Department of Rheumatology and Clinical Immunology<br>
                    Cairo University<br>
                    Egypt </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Xiao-Xin Yan</h4>
                  <p>Professor<br>
                    Department of Anatomy & Neurobiology<br>
                    Central South University<br>
                    China </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Gamal Hassan El-Sokkary</h4>
                  <p>Professor<br>
                    Department of Zoology<br>
                    Assiut University<br>
                    Egypt </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Vladan Bajic</h4>
                  <p>Professor<br>
                    Laboratory for Radiobiology and Molecular Genetics<br>
                    Institute for Nuclear Research<br>
                    Serbia </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ibrahim El-Sayed M. El-Hakim</h4>
                  <p>Professor<br>
                    Ain Shams University<br>
                    Department of Oral and Maxillofacial Surgery<br>
                    Egypt </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mahmoud A Hafez</h4>
                  <p>Professor<br>
                    Head of the Orthopaedic Unit<br>
                    October 6 University Hospital<br>
                    Egypt </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Tarek Aboul-Fadl Mohamed Hassan</h4>
                  <p>Professor<br>
                    Department of Pharmaceutical Chemistry<br>
                    Assiut University<br>
                    Egypt </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Turgay Celik</h4>
                  <p>Head of Coronary Care Unit<br>
                    Department of Cardiology<br>
                    Gulhane Military Medical Academy<br>
                    Turkey </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Masoud Neghab</h4>
                  <p>professor<br>
                    School of Health<br>
                    Shiraz University of Medical Sciences<br>
                    Iran </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Marcelo Rodrigues Azenha'</h4>
                  <p>Professor at Funorp (Surgery)<br>
                    Oral Surgeon - Stomatology Department<br>
                    University of São Paulo<br>
                    Brazil</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Seung-Yup Ku</h4>
                  <p>Professor<br>
                    Department of Obstetrics and Gynecology<br>
                    Seoul National University Hospital<br>
                    South Korea </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jinyong Peng</h4>
                  <p>Professor<br>
                    College of Pharmacy<br>
                    Dalian Medical University<br>
                    China </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Geoffrey Sperber</h4>
                  <p>Professor Emeritus<br>
                    Faculty of Medicine & Dentistry<br>
                    University of Alberta<br>
                    Canada</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ken KL Yung</h4>
                  <p>Associate Head and Professor<br>
                    Department of Biology<br>
                    Hong Kong Baptist University<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ameen Khraisat</h4>
                  <p>Professor<br>
                    Department of Conservative Dentistry and Prosthodontics<br>
                    University of Jordan<br>
                    Jordan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>P. Satyanarayana Murthy</h4>
                  <p>Professor<br>
                    ENT and Head & Neck Surgery<br>
                    Pinnamaneni Siddhartha Institute of Medical Sciences and Research Center<br>
                    India</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ashraf Ramadan Hafez Ibraheem</h4>
                  <p>Professor<br>
                    Head of Department of Orthopaedic Physical Therapy<br>
                    Deraya University<br>
                    Egypt</p>
                </div>
                <div class="medium-4 columns">
                  <h4>José Enrique Robles</h4>
                  <p>Professor<br>
                    Department of Urology<br>
                    University of Navarra<br>
                    Spain</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Aditi Banerjee</h4>
                  <p>Assistant Professor<br>
                    Department of Pediatrics<br>
                    University of Maryland School of Medicine<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ahmet Eroglu</h4>
                  <p>Professor<br>
                    Department of Anesthesiology and Reanimation<br>
                    Karadeniz Technical University<br>
                    Turkey</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Chen Yu</h4>
                  <p>Investigator<br>
                    Shenzhen Institutes of Advanced Technology<br>
                    Chinese Academy of Sciences<br>
                    China</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Emad Tawfik Mahmoud Daif</h4>
                  <p>Professor<br>
                    Faculty of Oral & Dental Medicine<br>
                    Cairo University<br>
                    Egypt</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Michel Bourin</h4>
                  <p>Professor<br>
                    Neurobiology of anxiety and mood disorders 98<br>
                    University of Nantes<br>
                    France</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sergey Savinov</h4>
                  <p>Extension Associate Professor<br>
                    Department of Biochemistry and Molecular Biology<br>
                    University of Massachusetts<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Dr. Imtiaz A. Chaudhry</h4>
                  <p>Ophthalmic Plastic & Reconstructive Surgery<br>
                    Texas Medical Center<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hom Nath Chalise</h4>
                  <p>Asian College for Advance Studies<br>
                    Purbanchal University<br>
                    Nepal</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Antonio G Tristano</h4>
                  <p>Rheumatologist<br>
                    Centro Medico Carpetana<br>
                    Spain</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 2, Issue 3</h4>
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 2, Issue 2</h4>
              <hr/>
              <!------BRCP.1000131 ------>
              <div class="article">
                <h4><a href="Cholesterol-and-saturated-fatty-acid-stabilize-dimerization-of-helical-transmembrane-peptides-by-lowering-energy-cost-related-to-peptides.php"> Cholesterol and saturated fatty acid stabilize dimerization of helical transmembrane peptides by lowering energy cost related to peptides desolvation from lipids upon dimerization: an insight from atomistic simulation </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Manami Nishizawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuhisa Nishizawa </p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 13, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000131  x------>
              <!------BRCP.1000132 ------>
              <div class="article">
                <h4><a href="Joint-Hypermobility-Syndrome-and-Postural-Orthostatic-Tachycardia-Syndrome-HyPOTS.php"> Joint Hypermobility Syndrome and Postural Orthostatic Tachycardia Syndrome (HyPOTS) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dana Mandel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali D. Askari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Charles J. Malemud <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Artan Kaso </p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 25, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000132  x------>
              <!------BRCP.1000133 ------>
              <div class="article">
                <h4><a href="Urgent-surgical-treatment-for-a-huge-metastatic-tumor-of-the-right-lung-via-plastron-thoracotomy-A-case-report.php"> Urgent surgical treatment for a huge metastatic tumor of the right lung via plastron thoracotomy: A case report </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryuta Fukai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihito Irie <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshiaki Katada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hajime Arifuku </p>
                <p class="mb5">Case Report-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 27, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000133  x------>
              <!------BRCP.1000134 ------>
              <div class="article">
                <h4><a href="Paracardiac-mass-in-a-55-year-old-female-a-case-report-of-right-sided-spontaneous-diaphragmatic-hernia.php"> Paracardiac mass in a 55-year-old female - a case report of right-sided spontaneous diaphragmatic hernia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rakul Nambiar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Archana Jayakumar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jayakumar Chinnathambi</p>
                <p class="mb5">Case Report-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 30, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000134  x------>
              <!------BRCP.1000135 ------>
              <div class="article">
                <h4><a href="Circadian-rhythms-of-angiogenic-factors-in-skin-and-wound-tissue-in-per2-mutant-mice.php"> Circadian rhythms of angiogenic factors in skin and wound tissue in per2-mutant mice</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoaki Hoshi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoko Toukairin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomomi Arai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Makoto Nogami</p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 14, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000135  x------>
              <!------BRCP.1000136 ------>
              <div class="article">
                <h4><a href="Alteration-of-tumor-markers-may-predict-survival-in-colorectal-cancer-patients-treated-with-TAS-102-or-regorafenib-as-salvage-line-chemotherapy-a-single-institutional-experience.php">Alteration of tumor markers may predict survival in colorectal cancer patients treated with TAS-102 or regorafenib as salvage-line chemotherapy: a single-institutional experience</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keiji Matsuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keijiro Nozawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Ohno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuka Okada
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takahiro Yagi
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuo Tsukamoto
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihisa Fukushima
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takuya Akahane
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Horiuchi
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryu Shimada
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tamuro Hayama
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koichi Okamoto
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshi Tsuchiya
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junko Tamura
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisae Iinuma
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuko Sasajima
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fukuo Kondo
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shoichi Fujii
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yojiro Hashiguchi</p>

                <p class="mb5">Article Type-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 15, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000136  x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 2, Issue 1</h4>
              <hr/>
              <!------BRCP.1000125------>
              <div class="article">
                <h4><a href="Coenzyme-Q10-gamma-cyclodextrin-complex-is-a-powerful-nutraceutical-for-anti-aging-and-health-improvements.php">Coenzyme Q10 - gamma cyclodextrin complex is a powerful nutraceutical for anti-aging and health improvements</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukiko Uekaji <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keiji Terao </p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 03, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000125 x------>
              <!------BRCP.1000126 ------>
              <div class="article">
                <h4><a href="Suppressive-effect-of-mycolactone-containing-fraction-from-Mycobacterium-ulcerans-on-antibody-production-against-co-administered-antigens.php">Suppressive effect of mycolactone-containing fraction from <em>Mycobacterium ulcerans </em>on antibody production against co-administered antigens</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noriko Shinoda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hajime Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mineo Watanabe</p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 05, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000126  x------>
              <!------BRCP.1000127 ------>
              <div class="article">
                <h4><a href="Biomedical-research-and-clinical-practice-your-journal.php">Biomedical research and clinical practice - your journal</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhisa Nishizawa</p>
                <p class="mb5">Editorial-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 09, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000127  x------>
              <!------BRCP.1000128 ------>
              <div class="article">
                <h4><a href="Extrinsic-effectors-regulating-genes-for-plasmalogen-biosynthetic-enzymes-in-HepG2-cells.php">Extrinsic effectors regulating genes for plasmalogen biosynthetic enzymes in HepG2 cells</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryouta Maeba <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shin-ichi Nakahara</p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 03, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000128  x------>
              <!------BRCP.1000129 ------>
              <div class="article">
                <h4><a href="Biofeedback-training-on-university-students-anxiety-management-A-systematic-review.php">Biofeedback training on university student's anxiety management: A systematic review</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paulo Chaló <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Patrícia Batista <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anabela Pereira</p>
                <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 09, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000129  x------>
              <!------BRCP.1000130 ------>
              <div class="article">
                <h4><a href="Infrared-light-as-a-potential-therapeutic-approach-for-neurodegeneration.php">Infrared light as a potential therapeutic approach for neurodegeneration</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhiro Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takayasu Kawasaki</p>
                <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000130  x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion">
                  <dd> <a href="#panel1c" class="accordian-active">Volume 2<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>April 2017</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>February 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000131 ------>
              <div class="article">
                <h4><a href="Cholesterol-and-saturated-fatty-acid-stabilize-dimerization-of-helical-transmembrane-peptides-by-lowering-energy-cost-related-to-peptides.php"> Cholesterol and saturated fatty acid stabilize dimerization of helical transmembrane peptides by lowering energy cost related to peptides desolvation from lipids upon dimerization: an insight from atomistic simulation </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Manami Nishizawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuhisa Nishizawa </p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 13, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000131  x------>
              <!------BRCP.1000132 ------>
              <div class="article">
                <h4><a href="Joint-Hypermobility-Syndrome-and-Postural-Orthostatic-Tachycardia-Syndrome-HyPOTS.php"> Joint Hypermobility Syndrome and Postural Orthostatic Tachycardia Syndrome (HyPOTS) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dana Mandel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali D. Askari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Charles J. Malemud <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Artan Kaso </p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 25, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000132  x------>
              <!------BRCP.1000133 ------>
              <div class="article">
                <h4><a href="Urgent-surgical-treatment-for-a-huge-metastatic-tumor-of-the-right-lung-via-plastron-thoracotomy-A-case-report.php"> Urgent surgical treatment for a huge metastatic tumor of the right lung via plastron thoracotomy: A case report </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryuta Fukai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihito Irie <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshiaki Katada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hajime Arifuku </p>
                <p class="mb5">Case Report-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 27, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000133  x------>
              <!------BRCP.1000134 ------>
              <div class="article">
                <h4><a href="Paracardiac-mass-in-a-55-year-old-female-a-case-report-of-right-sided-spontaneous-diaphragmatic-hernia.php"> Paracardiac mass in a 55-year-old female - a case report of right-sided spontaneous diaphragmatic hernia</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rakul Nambiar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Archana Jayakumar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jayakumar Chinnathambi</p>
                <p class="mb5">Case Report-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 30, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000134  x------>
              <!------BRCP.1000135 ------>
              <div class="article">
                <h4><a href="Circadian-rhythms-of-angiogenic-factors-in-skin-and-wound-tissue-in-per2-mutant-mice.php"> Circadian rhythms of angiogenic factors in skin and wound tissue in per2-mutant mice</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoaki Hoshi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoko Toukairin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomomi Arai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Makoto Nogami</p>
                <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 14, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000135  x------>
              <!------BRCP.1000136 ------>
              <div class="article">
                <h4><a href="Alteration-of-tumor-markers-may-predict-survival-in-colorectal-cancer-patients-treated-with-TAS-102-or-regorafenib-as-salvage-line-chemotherapy-a-single-institutional-experience.php">Alteration of tumor markers may predict survival in colorectal cancer patients treated with TAS-102 or regorafenib as salvage-line chemotherapy: a single-institutional experience</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keiji Matsuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keijiro Nozawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Ohno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuka Okada
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takahiro Yagi
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuo Tsukamoto
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshihisa Fukushima
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takuya Akahane
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Horiuchi
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryu Shimada
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tamuro Hayama
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koichi Okamoto
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshi Tsuchiya
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junko Tamura
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hisae Iinuma
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuko Sasajima
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fukuo Kondo
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shoichi Fujii
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yojiro Hashiguchi</p>

                <p class="mb5">Article Type-Biomedical Research and Clinical Practice (BRCP)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 15, 2017</p>
                </em>
                <hr/>
              </div>
              <!------BRCP.1000136  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000125------>
                                <div class="article">
                                  <h4><a href="Coenzyme-Q10-gamma-cyclodextrin-complex-is-a-powerful-nutraceutical-for-anti-aging-and-health-improvements.php">Coenzyme Q10 - gamma cyclodextrin complex is a powerful nutraceutical for anti-aging and health improvements</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukiko Uekaji <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keiji Terao </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 03, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000125 x------>
                                <!------BRCP.1000126 ------>
                                <div class="article">
                                  <h4><a href="Suppressive-effect-of-mycolactone-containing-fraction-from-Mycobacterium-ulcerans-on-antibody-production-against-co-administered-antigens.php">Suppressive effect of mycolactone-containing fraction from <em>Mycobacterium ulcerans </em>on antibody production against co-administered antigens</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noriko Shinoda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hajime Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mineo Watanabe</p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 05, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000126  x------>
                                <!------BRCP.1000127 ------>
                                <div class="article">
                                  <h4><a href="Biomedical-research-and-clinical-practice-your-journal.php">Biomedical research and clinical practice - your journal</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhisa Nishizawa</p>
                                  <p class="mb5">Editorial-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 09, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000127  x------>
                                <!------BRCP.1000128 ------>
                                <div class="article">
                                  <h4><a href="Extrinsic-effectors-regulating-genes-for-plasmalogen-biosynthetic-enzymes-in-HepG2-cells.php">Extrinsic effectors regulating genes for plasmalogen biosynthetic enzymes in HepG2 cells</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryouta Maeba <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shin-ichi Nakahara</p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 03, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000128  x------>
                                <!------BRCP.1000129 ------>
                                <div class="article">
                                  <h4><a href="Biofeedback-training-on-university-students-anxiety-management-A-systematic-review.php">Biofeedback training on university student's anxiety management: A systematic review</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paulo Chaló <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Patrícia Batista <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anabela Pereira</p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 09, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000129  x------>
                                <!------BRCP.1000130 ------>
                                <div class="article">
                                  <h4><a href="Infrared-light-as-a-potential-therapeutic-approach-for-neurodegeneration.php">Infrared light as a potential therapeutic approach for neurodegeneration</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhiro Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takayasu Kawasaki</p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000130  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#paneld"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>December 2016</span><span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>September 2016</span><span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2016</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000119------>
                                <div class="article">
                                  <h4><a href="Distinctive-expression-of-TMEM132A-and-its-regulation-after-the-nerve-injury-in-the-DRG.php"> Distinctive expression of TMEM132A and its regulation after the nerve injury in the DRG </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ghoshun Shimosato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kentaro Oh-hashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yosuke Yamaguchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumimasa Amaya</p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000119 x------>
                                <!------BRCP.1000120------>
                                <div class="article">
                                  <h4><a href="Perception-and-attitude-of-religious-leaders-and-outpatients-in-Dhaka-Bangladesh-with-regard-to-Ayurvedic-medicine.php"> Perception and attitude of religious leaders and outpatients in Dhaka, Bangladesh with regard to Ayurvedic medicine </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshitoku Yoshida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuko Yoshida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Md. Abdul Alim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zakia Alam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammad Asaduzzaman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Manikdrs Shahabuddin</p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000120 x------>
                                <!------BRCP.1000121 ------>
                                <div class="article">
                                  <h4><a href="DHA-enriched-supplement-ameliorates-cancer-associated-systemic-inflammatory-response-via-resolvin-D1-production-a-single-institutional-study.php"> DHA-enriched supplement ameliorates cancer-associated systemic inflammatory response via resolvin D1 production: a single institutional study. </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasuhiko Mohri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koji Tanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroki Imaoka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chikao Miki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroyuki Fujikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tadanobu Shimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuji Toiyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshimitsu Araki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masato Kusunoki </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000121  x------>
                                <!------BRCP.1000122------>
                                <div class="article">
                                  <h4><a href="Factors-regulating-Th17-cells-a-review.php"> Factors regulating Th17 cells: a review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Reiko Seki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuhisa Nishizawa </p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000122 x------>
                                <!------BRCP.1000123 ------>
                                <div class="article">
                                  <h4><a href="Comparison-between-resting-energy-expenditure-measured-by-indirect-calorimetry-and-metabolic-rate-estimate-based-on-Harris-Benedict-equation-in-septic-patients.php"> Comparison between resting energy expenditure measured by indirect calorimetry and metabolic rate estimate based on Harris-Benedict equation in septic patients </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jiro Kamiyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomonori Takazawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akihiro Yanagisawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masafumi Kanamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaru Tobe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Hinohara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumio Kunimoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shigeru Saito </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000123  x------>
                                <!------BRCP.1000124 ------>
                                <div class="article">
                                  <h4><a href="Unexpected-inhibition-of-cervical-carcinoma-cell-proliferation-by-expression-of-heat-shock-transcription-factor-1.php">Unexpected inhibition of cervical carcinoma cell
                                    proliferation by expression of heat shock transcription factor 1</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jiro Kamiyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomonori Takazawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akihiro Yanagisawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masafumi Kanamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaru Tobe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroshi Hinohara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fumio Kunimoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shigeru Saito </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000124  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000113------>
                                <div class="article">
                                  <h4><a href="Enhanced-Th17-responses-with-intestinal-dysbiosis-in-human-allergic-inflammatory-and-autoimmune-diseases.php "> Enhanced Th17 responses with intestinal dysbiosis in human allergic, inflammatory, and autoimmune diseases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Shimizu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Noboru Suzuki </p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 13, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000113 x------>
                                <!------BRCP.1000114 ------>
                                <div class="article">
                                  <h4><a href="Role-of-the-LIV-1-subfamily-of-zinc-transporters-in-the-development-and-progression-of-breast-cancers-A-mini-review.php "> Role of the LIV-1 subfamily of zinc transporters in the development and progression of breast cancers: A mini review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoka Takatani-Nakase <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chihiro Matsui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koichi Takahashi </p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 20, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000114  x------>
                                <!------BRCP.1000115------>
                                <div class="article">
                                  <h4><a href="Effects-of-passive-leg-raising-on-cardiovascular-functions-as-analyzed-by-fingertip-pulse-pressure-profiles.php"> Effects of passive leg raising on cardiovascular functions as analyzed by fingertip pulse pressure profiles </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kenji Nemoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takayo Kaiho <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Susumu Ito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koichi Yoshioka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akira Maki </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000115 x------>
                                <!------BRCP.1000116 ------>
                                <div class="article">
                                  <h4><a href="Mechanisms-of-immunosuppression-by-mesenchymal-stromal-cells-a-review-with-a-focus-on-molecules.php"> Mechanisms of immunosuppression by mesenchymal stromal cells: a review with a focus on molecules </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhisa Nishizawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Reiko Seki </p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000116  x------>
                                <!------BRCP.1000117 ------>
                                <div class="article">
                                  <h4><a href="Predictive-biomarkers-for-diagnosis-of-minimal-hepatic-encephalopathy-in-patients-with-liver-cirrhosis-A-preliminary-result-in-a-single-center-study-in-Japan.php"> Predictive biomarkers for diagnosis of minimal hepatic encephalopathy in patients with liver cirrhosis: A preliminary result in a single center study in Japan </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuyuki Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hidekatsu Kuroda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kei Sawara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuichi Yoshida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Keisuke Kakisaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuji Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akiko Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mio Onodera <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takayoshi Oikawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wang Tei <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryujin Endo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akinobu Kato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasuhiro Takikawa </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000117  x------>
                                <!------BRCP.1000118------>
                                <div class="article">
                                  <h4><a href="Novel-biosynthesis-of-monogalactosyl-alkylacyl-glycerolipid-in-Mop8-fibroblast-cells-transfected-with-a-ceramide-galactosyltransferase-gene.php"> Novel biosynthesis of monogalactosyl-alkylacyl glycerolipid in Mop8 fibroblast cells transfected with a ceramide galactosyltransferase gene </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ken-ichi Nagai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nobuyoshi Takahashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yukio Niimura </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000118 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000107 ------>
                                <div class="article">
                                  <h4><a href="Cost-benefit-analysis-of-the-rubella-vaccination-in-Japan-to-prevent-congenital-rubella-syndrome-analyses-from-three-perspectives.php"> Cost-benefit analysis of the rubella vaccination in Japan to prevent congenital rubella syndrome: analyses from three perspectives </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomoya Itatani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chika Honda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuo Hayakawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kaoru Konishi </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April13, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000107  x------>
                                <!------BRCP.1000108 ------>
                                <div class="article">
                                  <h4><a href="Automated-and-rapid-system-for-detection-of-ALK-rearrangement-genes-in-non-small-cell-lung-cancer-based-on-a-Quenching-Probe-method.php"> Automated and rapid system for detection of ALK rearrangement genes in non-small cell lung cancer based on a Quenching Probe method </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marifu Yamagishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koji Tsuta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatsunori Shimoi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuko Tanabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuharu Hirai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takashi Kohno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kouya Shiraishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takashi Nakaoku <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kuniko Sunami <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kenji Tamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akinobu Hamada </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000108  x------>
                                <!------BRCP.1000109 ------>
                                <div class="article">
                                  <h4><a href="Acute-influence-of-mild-hyperbaric-oxygen-at-125-atmospheres-absolute-with-normal-air-on-mitochondrial-enzymes-and-PGC-1-mRNA-levels-in-rat-skeletal-muscle.php"> Acute influence of mild hyperbaric oxygen at 1.25 atmospheres absolute with normal air on mitochondrial enzymes and PGC-1&alpha; mRNA levels in rat skeletal muscle </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Naoto Fujita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomoka Tomioka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miharu Ono <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masataka Deie </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000109  x------>
                                <!------BRCP.1000110------>
                                <div class="article">
                                  <h4><a href="Low-grade-endotoxemia--diet-andgut-microbiota-an-emphasis-on-the-earlyevents-leading-to-dysfunction-of-the-intestinal-epithelial-barrier.php"> Low-grade endotoxemia, diet, and gut microbiota – an emphasis on the early events leading to dysfunction of the intestinal epithelial barrier </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhisa Nishizawa </p>
                                  <p class="mb5">Review Article-Biomedical Research and Clinical Practice (BRCP) </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000110 x------>
                                <!------BRCP.1000111------>
                                <div class="article">
                                  <h4><a href="Exophytic-glioblastoma-multiforme-originating-from-the-medulla-oblongata.php"> Exophytic glioblastoma multiforme originating from the medulla oblongata </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshifumi Horita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masahiko Wanibuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yukinori Akiyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kengo Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshinori Omori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoshi Iihoshi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoko Ochi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshi Mikami <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nobuhiro Mikuni </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000111 x------>
                                <!------BRCP.1000112 ------>
                                <div class="article">
                                  <h4><a href="Prolonged-induction-of-warfarin-metabolism-and-a-paradoxical-INR-response-in-a-mitral-valve-replacement-patient-receiving-rifampicin-for-infective-endocarditis.php"> Prolonged induction of warfarin metabolism and a paradoxical INR response in a mitral valve replacement patient receiving rifampicin for infective endocarditis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jessica Dawson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maneesha Dedigama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David J Elliot <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michael Sorich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arduino A Mangoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew Rowland </p>
                                  <p class="mb5">Case Report-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000112  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------BRCP.1000101------>
                                <div class="article">
                                  <h4><a href="Biomedical-research-and-clinical-practice-open-access.php"> Biomedical research and clinical practice - open access </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuhisa Nishizawa </p>
                                  <p class="mb5">Editorial-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000101 x------>
                                <!------BRCP.1000102 ------>
                                <div class="article">
                                  <h4><a href="Effectiveness-of-epidural-and-systemic-postoperative-analgesia-in-laparoscopic-urologic-surgery-A-retrospective-cohort-study.php"> Effectiveness of epidural and systemic postoperative analgesia in laparoscopic urologic surgery - A retrospective cohort study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Conchita Monsalve <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ana Bogdanovich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Laura Izquierdo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Clara Hernández-Cera <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carme Busquets <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sebastián Videla </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000102  x------>
                                <!------BRCP.1000103 ------>
                                <div class="article">
                                  <h4><a href="An-in-vitro-study-on-the-effect-of-synthesized-tinIV-complexes-on-glioblastoma-colorectal-and-skin-cancer-cell-lines.php"> An <em>in vitro </em>study on the effect of synthesized tin(IV) complexes on glioblastoma, colorectal, and skin cancer cell lines </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jennie L Williams <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lesley C Lewis-Alleyne <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Melinda Solomon <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Long Nguyen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert Johnson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jennifer Vital <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ping Ji <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John Durant <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Camille Cooper <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Patrice Cagle <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Patrick Martin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Don VanDerveer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William L Jarrett <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alvin A Holder </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000103  x------>
                                <!------BRCP.1000104------>
                                <div class="article">
                                  <h4><a href="Gene-as-a-risk-factor-for-type-2-diabetes-mellitus-and-its-related-complications.php"> Gene as a risk factor for type 2 diabetes mellitus and its related complications </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alice JayaPradha Cheekurthy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>C Rambabu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amit Kumar </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000104 x------>
                                <!------BRCP.1000105------>
                                <div class="article">
                                  <h4><a href="Development-of-a-cranium-infection-rat-model-for-artificial-bone-implantation.php"> Development of a cranium-infection rat model for artificial bone implantation </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yoshikazu Inoue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoshiaki Sakamoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hiroko Ochiai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yohko Yoshimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takayuki Okumoto </p>
                                  <p class="mb5">Research Article-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 08, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000105 x------>
                                <!------BRCP.1000106 ------>
                                <div class="article">
                                  <h4><a href="Can-robotic-based-top-down-rehabilitation-therapies-improve-motor-control-in-children-with-cerebral-palsy-A-perspective-on-the-CPWalker-project.php"> Can robotic-based top-down rehabilitation therapies improve motor control in children with cerebral palsy? A perspective on the CPWalker project </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lerma Lara S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martínez Caballero I <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bayón C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>del Castillo MD <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Serrano I <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raya R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Belda-Lois JM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martín Lorenzo T <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Moral Saiz B <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ramírez Barragán A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Parra Mussin E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Loma-Ossorio García M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pérez-Somarriba A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i><strong></strong>Rocon E </p>
                                  <p class="mb5">Perspective-Biomedical Research and Clinical Practice (BRCP)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------BRCP.1000106  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr/>
              
              <table cellspacing="0" cellpadding="0" border="0" style="border: 0px;">
                <tr>
                  <td align="center" style="border: 0px;"><a href="http://www.icmje.org/journals-following-the-icmje-recommendations/" target="_blank"><img src="img/BRCP-icmje.png"></a></td>
                  <td align="center" style="border: 0px;"><a href="http://road.issn.org/issn/2397-9631-biomedical-research-and-clinical-practice#.WRqkEmh97IW " target="_blank"><img src="img/IMM_ROAD.JPG"></a></td>
                  <td align="center" style="border: 0px;"><a href="http://journalseeker.researchbib.com/view/issn/2397-9631" target="_blank"><img src="img/IFNM-Researcch-Bib.png"></a></td>
                </tr>
                <tr>
                  <td align="left" style="border: 0px;"><a href="http://medicine.academickeys.com/jour_main.php" target="_blank"><img src="img/AcademicKeys.jpg"></a></td>
                  <td align="center" style="border: 0px;"></td>
                  <td align="center" style="border: 0px;"></td>
                </tr>
              </table>

 </div>

            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p class="text-justify">BRCP welcomes direct submissions from authors: Attach your word file with e- mail and send it to <a href="mailto:submissions@oatext.com">submissions@oatext.com</a> alternatively to <a href="mailto:editor.brcp@oatext.com">editor.brcp@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
            <div id="Special_Issues" class="content">
              <h2>Special Issues</h2>
              <hr/>
              <ul>
                <li>
                  <div>
                    <h4>"Gut Microbiota and Health – from Biology to Clinics"</h4>
                    <br>
                    <h4>Editor Affiliation:</h4>
                    <p> Kazuhisa Nishizawa<br>
                      Teikyo University<br>
                      Japan </p>
                    <p>Description of the Special Issue: Recent studies highlighted the central role of the gut microbiota in our health and diseases.  High-fat-diet as a factor for dysbiosis and for entrance of endotoxin form the gut to the circulation, worsening atherosclerosis, is now known. Prebiotics/probiotics are being attempted in many clinical settings, and characterizations of specific bacterial species for potential usefulness are underway, propelled by technical improvements in metagenomics and in animal models. This special issue aims to widely discuss gut microbiota, from basics such as immunity-microbiota interplay, to pathology of diseases, and to therapeutic frontiers. </p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h4>Gene and protein expression in cancers: small science vs systematic approaches</h4>
                    <br>
                    <h4>Editor Affiliation:</h4>
                    <p>Kazuhisa Nishizawa<br>
                      Teikyo University<br>
                      Japan</p>
                    <p>Improvements of technologies in DNA sequencing and mass spectrometry analysis propelled high-throughput studies in cancer biology. Yet, in clinics only a limited number of genes/proteins are considered as predictive markers, which have primarily been characterized from biological interests in a small science format, suggesting importance of considering strengths and weaknesses of various approaches as a source for prognostic or predictive markers for cancer treatment.&nbsp; In this special issue, studies of any types on gene/protein expression in cancers are welcome.&nbsp; We welcome cases of &nbsp;characterization of specific genes in a conventional way of biological research, but, insights derived from more systematic and integrated, omics-based approaches may be discussed as well.&nbsp; Challenges in bridging these two extremes would become the ultimate theme to be discussed.</p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h4>Effects of long-chain n-3 PUFAs: biology and clinical relevance</h4>
                    <br>
                    <h4>Editor Affiliation:</h4>
                    <p>Kazuhisa Nishizawa<br>
                      Teikyo University<br>
                      Japan</p>
                    <p>Consumption of long-chain n-3 (omega-3) polyunsaturated fatty acids (PUFAs) of marine origin, such as eicosapentaenoic acid (EPA) and docosahexaenoic acid (DHA), has beneficial effects on our health in many settings. Reflecting their multifaceted and multilayers functions, research activities related to these PUFAs are diverse. This special issue aims to consider a wide range of studies on long chain n-3 PUFAs, ranging from biological, immunological, epidemiological, and clinical settings, including autoimmune diseases, cardiovascular diseases, cancers, and metabolic syndrome. While their overall anti-inflammatory effects are clear in simpler systems, their clinical translations are often showing no or marginal effects. We welcome not only positive but also negative findings and discussions about controversial subareas.&nbsp; In particular, EPA vs DHA ratio, n-6 vs n-3 ratio, effects showing dependency on age, stage of diseases and genetic factors relevant to fine tuning of supplementation protocols may become central topics of this issue.</p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h4>Advances in pathophysiology and prevention of cancer treatment-induced side effects</h4>
                    <br>
                    <h4>Editor Affiliation:</h4>
                    <p>Cory J. Xian<br>
                      University of South Australia<br>
                      Australia</p>
                    <p>With the greater success in treating cancers, the focus has now turned to reducing the associated toxicities. Cancer treatment-induced side effects include acute tissue damages such as gastrointestinal damage (mucositis) and bone marrow damage (marrow cell depletion), and chronic side effects such as skeletal damage (bone loss, osteonecrosis, bone marrow adiposity and fractures) and metabolic disorders. These are important issues that can impact significantly on cancer treatment itself (dosage and success), patient hospitalisation and quality of life for patients and survivors. Due to the commonality of the problems (with increasing numbers of cancer patients and/or survivors suffering from these issues) and lack of effective treatments, further work is required to understand the pathophysiology of cancer treatment-induced side effects and to develop therapies for their prevention and/or treatment. The purpose of this special issue is to report advances in clinical  and preclinical (molecular/cellular research/animal model) studies into pathophysiology of cancer treatment-induced tissue damage and repair, new treatment strategies for preventing or treating these acute or chronic side effects. Original research or review articles are invited for this special issue.</p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
