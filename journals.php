<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>OAText-Journals</title>
<meta name="description" content="OA Text has grown from a small group of like-minded individuals into an open movement with clear Mission and Vision to publish high quality research content." />
<meta name="keywords" content="Scientific Publishing, Open access, oat" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <h1>Improved knowledge creation and sharing to bring about a change in modern scholarly communications</h1>
    </div>
  </div>
</div>                
<!--banner x -->
<!--CONTENT -->
<div class="-inner-page">
  <div class="row">
    <div class="large-12 columns">
      <main id="top">
        <section class="filter-j">
          <h2> Journals by Subject </h2>
          <ul class="j-list">
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#all">All Journals</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Biomedicine">Biomedicine</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Chemistry">Chemistry</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Dentistry">Dentistry</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Life Sciences">Life Sciences</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Materials Science">Materials Science</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Medicine">Medicine</a></li>
                        <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Neurology">Neurology</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Pharmacy">Pharmacy</a></li>
            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#Radiology">Radiology</a></li>

          </ul>
          <div class="j-list-items"  id="all">
            <div class="j-main-title" data-scroll ><span>All Journals</span>
              <p id="1@#%^-bottom" class="hide"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Journals A - Z</h1>
            <ul>
              <li><a href="IntegrativeMolecularMedicine.php">Integrative Molecular Medicine (IMM)</a></li>
              <li><a href="Global-Dermatology-GOD.php">Global Dermatology (GOD)</a></li>
              <li><a href="Integrative-Cancer-Science-and-Therapeutics-ICST.php">Integrative Cancer Science and Therapeutics (ICST)</a></li>
              <li><a href="Journal-of-Integrative-Cardiology-JIC.php">Journal of Integrative Cardiology (JIC)</a></li>
              <li><a href="Integrative-Pharmacology-Toxicology-and-Genotoxicology-IPTG.php">Integrative Pharmacology, Toxicology and Genotoxicology (IPTG)</a></li>
              <li><a href="Integrative-Food-Nutrition-and-Metabolism-IFNM.php">Integrative Food, Nutrition and Metabolism (IFNM)</a></li>
              <li><a href="Dental-Oral-and-Craniofacial-Research-DOCR.php">Dental, Oral and Craniofacial Research (DOCR)</a></li>
              <li><a href="Clinical-Research-and-Trials-CRT.php">Clinical Research and Trials (CRT)</a></li>
              <li><a href="Integrative-Obesity-and-Diabetes-IOD.php">Integrative Obesity and Diabetes (IOD)</a></li>
              <li><a href="Global-Anesthesia-and-Perioperative-Medicine-GAPM.php">Global Anesthesia and Perioperative Medicine (GAPM)</a></li>
              <li><a href="Contemporary-Behavioral-Health-Care-CBHC.php">Contemporary Behavioral Health Care (CBHC)</a></li>
              <li><a href="Fractal-Geometry-and-Nonlinear-Analysis-in-Medicine-and-Biology-FGNAMB.php">Fractal Geometry and Nonlinear Analysis in Medicine and Biology (FGNAMB)</a></li>
              <li><a href="clinical-case-reports-and-reviews-ccrr.php">Clinical Case Reports and Reviews (CCRR)</a></li>
              <li><a href="Clinical-Obstetrics-Gynecology-and-Reproductive-Medicine-cogrm.php">Clinical Obstetrics, Gynecology and Reproductive Medicine (COGRM)</a></li>
              <li><a href="Journal-of-Translational-Science-JTS.php">Journal of Translational Science (JTS)</a></li>
              <li><a href="New-Frontiers-in-Ophthalmology-nfo.php">New Frontiers in Ophthalmology (NFO)</a></li>
              <li><a href="Clinical-Proteomics-and-Bioinformatics-CPB.php">Clinical Proteomics and Bioinformatics (CPB)</a></li>
              <li><a href="Journal-of-Systems-and-Integrative-Neuroscience-jsin.php">Journal of Systems and Integrative Neuroscience (JSIN)</a></li>
              <li><a href="Clinical-and-Diagnostic-Pathology-CDP.php">Clinical and Diagnostic Pathology (CDP)</a></li>
              <li><a href="Integrative-Endocrinology-and-Metabolism-IEM.php">Integrative Endocrinology and Metabolism (IEM)</a></li>
              <li><a href="Vascular-Diseases-and-Therapeutics-VDT.php">Vascular Diseases and Therapeutics (VDT)</a></li>
              <li><a href="Global-Drugs-and-Therapeutics-GDT.php">Global Drugs and Therapeutics (GDT)</a></li>
              <li><a href="Oral-Health-and-Care-OHC.php">Oral Health and Care (OHC)</a></li>
              <li><a href="General-Internal-Medicine-and-Clinical-Innovations-GIMCI.php">General Internal Medicine and Clinical Innovations (GIMCI)</a></li>
              <li><a href="Radiology-and-Diagnostic-Imaging-RDI.php">Radiology and Diagnostic Imaging (RDI)</a></li>
              <li><a href="Journal-of-Psychology-and-Psychiatry-JPP.php">Journal of Psychology and Psychiatry (JPP)</a></li>
              <li><a href="Brain-and-Nerves-JBN.php">Brain and Nerves (JBN)</a></li>
              <li><a href="Cardiothoracic-and-Vascular-Sciences-CVS.php">Cardiothoracic and Vascular Sciences (CVS)</a></li>
              <li><a href="Frontiers-of-Nanoscience-and-Nanotechnology-FNN.php">Frontiers in Nanoscience and Nanotechnology (FNN)</a></li>
              <li><a href="Global-Vaccines-and-Immunology-GVI.php">Global Vaccines and Immunology (GVI)</a></li>
              <li><a href="Pediatric-Dimensions-PD.php">Pediatric Dimensions (PD)</a></li>
              <li><a href="Pulmonary-and-Critical-Care-Medicine-PCCM.php">Pulmonary and Critical Care Medicine (PCCM)</a></li>
              <li><a href="Translational-Brain-Rhythmicity-TBR.php">Translational Brain Rhythmicity (TBR)</a></li>
              <li><a href="Biomedical-Research-and-Clinical-Practice-BRCP.php">Biomedical Research and Clinical Practice (BRCP)</a></li>
              <li><a href="Biomaterials-and-Tissue-Technology-BTT.php">Biomaterials and Tissue Technology (BTT)</a></li>
              <li><a href="Trauma-and-Emergency-Care-TEC.php">Trauma and Emergency Care (TEC)</a></li>
              <li><a href="Nursing-and-Palliative-Care-NPC.php">Nursing and Palliative Care (NPC)</a></li>
              <li><a href="Global-Surgery-gos.php">Global Surgery (GOS)</a></li>
              <li><a href="Frontiers-in-Womens-Health-FWH.php">Frontiers in Women&rsquo;s Health (FWH)</a></li>
              <li><a href="Advanced-Material-Science-AMS.php">Advanced Material Science (AMS)</a></li>
              <li><a href="Clinical-Microbiology-and-Infectious-Diseases-CMID.php">Clinical Microbiology and Infectious Diseases (CMID)</a></li>
              <li><a href="Interdisciplinary-Journal-of-Chemistry-IJC.php">Interdisciplinary Journal of Chemistry (IJC)</a></li>
              <li><a href="Mental-Health-and-Addiction-Research-MHAR.php">Mental Health and Addiction Research (MHAR)</a></li>
              <li><a href="Biomedical-Genetics-and-Genomics-BGG.php">Biomedical Genetics and Genomics (BGG)</a></li>
              <li><a href="Global-Imaging-Insights-GII.php">Global Imaging Insights (GII)</a></li>
              <li><a href="Medical-Devices-and-Diagnostic-Engineering-MDDE.php">Medical Devices and Diagnostic Engineering (MDDE)</a></li>
              <li><a href="Biology-Engineering-and-Medicine-BEM.php">Biology, Engineering and Medicine (BEM)</a></li>
              <li><a href="Liver-and-Pancreatic-Sciences-LPS.php">Liver and Pancreatic Sciences (LPS)</a></li>
              <li><a href="Histology-Cytology-and-Embryology-HCE.php">Histology, Cytology and Embryology (HCE)</a></li>
              <li><a href="Forensic-Science-and-Criminology-FSC.php">Forensic Science and Criminology (FSC)</a></li>
              <li><a href="Journal-of-Medicine-and-Therapeutics-JMT.php">Journal of Medicine and Therapeutics (JMT)</a></li>
              <li><a href="Otorhinolaryngology-Head-and-Neck-Surgery-OHNS.php">Otorhinolaryngology Head and Neck Surgery (OHNS)</a></li>
              <li><a href="Nuclear-Medicine-and-Biomedical-Imaging-NMBI.php">Nuclear Medicine and Biomedical Imaging (NMBI)</a></li>
              <li><a href="Journal-of-Stem-Cell-Research-and-Medicine-JSCRM.php">Journal of Stem Cell Research and Medicine (JSCRM)</a></li>
              <li><a href="Surgery-and-Rehabilitation-SRJ.php">Surgery and Rehabilitation (SRJ)</a></li>
              <li><a href="Eye-Care-and-Vision-ECV.php">Eye Care and Vision (ECV)</a></li>
              <li><a href="Lungs-and-Breathing-LBJ.php">Lungs and Breathing (LBJ)</a></li>
              <li><a href="Internal-Medicine-and-Care-IMC.php">Internal Medicine and Care (IMC)</a></li>
              <li><a href="Physical-Medicine-and-Rehabilitation-Research-PMRR.php">Physical Medicine and Rehabilitation Research (PMRR)</a></li>
              <li><a href="Gastroenterology-Hepatology-and-Endoscopy-GHE.php">Gastroenterology, Hepatology and Endoscopy (GHE)</a></li>
              <li><a href="Clinical-and-Medical-Investigations-CMI.php">Clinical and Medical Investigations (CMI)</a></li>
              <li><a href="Health-Education-and-Care-HEC.php">Health Education and Care (HEC)</a></li>
              <li><a href="Transplantation-Open-TO.php">Transplantation-Open (TO)</a></li>
              <li><a href="Hematology-Medical-Oncology-HMO.php">Hematology & Medical Oncology (HMO)</a></li>
              <li><a href="Cancer-Reports-and-Reviews-CRR.php">Cancer Reports and Reviews (CRR)</a></li>
              <li><a href="Biomarkers-Genes-BG.php">Biomarkers and Genes (BG)</a></li>
              <li><a href="Cardiovascular-Disorders-and-Medicine-CDM.php">Cardiovascular Disorders and Medicine (CDM)</a></li>
              <li><a href="Molecular-Biophysics-and-Biochemistry-MBB.php">Molecular Biophysics and Biochemistry (MBB)</a></li>
              <li><a href="Rheumatology-and-Orthopedic-Medicine-ROM.php">Rheumatology and Orthopedic Medicine (ROM)</a></li>
              <li><a href="Virology-Research-Reviews-VRR.php">Virology: Research and Reviews (VRR)</a></li>
              <li><a href="Neurological-Disorders-and-Therapeutics-NDT.php">Neurological Disorders and Therapeutics (NDT)</a></li>
              <li><a href="Journal-of-Spine-Care-JSC.php">Journal of Spine Care (JSC)</a></li>
              <li><a href="Nephrology-and-Renal-Diseases-NRD.php">Nephrology and Renal Diseases (NRD)</a></li>
              <li><a href="Alzheimers-Dementia-and-Cognitive-Neurology-ADCN.php">Alzheimer's, Dementia and Cognitive Neurology (ADCN)</a></li>
              <li><a href="Pharmacology-Drug-Development-Therapeutics-PDDT.php">Pharmacology, Drug Development and Therapeutics (PDDT)</a></li>
              <li><a href="Fetal-Neonatal-and-Developmental-Medicine-FNDM.php">Fetal, Neonatal and Developmental Medicine (FNDM)</a></li>
              <li><a href="Hypertension-Current-Concepts-and-Therapeutics-HCCT.php">Hypertension Current Concepts and Therapeutics (HCCT)</a></li>
              <li><a href="Animal-Husbandry-Dairy-and-Veterinary-Science-AHDVS.php">Animal Husbandry, Dairy and Veterinary Science (AHDVS)</a></li>
              <li><a href="Integrative-Clinical-Medicine-ICM.php">Integrative Clinical Medicine (ICM)</a></li>
              <li><a href="Medical-Research-and-Innovations-MRI.php">Medical Research and Innovations (MRI)</a></li>
              <li><a href="Journal-of-Allergy-and-Immunology-JAI.php">Journal of Allergy and Immunology (JAI)</a></li>
              <li><a href="Digestive-System-DSJ.php">Digestive System (DSJ)</a></li>
              <li><a href="Blood-Heart-and-Circulation-BHC.php">Blood, Heart and Circulation (BHC) </a></li>
              <li><a href="Sexual-Health-Issues-SHI.php">Sexual Health Issues (SHI)</a></li>
              <li><a href="Global-Medicine-and-Therapeutics-GMT.php">Global Medicine and Therapeutics (GMT)</a></li>
              <li><a href="Medical-and-Clinical-Archives-MCA.php">Medical and Clinical Archives (MCA)</a></li>
              <li><a href="Research-and-Review-Insights-RRI.php">Research and Review Insights (RRI)</a></li>
              <li><a href="Birth-Defects-BDJ.php">Birth Defects (BDJ)</a></li>
              <li><a href="Biomedical-Research-and-Reviews-BRR.php">Biomedical Research and Reviews (BRR)</a></li>
              <li><a href="Geriatric-Medicine-and-Care-GMC.php">Geriatric Medicine and Care (GMC) </a></li>
              <li><a href="Obstetrics-and-Gynecology-Reports-OGR.php">Obstetrics and Gynecology Reports (OGR)</a></li>
              <li><a href="Bones-Joints-and-Muscles-BJM.php">Bones, Joints and Muscles (BJM) </a></li>
              <li><a href="Symptoms.php">Symptoms</a></li>
              <li><a href="Health-and-Primary-Care-HPC.php">Health and Primary Care (HPC)</a></li>
              <li><a href="Journal-of-Pregnancy-and-Reproduction-JPR.php">Journal of Pregnancy and Reproduction (JPR)</a></li>
              <li><a href="General-Medicine-Open-GMO.php">General Medicine-Open (GMO)</a></li>
              
              
            </ul>
          </div>
          <div class="j-list-items"  id="Biomedicine">
            <div class="j-main-title" data-scroll ><span>Biomedicine</span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Biomedicine</h1>
            <ul>
              <li><a href="Biomedical-Research-and-Clinical-Practice-BRCP.php">Biomedical Research and Clinical Practice (BRCP)</a></li>
              <li><a href="IntegrativeMolecularMedicine.php">Integrative Molecular Medicine (IMM)</a></li>
              <li><a href="Clinical-Research-and-Trials-CRT.php">Clinical Research and Trials (CRT)</a></li>
              <li><a href="Fractal-Geometry-and-Nonlinear-Analysis-in-Medicine-and-Biology-FGNAMB.php">Fractal Geometry and Nonlinear Analysis in Medicine and Biology (FGNAMB)</a></li>
              <li><a href="Journal-of-Translational-Science-JTS.php">Journal of Translational Science (JTS)</a></li>
              <li><a href="Endocrinology-Metabolism-and-Genetics-EMG.php">Endocrinology, Metabolism and Genetics (EMG)</a></li>
              <li><a href="Biomedical-Genetics-and-Genomics-BGG.php">Biomedical Genetics and Genomics (BGG)</a></li>
              <li><a href="Biology-Engineering-and-Medicine-BEM.php">Biology, Engineering and Medicine (BEM)</a></li>
              <li><a href="Histology-Cytology-and-Embryology-HCE.php">Histology, Cytology and Embryology (HCE)</a></li>
              <li><a href="Journal-of-Medicine-and-Therapeutics-JMT.php">Journal of Medicine and Therapeutics (JMT)</a></li>
              <li><a href="Biomarkers-Genes-BG.php">Biomarkers and Genes (BG)</a></li>
              <li><a href="Medical-Devices-and-Diagnostic-Engineering-MDDE.php">Medical Devices and Diagnostic Engineering (MDDE)</a></li>
              <li><a href="Medical-Research-and-Innovations-MRI.php">Medical Research and Innovations (MRI)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Chemistry">
            <div class="j-main-title" data-scroll ><span>Chemistry</span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Chemistry</h1>
            <ul>
              <li><a href="Interdisciplinary-Journal-of-Chemistry-IJC.php">Interdisciplinary Journal of Chemistry (IJC)</a></li>
              <li><a href="Frontiers-of-Nanoscience-and-Nanotechnology-FNN.php">Frontiers in Nanoscience and Nanotechnology (FNN)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Dentistry">
            <div class="j-main-title"  data-scroll ><span>Dentistry</span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Dentistry</h1>
            <ul>
              <li><a href="Dental-Oral-and-Craniofacial-Research-DOCR.php">Dental, Oral and Craniofacial Research (DOCR)</a></li>
              <li><a href="Oral-Health-and-Care-OHC.php">Oral Health and Care (OHC)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Life Sciences">
            <div class="j-main-title" data-scroll ><span>Life Sciences</span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Life Sciences</h1>
            <ul>
              <li><a href="Integrative-Food-Nutrition-and-Metabolism-IFNM.php">Integrative Food, Nutrition and Metabolism (IFNM)</a></li>
              <li><a href="Clinical-Proteomics-and-Bioinformatics-CPB.php">Clinical Proteomics and Bioinformatics (CPB)</a></li>
              <li><a href="Journal-of-Stem-Cell-Research-and-Medicine-JSCRM.php">Journal of Stem Cell Research and Medicine (JSCRM)</a></li>
              <li><a href="Molecular-Biophysics-and-Biochemistry-MBB.php">Molecular Biophysics and Biochemistry (MBB)</a></li>
              <li><a href="Animal-Husbandry-Dairy-and-Veterinary-Science-AHDVS.php">Animal Husbandry, Dairy and Veterinary Science (AHDVS)</a></li>
              <li><a href="Clinical-Microbiology-and-Infectious-Diseases-CMID.php">Clinical Microbiology and Infectious Diseases (CMID)</a></li>
              <li><a href="Virology-Research-Reviews-VRR.php">Virology: Research and Reviews (VRR)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Materials Science">
            <div class="j-main-title"  data-scroll ><span>Materials Science</span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Materials Science</h1>
            <ul>
              <li><a href="Advanced-Material-Science-AMS.php">Advanced Material Science (AMS)</a></li>
              <li><a href="Biomaterials-and-Tissue-Technology-BTT.php">Biomaterials and Tissue Technology (BTT)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Medicine">
            <div class="j-main-title" data-scroll ><span>Medicine </span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Medicine </h1>
            <ul>
              <li><a href="Global-Dermatology-GOD.php">Global Dermatology (GOD)</a></li>
              <li><a href="Integrative-Cancer-Science-and-Therapeutics-ICST.php">Integrative Cancer Science and Therapeutics (ICST)</a></li>
              <li><a href="Journal-of-Integrative-Cardiology-JIC.php">Journal of Integrative Cardiology (JIC)</a></li>
              <li><a href="Global-Dermatology-GOD.php">Global Dermatology (GOD)</a></li>
              <li><a href="Global-Anesthesia-and-Perioperative-Medicine-GAPM.php">Global Anesthesia and Perioperative Medicine (GAPM)</a></li>
              <li><a href="clinical-case-reports-and-reviews-ccrr.php">Clinical Case Reports and Reviews (CCRR)</a></li>
              <li><a href="Clinical-Obstetrics-Gynecology-and-Reproductive-Medicine-cogrm.php">Clinical Obstetrics, Gynecology and Reproductive Medicine (COGRM)</a></li>
              <li><a href="New-Frontiers-in-Ophthalmology-nfo.php">New Frontiers in Ophthalmology (NFO)</a></li>
              <li><a href="Clinical-and-Diagnostic-Pathology-CDP.php">Clinical and Diagnostic Pathology (CDP)</a></li>
              <li><a href="Vascular-Diseases-and-Therapeutics-VDT.php">Vascular Diseases and Therapeutics (VDT)</a></li>
              <li><a href="General-Internal-Medicine-and-Clinical-Innovations-GIMCI.php">General Internal Medicine and Clinical Innovations (GIMCI)</a></li>
              <li><a href="Global-Vaccines-and-Immunology-GVI.php">Global Vaccines and Immunology (GVI)</a></li>
              <li><a href="Pediatric-Dimensions-PD.php">Pediatric Dimensions (PD)</a></li>
              <li><a href="Pulmonary-and-Critical-Care-Medicine-PCCM.php">Pulmonary and Critical Care Medicine (PCCM)</a></li>
              <li><a href="Trauma-and-Emergency-Care-TEC.php">Trauma and Emergency Care (TEC)</a></li>
              <li><a href="Nursing-and-Palliative-Care-NPC.php">Nursing and Palliative Care (NPC)</a></li>
              <li><a href="Global-Surgery-gos.php">Global Surgery (GOS)</a></li>
              <li><a href="Frontiers-in-Womens-Health-FWH.php">Frontiers in Women&rsquo;s Health (FWH)</a></li>
              <li><a href="Global-Imaging-Insights-GII.php">Global Imaging Insights (GII)</a></li>
              <li><a href="Liver-and-Pancreatic-Sciences-LPS.php">Liver and Pancreatic Sciences (LPS)</a></li>
              <li><a href="Otorhinolaryngology-Head-and-Neck-Surgery-OHNS.php">Otorhinolaryngology Head and Neck Surgery (OHNS)</a></li>
              <li><a href="Gastroenterology-Hepatology-and-Endoscopy-GHE.php">Gastroenterology, Hepatology and Endoscopy (GHE)</a></li>
              <li><a href="Clinical-and-Medical-Investigations-CMI.php">Clinical and Medical Investigations (CMI)</a></li>
              <li><a href="Health-Education-and-Care-HEC.php">Health Education and Care (HEC)</a></li>
              <li><a href="Transplantation-Open-TO.php">Transplantation-Open (TO)</a></li>
              <li><a href="Hematology-Medical-Oncology-HMO.php">Hematology & Medical Oncology (HMO)</a></li>
              <li><a href="Cancer-Reports-and-Reviews-CRR.php">Cancer Reports and Reviews (CRR)</a></li>
              <li><a href="Cardiovascular-Disorders-and-Medicine-CDM.php">Cardiovascular Disorders and Medicine (CDM)</a></li>
              <li><a href="Rheumatology-and-Orthopedic-Medicine-ROM.php">Rheumatology and Orthopedic Medicine (ROM)</a></li>
              <li><a href="Nephrology-and-Renal-Diseases-NRD.php">Nephrology and Renal Diseases (NRD)</a></li>
              <li><a href="Fetal-Neonatal-and-Developmental-Medicine-FNDM.php">Fetal, Neonatal and Developmental Medicine (FNDM)</a></li>
              <li><a href="Hypertension-Current-Concepts-and-Therapeutics-HCCT.php">Hypertension Current Concepts and Therapeutics (HCCT)</a></li>
              <li><a href="Forensic-Science-and-Criminology-FSC.php">Forensic Science and Criminology (FSC)</a></li>
              <li><a href="Sexual-Health-Issues-SHI.php">Sexual Health Issues (SHI)</a></li>
              <li><a href="Digestive-System-DSJ.php">Digestive System (DSJ)</a></li>
              <li><a href="Medical-and-Clinical-Archives-MCA.php">Medical and Clinical Archives (MCA)</a></li>
              <li><a href="Research-and-Review-Insights-RRI.php">Research and Review Insights (RRI)</a></li>
              <li><a href="Journal-of-Allergy-and-Immunology-JAI.php">Journal of Allergy and Immunology (JAI)</a></li>
              <li><a href="Cardiothoracic-and-Vascular-Sciences-CVS.php">Cardiothoracic and Vascular Sciences (CVS)</a></li>
              <li><a href="Lungs-and-Breathing-LBJ.php">Lungs and Breathing (LBJ)</a></li>
              <li><a href="Eye-Care-and-Vision-ECV.php">Eye Care and Vision (ECV)</a></li>
              <li><a href="Surgery-and-Rehabilitation-SRJ.php">Surgery and Rehabilitation (SRJ)</a></li>
              <li><a href="Internal-Medicine-and-Care-IMC.php">Internal Medicine and Care (IMC)</a></li>
              <li><a href="Blood-Heart-and-Circulation-BHC.php">Blood, Heart and Circulation (BHC) </a></li>
              <li><a href="Integrative-Clinical-Medicine-ICM.php">Integrative Clinical Medicine (ICM)</a></li>
              <li><a href="Global-Medicine-and-Therapeutics-GMT.php">Global Medicine and Therapeutics (GMT)</a></li>
              <li><a href="Physical-Medicine-and-Rehabilitation-Research-PMRR.php">Physical Medicine and Rehabilitation Research (PMRR)</a></li>
              <li><a href="Integrative-Obesity-and-Diabetes-IOD.php">Integrative Obesity and Diabetes (IOD)</a></li>
              <li><a href="General-Medicine-Open-GMO.php">General Medicine-Open (GMO)</a></li>
              
            </ul>
          </div>
          <div class="j-list-items"  id="Pharmacy">
            <div class="j-main-title" data-scroll ><span>Neurology </span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Neurology </h1>
            <ul>
              <li><a href="Alzheimers-Dementia-and-Cognitive-Neurology-ADCN.php">Alzheimer's, Dementia and Cognitive Neurology (ADCN)</a></li>
              <li><a href="Journal-of-Systems-and-Integrative-Neuroscience-jsin.php">Journal of Systems and Integrative Neuroscience (JSIN)</a></li>
              <li><a href="Neurological-Disorders-and-Therapeutics-NDT.php">Neurological Disorders and Therapeutics (NDT)</a></li>
              <li><a href="Journal-of-Spine-Care-JSC.php">Journal of Spine Care (JSC)</a></li>
              <li><a href="Contemporary-Behavioral-Health-Care-CBHC.php">Contemporary Behavioral Health Care (CBHC)</a></li>
              <li><a href="Journal-of-Psychology-and-Psychiatry-JPP.php">Journal of Psychology and Psychiatry (JPP)</a></li>
              <li><a href="Translational-Brain-Rhythmicity-TBR.php">Translational Brain Rhythmicity (TBR)</a></li>
              <li><a href="Mental-Health-and-Addiction-Research-MHAR.php">Mental Health and Addiction Research (MHAR)</a></li>
              <li><a href="Brain-and-Nerves-JBN.php">Brain and Nerves (JBN)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Radiology">
            <div class="j-main-title"  data-scroll ><span>Pharmacy </span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top &uarr;</a></p>
            </div>
            <h1>Pharmacy </h1>
            <ul>
              <li><a href="Pharmacology-Drug-Development-Therapeutics-PDDT.php">Pharmacology, Drug Development and Therapeutics (PDDT)</a></li>
              <li><a href="Integrative-Pharmacology-Toxicology-and-Genotoxicology-IPTG.php">Integrative Pharmacology, Toxicology and Genotoxicology (IPTG)</a></li>
              <li><a href="Global-Drugs-and-Therapeutics-GDT.php">Global Drugs and Therapeutics (GDT)</a></li>
            </ul>
          </div>
          <div class="j-list-items"  id="Neurology">
            <div class="j-main-title"  data-scroll ><span>Radiology </span>
              <p id="1@#%^-bottom"><a data-scroll data-options='{ "easing": "easeOutCubic" }' href="#">Back to the top</a></p>
            </div>
            <h1>Radiology </h1>
            <ul>
              <li><a href="Nuclear-Medicine-and-Biomedical-Imaging-NMBI.php">Nuclear Medicine and Biomedical Imaging (NMBI)</a></li>
              <li><a href="Radiology-and-Diagnostic-Imaging-RDI.php">Radiology and Diagnostic Imaging (RDI)</a></li>
            </ul>
          </div>
        </section>
      </main>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script src="js/smooth-scroll.js"></script>
<script>
smoothScroll.init({
    speed: 500,
    easing: 'easeInOutCubic',
    offset: 40,
    updateURL: false,
    callbackBefore: function ( toggle, anchor ) {
    },
    callbackAfter: function ( toggle, anchor ) {
        $(toggle).addClass('active');
    }
});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
	  
	  
	   var seen = {};
$('ul li').each(function() {
    var txt = $(this).text();
    if (seen[txt])
        $(this).remove();
    else
        seen[txt] = true;
});
    return false;
}
);
	  
    </script>
</body>
</html>
