<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Medical and Clinical Archives (MCA)</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Medical and Clinical Archives (MCA)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Medical and Clinical Archives (MCA)</h2>
                <!-- <h4>Online ISSN: 2059-0393</h4>  -->
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!-- <span class="black-text"><i class="fa fa-university"></i>    University of Lincoln<br><br></span> -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
              
             
                 
                 

<p><a href="img/covers-MCA.jpg" target="_blank"><img src="img/covers-MCA.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a>Medical and Clinical Archives (MCA) is a bimonthly, open access, peer-reviewed journal which considers manuscripts on all aspects of Medicine and Clinical sciences.</p>

<p>MCA operate a system of rapid review process and publication which aims to increase the visibility to provide freely accessible research information worldwide. Manuscripts may take the form of original empirical research, critical reviews of the literature, brief commentaries, meeting reports, case reports, innovations in clinical practice, letters and drug clinical trial studies.</p>

<p>MCA welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.mca@oatext.com">editor.mca@oatext.com</a></p>

<p>Please, follow the&nbsp;<a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>

<p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by MCA, is properly cited.</p>



               
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Editor in chief</h2>
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Antonio Siniscalchi</h4>
                  <p>Department of Neurology<br>
                      Annunziata Hospital<br>
                      Cosenza<br>
                      Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Chaohui Yu</h4>
                  <p>Department of Gastroenterology<br>
                    The First Affiliated Hospital<br>
                    College of Medicine<br>
                    Zhejiang University, Hangzhou<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Iyer Krishna Mohan</h4>
                  <p>Senior Consultant Orthopaedic Surgeon<br>
                      Bangalore<br>
                      India</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Haris Kokotas</h4>
                  <p>Department of Genetics<br>
                    Institute of Child Health<br>
                    Athens, Greece</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dmitry Lvovich Aminin</h4>
                  <p>Head of the Laboratory of Bioassay and Mechanism of action of biologically active compounds<br>
                    G.B. Elyakov Pacific Institute of Bioorganic Chemistry<br>
                    Russian Academy of Sciences<br>
                    Russia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ying-Fu Chen</h4>
                  <p>Professor of Surgery and Graduate Institute of Medicine<br>
                      Kaohsiung Medical University<br>
                      Taiwan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Mohamed Abdel-Wanis</h4>
                  <p>Professor of Orthopedic Surgery<br>
                    Sohag Faculty of Medicine<br>
                    Sohag University, Egypt</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hala Mohamed Mohamed Bayoumy</h4>
                  <p>King Saud Bin Abdulaziz University for Health Sciences<br>
                    KSA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Gaurav Malviya</h4>
                  <p>Head<br>
                    Nuclear Imaging at Cancer Research UK Beatson Institute<br>
                    United Kingdom</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Minho Moon</h4>
                  <p>Department of Biochemistry at College of Medicine<br>
                      Konyang University<br>
                      Korea</p>
                </div>
                <div class="medium-4 columns">
                	<h4>Florin Burada</h4>
                	<p>General Medicine<br>
          					University of Medicine and Pharmacy of Craiova<br>
          					Romania</p>
                </div>
                <div class="medium-4 columns">
                	<h4>Amol S. Katkar</h4>
                	<p>Department of Abdominal Imaging & Intervention (Radiology)<br>
        					University of Texas Health Science Center<br>
        					USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Giuseppe Lanza</h4>
                  <p>Consultant Neurologist and Clinical Researcher<br>
                    Department of Neurology<br>
                    Institute for Research on Mental Retardation and Brain Aging<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ioannis P Galanakis</h4>
                  <p>Consultant Urologist<br> 
                  Urologic Department of Athens Naval Hospital<br>
                  Athens, Greece</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Heba Helmy Abdel Latif Abou El Naga</h4>
                  <p>Chest Diseases Specialist<br>
                    Faculty of Medicine 6th October University<br>
                    Egypt</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Aramide Folayemi Faponle</h4>
                  <p>Professor of Anaesthesia/ Consultant Anaesthetist<br>
                    Obafemi Awolowo University<br>
                    Nigeria</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Andrzej Chciałowski</h4>
                  <p>Military Institute of Medicine<br>
                    Warsaw<br> Poland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Miguel Gonçalves Meira e Cruz</h4>
                  <p>Portuguese Association of Chronobiology and Sleep Medicine<br>
                      Autonomic Function Group, Cardiovascular Center of Lisbon University<br>
                      Portugal</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Xia Wang</h4>
                  <p>The University of Sydney<br>
                    Institute of Bone and Joint Research<br>
                    Royal North Shore Hospital<br>
                    Australia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pengcheng Liu</h4>
                  <p>Shanghai Tenth People's Hospital<br>
                     Tongji University<br>
                     China</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>

              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>





            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <!--<h4>Volume 1, Issue 6</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <!--<h4>Volume 1, Issue 5</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <!--<h4>Volume 1, Issue 4</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <p>Will be updated soon.</p>
             <!-- <div class="lee">
            <dl data-accordion="" class="accordion" >
              <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
              <div class="content" id="panel1b">
                  <div class="hr-tabs">
                  <ul class="tabs" data-tab>
                      <li class="tab-title active"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                    <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                  </ul>
                  <div class="tabs-content">
                    <div class="content  " id="panela">
                      <div class="row pt10">
                      <div class="small-12 columns">
                    AAA
                        </div>
                        </div>
                        </div>

          <div class="content" id="panelb">B</div>

            <div class="content" id="panelc">C</div> 

        </div>

          </div>
          
        </dd>
      </dl>
    </div> -->
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              
              <p>MCA welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.mca@oatext.com">editor.mca@oatext.com</a></p>



            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
