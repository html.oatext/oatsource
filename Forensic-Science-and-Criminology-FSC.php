<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Forensic Science and Criminology (FSC)</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Forensic Science and Criminology (FSC)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Forensic Science and Criminology (FSC)</h2>
                <h4>Online ISSN: 2513-8677</h4>
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!-- <span class="black-text"><i class="fa fa-university"></i>    University of Lincoln<br><br></span> -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
                <p><a href="img/covers-FSC.jpg" target="_blank"><img src="img/covers-FSC.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Forensic Science and Criminology (FSC) is a bimonthly, open access, peer-reviewed journal which considers manuscripts in the field of forensic research, evidence analysis and criminal investigation.</p>
                <p>Manuscripts may take the form of original empirical research, critical reviews of the literature, brief commentaries, meeting reports, short communications and technical notes.</p>
                <p>FSC welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.fsc@oatext.com">editor.fsc@oatext.com</a></p>
                <p>Please, follow the&nbsp;<a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by FSC, is properly cited.</p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Takaki Ishikawa</h2>
              <hr/>
              <p><img src="img/Takaki-Ishikawa.jpg" align="right" class="profile-pic"> Takaki Ishikawa, M.D.,Ph.D. is Chief Professor at Department of Legal Medicine, Osaka City University Medical School, Osaka, Japan. He was Associate Professor at department of Legal Medicine, Osaka City University Medical School in 2012, and a guest researcher at Institute of Legal Medicine, University Hospital Freiburg, Germany in 2009. He works as an expert in forensic pathology and toxicology with primary specialties in clinical forensic pathology and environmental injury. His current researches focus on forensic endocrinology and forensic pathological evaluation of various stress responses.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Janina Zięba-Palus</h4>
                  <p>Professor of Forensic chemistry<br>
                    Institute of Forensic Research<br>
                    Kraków<br>
                    Poland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Andrea Porzionato</h4>
                  <p>Executive Editor<br>
                    Associate Professor<br>
                    Department of Neuroscience<br>
                    University of Padua<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vincent Stefan</h4>
                  <p>Department of Anthropology<br>
                    Herbert H. Lehman College - CUNY<br>
                    USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Kenneth F. Cohrn</h4>
                  <p>Chief Forensic Odontologist<br>
                    District 9 Medical examiner’s Office Orlando, USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Arleen C. Gonzalez, J.D</h4>
                  <p>Associate Professor of Criminal Justice<br>
                    School of Social and Behavioral Sciences<br>
                    Stockton University, USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Roger Metcalf</h4>
                  <p>Forensic dentist<br>
                    Tarrant County Medical Examiner's District, USA</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Anand Mugadlimath</h4>
                  <p>Associate Professor<br>
                    Department of Forensic Medicine & Toxicology<br>
                    S. Nijalingappa Medical College<br>
                    India</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Philippe Charlier</h4>
                  <p>Assistant professor<br>
                    Paris-Descartes University<br>
                    France</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hongliang Zhang</h4>
                  <p>Associate Professor<br>
                    National Natural Science Foundation of China</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Christopher M. Engle-Tjaden</h4>
                  <p>Adjunct Professor<br>
                    Forensic Photography<br>
                    Marian University<br>
                    Fond du Lac, WI<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ambarkova Vesna</h4>
                  <p>Faculty of Dental Medicine<br>
                    Vodnjanska 17 University<br>
                    Macedonia</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 2, Issue 2</h4>
              <hr/>
              <!------FSC.1000112 ------>
              <div class="article">
                <h4><a href="The-usefulness-of-infrared-spectroscopy-in-examinations-of-adhesive-tapes-for-forensic-purposes.php"> The usefulness of infrared spectroscopy in examinations of adhesive tapes for forensic purposes </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Janina Zięba-Palus </p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 07, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000112  x------>
              <!------FSC.1000113  ------>
              <div class="article">
                <h4><a href="Forensic-examinations-of-micro-traces.php"> Forensic examinations of micro-traces </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Janina Zięba-Palus </p>
                <p class="mb5">Editorial-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 07, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000113   x------>
              <!------FSC.1000114  ------>
              <div class="article">
                <h4><a href="Non-destructive-technique-for-individualizing-trace-evidence-analysis-of-tiger-nail.php">Non-destructive technique for individualizing trace evidence analysis of tiger nail</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ashvinkumar H. Italiya
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Niha Ansari
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shobhana K. Menon</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 08, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000114   x------>
              <!------FSC.1000115  ------>
              <div class="article">
                <h4><a href="Is-north-Indian-population-changing-it-craniofacial-form-A-study-of-secular-trends-in-craniometric-indices-and-its-relation-to-sex-and-ancestry-estimation.php">Is-north-Indian-population-changing-it-craniofacial-form-A-study-of-secular-trends-in-craniometric-indices-and-its-relation-to-sex-and-ancestry-estimation.php</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vineeta Saini
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mitali Mehta
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rajshri Saini
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Satya Narayan Shamal
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tej Bali Singh
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sunil Kumar Tripathi</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 14, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000115   x------>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 2, Issue 1</h4>
              <hr/>
              <!------FSC.1000106------>
              <div class="article">
                <h4><a href="Noneconomic-damages-due-to-physical-and-sexual-assault-estimates-from-civil-jury-awards.php">Noneconomic damages due to physical and sexual assault: estimates from civil jury awards</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ted R. Miller <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark A. Cohen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Delia Hendrie</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000106 x------>
              <!------FSC.1000107------>
              <div class="article">
                <h4><a href="Accuracy-and-reliability-of-Southern-European-standards-for-the-tibia-a-test-of-two-Mediterranean-populations.php">Accuracy and reliability of Southern European standards for the tibia: a test of two Mediterranean populations</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Julieta G. García-Donas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Oguzhan Ekizoglu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali Er <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Bozdag <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Akcaoglu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ismail Ozgur Can <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena F. Kranioti</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 20, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000107 x------>
              <!------FSC.1000108------>
              <div class="article">
                <h4><a href="Identification-by-comparison-of-caries-free-bitewing-radiographs-Impact-of-observer-qualifications-and-their-clinical-experience.php">Identification by comparison of caries free bitewing radiographs: Impact of observer qualifications and their clinical experience</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sudheer B. Balla <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew Forgie</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 07, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000108 x------>
              <!------FSC.1000109------>
              <div class="article">
                <h4><a href="Soil-transference-patterns-on-clothing-fabrics-and-plastic-buttons-Image-processing-and-laboratory-dragging-experiments.php">Soil transference patterns on clothing fabrics and plastic buttons: Image processing and laboratory dragging experiments</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kathleen R. Murray <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert W. Fitzpatrick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ralph Bottrill <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hilton Kobus</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000109 x------>
              <!------FSC.10001010------>
              <div class="article">
                <h4><a href="An-investigation-of-the-pattern-formed-by-soil-transfer-when-clothing-fabrics-are-placed-on-soil-using-visual-examination-and-image-processing-analysis.php">An investigation of the pattern formed by soil transfer when clothing fabrics are placed on soil using visual examination and image processing analysis</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kathleen R. Murray <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert W. Fitzpatrick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ralph Bottrill <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hilton Kobus</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                </em>
                <hr/>
              </div>
              <!------FSC.10001010 x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 1, Issue 1</h4>
              <hr/>
              <!------FSC.1000101------>
              <div class="article">
                <h4><a href="Beyond-DVI-Future-identification-research-and-archiving.php">Beyond DVI: Future identification, research and archiving</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khoo Lay See <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sarah Aziz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohd Shah Mahmood</p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000101 x------>
              <!------FSC.1000102------>
              <div class="article">
                <h4><a href="Secondary-identifier-for-positive-identification-in-DVI.php">Secondary identifier for positive identification in DVI</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lay See Khoo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Poh Soon Lai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmad Hafizam Hasmi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohd Shah Mahmood</p>
                <p class="mb5">Case Study-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 02, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000102 x------>
              <!------FSC.1000103------>
              <div class="article">
                <h4><a href="Species-identification-of-a-suspected-bone-found-in-blood-sausage.php">Species identification of a suspected bone found in blood sausage</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Posik DM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bustamante AV <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lyall V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Peral García P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Padola NL <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giovambattista G</p>
                <p class="mb5">Case Study-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000103 x------>
              <!------FSC.1000104------>
              <div class="article">
                <h4><a href="Forensic-histopathology-of-the-carotid-body.php">Forensic histopathology of the carotid body</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrea Porzionato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria M. Sfriso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Veronica Macchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martina Contran <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Rambaldo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lucia Petrelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raffaele De Caro</p>
                <p class="mb5">Review Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 14, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000104 x------>
              <!------FSC.1000105------>
              <div class="article">
                <h4><a href="Narcissism-self-esteem-shame-regulation-and-juvenile-delinquency.php"> Narcissism, self-esteem, shame regulation and juvenile delinquency </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Schalkwijk F.W <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jack Dekker <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jaap Peen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Geert Jan Stams </p>
                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 19, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------FSC.1000105 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion">
                  <dd> <a href="#panel1c" class="accordian-active">Volume 2<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <!--<li class="tab-title"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2016</span><span class="mt3 grey-text">Issue 2</span> </a></li>-->
                          <li class="tab-title active"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>February 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns"> BBB </div>
                            </div>
                          </div>
                          <div class="content active" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------FSC.1000106------>
                                <div class="article">
                                  <h4><a href="Noneconomic-damages-due-to-physical-and-sexual-assault-estimates-from-civil-jury-awards.php">Noneconomic damages due to physical and sexual assault: estimates from civil jury awards</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ted R. Miller <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark A. Cohen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Delia Hendrie</p>
                                  <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------FSC.1000106 x------>
                                <!------FSC.1000108------>
                                <div class="article">
                                  <h4><a href="Identification-by-comparison-of-caries-free-bitewing-radiographs-Impact-of-observer-qualifications-and-their-clinical-experience.php">Identification by comparison of caries free bitewing radiographs: Impact of observer qualifications and their clinical experience</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sudheer B. Balla <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew Forgie</p>
                                  <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 07, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------FSC.1000108 x------>
                                <!------FSC.1000107------>
                                <div class="article">
                                  <h4><a href="Accuracy-and-reliability-of-Southern-European-standards-for-the-tibia-a-test-of-two-Mediterranean-populations.php">Accuracy and reliability of Southern European standards for the tibia: a test of two Mediterranean populations</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Julieta G. García-Donas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Oguzhan Ekizoglu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali Er <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Bozdag <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Akcaoglu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ismail Ozgur Can <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elena F. Kranioti</p>
                                  <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 20, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------FSC.1000107 x------>
                                <!------FSC.1000109------>
                                <div class="article">
                                  <h4><a href="Soil-transference-patterns-on-clothing-fabrics-and-plastic-buttons-Image-processing-and-laboratory-dragging-experiments.php">Soil transference patterns on clothing fabrics and plastic buttons: Image processing and laboratory dragging experiments</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kathleen R. Murray <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert W. Fitzpatrick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ralph Bottrill <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hilton Kobus</p>
                                  <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------FSC.1000109 x------>
                                <!------FSC.10001010------>
                                <div class="article">
                                  <h4><a href="An-investigation-of-the-pattern-formed-by-soil-transfer-when-clothing-fabrics-are-placed-on-soil-using-visual-examination-and-image-processing-analysis.php">An investigation of the pattern formed by soil transfer when clothing fabrics are placed on soil using visual examination and image processing analysis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kathleen R. Murray <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert W. Fitzpatrick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ralph Bottrill <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hilton Kobus</p>
                                  <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------FSC.10001010 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                    <div class="hr-tabs">
                      <ul class="tabs" data-tab>
                        <li class="tab-title active"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>December 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                      </ul>
                      <div class="tabs-content">
                        <div class="content active" id="panela">
                          <div class="row pt10">
                            <div class="small-12 columns">
                              <!------FSC.1000101------>
                              <div class="article">
                                <h4><a href="Beyond-DVI-Future-identification-research-and-archiving.php">Beyond DVI: Future identification, research and archiving</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khoo Lay See <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sarah Aziz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohd Shah Mahmood</p>
                                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 21, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------FSC.1000101 x------>
                              <!------FSC.1000102------>
                              <div class="article">
                                <h4><a href="Secondary-identifier-for-positive-identification-in-DVI.php">Secondary identifier for positive identification in DVI</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lay See Khoo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Poh Soon Lai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmad Hafizam Hasmi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohd Shah Mahmood</p>
                                <p class="mb5">Case Study-Forensic Science and Criminology (FSC)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 02, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------FSC.1000102 x------>
                              <!------FSC.1000103------>
                              <div class="article">
                                <h4><a href="Species-identification-of-a-suspected-bone-found-in-blood-sausage.php">Species identification of a suspected bone found in blood sausage</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Posik DM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bustamante AV <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lyall V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Peral García P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Padola NL <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giovambattista G</p>
                                <p class="mb5">Case Study-Forensic Science and Criminology (FSC)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------FSC.1000103 x------>
                              <!------FSC.1000104------>
                              <div class="article">
                                <h4><a href="Forensic-histopathology-of-the-carotid-body.php">Forensic histopathology of the carotid body</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrea Porzionato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria M. Sfriso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Veronica Macchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martina Contran <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Rambaldo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lucia Petrelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raffaele De Caro</p>
                                <p class="mb5">Review Article-Forensic Science and Criminology (FSC)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 14, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------FSC.1000104 x------>
                              <!------FSC.1000105------>
                              <div class="article">
                                <h4><a href="Narcissism-self-esteem-shame-regulation-and-juvenile-delinquency.php"> Narcissism, self-esteem, shame regulation and juvenile delinquency </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Schalkwijk F.W <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jack Dekker <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jaap Peen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Geert Jan Stams </p>
                                <p class="mb5">Research Article-Forensic Science and Criminology (FSC)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 19, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------FSC.1000105 x------>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>FSC welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.fsc@oatext.com">editor.fsc@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
