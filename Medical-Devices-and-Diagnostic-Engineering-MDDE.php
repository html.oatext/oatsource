<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Medical Devices and Diagnostic Engineering (MDDE) - OATEXT</title>
<meta name="description" content="Medical Devices and Diagnostic Engineering (MDDE) is a bimonthly, peer-reviewed journal provides up-to-date information on all aspects of medical devices and diagnostic techniques including biomechanics, bioimaging and imaging informatics on oatext" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<meta name="robots" content=" ALL, index, follow"/>
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For All" />
<meta name="language" content="English" />
<meta http-equiv="window-target" content="_top"/>
<meta http-equiv="pics-label" content="for all ages"/>
<meta name="rating" content="general"/>
<meta content="All, FOLLOW" name="GOOGLEBOTS"/>
<meta content="All, FOLLOW" name="YAHOOBOTS"/>
<meta content="All, FOLLOW" name="MSNBOTS"/>
<meta content="All, FOLLOW" name="BINGBOTS"/>
<meta content="all" name="Googlebot-Image"/>
<meta content="all" name="Slurp"/>
<meta content="all" name="Scooter"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer([' Medical Devices and Diagnostic Engineering (MDDE)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName"  href="#Review_Board"   class="anchor">Review Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes"    class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <!--><dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd><!-->
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Medical Devices and Diagnostic Engineering (MDDE)</h2>
                 <h4>Online ISSN: 2399-6854</h4> 
                <h2 class="mb5"><a href="#Editor-in-Chief">Jui-Teng Lin</a><span class="f14">(Editor-in-Chief)</span> </h2>
                <span class="black-text"><i class="fa fa-university"></i> Chairman & CEO New Vision Inc. Medical Photon Inc<br>
                </span>
                <!--United Lincolnshire Hospital NHS Trust-->
                <hr/>
                <p class="text-justify"> <a href="img/MDDE-cover.jpg" target="_blank"><img src="img/MDDE-cover.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Medical Devices and Diagnostic Engineering (MDDE) is a bimonthly, peer-reviewed journal provides up-to-date information on all aspects of medical devices and diagnostic techniques including biomechanics, bioimaging and imaging informatics.</p>
                <p class="text-justify">Journal emphasizes on current concepts and techniques - with particular emphasis on the application of medical
                  devices to pathologic diagnosis, monitoring, treatment and patient care and also development of medical devices
                  and diagnostic techniques, including aspects of medical product regulation and of product development</p>
                <p class="text-justify">Manuscripts may take the form of original empirical research, critical reviews of the literature, brief commentaries, meeting reports, case reports, innovations in clinical practice, letters and drug clinical trial studies.</p>
                <p class="text-justify">MDDE welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="submissions@oatext.com">submissions@oatext.com</a> alternatively to: <a href="mailto:editor.mdde@oatext.com">editor.mdde@oatext.com</a></p>
                <p>Please, follow the <a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                <p class="text-justify">Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are
                  distributed under the terms of the Creative Commons Attribution License
                  (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in
                  any medium, provided the original work, first published by MDDE, is properly cited.</p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Jui Teng Lin</h2>
              <hr/>
              <p><img src="img/Jui_Teng_Lin.jpg" class="profile-pic" align="right">Prof. J.T. Lin, PhD (1981), University of Rochester (USA). He is the Chairman &amp; CEO of New Vision Inc.
                and Visiting Professor at HE Medical University (China). He served as a Visiting Professor at National
                Chao-tung University and Associate Professor at the University of Central Florida, Founder &amp; CEO of
                Nasdaq-listed companies, He holds over 40 patents and is the inventor (US patents, 1992, 2000) of LASIK
                procedure using flying-spot scanning method and the solid-state UV laser. He has published over 55
                book chapters and 70 SCI journal papers. He is WHO&#39;s WHO in Leading American Executives, Model of
                oversea Chinese Young Entrepreneur and Fellow of American Society of Laser Surgery &amp; Medicine. His
                research areas include Medical laser devices design, and applications in ophthalmology, dental and
                cancer therapy, basic study of photodynamic therapy, laser and tissue interactions, nanotechnology and
                biomaterials.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Federica Scaletti</h4>
                  <p>Department of Chemistry<br>
                    University of Massachusetts<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Claudio De Lazzari</h4>
                  <p>Evaluator, Biomedical Engineering
                    National Research Council<br>
                    Italy</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jui Teng Lin</h4>
                  <p>Chairman & CEO,<br>
                    New Vision Inc. Medical Photon Inc.<br>
                    Taiwan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                  <div class="medium-4 columns">
                    <h4>Moumita Ray</h4>
                    <p>Department of Chemistry<br>
                      University of Massachusetts<br>
                      USA</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Yuh-Show Tsai</h4>
                    <p>Department of Biomedical Engineering<br>
                      Vanderbilt University<br>
                      USA</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Roberto Perris</h4>
                    <p>Director of Centre for Molecular and Translational Oncology<br>
                       Universityu of Parma<br>
                      Italy</p>
                  </div>
              </div>
              <hr/>
              <div class="row">
                  <div class="medium-4 columns">
                      <h4>Eddie Y.K. NG</h4>
                      <p>School of Mechanical & Aerospace Engineering (MAE) College of Engineering<br>
                        Nanyang Technological University (NTU)<br>
                        Singapore</p>
                  </div>
                  <div class="medium-4 columns">
                      <h4>Hui-Fen Wu</h4>
                      <p>Department of Chemistry<br>
                      National Sun Yat-Sen University<br>
                      Taiwan
                      </p>                </div>
                  <div class="medium-4 columns">
                      <h4>Fong-Chin Su</h4>
                      <p>National Cheng Kung University<br>
                      Taiwan</p>
                    </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>John D Bullock</h4>
                 <p>Center for Global Health Systems<br>
                    Research Boulevard <br>
                    USA</p>

                </div>
                <div class="medium-4 columns">
                  <h4>Ashraf A Darwish</h4>
                  <p>Ashraf A Darwish<br>
                    Associate Professor of Mathematics and Computer Science<br>
                    Helwan University<br>
                    Egypt</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>

               <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>  



            </div>

             <div id="Review_Board" class="content">
              <h2>Review Board</h2>
              <hr/>
                <div class="row">
                  <div class="medium-4 columns">
                    <h4>Viviana di Giacomo</h4>
                    <p>Department of Pharmacy<br>
                      "G. d'Annunzio" University<br>
                      Italy</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Dr. Naisana S. Asli</h4>
                    <p>Lecturer, Life Sciences<br>
                       Faculty of Dentistry<br>
                       University of Sydney<br>
                       Australia</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Idiberto José Zotarelli Filho</h4>
                    <p>Stanford University School of Medicine<br>
                       US</p>
                  </div>
                </div>
                <hr/>
                <div class="row">
                  <div class="medium-4 columns">
                    <h4>Mengfei Li</h4>
                    <p>Faculty of Computer Science<br>
                      Department of Simulation and Graphics<br>
                      University of Magdeburg<br>
                      Germany</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Dr. Pedro Romero Pérez</h4>
                    <p>Urology Department<br>
                    Denia Hospital<br>
                    Spain</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Ryad Djeribi</h4>
                    <p>Faculty of Science<br>
                      Biofilms and Biocontamination of Materials Laboratory<br>
                      Badji Mokhtar University<br>
                      Algeria</p>
                  </div>
                </div>
                <hr/>
                <div class="row">
                  <div class="medium-4 columns">
                    <h4>Mehmet Burak Selek</h4>
                    <p>Department of Medical Microbiology<br>
                       Gülhane Military Medical Academy<br>
                       Haydarpaşa Training Hospital<br>
                       Turkey</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Juergen Gschossmann</h4>
                    <p>Department of Internal Medicine<br>
                       Klinikum Forchheim<br>
                       Germany</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Filip Konecny</h4>
                    <p>Plastic Surgery Laboratory<br>
                       Training Coordinator<br>
                       University of Toronto<br>
                       Canada</p>
                  </div>
                </div>
                <hr/>
                <div class="row">
                  <div class="medium-4 columns">
                    <h4>Giovanni Di Bonaventura</h4>
                    <p>Associate Professor of Clinical Microbiology<br>
                       School of Medicine and Health Sciences<br>
                       University of Chieti-Pescara Via Vestini<br>
                       ITALY</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Majed Mohammad Masadeh</h4>
                    <p>Chairman Department of Clinical Laboratory<br>
                       International Colleges for Applied Medical Sciences<br>
                       Jordan</p>
                  </div>
                  <div class="medium-4 columns">
                    <h4>Tao Tan</h4>
                    <p>Biomedical Research Building<br>
                      University at Buffalo<br>
                      USA</p>

                  </div>
                </div>
               
                <div class="row">
                  <div class="medium-4 columns"></div>
                  <div class="medium-4 columns"></div>
                  <div class="medium-4 columns"></div>
                </div>
                






              </div>


            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 2, Issue 1</h4>
              <hr/>
             <!------MDDE.1000113------>
                <div class="article">
                <h4><a href="Low-thermal-coagulation-of-pulmonary-bullae-by-general-purpose-radiofrequency-cautery-device-in-pneumothorax-surgery.php">Low-thermal coagulation of pulmonary bullae by general-purpose radiofrequency cautery device in pneumothorax surgery</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Takashi Iwata
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Aya Yamamoto
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kantaro Hara
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shoji Hanada </p>
                <p class="mb5">Case Series-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 27, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000113 x------>
              <!------MDDE.1000114------>
                <div class="article">
                <h4><a href="Tighter-formed-staples-produce-stronger-sealing-against-luminal-leakage.php">Tighter formed staples produce stronger sealing against luminal leakage</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dwight D Henninger
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jason Jones
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jeffrey W Clymer</p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 27, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000114 x------>
              <!------MDDE.1000115------>
                <div class="article">
                <h4><a href="Identification-of-unmet-clinical-needs-in-the-field-of-nephrology–A-modified-bio-design-approach.php">Identification of unmet clinical needs in the field of nephrology – A modified bio-design approach</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pranay Arun-Kumar
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shreya Sridhar
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jagdish Chaturvedi
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ravi Prakash Deshpande</p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 25, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000115 x------>
              <!------MDDE.1000116------>
                <div class="article">
                <h4><a href="Evolution-of-root-form-endosseous-dental-implant-Transformation-from-bone-anchored-to-ligament-anchored-implants-and-its-neurological-aspects.php">Evolution of root form endosseous dental implant-Transformation from bone anchored to ligament anchored implants and its neurological aspects</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Siddhartha Das
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vivek Soni
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jayesh R Bellare</p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 29, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000116 x------>
              <!------MDDE.1000117------>
                <div class="article">
                <h4><a href="Measurement-of-temperature-dependent-heat-flow-rate-from-human-limbs-towards-thermoelectric-cooling-device.php">Measurement of temperature dependent heat flow rate from human limbs towards thermoelectric cooling device</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gorden Link
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Martin Stelzle</p>
                <p class="mb5">Short Communication-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 31, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000117 x------>
              <!------MDDE.1000118------>
                <div class="article">
                <h4><a href="Design-aspects-of-medical-laser-devices.php">Design aspects of medical laser devices</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin</p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 29, 2017</p></em>
                <hr/>
                </div>
              <!------MDDE.1000118 x------>
              



            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 1, Issue 2</h4> 
              <hr/>
              <!------MDDE.1000107------>
                <div class="article">
                <h4><a href="Dr-William-Thomas-Duffy-Statement.php">Dr. William Thomas Duffy Statement</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000107 x------>
              <!------MDDE.1000108------>
                <div class="article">
                <h4><a href="Definition-of-K-BTE-Medical-Laser-Device.php">Definition of K-BTE Medical Laser Device</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000108 x------>
              


<!------MDDE.1000109 ------>
<div class="article">
<h4><a href="Cytosolar-protein-delivery-for-therapeutic-purpose.php">

Cytosolar protein delivery for therapeutic purpose




</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Moumita Ray 

</p>
<p class="mb5">Editorial-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 25, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000109  x------>




<!------MDDE.1000110 ------>
<div class="article">
<h4><a href="Analysis-of-thermal-therapy-using-infrared-diode-lasers-operated-in-continuous-wave-and-pulsed-mode.php"> Analysis of thermal therapy using infrared diode lasers
operated in continuous wave and pulsed mode </a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin </p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE) </p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016 </p>
</em>
<hr/>
</div>
<!------MDDE.1000110  x------>

<!------MDDE.1000111------>
<div class="article">
<h4><a href="Progress-of-medical-lasers-Fundamentals-and-Applications.php">Progress of medical lasers: Fundamentals and Applications</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin</p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000111 x------>
<!------MDDE.1000111------>
<div class="article">
<h4><a href="Progress-of-medical-lasers-Fundamentals-and-Applications.php">Progress of medical lasers: Fundamentals and Applications</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin</p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000111 x------>
<!------MDDE.1000112------>
<div class="article">
<h4><a href="Dementia-Testimony.php">Dementia Testimony</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic</p>
<p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 23, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000112 x------>
              
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 1, Issue 1</h4>
              <hr/>
              <!------MDDE.1000101------>
              <div class="article">
                <h4><a href="Disinfecting-gingival-crevices-by-Non-thermal-Atmospheric-Pressure-Plasma.php"> Disinfecting gingival crevices by Non-thermal Atmospheric Pressure Plasma </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sun SY <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huan J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wang MC </p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000101 x------>
              <!------MDDE.1000102 ------>
              <div class="article">
                <h4><a href="The-use-of-external-fixator-for-ankle-and-foot-injuries-management-a-review-on-biomechanical-perspective.php"> The use of external fixator for ankle and foot injuries management-a review on biomechanical perspective </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muhammad Hanif Ramlee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> AAzura Derus </p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000102  x------>
              <!------MDDE.1000103 ------>
              <div class="article">
                <h4><a href="Human-Recognition-Based-On-Ear-Shape-Images-Using-PCA-Wavelets-and-Different-Classification-Methods.php"> Human Recognition Based On Ear Shape Images Using PCA-Wavelets and Different Classification Methods </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ali Mahmoud Mayya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mariam Mohammad Saii </p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 16, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000103  x------>
              <!------MDDE.1000104------>
                <div class="article">
                <h4><a href="When-Nano-Becomes-Smart-Nanomaterial-Containing-Medical-Devices.php">When Nano Becomes Smart: Nanomaterial-Containing Medical Devices </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Federica Scaletti</p>
                <p class="mb5">Editorial-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 05, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000104 x------>
              <!------MDDE.1000105------>
                <div class="article">
                <h4><a href="Ultrastructural-analysis-application-for-spermatozoa-study-in-male-infertility.php">Ultrastructural analysis application for spermatozoa study in male infertility</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pichugova SV
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tulakina LG
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Klein AV
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Beykin YaB </p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 06, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000105 x------>
              <!------MDDE.1000106------>
                <div class="article">
                <h4><a href="New-Advanced-Technology-in-Medicine-Bio-Electrons-Photons.php">New Advanced Technology in Medicine: Bio Electron’s Photons</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000106 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
                    <div class="lee">
            <dl data-accordion="" class="accordion" >
              <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
              <div class="content" id="panel1b">
                  <div class="hr-tabs">
                  <ul class="tabs" data-tab>
                      <!--<li class="tab-title"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text">Issue 3</a></li>-->
                    <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>October 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                  </ul>
                 <div class="tabs-content">
                     <!--<div class="content " id="panelc">
                      <div class="row pt10">
                      <div class="small-12 columns">CCC</div>
                        </div>
                        </div>-->

                    <div class="content active" id="panelb">
                      <div class="row pt10">
                        <div class="small-12 columns">
                          <!------MDDE.1000107------>
                <div class="article">
                <h4><a href="Dr-William-Thomas-Duffy-Statement.php">Dr. William Thomas Duffy Statement</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000107 x------>
              <!------MDDE.1000108------>
                <div class="article">
                <h4><a href="Definition-of-K-BTE-Medical-Laser-Device.php">Definition of K-BTE Medical Laser Device</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000108 x------>
              


<!------MDDE.1000109 ------>
<div class="article">
<h4><a href="Cytosolar-protein-delivery-for-therapeutic-purpose.php">

Cytosolar protein delivery for therapeutic purpose




</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Moumita Ray 

</p>
<p class="mb5">Editorial-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 25, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000109  x------>




<!------MDDE.1000110 ------>
<div class="article">
<h4><a href="Analysis-of-thermal-therapy-using-infrared-diode-lasers-operated-in-continuous-wave-and-pulsed-mode.php"> Analysis of thermal therapy using infrared diode lasers
operated in continuous wave and pulsed mode </a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin </p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE) </p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016 </p>
</em>
<hr/>
</div>
<!------MDDE.1000110  x------>

<!------MDDE.1000111------>
<div class="article">
<h4><a href="Progress-of-medical-lasers-Fundamentals-and-Applications.php">Progress of medical lasers: Fundamentals and Applications</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin</p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000111 x------>
<!------MDDE.1000111------>
<div class="article">
<h4><a href="Progress-of-medical-lasers-Fundamentals-and-Applications.php">Progress of medical lasers: Fundamentals and Applications</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jui-Teng Lin</p>
<p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000111 x------>
<!------MDDE.1000112------>
<div class="article">
<h4><a href="Dementia-Testimony.php">Dementia Testimony</a></h4>
<p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic</p>
<p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
<em>
<p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 23, 2016   </p>
</em>
<hr/>
</div>
<!------MDDE.1000112 x------>
                        </div>
                      </div>
                    </div>

                    <div class="content" id="panela">
                      <div class="row pt10">
                        <div class="small-12 columns">
                          <!------MDDE.1000101------>
              <div class="article">
                <h4><a href="Disinfecting-gingival-crevices-by-Non-thermal-Atmospheric-Pressure-Plasma.php"> Disinfecting gingival crevices by Non-thermal Atmospheric Pressure Plasma </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sun SY <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huan J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wang MC </p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000101 x------>
              <!------MDDE.1000102 ------>
              <div class="article">
                <h4><a href="The-use-of-external-fixator-for-ankle-and-foot-injuries-management-a-review-on-biomechanical-perspective.php"> The use of external fixator for ankle and foot injuries management-a review on biomechanical perspective </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muhammad Hanif Ramlee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> AAzura Derus </p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 05, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000102  x------>
              <!------MDDE.1000103 ------>
              <div class="article">
                <h4><a href="Human-Recognition-Based-On-Ear-Shape-Images-Using-PCA-Wavelets-and-Different-Classification-Methods.php"> Human Recognition Based On Ear Shape Images Using PCA-Wavelets and Different Classification Methods </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ali Mahmoud Mayya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mariam Mohammad Saii </p>
                <p class="mb5">Review Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 16, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------MDDE.1000103  x------>
              <!------MDDE.1000104------>
                <div class="article">
                <h4><a href="When-Nano-Becomes-Smart-Nanomaterial-Containing-Medical-Devices.php">When Nano Becomes Smart: Nanomaterial-Containing Medical Devices </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Federica Scaletti</p>
                <p class="mb5">Editorial-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 05, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000104 x------>
              <!------MDDE.1000105------>
                <div class="article">
                <h4><a href="Ultrastructural-analysis-application-for-spermatozoa-study-in-male-infertility.php">Ultrastructural analysis application for spermatozoa study in male infertility</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pichugova SV
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tulakina LG
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Klein AV
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Beykin YaB </p>
                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 06, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000105 x------>
              <!------MDDE.1000106------>
                <div class="article">
                <h4><a href="New-Advanced-Technology-in-Medicine-Bio-Electrons-Photons.php">New Advanced Technology in Medicine: Bio Electron’s Photons</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nick Kostovic </p>
                <p class="mb5">Opinion Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                <em><p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  October 24, 2016   </p></em>
                <hr/>
                </div>
              <!------MDDE.1000106 x------>
                        </div>
                      </div>
                    </div>

                  </div>

          </div>
          
        </dd>
      </dl>
    </div> 
            </div>
            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr/>
              <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border:0px;">
                <tr>
                  <td align="Center" style="border:0px;"><a href="http://road.issn.org/"><img src="img/IMM_ROAD.JPG"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://www.worldcat.org/"><img src="img/IMM_WorldCat.JPG"></a></td>
                  <td align="Center" style="border:0px;"><a href="http://www.icmje.org/"><img src="img/MDDE-icmje.gif"></a></td>
                </tr>
                <tr>
                  <td align="Center" style="border:0px;"><a href="https://www.researchbib.com/"><img src="img/IFNM-Researcch-Bib.png"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://doaj.org/"><img src="img/MDDE-cropped.jpg"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://publons.com/home/"><img src="img/MDD-squarespace.com.png"></a></td>
                </tr>
              </table>
              <p>Note : DOAJ under Progress</p>
            </div>
            
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p class="">MDDE welcomes direct submissions of manuscripts from authors. You can submit your manuscript to: <a href="submissions@oatext.com">submissions@oatext.com</a> alternatively to: <a href="mailto:editor.mdde@oatext.com">editor.mdde@oatext.com</a> 
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">&COPY; 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
