<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="OA Text is an independent open-access scientific publisher showcases innovative research and ideas aimed at improving health by linking research and practice to the benefit of society." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT oatext, oat, open access text" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<title>Is north Indian population changing it craniofacial form? A study of secular trends in craniometric indices and its relation to sex and ancestry estimation</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
<div class="small-12 columns">
<hr class="mt0 green-hr-line"/>
</div></div>
<!--banner -->
  <div class="row">
    <div class="medium-12 text-center columns">
<div class="inner-banner">
      <h1>Take a look at the Recent articles</h1>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="row">
    <div class="large-8 columns">
    

    
    
      <div class="inner-left">
        <h2 class="w600">Is north Indian population changing it craniofacial form? A study of secular trends in craniometric indices and its relation to sex and ancestry estimation</h2>
        
        
<div data-dropdown="a1" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Vineeta Saini</p>
</div>
<div id="a1" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Forensic Medicine, Institute of Medical Sciences, BHU, Varanasi, India</p>
<p><i class="fa  fa-envelope-o"></i> E-mail : <a href="mailto:bhuvaneswari.bibleraaj@uhsm.nhs.uk">bhuvaneswari.bibleraaj@uhsm.nhs.uk</a></p>
</div>

<div data-dropdown="a2" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Mitali Mehta</p>
</div>
<div id="a2" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Forensic Science, University School of Sciences, Gujarat University, Navarangpura, Ahmedabad 380009, Gujarat, India</p>
</div>
<div data-dropdown="a3" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Rajshri Saini</p>
</div>
<div id="a3" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Faculty of Computer Science, B.S.A. College of Engineering & Technology, Mathura. Dr APJ Abdul Kalam Technical University, Lucknow, India</p>
</div>
<div data-dropdown="a4" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Satya Narayan Shamal</p>
</div>
<div id="a4" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Anatomy, Institute of Medical Sciences, BHU, Varanasi, India, 221005</p>
</div>
<div data-dropdown="a5" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Tej Bali Singh</p>
</div>
<div id="a5" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Community Medicine, Institute of Medical Sciences, BHU, Varanasi, India, 221005</p>
</div>
<div data-dropdown="a6" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Sunil Kumar Tripathi</p>
</div>
<div id="a6" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Forensic Medicine, Institute of Medical Sciences, BHU, Varanasi, India</p>
</div>




 
      
      <p class="mt10">DOI: 10.15761/FSC.1000115</p>
      <div class="mt25"></div>
      <div class="custom-horizontal">
        <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs">
          <dd class="active"><a  name="AnchorName"  href="#Article"   class="anchor" >Article</a></dd>
          <dd><a  name="AnchorName"  href="#Article_Info"   class="anchor">Article Info</a></dd>
          <dd><a   name="AnchorName" href="#Author_Info"    class="anchor">Author Info</a></dd>
          <dd><a   name="AnchorName" href="#Figures_Data"   class="anchor">Figures & Data</a></dd>
        </dl>
        <div class="tabs-content">
          <div id="Article" class="content active">
          

<div class="text-justify">

<h2 id="jumpmenu1">Abstract</h2>
            <p><strong>Objectives: </strong>Ancestry and sex estimation using craniometric indices is an important aspect of skeletal identification process, as it limits the number of antemortem records used to compare with a postmortem profile and establish a positive identification. These indices are also affected by secular or temporal changes. So the aim of the study to explore the usefulness of indices for sex and ancestry estimation in North Indian population and effect of secular changes on these indices.</p>

            <p><strong>Materials and methods: </strong>In present study two temporally different skull samples of the North Indian population have been taken and eighteen craniometric indices were calculated on the basis of 17 craniofacial measurements for both sexes to examine the temporal changes and their use in ancestry and sexual differenciation. Contemporary sample comprised of 158 skulls (110 male; 48 females) and subrecent 325 skulls (206 males and 119 females) with an age range of 20-68 years.</p>

            <p><strong>Results</strong>: Craniofacial measurements showed significantly higher values in contemporary males and females. In males highly significant secular/temporal changes were observed in orbital index and transverse frontal index and to a lesser extent in foraman magnum and gnathic index. In females, highly significant differences were observed in cranial, vertical, auriculo vertical and longitudinal craniofacial index.</p>

            <p><strong>Discussion:</strong> There is a lot of variability in Indian and non Indian populations so these indices can be used for ancestry estimation but not for sex discrimination due to poor classification rate. Moreover, the causes of these temporal variations with other issues have also been discussed.</p>




<h2 id="jumpmenu2">Key words</h2>
                  <p>Anthropology population data, temporal changes, effect size, sexual dimorphism, ancestry estimation</p>

<h2 id="jumpmenu3">Introduction</h2>
                  <p>Skeletal features and craniometry (measurements of skull) are often used in forensic anthropology and bioarchaeology to make estimations of biological profile or identifications of unknown. In the early 20<sup>th</sup> century, indices were widely utilized by Anthropologists to categorize human populations. But these days it is mainly used to express the individual&rsquo;s appearances. An index is a mathematically expressed relationship between the quantifications, which eliminates the chances of overlapping of craniofacial measurements that might have taken place among different populations or between male and female. To find out an index, the shorter measurement is arranged as numerator and the longer as denominator, which gives elevate to a fraction that has to be multiplied by 100. Features that can be expressed as actual measurements, e.g. cranial index, nasal index; provide a system for metrical recording of sizes and proportions of cranial features in lieu of subjective impressions [1]. Since these indices yield a numerical expression, it is an important parameter for evaluating inter and intra population comparisons of crania as well as the sexual dimorphism [1,2]. As craniofacial shape can be access directly via this method, morphological differences or similarities among populations to be expressed in a more substantial way than in the case of individual linear measurements [3]. Shape indices thus make a relative statement about cranial morphology; e.g., a cranium is broad for its length [4]. These are highly variable and discriminative among populations across geographical regions and also permit comparison of cranial morphometry between parents, offspring and siblings, which may provide a clue to genetic transmission of inherited characters [2]. In addition, standardized cephalometric data enable diagnostic comparison between diseased and the normal population [5-7].</p>

                <p>These craniometric indices may be affected by secular or temporal changes [8]. Secular changes are physical changes that may take place within the given population due to dramatic shifts in living standards or exposure to a new environment. These types of changes over the short-term are thought to be the result of an improvement or decline in environmental conditions, particularly nutrition [9]. Previously, significant craniofacial changes in the dimension of past and present populations of Eastern and Western band Cherokee population <u>(</u>a&nbsp;Native American&nbsp;tribe) [8], African and White [8,10] Japanese [11-13] Croatian [14,15] Mexican [16] and Indians [17,18] have been reported. Significant secular increase has been found in all those variables which are used to calculate craniofacial indices e.g. cranial length and breadth, facial height, cranial height and bizygomatic breadth [8,10-13,18,19]. The specificity of the particular cranial feature may be diluted due to temporal changes over decades, as the cranium is becoming higher and narrower [20-22]. Indian population has gone through a considerable change after the independence in 1947 [17,18]. These studies showed the evidences of remodeling of cranial vault in Indian population and confirm that the sex classification accuracies may differ due to varying levels of dimorphism between two successive populations of the same geographical region. The extent of sexual dimorphism found to decrease in contemporary population while a trend toward increased cranial breadth (brachycephalization) observed in contemporary females. It is supposed that the marked intra-population differences in the cranial measurements of temporally-different samples may also contribute in changing pattern of cranial indices, which has never investigated.</p>

                <p>Previously it has been claimed that craniometric indices are useful for sex and ancestry estimation [1,2]. which is a vital part of forensic anthropology.</p>

                <p>Sex estimation is the easiest and paramount among the BIG FOUR (Sex, age, ancestry and stature); erudition of this information can reduce search out population by proximately half at once. Moreover, the subsequent methods of age and stature estimation are largely sex dependent. The term &ldquo;race&rdquo; is a concept with both cultural and biological elements, which has been superseded recently with the term ancestry or cultural affiliation. This aspect of the biological profile is one of the most arduous to assess due extreme human variability. Several factors influence the ability to assign a geographic location to a particular skeletal part such as population intermixing, and temporal changes [23], though sex resoluteness is withal affected by these factors, but to a lesser extent. Observing anatomical or morphological traits of craniofacial region (where morphological variation is greatest) is the most reliable and popular way of attributing &lsquo;ancestry&rsquo; because they are known to be more genetically driven and less affected by environmental factors. While for sex resoluteness, it is the second best option after pelvis. Skull variation between populations can withal be tenacious by metrical method, which will yield indices and discriminant functions. These methods are highly population specific and need to be customized according to the population under consideration.</p>

                <p>Previously few computerized programs have been published e.g. FORDISC [24] CRANID [25] and ANCESTREES [26] to estimate ancestry of unknown crania using software based programs. The CRANID used 29 parameters of the craniofacial region to compare unknown crania to 74 geographical samples that are from a collection of 3,163 crania from 39 different populations from around the world. But this dataset is consists of&nbsp;Howells&nbsp;(1973) study of cranial variation of 2524 crania from 28 populations from around the world [27].&nbsp;The other skulls used to make this software were also of very old period, and Indian skull sample (population), was also represented poorly. CRANID has greater validity in Australia and Europe because of greater representation of indigenous Australian and European reference crania. Kallenberger and Pilbrow [28] found that the CRANID program was only able to accurately assign 39% of specimens to geographically closest matching reference samples. FORDISC uses Howells&rsquo; dataset as a reference sample but with additional samples from the American Forensic Data Bank and the Terry and Hamann-Todd Collection. These museum collections have skeletal remains that were amassed from 50 to 100 years ago. Several researchers questioned the validity of these old age collections in medico-legal context for producing the accurate determination of the biological profile. FORDISC is used internationally but it has particular relevance to the American context because the American Forensic Data Bank forms a large proportion of the reference materials [29]. ANCESTREES provide better classification when only African and European ancestral groups were examined. Outside the reference population, all these software programs always performed below average [28].</p>

                <p>There are many populations in the world which cannot be categorized using above three ancestries/races identification programs. Moreover, any of this software does not represent Indian populations which are wide enough to make its own population (ancestry) software. Another hurdle to worsen the situation is the shortage of skeletal assemblage in India as the Hindu majority cremates their deceased. Therefore, in the present study a reference data is provided in form of craniofacial measurements and craniometric indices for North Indian population, which can be used to make a database for Indian populations. A comparison will also be performed to see whether changes in contemporary and subrecent populations are significant or not. In addition, up to what degree these craniometric indices are helpful for sex and ancestral estimation.</p>

<h2 id="jumpmenu4">Material and method</h2>

                <p>A total of 483 adult crania were collected from a North Indian state, Uttar Pradesh, which is the fourth largest state of India. The contemporary sample comprised of a total of 158 crania collected from the Department of Forensic Medicine, Institute of Medical Sciences, Banaras Hindu University, Varanasi during the period of 2006 to 2011. The sample included 110 males (mean age- 38.58 years) and 48 females (mean age-31.75 years) with an age range of 22-65 years. The sub recent sample was drawn from Ganesh Shankar Vidyarthi Medical College, Kanpur, India. This sample included 206 males (mean age- 44.77 years) and 119 females (mean age-36.34 years) with age range of 20.68 years [17,18]. Crania with obvious pathological changes, fracture or deformation were excluded from the study. Seventeen variables of skull were measured according to the standards provided by Buikstra and Ubelaker [30] and Singh and Bhasin [31].</p>

                <ol>
                  <li>Maximum Cranial Length (MaxCLt): It is the distance between glabella (g) and opisthocranion (op) in the midsagittal plane, measured in a straight line. Instrument: Spreading calliper [30].</li>
                  <li>Maximum Cranial Breadth (MaxCBr): The maximum width of skull perpendicular to midsagittal plane wherever it is located, with the exception of the inferior temporal lines and the area immediately surrounding them. Instrument: Spreading caliper [30].</li>
                  <li>Basion-Bregma Height (BBrHt): It is the direct distance from the lowest point on the anterior margin of foramen magnum (ba), to bregma (b). Instrument: Spreading caliper [30].</li>
                  <li>Maximum Frontal Breadth (MaxFBr): The straight distance between two coronalia (o) Instrument: Spreading caliper [30].</li>
                  <li>Minimum Frontal Breadth (MinFBr): The direct distance between the two frontotemporale (ft) Instrument: Spreading caliper [30].</li>
                  <li>Auriculo-bregmatic height (ABrHt): The direct distance from porion (po) to bregma (b) Instrument: Spreading caliper [30].</li>
                  <li>Nasal Height (NHt): direct distance from nasion (n) to the midpoint of a line connecting the lowest points of the inferior margin of the nasal notches (ns) Instrument: Sliding caliper [30].</li>
                  <li>Nasal Breadth (NBr): maximum breadth of the nasal aperture (al-al) Instrument: Sliding caliper [30].</li>
                  <li>Orbital Height (OHt): Maximum height from the upper to lower orbital borders (or) perpendicular to the d-ec line (orbital breadth) Instrument: Sliding caliper [30].</li>
                  <li>Orbital Breadth (OBr): Laterally sloping distance from dacryon (d) to ectoconchion (ec) Instrument: Sliding caliper [30].</li>
                  <li>Interorbital Breadth (IOBr): Direct distance between right and left dacryon (d) Instrument: Sliding caliper [30].</li>
                  <li>Facial Length/ depth (FLt): Direct distance from basion (ba) to prosthion (pr) Instrument: Sliding caliper [30].</li>
                  <li>Cranial Base Length (CBLt): Direct distance from nasion (n) to basion (ba) Instrument: Sliding caliper . [30].</li>
                  <li>Foramen Magnum Breadth (FMBr): Distance between the lateral margins of the foramen magnum at the points of greatest curvature Instrument: Sliding caliper [30].</li>
                  <li>Foramen Magnum Length (FMLt): Direct distance from basion (ba) to opisthion (ops) Instrument: Sliding caliper [30].</li>
                  <li>Upper Facial Height (UFHt): Direct distance from nasion (n) to prosthion (pr) Instrument: Sliding caliper [30].</li>
                  <li>&nbsp;Bizygomatic Breadth (BZBr): Direct distance between two zygia (zy) <em>i.e</em>., the most laterally placed points on the zygomatic arch Instrument: Sliding caliper [30].</li>
                </ol>

                <p>All the measurements were taken with sliding or spreading callipers (0.1 mm precision) three times to minimize the intra-observer errors and average is used for calculating the 18 craniometric indices. The craniometric indices were computed in EXCEL using average value and categorized based on different classifications to find the variability in the skulls.</p>

          <h4>Statistical analysis </h4>

                <p>The data (indices) were analyzed using the SPSS 16.0 programme. Secular/temporal changes in craniofacial variables were examined by using t- test. Sexual dimorphism in the craniometric indices of both samples was also assessed by using a t-test. To see the efficiency to classify sex a discriminant analysis also performed. Mean population differences between the modern sample and the subrecent samples were tested through t-test to infer about possible secular/temporal changes in the features (indices) which are here investigated. An effect size for these differences is also evaluated, which quantifies the magnitude of the difference between these population groups. Effect sizes allow researchers to move away from the simple identification of statistical significance and toward a more generally interpretable, quantitative description of the size of an effect. They provide a description of the size of observed effects that is independent of the possibly misleading influences of sample size. Studies with different sample sizes but the same basic descriptive characteristics (e.g., distributions, means, and standard deviations) will differ in their statistical significance values but not in their effect size estimates. Effect sizes describe the observed effects; effects that are large but non-significant may suggest further research with greater power, whereas effects that are trivially small but nevertheless significant because of large sample sizes can warn researchers against possibly overvaluing the observed effect [32,33]. Effect size for differences in means is given by Cohen&#39;s d is defined in terms of population means and a population standard deviation, as shown below.</p>

                <p>Effect size = (<u>Mean of contemporary sample) - (mean of subrecent sample)</u></p>

                <h4>Standard Deviation</h4>

                <p>Sullivan and Feinn [34] suggested that in reporting and interpreting studies, both the substantive significance (effect size) and statistical significance (P value) are essential. The P value reveals only the existence of an effect (<em>i.e., </em>difference), not the size of the effect. Cohen classified effect sizes as small (d = 0.2), medium (d= 0.5), and large (d &ge; 0.8). In an attempt to help with the interpretation of &lsquo;d&rsquo; Cohen (1988) suggested that d values of 0.8, 0.5, and 0.2 represented large, medium, and small effect sizes, respectively. It can be more meaningfully described as obvious <em>i.e.,</em> visible to the naked eye of all observers, subtle <em>i.e.,</em> visible to the naked eye of a meticulous observer, and merely statistical <em>i.e.,</em> noticeably smaller than medium but not so small as to be trivial [33-35].</p>

<h2 id="jumpmenu5">Results </h2>

          <h4>Secular changes in craniofacial measurements </h4>

                <p>Descriptive statistics, with t-test and significance of difference between contemporary and subrecent males are given in Table 1. All the variables (except OHt, FMLt and UFHt) were significantly greater in contemporary males. Table 2 provides the descriptive data for females of both population groups. Contemporary females showed greater dimensions than their predecessors (except MaxCLt, NHt, FMBr and FMLt).</p>

                <p><strong>Table 1.</strong>&nbsp;Descriptive statistics, t-test and significance of differences between mean of contemporary and subrecent- males.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:91px;"><p align="center"><strong>Variables</strong></p></td>
      <td colspan="3" style="width:245px;"><p align="center"><strong>Contemporary- Males</strong></p></td>
      <td colspan="3" style="width:247px;"><p align="center"><strong>Subrecent- Males</strong></p></td>
      <td rowspan="2" style="width:72px;"><p align="center"><strong>t-value</strong></p></td>
      <td rowspan="2" style="width:62px;"><p align="center"><strong>Sig.</strong></p></td>
    </tr>
    <tr>
      <td style="width:70px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:61px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;"><p align="center"><strong>Min.-Max.</strong></p></td>
      <td style="width:79px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:108px;"><p align="center"><strong>Min.-Max.</strong></p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxCLt</p></td>
      <td style="width:70px;"><p align="right">182.52</p></td>
      <td style="width:61px;"><p align="right">6.59</p></td>
      <td style="width:114px;"><p align="right">160.27-193.8</p></td>
      <td style="width:79px;"><p align="right">179.60</p></td>
      <td style="width:60px;"><p align="right">6.51</p></td>
      <td style="width:108px;"><p align="right">163.0-197.80</p></td>
      <td style="width:72px;"><p align="right">3.780</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxCBr</p></td>
      <td style="width:70px;"><p align="right">128.24</p></td>
      <td style="width:61px;"><p align="right">5.30</p></td>
      <td style="width:114px;"><p align="right">116.0-143.13</p></td>
      <td style="width:79px;"><p align="right">125.97</p></td>
      <td style="width:60px;"><p align="right">4.57</p></td>
      <td style="width:108px;"><p align="right">115.6-138.90</p></td>
      <td style="width:72px;"><p align="right">3.972</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>BBrHt</p></td>
      <td style="width:70px;"><p align="right">134.19</p></td>
      <td style="width:61px;"><p align="right">5.06</p></td>
      <td style="width:114px;"><p align="right">121.17-145.60</p></td>
      <td style="width:79px;"><p align="right">131.59</p></td>
      <td style="width:60px;"><p align="right">4.95</p></td>
      <td style="width:108px;"><p align="right">115.7-147.90</p></td>
      <td style="width:72px;"><p align="right">4.397</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxFBr</p></td>
      <td style="width:70px;"><p align="right">114.39</p></td>
      <td style="width:61px;"><p align="right">4.68</p></td>
      <td style="width:114px;"><p align="right">104.6-125.70</p></td>
      <td style="width:79px;"><p align="right">110.77</p></td>
      <td style="width:60px;"><p align="right">4.46</p></td>
      <td style="width:108px;"><p align="right">97.9-123.40</p></td>
      <td style="width:72px;"><p align="right">6.738</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MinFBr</p></td>
      <td style="width:70px;"><p align="right">93.32</p></td>
      <td style="width:61px;"><p align="right">5.02</p></td>
      <td style="width:114px;"><p align="right">84.16-108.18</p></td>
      <td style="width:79px;"><p align="right">91.68</p></td>
      <td style="width:60px;"><p align="right">3.93</p></td>
      <td style="width:108px;"><p align="right">78.43-102.23</p></td>
      <td style="width:72px;"><p align="right">3.192</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>ABrHt</p></td>
      <td style="width:70px;"><p align="right">126.71</p></td>
      <td style="width:61px;"><p align="right">4.10</p></td>
      <td style="width:114px;"><p align="right">118.9-136.8</p></td>
      <td style="width:79px;"><p align="right">124.28</p></td>
      <td style="width:60px;"><p align="right">4.01</p></td>
      <td style="width:108px;"><p align="right">110.1-135.6</p></td>
      <td style="width:72px;"><p align="right">5.072</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>NHt</p></td>
      <td style="width:70px;"><p align="right">49.83</p></td>
      <td style="width:61px;"><p align="right">2.77</p></td>
      <td style="width:114px;"><p align="right">42.08-56.79</p></td>
      <td style="width:79px;"><p align="right">48.71</p></td>
      <td style="width:60px;"><p align="right">3.31</p></td>
      <td style="width:108px;"><p align="right">39.2-57.25</p></td>
      <td style="width:72px;"><p align="right">3.008</p></td>
      <td style="width:62px;"><p align="right">.003**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>NBr</p></td>
      <td style="width:70px;"><p align="right">25.38</p></td>
      <td style="width:61px;"><p align="right">1.90</p></td>
      <td style="width:114px;"><p align="right">21.58-30.38</p></td>
      <td style="width:79px;"><p align="right">24.63</p></td>
      <td style="width:60px;"><p align="right">2.04</p></td>
      <td style="width:108px;"><p align="right">16.0-29.25</p></td>
      <td style="width:72px;"><p align="right">3.177</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>OHt</p></td>
      <td style="width:70px;"><p align="right">32.63</p></td>
      <td style="width:61px;"><p align="right">1.50</p></td>
      <td style="width:114px;"><p align="right">29.79-36.63</p></td>
      <td style="width:79px;"><p align="right">32.67</p></td>
      <td style="width:60px;"><p align="right">2.07</p></td>
      <td style="width:108px;"><p align="right">26.15-37.33</p></td>
      <td style="width:72px;"><p align="right">-.173</p></td>
      <td style="width:62px;"><p align="right">.863</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>OBr</p></td>
      <td style="width:70px;"><p align="right">&nbsp;39.15</p></td>
      <td style="width:61px;"><p align="right">1.87</p></td>
      <td style="width:114px;"><p align="right">35.08-43.29</p></td>
      <td style="width:79px;"><p align="right">38.30</p></td>
      <td style="width:60px;"><p align="right">1.80</p></td>
      <td style="width:108px;"><p align="right">32.35-42.18</p></td>
      <td style="width:72px;"><p align="right">3.933</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>IOBr</p></td>
      <td style="width:70px;"><p align="right">20.62</p></td>
      <td style="width:61px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">17.26-26.14</p></td>
      <td style="width:79px;"><p align="right">19.727</p></td>
      <td style="width:60px;"><p align="right">2.10</p></td>
      <td style="width:108px;"><p align="right">15.05-25.45</p></td>
      <td style="width:72px;"><p align="right">3.638</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FLt</p></td>
      <td style="width:70px;"><p align="right">96.19</p></td>
      <td style="width:61px;"><p align="right">4.45</p></td>
      <td style="width:114px;"><p align="right">81.24-105.22</p></td>
      <td style="width:79px;"><p align="right">93.58</p></td>
      <td style="width:60px;"><p align="right">5.05</p></td>
      <td style="width:108px;"><p align="right">80.05-111.40</p></td>
      <td style="width:72px;"><p align="right">4.566</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>CBLt</p></td>
      <td style="width:70px;"><p align="right">101.75</p></td>
      <td style="width:61px;"><p align="right">3.69</p></td>
      <td style="width:114px;"><p align="right">91.97-110.53</p></td>
      <td style="width:79px;"><p align="right">99.95</p></td>
      <td style="width:60px;"><p align="right">4.45</p></td>
      <td style="width:108px;"><p align="right">85.0-112.40</p></td>
      <td style="width:72px;"><p align="right">3.629</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FMBr</p></td>
      <td style="width:70px;"><p align="right">28.99</p></td>
      <td style="width:61px;"><p align="right">1.77</p></td>
      <td style="width:114px;"><p align="right">25.0-32.18</p></td>
      <td style="width:79px;"><p align="right">28.43</p></td>
      <td style="width:60px;"><p align="right">2.05</p></td>
      <td style="width:108px;"><p align="right">23.0-33.45</p></td>
      <td style="width:72px;"><p align="right">2.418</p></td>
      <td style="width:62px;"><p align="right">.016*</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FMLt</p></td>
      <td style="width:70px;"><p align="right">34.24</p></td>
      <td style="width:61px;"><p align="right">2.31</p></td>
      <td style="width:114px;"><p align="right">28.74-39.29</p></td>
      <td style="width:79px;"><p align="right">34.30</p></td>
      <td style="width:60px;"><p align="right">2.52</p></td>
      <td style="width:108px;"><p align="right">25.28-40.05</p></td>
      <td style="width:72px;"><p align="right">-.222</p></td>
      <td style="width:62px;"><p align="right">.824</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>UFHt</p></td>
      <td style="width:70px;"><p align="right">66.75</p></td>
      <td style="width:61px;"><p align="right">3.73</p></td>
      <td style="width:114px;"><p align="right">56.56-75.74</p></td>
      <td style="width:79px;"><p align="right">65.43</p></td>
      <td style="width:60px;"><p align="right">4.67</p></td>
      <td style="width:108px;"><p align="right">52.45-80.25</p></td>
      <td style="width:72px;"><p align="right">2.553</p></td>
      <td style="width:62px;"><p align="right">.011</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>&nbsp;BZBr</p></td>
      <td style="width:70px;"><p align="right">127.07</p></td>
      <td style="width:61px;"><p align="right">3.87</p></td>
      <td style="width:114px;"><p align="right">117.38-135.67</p></td>
      <td style="width:79px;"><p align="right">123.99</p></td>
      <td style="width:60px;"><p align="right">4.68</p></td>
      <td style="width:108px;"><p align="right">112.25-140.30</p></td>
      <td style="width:72px;"><p align="right">5.884</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
  </tbody>
</table>

<p><strong>Table<em> 2.</em></strong>&nbsp;Descriptive statistics, t-test and significance of differences between mean of contemporary and subrecent-females.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:77px;"><p align="center"><strong>Variables</strong></p></td>
      <td colspan="3" style="width:246px;"><p align="center"><strong>Contemporary- Females</strong></p></td>
      <td colspan="3" style="width:250px;"><p align="center"><strong>Subrecent- Females</strong></p></td>
      <td rowspan="2" style="width:66px;"><p align="center"><strong>t-value</strong></p></td>
      <td rowspan="2" style="width:62px;"><p align="center"><strong>Sig.</strong></p></td>
    </tr>
    <tr>
      <td style="width:72px;height:22px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;height:22px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;height:22px;"><p align="center"><strong>Min.-Max.</strong></p></td>
      <td style="width:76px;height:22px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;height:22px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;height:22px;"><p align="center"><strong>Min.-Max.</strong></p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxCLt</p></td>
      <td style="width:72px;"><p align="right">171.67</p></td>
      <td style="width:60px;"><p align="right">6.54</p></td>
      <td style="width:114px;"><p align="right">160-183.4</p></td>
      <td style="width:76px;"><p align="right">171.32</p></td>
      <td style="width:60px;"><p align="right">5.65</p></td>
      <td style="width:114px;"><p align="right">152.3-185.60</p></td>
      <td style="width:66px;"><p align="right">.338</p></td>
      <td style="width:62px;"><p align="right">.736</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxCBr</p></td>
      <td style="width:72px;"><p align="right">127.29</p></td>
      <td style="width:60px;"><p align="right">4.11</p></td>
      <td style="width:114px;"><p align="right">120.5-137.13</p></td>
      <td style="width:76px;"><p align="right">123.08</p></td>
      <td style="width:60px;"><p align="right">4.85</p></td>
      <td style="width:114px;"><p align="right">110.0-135.60</p></td>
      <td style="width:66px;"><p align="right">5.286</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>BBrHt</p></td>
      <td style="width:72px;"><p align="right">129.8</p></td>
      <td style="width:60px;"><p align="right">4.33</p></td>
      <td style="width:114px;"><p align="right">121.3-137.90</p></td>
      <td style="width:76px;"><p align="right">125.80</p></td>
      <td style="width:60px;"><p align="right">4.68</p></td>
      <td style="width:114px;"><p align="right">114.5-137.9</p></td>
      <td style="width:66px;"><p align="right">5.098</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxFBr</p></td>
      <td style="width:72px;"><p align="right">109.18</p></td>
      <td style="width:60px;"><p align="right">4.37</p></td>
      <td style="width:114px;"><p align="right">102.33-118.97</p></td>
      <td style="width:76px;"><p align="right">106.23</p></td>
      <td style="width:60px;"><p align="right">3.43</p></td>
      <td style="width:114px;"><p align="right">97.9-116.8</p></td>
      <td style="width:66px;"><p align="right">4.636</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MinFBr</p></td>
      <td style="width:72px;"><p align="right">90.84</p></td>
      <td style="width:60px;"><p align="right">3.50</p></td>
      <td style="width:114px;"><p align="right">82.34-98.33</p></td>
      <td style="width:76px;"><p align="right">87.89</p></td>
      <td style="width:60px;"><p align="right">3.54</p></td>
      <td style="width:114px;"><p align="right">78.45-96.10</p></td>
      <td style="width:66px;"><p align="right">4.872</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>ABrHt</p></td>
      <td style="width:72px;"><p align="right">122.05</p></td>
      <td style="width:60px;"><p align="right">3.06</p></td>
      <td style="width:114px;"><p align="right">115.6-127.53</p></td>
      <td style="width:76px;"><p align="right">118.82</p></td>
      <td style="width:60px;"><p align="right">3.82</p></td>
      <td style="width:114px;"><p align="right">107.90-128.90</p></td>
      <td style="width:66px;"><p align="right">5.207</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>NHt</p></td>
      <td style="width:72px;"><p align="right">46.47</p></td>
      <td style="width:60px;"><p align="right">3.44</p></td>
      <td style="width:114px;"><p align="right">39.21-53.04</p></td>
      <td style="width:76px;"><p align="right">45.61</p></td>
      <td style="width:60px;"><p align="right">2.62</p></td>
      <td style="width:114px;"><p align="right">39.05-53.40</p></td>
      <td style="width:66px;"><p align="right">1.743</p></td>
      <td style="width:62px;"><p align="right">.083</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>NBr</p></td>
      <td style="width:72px;"><p align="right">24.08</p></td>
      <td style="width:60px;"><p align="right">1.34</p></td>
      <td style="width:114px;"><p align="right">21.33-26.92</p></td>
      <td style="width:76px;"><p align="right">22.99</p></td>
      <td style="width:60px;"><p align="right">1.80</p></td>
      <td style="width:114px;"><p align="right">18.40-28.00</p></td>
      <td style="width:66px;"><p align="right">3.763</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>OHt</p></td>
      <td style="width:72px;"><p align="right">32.53</p></td>
      <td style="width:60px;"><p align="right">1.75</p></td>
      <td style="width:114px;"><p align="right">29.43-35.56</p></td>
      <td style="width:76px;"><p align="right">31.72</p></td>
      <td style="width:60px;"><p align="right">2.01</p></td>
      <td style="width:114px;"><p align="right">25.30-36.33</p></td>
      <td style="width:66px;"><p align="right">2.455</p></td>
      <td style="width:62px;"><p align="right">.015*</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>OBr</p></td>
      <td style="width:72px;"><p align="right">37.83</p></td>
      <td style="width:60px;"><p align="right">1.20</p></td>
      <td style="width:114px;"><p align="right">35.06-40.05</p></td>
      <td style="width:76px;"><p align="right">36.91</p></td>
      <td style="width:60px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">31.38-40.40</p></td>
      <td style="width:66px;"><p align="right">2.977</p></td>
      <td style="width:62px;"><p align="right">.003**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>IOBr</p></td>
      <td style="width:72px;"><p align="right">19.54</p></td>
      <td style="width:60px;"><p align="right">1.82</p></td>
      <td style="width:114px;"><p align="right">15.41-22.57</p></td>
      <td style="width:76px;"><p align="right">18.28</p></td>
      <td style="width:60px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">14.1-24.25</p></td>
      <td style="width:66px;"><p align="right">3.758</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FLt</p></td>
      <td style="width:72px;"><p align="right">92.56</p></td>
      <td style="width:60px;"><p align="right">4.03</p></td>
      <td style="width:114px;"><p align="right">83.57-101.83</p></td>
      <td style="width:76px;"><p align="right">89.29</p></td>
      <td style="width:60px;"><p align="right">4.60</p></td>
      <td style="width:114px;"><p align="right">77.4-100.45</p></td>
      <td style="width:66px;"><p align="right">4.305</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>CBLt</p></td>
      <td style="width:72px;"><p align="right">96.99</p></td>
      <td style="width:60px;"><p align="right">3.70</p></td>
      <td style="width:114px;"><p align="right">89.07-102.73</p></td>
      <td style="width:76px;"><p align="right">94.69</p></td>
      <td style="width:60px;"><p align="right">4.04</p></td>
      <td style="width:114px;"><p align="right">82.4-104.50</p></td>
      <td style="width:66px;"><p align="right">3.404</p></td>
      <td style="width:62px;"><p align="right">.001**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FMBr</p></td>
      <td style="width:72px;"><p align="right">27.75</p></td>
      <td style="width:60px;"><p align="right">1.68</p></td>
      <td style="width:114px;"><p align="right">24.33-30.21</p></td>
      <td style="width:76px;"><p align="right">27.15</p></td>
      <td style="width:60px;"><p align="right">2.20</p></td>
      <td style="width:114px;"><p align="right">22.45-33.10</p></td>
      <td style="width:66px;"><p align="right">1.704</p></td>
      <td style="width:62px;"><p align="right">.090</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FMLt</p></td>
      <td style="width:72px;"><p align="right">33.03</p></td>
      <td style="width:60px;"><p align="right">1.88</p></td>
      <td style="width:114px;"><p align="right">30.22-36.43</p></td>
      <td style="width:76px;"><p align="right">32.89</p></td>
      <td style="width:60px;"><p align="right">2.31</p></td>
      <td style="width:114px;"><p align="right">26.3-38.45</p></td>
      <td style="width:66px;"><p align="right">.390</p></td>
      <td style="width:62px;"><p align="right">.697</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>UFHt</p></td>
      <td style="width:72px;"><p align="right">63.02</p></td>
      <td style="width:60px;"><p align="right">4.43</p></td>
      <td style="width:114px;"><p align="right">56.05-72.22</p></td>
      <td style="width:76px;"><p align="right">60.85</p></td>
      <td style="width:60px;"><p align="right">3.93</p></td>
      <td style="width:114px;"><p align="right">53.15-72.10</p></td>
      <td style="width:66px;"><p align="right">3.112</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>&nbsp;BZBr</p></td>
      <td style="width:72px;"><p align="right">119.99</p></td>
      <td style="width:60px;"><p align="right">4.52</p></td>
      <td style="width:114px;"><p align="right">111.87-130.33</p></td>
      <td style="width:76px;"><p align="right">115.6</p></td>
      <td style="width:60px;"><p align="right">3.88</p></td>
      <td style="width:114px;"><p align="right">107.1-124.40</p></td>
      <td style="width:66px;"><p align="right">6.301</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
  </tbody>
</table>

                <p>Figure 1 shows the graphical presentation of sexual differences and secular changes in craniofacial measurements of both males and females.</p>

                <img src="img/FSC-2-115-g001.gif">
                  <p><b>Figure 1. </b>Graphical presentation of sexual differences and secular changes in craniofacial measurements of both males and females.</p>

          <h4>Classification of North Indian crania according to indices</h4>

                <p>Table 3 provides the formulae to calculate respective index, normal range and the classification system according to respective Anthropologists and population frequency for contemporary and subrecent samples for each craniofacial index. Range is useful in providing the extent and ubiquity of shape variation within the cranial series in terms of the standard index categories used in physical anthropology. Both of the population groups showed 100% population frequency only for Auriculo vertical and Auriculo transverse index and showed a high and narrow cranial series.</p>

                <p><strong>Table 3.</strong>&nbsp;Formulae to calculate respective index, normal range and the classification system according to respective anthropologists and population frequency for contemporary and subrecent samples for each craniofacial index in North Indian population.</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:150px;"><p align="center"><strong>Name of the index</strong></p></td>
      <td style="width:150px;"><p align="center"><strong>Formula of the index</strong></p></td>
      <td style="width:138px;"><p align="center"><strong>Classification I</strong></p></td>
      <td style="width:102px;"><p align="center"><strong>Classification II</strong></p></td>
      <td style="width:126px;"><p align="center"><strong>Normal range variation</strong></p></td>
      <td style="width:59px;"><p align="center"><strong>Scientist found the normal range variation</strong></p></td>
      <td style="width:109px;"><p align="center"><strong>Contemporary population Frequency in %&nbsp; (N=158)</strong></p></td>
      <td style="width:109px;"><p align="center"><strong>Subrecent population Frequency in %&nbsp; (N=325)</strong></p></td>
    </tr>
    <tr>
      <td rowspan="7" style="width:150px;">
      <ol>
        <li align="center">Cranial index or length &ndash; breadth cranial index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="7" style="width:150px;"><p align="center">(max. cranial breadth/max. cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Ultradolichocranial</p></td>
      <td style="width:102px;"><p align="center">Very long skull</p></td>
      <td style="width:126px;"><p align="center">&le;64.99</p></td>
      <td rowspan="7" style="width:59px;"><p align="center">Garson</p></td>
      <td style="width:109px;"><p align="center">03.79</p></td>
      <td style="width:109px;"><p align="center">2.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperdolichocranial</p></td>
      <td style="width:102px;"><p align="center">Very long skull</p></td>
      <td style="width:126px;"><p align="center">65.0-69.99</p></td>
      <td style="width:109px;"><p align="center">36.71</p></td>
      <td style="width:109px;"><p align="center">36.0</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:27px;"><p align="center">Dolichocranial</p></td>
      <td style="width:102px;height:27px;"><p align="center">Long skull</p></td>
      <td style="width:126px;height:27px;"><p align="center">70.0-74.99</p></td>
      <td style="width:109px;height:27px;"><p align="center">40.51</p></td>
      <td style="width:109px;height:27px;"><p align="center">49.23</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">75.0-79.99</p></td>
      <td style="width:109px;"><p align="center">13.92</p></td>
      <td style="width:109px;"><p align="center">10.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Brachycranial</p></td>
      <td style="width:102px;"><p align="center">Broad, short skull</p></td>
      <td style="width:126px;"><p align="center">80.0-84.99</p></td>
      <td style="width:109px;"><p align="center">5.06</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperbrachycranial</p></td>
      <td style="width:102px;"><p align="center">Very broad and short skull</p></td>
      <td style="width:126px;"><p align="center">85.0-89.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:29px;"><p align="center">Ultrabrachycranial</p></td>
      <td style="width:102px;height:29px;"><p align="center">Extremely broad and short skull</p></td>
      <td style="width:126px;height:29px;"><p align="center">&ge;90.0</p></td>
      <td style="width:109px;height:29px;"><p align="center">0</p></td>
      <td style="width:109px;height:29px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td rowspan="3">
      <ol>
        <li align="center" value="2">Length &ndash; height cranial index or vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Basion &ndash; bregma height/maximum cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Chamaecranial</p></td>
      <td style="width:102px;"><p align="center">Low skull</p></td>
      <td style="width:126px;"><p align="center">&le;69.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">10.13</p></td>
      <td style="width:109px;"><p align="center">11.08</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Orthocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">70.0-74.99</p></td>
      <td style="width:109px;"><p align="center">45.57</p></td>
      <td style="width:109px;"><p align="center">58.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:16px;"><p align="center">Hypsicranial</p></td>
      <td style="width:102px;height:16px;"><p align="center">High skull</p></td>
      <td style="width:126px;height:16px;"><p align="center">&ge;75.0</p></td>
      <td style="width:109px;height:16px;"><p align="center">44.3</p></td>
      <td style="width:109px;height:16px;"><p align="center">30.15</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="3">Breadth- height index or transverse vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Basion &ndash; bregma height/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Tapeinocranial</p></td>
      <td style="width:102px;"><p align="center">Low, broad skull</p></td>
      <td style="width:126px;"><p align="center">&le;91.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">92.0-97.99</p></td>
      <td style="width:109px;"><p align="center">8.86</p></td>
      <td style="width:109px;"><p align="center">9.54</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Acrocranial</p></td>
      <td style="width:102px;"><p align="center">High, narrow skull</p></td>
      <td style="width:126px;"><p align="center">&ge;98.0</p></td>
      <td style="width:109px;"><p align="center">91.14</p></td>
      <td style="width:109px;"><p align="center">90.46</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="4">Auriculo vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Auriculo&ndash; bregmatic height/ max cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Chamaecranial</p></td>
      <td style="width:102px;"><p align="center">Low skull</p></td>
      <td style="width:126px;"><p align="center">&le;57.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Orthocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">58.0-62.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hypsicranial</p></td>
      <td style="width:102px;"><p align="center">High skull</p></td>
      <td style="width:126px;"><p align="center">&ge;63.0</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="5">Auriculo transverse index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Auriculo &ndash; bregmatic height/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Tapeinocranial</p></td>
      <td style="width:102px;"><p align="center">Low, broad skull</p></td>
      <td style="width:126px;"><p align="center">&le;79.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">80.0-85.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Acrocranial</p></td>
      <td style="width:102px;"><p align="center">High, narrow skull</p></td>
      <td style="width:126px;"><p align="center">&ge;86.0</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="6">. Frontal index /Transverse Fronto Prietal index /trans.fronto breadth index/ (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(minimum frontal breadth/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Stenometopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&le;65.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">2.53</p></td>
      <td style="width:109px;"><p align="center">2.46</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriometopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">66.0-68.99</p></td>
      <td style="width:109px;"><p align="center">12.66</p></td>
      <td style="width:109px;"><p align="center">14.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Eurymetopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&ge;69.0</p></td>
      <td style="width:109px;"><p align="center">84.81</p></td>
      <td style="width:109px;"><p align="center">82.77</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;height:20px;"><p align="center">7. Foramen magnum index (Singh and Bhasin, 2004)</p></td>
      <td rowspan="3" style="width:150px;height:20px;"><p align="center">(breadth of foramen magnum/ length of foramen magnum)*100</p></td>
      <td style="width:138px;height:20px;"><p align="center">Narrow</p></td>
      <td rowspan="3" style="width:102px;height:20px;"><p align="center">-----</p></td>
      <td style="width:126px;height:20px;"><p align="center">&le;81.99</p></td>
      <td rowspan="3" style="width:59px;height:20px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;height:20px;"><p align="center">&nbsp;37.97.</p></td>
      <td style="width:109px;height:20px;"><p align="center">44.31</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:20px;"><p align="center">Medium</p></td>
      <td style="width:126px;height:20px;"><p align="center">82.0-85.9</p></td>
      <td style="width:109px;height:20px;"><p align="center">24.05</p></td>
      <td style="width:109px;height:20px;"><p align="center">26.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:20px;"><p align="center">Broad</p></td>
      <td style="width:126px;height:20px;"><p align="center">&ge;86.0</p></td>
      <td style="width:109px;height:20px;"><p align="center">37.97</p></td>
      <td style="width:109px;height:20px;"><p align="center">28.92</p></td>
    </tr>
    <tr>
      <td rowspan="5" style="width:150px;height:44px;"><p>8. Upper facial index (Krogman and Iscan, 1986)</p></td>
      <td rowspan="5" style="width:150px;height:44px;"><p align="center">(upper facial height/ bizygomatic breadth)*100</p></td>
      <td style="width:138px;height:44px;"><p align="center">Hypereuryen</p></td>
      <td style="width:102px;height:44px;"><p align="center">Very short upper face(very broad face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:44px;"><p align="center">&le;44.99</p></td>
      <td rowspan="5" style="width:59px;height:44px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;height:44px;"><p align="center">01.26</p></td>
      <td style="width:109px;height:44px;"><p align="center">0.6</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Euryen</p></td>
      <td style="width:102px;"><p align="center">Short upper face (Broad face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">45.0-49.99</p></td>
      <td style="width:109px;"><p align="center">18.35</p></td>
      <td style="width:109px;"><p align="center">20.92</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesen</p></td>
      <td style="width:102px;"><p align="center">Medium upper face (Round face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">50.0-54.99</p></td>
      <td style="width:109px;"><p align="center">63.29</p></td>
      <td style="width:109px;"><p align="center">52.31</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Lepten</p></td>
      <td style="width:102px;"><p align="center">Long upper face (long face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">55.0-59.99</p></td>
      <td style="width:109px;"><p align="center">14.56</p></td>
      <td style="width:109px;"><p align="center">24.62</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:47px;"><p align="center">Hyperlepten</p></td>
      <td style="width:102px;height:47px;"><p align="center">Very long upper face (very long face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:47px;"><p align="center">&ge;60.0</p></td>
      <td style="width:109px;height:47px;"><p align="center">02.53</p></td>
      <td style="width:109px;height:47px;"><p align="center">1.54</p></td>
    </tr>
    <tr>
      <td rowspan="4" style="width:150px;"><p>9.Nasal index (Singh and Bhasin, 2004)</p></td>
      <td rowspan="4" style="width:150px;"><p align="center">(nasal breadth/ nasal height)*100</p></td>
      <td style="width:138px;"><p align="center">Leptorhinae</p></td>
      <td style="width:102px;"><p align="center">Narrow Nose</p></td>
      <td style="width:126px;"><p align="center">&le;46.99</p></td>
      <td rowspan="4" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">18.99</p></td>
      <td style="width:109px;"><p align="center">22.15</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesorhinae</p></td>
      <td style="width:102px;"><p align="center">Medium nose</p></td>
      <td style="width:126px;"><p align="center">47.0-50.99</p></td>
      <td style="width:109px;"><p align="center">29.11</p></td>
      <td style="width:109px;"><p align="center">31.08</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Chamaerhinae</p></td>
      <td style="width:102px;"><p align="center">Broad nose</p></td>
      <td style="width:126px;"><p align="center">51.0-57.99</p></td>
      <td style="width:109px;"><p align="center">43.04</p></td>
      <td style="width:109px;"><p align="center">39.38</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperchamaerhinae</p></td>
      <td style="width:102px;"><p align="center">Very broad nose</p></td>
      <td style="width:126px;"><p align="center">&ge;58.0</p></td>
      <td style="width:109px;"><p align="center">8.86</p></td>
      <td style="width:109px;"><p align="center">7.38</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li value="10">Orbital index (Krogman and Iscan, 1986)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(max. orbital hieght/ maximum orbital breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Chmaeconch</p></td>
      <td style="width:102px;"><p align="center">Low</p></td>
      <td style="width:126px;"><p align="center">&le;82.9</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Broca</p></td>
      <td style="width:109px;"><p align="center">39.24</p></td>
      <td style="width:109px;"><p align="center">29.85</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesoconch</p></td>
      <td style="width:102px;"><p align="center">Medium</p></td>
      <td style="width:126px;"><p align="center">83.0-88.9</p></td>
      <td style="width:109px;"><p align="center">43.04</p></td>
      <td style="width:109px;"><p align="center">43.38</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hypsiconch</p></td>
      <td style="width:102px;"><p align="center">High</p></td>
      <td style="width:126px;"><p align="center">&ge;89.0</p></td>
      <td style="width:109px;"><p align="center">15.19</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">26.77</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="11">Gnathic index (Krogman and Iscan, 1986)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(basion &ndash; prosthion distance/basion nasion distance)*100</p></td>
      <td style="width:138px;"><p align="center">Orthognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&le;97.9</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">82.28</p></td>
      <td style="width:109px;"><p align="center">84.62</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">98.0-102.9</p></td>
      <td style="width:109px;"><p align="center">13.92</p></td>
      <td style="width:109px;"><p align="center">14.46</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Prognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&ge;103.0</p></td>
      <td style="width:109px;"><p align="center">3.8</p></td>
      <td style="width:109px;"><p align="center">0.92</p></td>
    </tr>
    <tr>
      <td rowspan="5" style="width:150px;height:35px;">
      <ol>
        <li align="center" value="12">Jugo-frontal index (Wilder, 1920)</li>
      </ol></td>
      <td rowspan="5" style="width:150px;height:35px;"><p align="center">(Minimum frontal breadth/ Bizygomatic breadth)* 100</p></td>
      <td rowspan="5" style="width:138px;height:35px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;height:35px;"><p align="center">Very narrow</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:35px;"><p align="center">x-69.99, x-71.99</p>

      <p align="center">&nbsp;</p></td>
      <td rowspan="5" style="width:59px;height:35px;"><p align="center">&nbsp;Martin</p></td>
      <td style="width:109px;height:35px;"><p>13.92</p></td>
      <td style="width:109px;height:35px;"><p>8.62</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Narrow</p></td>
      <td style="width:126px;"><p align="center">70-74.99,72-76.99</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>53.16</p></td>
      <td style="width:109px;"><p>54.15</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Moderate</p></td>
      <td style="width:126px;"><p align="center">75-79.99,77-81.99</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>30.38</p></td>
      <td style="width:109px;"><p>&nbsp;34.77</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Wide</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">80-84.99, 82-86.99</p></td>
      <td style="width:109px;"><p>2.53</p></td>
      <td style="width:109px;"><p>2.46</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Very wide</p></td>
      <td style="width:126px;"><p align="center">85-x, 87-x</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p>0</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>13. Nasal width &ndash; facial height index (Cameron, 1929)**</p></td>
      <td style="width:150px;"><p align="center">(nasal breadth/upper facial height)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>14.Interorbital breadth &ndash; facial height index&nbsp; (Cameron, 1929)**</p></td>
      <td style="width:150px;"><p align="center">(interorbital breadth/ upper facial height)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>15. Transverse frontal index (Wilder, 1920**)</p></td>
      <td style="width:150px;"><p align="center">(Min frontal breadth/Max frontal breadth)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>16.Trans. cranio-facial index (Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">Bizygomatic breadth/ Maximum cranial breadth)* 100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>17. Longitudinal craniofacial index(Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">(Basion &ndash;prosthion length/ Max cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>18.Vertical craniofacial index (Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">(Nasion &ndash;prosthion/basion-bregma)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<p># The index classification is slightly modified. The range for indices is modified by adding .09 in higher range of each category. so that the population can be classified according to these classification. ** the classification range was not available for these variables.</p>

          <h4>Sexual dimorphism in craniometric indices</h4>

                <p>Tables 4 and 5 shows mean craniometric indices for contemporary and subrecent population respectively with t&rsquo;test and sex classification accuracies. The sex classification accuracies were low so discriminant functions are not provided in the tables. Indices for males and females calculated separately using respective formulae. Contemporary sample shows significant sexual differences in twelve indices while subrecent sample shows differences only in seven indices. The t-test and classification accuracies revealed that even after highly significant differences between males and females of both population groups the sex classification accuracies are not of much forensic significance.</p>

                <p><strong>Table 4.</strong>&nbsp;Craniometric Indices for Contemporary North Indian sample with significance (P value) and sexing classification accuracy.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:209px;height:22px;"><p align="center"><strong>Indices</strong></p></td>
      <td style="width:82px;height:22px;"><p align="center"><strong>Male</strong></p></td>
      <td style="width:99px;height:22px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:83px;height:22px;"><p align="center"><strong>Female</strong></p></td>
      <td style="width:94px;height:22px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:77px;height:22px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:82px;height:22px;"><p align="center"><strong>Sexing accuracy (%)</strong></p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Cranial index</p></td>
      <td style="width:82px;height:23px;"><p align="center">70.35</p></td>
      <td style="width:99px;height:23px;"><p align="center">63.35-84.65</p></td>
      <td style="width:83px;height:23px;"><p align="center">74.26</p></td>
      <td style="width:94px;height:23px;"><p align="center">68.92-82.23</p></td>
      <td style="width:77px;height:23px;"><p align="center">.000***</p></td>
      <td style="width:82px;height:23px;"><p align="center">72.2</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Vertical index</p></td>
      <td style="width:82px;height:22px;"><p align="center">73.62</p></td>
      <td style="width:99px;height:22px;"><p align="center">65.96-85.09</p></td>
      <td style="width:83px;height:22px;"><p align="center">75.67</p></td>
      <td style="width:94px;height:22px;"><p align="center">70.12-82.03</p></td>
      <td style="width:77px;height:22px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:22px;"><p align="center">60.8</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Transverse vertical Index</p></td>
      <td style="width:82px;height:22px;"><p align="center">104.76</p></td>
      <td style="width:99px;height:22px;"><p align="center">92.34-116.32</p></td>
      <td style="width:83px;height:22px;"><p align="center">102.06</p></td>
      <td style="width:94px;height:22px;"><p align="center">93.7-109.5</p></td>
      <td style="width:77px;height:22px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:22px;"><p align="center">62.0</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Auriculo vertical index</p></td>
      <td style="width:82px;height:23px;"><p align="center">69.49</p></td>
      <td style="width:99px;height:23px;"><p align="center">64.25-79.80</p></td>
      <td style="width:83px;height:23px;"><p align="center">71.17</p></td>
      <td style="width:94px;height:23px;"><p align="center">65.58-75.63</p></td>
      <td style="width:77px;height:23px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:23px;"><p align="center">63.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:19px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:82px;height:19px;"><p align="center">98.91</p></td>
      <td style="width:99px;height:19px;"><p align="center">91.16-110.49</p></td>
      <td style="width:83px;height:19px;"><p align="center">95.98</p></td>
      <td style="width:94px;height:19px;"><p align="center">89.73-104.32</p></td>
      <td style="width:77px;height:19px;"><p align="center">.000***</p></td>
      <td style="width:82px;height:19px;"><p align="center">60.8</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Frontal index</p></td>
      <td style="width:82px;height:23px;"><p align="center">72.82</p></td>
      <td style="width:99px;height:23px;"><p align="center">65.82-83.84</p></td>
      <td style="width:83px;height:23px;"><p align="center">71.41</p></td>
      <td style="width:94px;height:23px;"><p align="center">63.81-78.72</p></td>
      <td style="width:77px;height:23px;"><p align="center">.022*</p></td>
      <td style="width:82px;height:23px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:21px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:82px;height:21px;"><p align="center">84.89</p></td>
      <td style="width:99px;height:21px;"><p align="center">75.09-105.25</p></td>
      <td style="width:83px;height:21px;"><p align="center">84.24</p></td>
      <td style="width:94px;height:21px;"><p align="center">70.24-96.26</p></td>
      <td style="width:77px;height:21px;"><p align="center">.531</p></td>
      <td style="width:82px;height:21px;"><p align="center">44.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Upper Facial index</p></td>
      <td style="width:82px;height:23px;"><p align="center">52.55</p></td>
      <td style="width:99px;height:23px;"><p align="center">44.87-61.97</p></td>
      <td style="width:83px;height:23px;"><p align="center">52.51</p></td>
      <td style="width:94px;height:23px;"><p align="center">46.03-57.62</p></td>
      <td style="width:77px;height:23px;"><p align="center">.929</p></td>
      <td style="width:82px;height:23px;"><p align="center">35.4</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Nasal index</p></td>
      <td style="width:82px;height:23px;"><p align="center">51.10</p></td>
      <td style="width:99px;height:23px;"><p align="center">41.28-64.11</p></td>
      <td style="width:83px;height:23px;"><p align="center">52.06</p></td>
      <td style="width:94px;height:23px;"><p align="center">44.36-61.39</p></td>
      <td style="width:77px;height:23px;"><p align="center">.242</p></td>
      <td style="width:82px;height:23px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Posterior Orbital index</p></td>
      <td style="width:82px;height:22px;"><p align="center">83.52</p></td>
      <td style="width:99px;height:22px;"><p align="center">70.74-95.16</p></td>
      <td style="width:83px;height:22px;"><p align="center">86.09</p></td>
      <td style="width:94px;height:22px;"><p align="center">77.8-99.98</p></td>
      <td style="width:77px;height:22px;"><p align="center">.006**</p></td>
      <td style="width:82px;height:22px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Gnathic index</p></td>
      <td style="width:82px;height:23px;"><p align="center">94.58</p></td>
      <td style="width:99px;height:23px;"><p align="center">85.67-106.37</p></td>
      <td style="width:83px;height:23px;"><p align="center">95.5</p></td>
      <td style="width:94px;height:23px;"><p align="center">89.8-104.19</p></td>
      <td style="width:77px;height:23px;"><p align="center">.192</p></td>
      <td style="width:82px;height:23px;"><p align="center">48.1</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Jugo frontal</p></td>
      <td style="width:82px;height:23px;"><p align="center">73.45</p></td>
      <td style="width:99px;height:23px;"><p align="center">64.49-82.69</p></td>
      <td style="width:83px;height:23px;"><p align="center">75.74</p></td>
      <td style="width:94px;height:23px;"><p align="center">70.91-80.46</p></td>
      <td style="width:77px;height:23px;"><p align="center">0.000***</p></td>
      <td style="width:82px;height:23px;"><p align="center">56.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Nasal breadth-facial ht index</p></td>
      <td style="width:82px;height:23px;"><p align="center">38.17</p></td>
      <td style="width:99px;height:23px;"><p align="center">31.29-49.59</p></td>
      <td style="width:83px;height:23px;"><p align="center">38.34</p></td>
      <td style="width:94px;height:23px;"><p align="center">31.69-43.28</p></td>
      <td style="width:77px;height:23px;"><p align="center">.780</p></td>
      <td style="width:82px;height:23px;"><p align="center">55.7</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Interorbital breadth-facial ht index</p></td>
      <td style="width:82px;height:20px;"><p align="center">30.95</p></td>
      <td style="width:99px;height:20px;"><p align="center">25.45-42.36</p></td>
      <td style="width:83px;height:20px;"><p align="center">31.09</p></td>
      <td style="width:94px;height:20px;"><p align="center">24.09-35.65</p></td>
      <td style="width:77px;height:20px;"><p align="center">.094</p></td>
      <td style="width:82px;height:20px;"><p align="center">50.0</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Transverse frontal index</p></td>
      <td style="width:82px;height:20px;"><p align="center">81.60</p></td>
      <td style="width:99px;height:20px;"><p align="center">74.43-89.19</p></td>
      <td style="width:83px;height:20px;"><p align="center">83.23</p></td>
      <td style="width:94px;height:20px;"><p align="center">79.49-88.19</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.003**</p></td>
      <td style="width:82px;height:20px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Transverse craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">99.24</p></td>
      <td style="width:99px;height:20px;"><p align="center">88.13-111.1</p></td>
      <td style="width:83px;height:20px;"><p align="center">94.35</p></td>
      <td style="width:94px;height:20px;"><p align="center">87.64-104.34</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.000***</p></td>
      <td style="width:82px;height:20px;"><p align="center">58.2</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">52.74</p></td>
      <td style="width:99px;height:20px;"><p align="center">46.74-57.89</p></td>
      <td style="width:83px;height:20px;"><p align="center">53.96</p></td>
      <td style="width:94px;height:20px;"><p align="center">49.56-60.00</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.005**</p></td>
      <td style="width:82px;height:20px;"><p align="center">52.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Vertical craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">49.81</p></td>
      <td style="width:99px;height:20px;"><p align="center">48.01-58.71</p></td>
      <td style="width:83px;height:20px;"><p align="center">48.56</p></td>
      <td style="width:94px;height:20px;"><p align="center">42.70-55.49</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.0287*</p></td>
      <td style="width:82px;height:20px;"><p align="center">56.3</p></td>
    </tr>
  </tbody>
</table>

<p><em>*p&lt;.05 Significant, **p&lt;.01Moderate Significant, ***P&lt;.001Highly Significant</em></p>


<p><strong>Table 5.</strong>&nbsp;Craniometric Indices for Subrecent North Indian sample with significance (P value) and sexing classification accuracy.</p>


<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:245px;"><p align="center"><strong>Indices</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>Male</strong></p></td>
      <td style="width:99px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:72px;"><p align="center"><strong>Female</strong></p></td>
      <td style="width:96px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:137px;"><p align="center"><strong>Sexing Accuracy (%)</strong></p></td>
    </tr>
    <tr>
      <td style="width:245px;height:19px;"><p>Cranial index</p></td>
      <td style="width:60px;height:19px;"><p align="center">70.20</p></td>
      <td style="width:99px;height:19px;"><p align="center">63.47-77.74</p></td>
      <td style="width:72px;height:19px;"><p align="center">71.89</p></td>
      <td style="width:96px;height:19px;"><p align="center">62.97-79.45</p></td>
      <td style="width:60px;height:19px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:19px;"><p align="center">62.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:24px;"><p>Vertical index</p></td>
      <td style="width:60px;height:24px;"><p align="center">73.32</p></td>
      <td style="width:99px;height:24px;"><p align="center">66.89-79.86</p></td>
      <td style="width:72px;height:24px;"><p align="center">73.46</p></td>
      <td style="width:96px;height:24px;"><p align="center">66.69-78.69</p></td>
      <td style="width:60px;height:24px;"><p align="center">.648</p></td>
      <td style="width:137px;height:24px;"><p align="center">51.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Transverse vertical Index</p></td>
      <td style="width:60px;"><p align="center">104.54</p></td>
      <td style="width:99px;"><p align="center">92.44-115.18</p></td>
      <td style="width:72px;"><p align="center">102.31</p></td>
      <td style="width:96px;"><p align="center">93.26-117.27</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">60.3</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Auriculo vertical index</p></td>
      <td style="width:60px;"><p align="center">69.25</p></td>
      <td style="width:99px;"><p align="center">63.69-75.07</p></td>
      <td style="width:72px;"><p align="center">69.39</p></td>
      <td style="width:96px;"><p align="center">63.38-75.06</p></td>
      <td style="width:60px;"><p align="center">.563</p></td>
      <td style="width:137px;"><p align="center">51.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:60px;"><p align="center">98.73</p></td>
      <td style="width:99px;"><p align="center">90.78-107.27</p></td>
      <td style="width:72px;"><p align="center">96.62</p></td>
      <td style="width:96px;"><p align="center">89.78-107.89</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">61.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Frontal index</p></td>
      <td style="width:60px;"><p align="center">72.85</p></td>
      <td style="width:99px;"><p align="center">62.39-82.50</p></td>
      <td style="width:72px;"><p align="center">71.5</p></td>
      <td style="width:96px;"><p align="center">65.18-82.22</p></td>
      <td style="width:60px;"><p align="center">.001**</p></td>
      <td style="width:137px;"><p align="center">59.4</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:60px;"><p align="center">83.12</p></td>
      <td style="width:99px;"><p align="center">68.95-107.52</p></td>
      <td style="width:72px;"><p align="center">82.64</p></td>
      <td style="width:96px;"><p align="center">70.89-95.68</p></td>
      <td style="width:60px;"><p align="center">.481</p></td>
      <td style="width:137px;"><p align="center">50.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Upper Facial index</p></td>
      <td style="width:60px;"><p align="center">52.79</p></td>
      <td style="width:99px;"><p align="center">44.69-65.17</p></td>
      <td style="width:72px;"><p align="center">52.66</p></td>
      <td style="width:96px;"><p align="center">44.33-61.30</p></td>
      <td style="width:60px;"><p align="center">.737</p></td>
      <td style="width:137px;"><p align="center">49.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Nasal index</p></td>
      <td style="width:60px;"><p align="center">50.85</p></td>
      <td style="width:99px;"><p align="center">38.16-63.88</p></td>
      <td style="width:72px;"><p align="center">50.56</p></td>
      <td style="width:96px;"><p align="center">40.88-64.81</p></td>
      <td style="width:60px;"><p align="center">.616</p></td>
      <td style="width:137px;"><p align="center">50.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Orbital index</p></td>
      <td style="width:60px;"><p align="center">85.41</p></td>
      <td style="width:99px;"><p align="center">69.83-103.19</p></td>
      <td style="width:72px;"><p align="center">86.05</p></td>
      <td style="width:96px;"><p align="center">72.15-100</p></td>
      <td style="width:60px;"><p align="center">.314</p></td>
      <td style="width:137px;"><p align="center">55.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Gnathic index</p></td>
      <td style="width:60px;"><p align="center">93.67</p></td>
      <td style="width:99px;"><p align="center">83.13-103.32</p></td>
      <td style="width:72px;"><p align="center">94.35</p></td>
      <td style="width:96px;"><p align="center">83.78-104.55</p></td>
      <td style="width:60px;"><p align="center">.141</p></td>
      <td style="width:137px;"><p align="center">54.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Jugo frontal</p></td>
      <td style="width:60px;"><p align="center">73.99</p></td>
      <td style="width:99px;"><p align="center">58.93-82.29</p></td>
      <td style="width:72px;"><p align="center">76.06</p></td>
      <td style="width:96px;"><p align="center">67.39-82.85</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">53.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:23px;"><p>Nasal breadth-facial height index</p></td>
      <td style="width:60px;height:23px;"><p align="center">37.89</p></td>
      <td style="width:99px;height:23px;"><p align="center">27.53-49.60</p></td>
      <td style="width:72px;height:23px;"><p align="center">37.94</p></td>
      <td style="width:96px;height:23px;"><p align="center">29.35-49.12</p></td>
      <td style="width:60px;height:23px;"><p align="center">.925</p></td>
      <td style="width:137px;height:23px;"><p align="center">45.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Interorbital breadth-facial height index</p></td>
      <td style="width:60px;height:21px;"><p align="center">30.27</p></td>
      <td style="width:99px;height:21px;"><p align="center">22.93-41.67</p></td>
      <td style="width:72px;height:21px;"><p align="center">30.16</p></td>
      <td style="width:96px;height:21px;"><p align="center">19.8-42.36</p></td>
      <td style="width:60px;height:21px;"><p align="center">.794</p></td>
      <td style="width:137px;height:21px;"><p align="center">49.4</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Transverse frontal index</p></td>
      <td style="width:60px;height:21px;">
      <table border="0" cellpadding="0" cellspacing="3">
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>

      <p align="center">82.8</p></td>
      <td style="width:99px;height:21px;"><p align="center">75.84-92.01</p></td>
      <td style="width:72px;height:21px;"><p align="center">82.76</p></td>
      <td style="width:96px;height:21px;"><p align="center">76.05-90.45</p></td>
      <td style="width:60px;height:21px;"><p align="center">.884</p></td>
      <td style="width:137px;height:21px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Transverse craniofacial index</p></td>
      <td style="width:60px;height:21px;"><p align="center">98.52</p></td>
      <td style="width:99px;height:21px;"><p align="center">87.89-110.73</p></td>
      <td style="width:72px;height:21px;"><p align="center">94.04</p></td>
      <td style="width:96px;height:21px;"><p align="center">83.73-107.47</p></td>
      <td style="width:60px;height:21px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:21px;"><p align="center">52.0</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:60px;height:21px;"><p align="center">52.14</p></td>
      <td style="width:99px;height:21px;"><p align="center">45.30-58.68</p></td>
      <td style="width:72px;height:21px;"><p align="center">52.15</p></td>
      <td style="width:96px;height:21px;"><p align="center">45.56-61.09</p></td>
      <td style="width:60px;height:21px;"><p align="center">.986</p></td>
      <td style="width:137px;height:21px;"><p align="center">49.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Vertical craniofacial index</p></td>
      <td style="width:60px;height:21px;">
      <table border="0" cellpadding="0" cellspacing="3">
        <tbody>
          <tr>
            <td>
            <p align="center">49.76</p>
            </td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>

      <p align="center">&nbsp;</p></td>
      <td style="width:99px;height:21px;"><p align="center">40-95-60.05</p></td>
      <td style="width:72px;height:21px;"><p align="center">48.40</p></td>
      <td style="width:96px;height:21px;"><p align="center">40-89-58-19</p></td>
      <td style="width:60px;height:21px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:21px;"><p align="center">55.7</p></td>
    </tr>
  </tbody>
</table>

<p><em>*p&lt;.05 Significant, **p&lt;.01Moderate Significant, ***P&lt;.001Highly Significant</em></p>

          <h4>Secular changes in craniofacial indices</h4>

                <p>Table 6 shows differences in males of contemporary and subrecent population while Table 7 shows differences in females. The population differences were less pronounced than the sexual. In males subtle changes were observed in foraman magnum index and gnathic index while obvious changes were observed in orbital index and transverse frontal index. Here it is worth mentioning that, in both males and females showed significant secular changes in their craniofacial dimensions (Table 1 and 2) but differences between craniofacial indices were limited to only few indices. The posterior orbital index of the contemporary males showed a clear trend towards Chmaeconch (low) orbits. On the other hand, contemporary females showed a trend towards Brachycranial (broad and short) and Hypsicranial (high skull<em> i.e., </em>greater BBrHt) profile. The moderate to large values of &ldquo;d&rdquo; for cranial index, vertical index, auriculo-vertical index and longitudinal craniofacial indices confirm that the difference between these females of two population groups is large enough&nbsp;and&nbsp;consistent enough to be really important. In females, the observed differences in mean values seems very high (e.g. foraman magnum index and nasal index), but there is no significance. The effect size for these variables is 0.271 and 0.322 which suggest further scrutiny of these indices. The larger effect size in females implies greater changes in their craniofacial form.</p>

                <p><strong>Table 6.</strong>&nbsp;Significance of difference between craniometric indices of contemporary and subrecent male sample with effect size.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:207px;height:20px;"><p align="center"><strong>Index</strong></p></td>
      <td colspan="2" style="width:198px;height:20px;"><p align="center"><strong>Males (Mean &plusmn; SD)</strong></p></td>
      <td rowspan="2" style="width:72px;height:20px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:78px;height:20px;"><p align="center"><strong>Effect Size</strong></p></td>
    </tr>
    <tr>
      <td style="width:102px;height:18px;"><p align="center"><strong>Contemporary</strong></p></td>
      <td style="width:96px;height:18px;"><p align="center"><strong>Subrecent</strong></p></td>
      <td style="width:78px;height:18px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:18px;"><p>Cranial index</p></td>
      <td style="width:102px;height:18px;"><p align="center">70.35 &plusmn; 3.83</p></td>
      <td style="width:96px;height:18px;"><p align="center">70.2 &plusmn; 2.91</p></td>
      <td style="width:72px;height:18px;"><p align="center">.697</p></td>
      <td style="width:78px;height:18px;"><p align="center">.044</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:17px;"><p>Vertical index</p></td>
      <td style="width:102px;height:17px;"><p align="center">73.62 &plusmn; 3.85</p></td>
      <td style="width:96px;height:17px;"><p align="center">73.32 &plusmn; 2.80</p></td>
      <td style="width:72px;height:17px;"><p align="center">.437</p></td>
      <td style="width:78px;height:17px;"><p align="center">.087</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:13px;"><p>Transverse vertical Index</p></td>
      <td style="width:102px;height:13px;"><p align="center">104.76 &plusmn; 4.80</p></td>
      <td style="width:96px;height:13px;"><p align="center">104.54 &plusmn; 4.17</p></td>
      <td style="width:72px;height:13px;"><p align="center">.674</p></td>
      <td style="width:78px;height:13px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Auriculo vertical index</p></td>
      <td style="width:102px;"><p align="center">69.49 &plusmn; 2.97</p></td>
      <td style="width:96px;"><p align="center">69.25 &plusmn; 2.20</p></td>
      <td style="width:72px;"><p align="center">.403</p></td>
      <td style="width:78px;"><p align="center">.095</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:102px;"><p align="center">98.91 &plusmn; 3.88</p></td>
      <td style="width:96px;"><p align="center">98.73 &plusmn; 3.32</p></td>
      <td style="width:72px;"><p align="center">.669</p></td>
      <td style="width:78px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Frontal index</p></td>
      <td style="width:102px;"><p align="center">72.82 &plusmn; 3.74</p></td>
      <td style="width:96px;"><p align="center">72.85 &plusmn; 3.62</p></td>
      <td style="width:72px;"><p align="center">.944</p></td>
      <td style="width:78px;"><p align="center">.00</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:102px;"><p align="center">84.89 &plusmn; 5.78</p></td>
      <td style="width:96px;"><p align="center">83.12 &plusmn; 6.28</p></td>
      <td style="width:72px;"><p align="center">.015*</p></td>
      <td style="width:78px;"><p align="center">.294</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Upper Facial index</p></td>
      <td style="width:102px;"><p align="center">52.55 &plusmn; 2.93</p></td>
      <td style="width:96px;"><p align="center">52.79 &plusmn; 3.55</p></td>
      <td style="width:72px;"><p align="center">.546</p></td>
      <td style="width:78px;"><p align="center">.074</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Nasal index</p></td>
      <td style="width:102px;"><p align="center">51.10 &plusmn; 4.87</p></td>
      <td style="width:96px;"><p align="center">50.85 &plusmn; 4.91</p></td>
      <td style="width:72px;"><p align="center">.665</p></td>
      <td style="width:78px;"><p align="center">.051</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:15px;"><p>Orbital index</p></td>
      <td style="width:102px;height:15px;"><p align="center">83.52 &plusmn; 5.21</p></td>
      <td style="width:96px;height:15px;"><p align="center">85.41 &plusmn; 5.67</p></td>
      <td style="width:72px;height:15px;"><p align="center">.004***</p></td>
      <td style="width:78px;height:15px;"><p align="center">.347</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Gnathic index</p></td>
      <td style="width:102px;"><p align="center">94.58 &plusmn; 4.01</p></td>
      <td style="width:96px;"><p align="center">93.67 &plusmn; 3.89</p></td>
      <td style="width:72px;"><p align="center">.049*</p></td>
      <td style="width:78px;"><p align="center">.232</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Jugo frontal</p></td>
      <td style="width:102px;"><p align="center">73.45 &plusmn; 3.36</p></td>
      <td style="width:96px;"><p align="center">73.99 &plusmn; 3.35</p></td>
      <td style="width:72px;"><p align="center">0.1648</p></td>
      <td style="width:78px;"><p align="center">.165</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Nasal breadth-facial ht index</p></td>
      <td style="width:102px;"><p align="center">38.17 &plusmn; 3.90</p></td>
      <td style="width:96px;"><p align="center">37.89 &plusmn; 3.99</p></td>
      <td style="width:72px;"><p align="center">.559</p></td>
      <td style="width:78px;"><p align="center">.069</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Interorbital breadth-facial ht index</p></td>
      <td style="width:102px;"><p align="center">30.95 &plusmn; 3.29</p></td>
      <td style="width:96px;"><p align="center">30.27 &plusmn; 3.60</p></td>
      <td style="width:72px;"><p align="center">.094</p></td>
      <td style="width:78px;"><p align="center">.201</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Transverse frontal index</p></td>
      <td style="width:102px;"><p align="center">81.60 &plusmn; 3.49</p></td>
      <td style="width:96px;"><p align="center">82.81 &plusmn; 2.97</p></td>
      <td style="width:72px;"><p align="center">0.000***</p></td>
      <td style="width:78px;"><p align="center">.372</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Transverse craniofacial index</p></td>
      <td style="width:102px;"><p align="center">99.24 &plusmn; 4.94</p></td>
      <td style="width:96px;"><p align="center">98.52 &plusmn; 4.29</p></td>
      <td style="width:72px;"><p align="center">0.1783</p></td>
      <td style="width:78px;"><p align="center">.156</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:102px;"><p align="center">52.74 &plusmn; 2.52</p></td>
      <td style="width:96px;"><p align="center">52.14 &plusmn; 2.82</p></td>
      <td style="width:72px;"><p align="center">0.0614</p></td>
      <td style="width:78px;"><p align="center">.226</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Vertical craniofacial index</p></td>
      <td style="width:102px;"><p align="center">49.81 &plusmn; 3.28</p></td>
      <td style="width:96px;"><p align="center">49.76 &plusmn; 3.53</p></td>
      <td style="width:72px;"><p align="center">0.9012</p></td>
      <td style="width:78px;"><p align="center">.015</p></td>
    </tr>
  </tbody>
</table>


<p><strong>Table 7. </strong>Significance of difference between craniometric indices of contemporary and subrecent female sample with effect size.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:255px;height:20px;"><p align="center"><strong>Index</strong></p></td>
      <td colspan="2" style="width:228px;height:20px;"><p align="center"><strong>Females (Mean &plusmn; SD)</strong></p></td>
      <td rowspan="2" style="width:69px;height:20px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:81px;height:20px;"><p align="center"><strong>Effect Size</strong></p></td>
    </tr>
    <tr>
      <td style="width:99px;"><p align="center"><strong>Contemporary</strong></p></td>
      <td style="width:129px;"><p align="center"><strong>Subrecent</strong></p></td>
      <td style="width:81px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Cranial index</p></td>
      <td style="width:99px;"><p align="center">74.26 &plusmn; 3.90</p></td>
      <td style="width:129px;"><p align="center">71.89 &plusmn; 3.17</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.664</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Vertical index</p></td>
      <td style="width:99px;"><p align="center">75.67 &plusmn; 2.70</p></td>
      <td style="width:129px;"><p align="center">73.46 &plusmn; 2.58</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.829</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse vertical Index</p></td>
      <td style="width:99px;"><p align="center">102.06 &plusmn; 4.3</p></td>
      <td style="width:129px;"><p align="center">102.31 &plusmn; 4.33</p></td>
      <td style="width:69px;"><p align="center">.735</p></td>
      <td style="width:81px;"><p align="center">.058</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Auriculo vertical index</p></td>
      <td style="width:99px;"><p align="center">71.17 &plusmn; 2.64</p></td>
      <td style="width:129px;"><p align="center">69.39 &plusmn; 2.40</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.702</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:99px;"><p align="center">95.98 &plusmn; 3.77</p></td>
      <td style="width:129px;"><p align="center">96.62 &plusmn; 3.30</p></td>
      <td style="width:69px;"><p align="center">.277</p></td>
      <td style="width:81px;"><p align="center">.181</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Frontal index</p></td>
      <td style="width:99px;"><p align="center">71.41 &plusmn; 2.99</p></td>
      <td style="width:129px;"><p align="center">71.5 &plusmn; 3.57</p></td>
      <td style="width:69px;"><p align="center">.881</p></td>
      <td style="width:81px;"><p align="center">.025</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:99px;"><p align="center">84.24 &plusmn; 6.53</p></td>
      <td style="width:129px;"><p align="center">82.64 &plusmn; 5.10</p></td>
      <td style="width:69px;"><p align="center">.097</p></td>
      <td style="width:81px;"><p align="center">.271</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Upper Facial index</p></td>
      <td style="width:99px;"><p align="center">52.51 &plusmn; 2.90</p></td>
      <td style="width:129px;"><p align="center">52.66 &plusmn; 3.24</p></td>
      <td style="width:69px;"><p align="center">.781</p></td>
      <td style="width:81px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Nasal index</p></td>
      <td style="width:99px;"><p align="center">52.06 &plusmn; 4.48</p></td>
      <td style="width:129px;"><p align="center">50.56 &plusmn; 4.84</p></td>
      <td style="width:69px;"><p align="center">.066</p></td>
      <td style="width:81px;"><p align="center">.322</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Orbital index</p></td>
      <td style="width:99px;"><p align="center">86.09 &plusmn; 5.45</p></td>
      <td style="width:129px;"><p align="center">86.05 &plusmn; 5.17</p></td>
      <td style="width:69px;"><p align="center">.966</p></td>
      <td style="width:81px;"><p align="center">.007</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Gnathic index</p></td>
      <td style="width:99px;"><p align="center">95.50 &plusmn; 4.09</p></td>
      <td style="width:129px;"><p align="center">94.35 &plusmn; 4.25</p></td>
      <td style="width:69px;"><p align="center">.112</p></td>
      <td style="width:81px;"><p align="center">.276</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Jugo frontal</p></td>
      <td style="width:99px;"><p align="center">75.74 &plusmn; 2.20</p></td>
      <td style="width:129px;"><p align="center">76.06 &plusmn; 2.73</p></td>
      <td style="width:69px;"><p align="center">0.488</p></td>
      <td style="width:81px;"><p align="center">.119</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Nasal breadth-facial height index</p></td>
      <td style="width:99px;"><p align="center">38.34 &plusmn; 2.85</p></td>
      <td style="width:129px;"><p align="center">37.94 &plusmn; 3.91</p></td>
      <td style="width:69px;"><p align="center">.515</p></td>
      <td style="width:81px;"><p align="center">.117</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Interorbital breadth-facial height index</p></td>
      <td style="width:99px;"><p align="center">31.09 &plusmn; 3.15</p></td>
      <td style="width:129px;"><p align="center">30.16 &plusmn; 3.78</p></td>
      <td style="width:69px;"><p align="center">.129</p></td>
      <td style="width:81px;"><p align="center">.267</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse frontal index</p></td>
      <td style="width:99px;"><p align="center">83.23 &plusmn; 2.29</p></td>
      <td style="width:129px;"><p align="center">82.76 &plusmn; 2.79</p></td>
      <td style="width:69px;"><p align="center">0.304</p></td>
      <td style="width:81px;"><p align="center">.183</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse craniofacial index</p></td>
      <td style="width:99px;"><p align="center">94.35 &plusmn; 3.39</p></td>
      <td style="width:129px;"><p align="center">94.04 &plusmn; 4.36</p></td>
      <td style="width:69px;"><p align="center">0.687</p></td>
      <td style="width:81px;"><p align="center">.070</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:99px;"><p align="center">53.96 &plusmn; 2.39</p></td>
      <td style="width:129px;"><p align="center">52.15 &plusmn; 2.71</p></td>
      <td style="width:69px;"><p align="center">0.000***</p></td>
      <td style="width:81px;"><p align="center">.709</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>&nbsp;Vertical craniofacial index</p></td>
      <td style="width:99px;"><p align="center">48.56 &plusmn; 3.22</p></td>
      <td style="width:129px;"><p align="center">48.40 &plusmn; 3.16</p></td>
      <td style="width:69px;"><p align="center">0.769</p></td>
      <td style="width:81px;"><p align="center">.049</p></td>
    </tr>
  </tbody>
</table>

<h2 id="jumpmenu6">Discussion</h2>

                <p>Biological (racial/ancestral) differences in skeletal structure pristinely arose when minuscule genetic changes developed in populations isolated by geography [4,9]. Some of these differences are vigorously inherited and others, such as body size and shape, are vigorously influenced by alimentation, way of life, and other aspects of the environment. Genetic distinctions between populations commonly consist of differences in the frequencies of all inherited traits, including those that are environmentally malleable. The inherited features are most significantly expressed by skull from one generation to the next [4,9,10]. Many of these features have developed in replication to evolutionary processes, including adaptation to the environment. Since certain anatomical features are found with more preponderant frequency in certain populations, their presence or absence are clues to ancestry, e.g., Laptorhinae is mundane in Eskimos and White population but rare in Negros.</p>

                <p>India has the highest genetic and climatic diversity of any continental region after Africa. Yet it has comparatively neglected in human craniometric studies. The present study utilized craniometric data of two temporally different groups of North India.</p>

                <p>Indices are used for conveniently expressing the relative proportions between the two measurements and do not take into account of the actual size of the measurements itself. Higher the value of the index the more nearly equals the two measurements. The craniometric indices would have their main value in discriminating between closely related groups, between which there is geographical., cultural., historical., linguistic, archaeological., or paleo-anthropological evidence of an affinity; but they could not on their own be adduce as evidence of genetic affinity between less closely related groups [36].</p>

                <p>Here each index is separately discussed to provide comprehensive knowledge to the readers.</p>

          <h4>Cranial vault indices</h4>

                <p>The shape of cranial vault is typically expressed by three indices: the cranial, height length and height breadth.</p>

                <p><strong>Cranial index:</strong> It was first defined by&nbsp;Swedish&nbsp;anatomist&nbsp;Anders Retzius&nbsp;(1796&ndash;1860). It is the most studied index in the literature and shows racial and ethnic variability. It is known to be higher in females than in males though some studies [37,38] on African population groups showed visa-versa. Researchers reported wide range of variation in cranial indices according to geographical areas e.g. Southeast Asians of Thailand and Hongkong are brachycranic and mesocranic respectively [39]; Northern Chinese and Siberians are brachycranic or hyperbrachycranic [40], Malaysian Indians have brachycephalic head with a tendency towards mesocephalic [41], Iranian have hyperbrachycephalic or brachycephalic type [42,43], Srilankan are brachycephalic [44], Chinese Hakka are mesoccephaeslic [45], Fijians tend to brachycephaly [46], coastal Andhra population of India have dolicocranial type [47] and mixed population of South India showed dominance of both Mesaticephalic and Brachycephalic skulls [48],. In above mentioned studies the type of index may be same but the mean index values are quite different. Weidenreich [49], opined that the round head when compared with the long head owes its special form to a considerable shortening of its length rather than to an increase of its breadth. The index varies, of course, from individual to individual but its amplitude can remain within certain limits in a given population. This peculiarity made the index suitable as a racial character [49].</p>

                <p>In general, it is expected that under cold stress, a brachycephalic shape (cranial index of 80.0-84.99, broad or round headed) and under hot climatic conditions, a long head <em>i.e.,</em> dolichocephalic would be more advantageous. [50-52]. But these findings are contradicted in the cases of Northeast Asian samples from cold-climates, which exhibit broad and long crania that do not fit neatly into purported cranial shape adaptations for cold climate populations [4]. African ethnic groups of Nigeria residing under high temperature are reported to be dolichocranial [53] mesocranial and ultrabrachycranial; means extremely broad skull to short skull [37]. Even discrepancies are noted in cephalic indices (roughly 2 units higher than Cranial Index) of same temperature zones, for e.g., North Indians of Punjab shows a Brachycephalic type [54], while population of Haryana [55] and Rajasthan [56] shows a dolichocephalic type. So it is obvious that inhereditary factor is primarily responsible for this variability in head shape in various ancestries, ethnicities and geographical regions, however environment has secondary effect on it [43]. There is an established relationship between the variation in cranial phenotypes and geographical distances [57].</p>

                <p>Some anatomical abnormality are also associate with head shape e.g. because of early fusion of saggital suture skull get to be extended (scapho-cephaly) and its occurrence is 2.5%, premature closure of foremost fontanels and other sutures causes microcephaly and brachiocephaly happen due to premature closure of bilateral coronal suture [56]. Artificial cranial distortion and long term malnutrition also cause changes in head shape.</p>

                <p>The studied population found to be Dolicocranial type (about 45%) followed by Hyperdolicocranial (about 36%). A trend towards Brachycranial skull was also noted in contemporary population (5.06%) which is the result of increased cranial breadth in contemporary population.</p>

                <p>In regard to sexual differences Cephalic index (cranial) of Iranians [43], Nepalese [58], Albanians [59], and Nigerians [38], Srilankan [44], Mumbai Indians [60] and Uttar Pradesh (Present study) demonstrated significant sexual dimorphism, While the studies on some Indian groups [61-63] did not exhibit any significant differences.</p>

                <p><strong>Vertical index or length height index:</strong> Most of the skulls from both population groups were belongs to orthocranial type followed by hypsicranial. But the contemporary population showed 14.15% more hypsicranial types skulls. This was due to increased basion-bregma height in contemporary males and females (Table 1 and 2). Sex differences were significant only in contemporary sample, while temporal changes were only significant in females. The value of effect size clearly showed that this difference is quite obvious. According to Gabel [46] Fijian have high cranial vault relative to head length and having a mean index between 68-69 [4] which put them in Chamaecranial category. Jheng <em>et al</em>. [45] noted a predominance Hypsicephalic head shape in Chinese Hakka population.</p>

                <p><strong>Transverse vertical index or breadth height index:</strong> In both population groups more than 90% crania belong to high and narrow skull type (Acrocranial) followed by metriocranial skull. Both population groups showed significant sexual differences but classification accuracies were poor. The temporal differences were absent in both sexes. Salve and Chnadrshekhar [60] found Mumbai (India) population of acrocranial type with significant sexual differences. According to Gabel [46] head height relative to total breadth is 83 percent in Fijian which put them in Tapeinocranial type, while most of the Chinese Hakka population exhibited a Metrioranial (Low, broad) type skull [45].</p>

                <p><strong>Auriculo vertical index:</strong> All studied crania showed a Hypsicranial type <em>i.e., </em>auriculao-bregmatic height relative to the cranial length is about 69 to 71%. Significant sexual differences were observed only in contemporary sample. Considering temporal variation, the differences were shown by only females. Contemporary females showed a significantly higher index value than the subrecent females with a large effect size (0.702). Ilayperuma [44] studied Srilankan population and found it to be hypsicephalic (78.68).</p>

                <p><strong>Auriculo transverse index:</strong> The value of this index fall the crania of both populations in acrocranial type with significant sexual differences in both groups. Though temporal variations were absent in both sexes. Ilayperuma [44] found SriLankan population to be in same category and with significant sex differences.</p>

                <p><strong>Foramen magnum index:</strong> Foramen magnum is an important landmark of the skull base. No clear trend was observed for population frequency for this index. Though, narrow or broad types were more frequent. Both populations showed medium type foraman magnum index without sexual differences. Though, marginal population differences exist only in males. According to Howale and associates [64] Maharashtrian population (Indian) have the medium type of index,. 84.85.</p>

                <p><strong>Frontal Index or Transverse fronto parietal index or frontoparietal index:</strong> It is used to show frontal view of the cranium. The index is strikingly alike among the contemporary and subrecent males. The minimum breadth of frontal bone relative to total cranial width is 72.82% and 72.85% for contemporary and subrecent males. More than 80% of the populations from both groups belong to Eurymetopic type followed by Metriometopic type, though a small percentage (2.5%) is Stenometopic type <em>i.e., </em>having a smaller forehead breadth than the cranial breadth. In the females of both groups the fronto-parietal ratio is about 71.2%. Significant sexual differences in both population groups were noted though temporal variations were absent in both of the sexes. A worldwide variation was also observed in this index. Fijian population has Eurymetopic type (mean-70.6) [46] while, Sangvichien <em>et al</em>., [65] found Thai population of Stenometopic type with significant sexual differences. The Chinese Hakka population showed the mean index value of 71.3 and 73.2 for males and females respectively<em> i.e., </em>Eurymetopic type. They showed higher index value for females [45].</p>

                <p><strong>Transverse frontal index:</strong> Sangvichien <em>et al</em>. [65] reported Transverse frontal index values of 67.13 and 65.33 for Thai males and females respectively. They found no sexual difference in the index. In current study this index showed significant sexual and as well as population variation (temporal changes). Index values in North Indians and Thai populations are very different. Further the female of contemporary North Indian population showed greater index value than the males while Thais showed smaller index values for females.</p>

                <p><strong>Transverse cranio-facial., longitudinal craniofacial and vertical craniofacial index: </strong>These indices were first pointed by Wilder [66] and also add in shape characteristics of cranium. After a thorough search we couldn&rsquo;t found data/ranges from any population of the world covering these indices. So the classification values couldn&rsquo;t be provided in present study. The value of these indices has been calculated from the mean values of particular variable of the other populations for the purpose of comparison.</p>

                <p><strong>Transverse craniofacial index:</strong> It has biozygomatic breadth as numerator and maximum cranial breadth as denominator component. In present study this index showed a sexual difference without temporal variation (contemporary vs subrecent). Gabel [46] studied Fijian population and found the range of 82 to 108 with a mean value of 93.5. Srivastava <em>et al</em>. [67] also reported this index in North Indian population and found that the maximum individuals (71.69%) showed transcraniofacial index value between 80 to 100. In present study the male and female mean values were 99.24 and 94.35 respectively for contemporary North Indian population represented by about 74% population.</p>

                <p>We have calculated this index for other population groups (based on mean values of variable), which showed vast differences in index values, e.g. South African Indigenous (M-99.51:F-95.93) [68] Cretans (M-94.84: F-91.15) [69], South Indian population (M-95.78) [70], Brazilians (M-78.92: F-74.08) [71], central European population of Romania (M-91.95: F-88.94) [72], Australeans (M-99.21:F- 94.81) [73]. It is obvious form the above examples that value of this index is always greater for males. North Indians and Australians have almost same transverse craniofacial profile.</p>

                <p><strong>Longitudinal craniofacial index:</strong> It has showed significant sexual differences only in contemporary population. The temporal variation was shown by an increase only in contemporary females. A very little variation was observed in the value of this index in other population of the world e.g. contemporary North Indians of present study (M/F:52.74/53.96), South African Africans (M/F:53.81/54.28) [74], Cretans (M/F:52.22/51.33), [69] Australians (M/F:51.33/50.91), [73] and Romanian (M/F:57.78/57.16) [72]. It is also observed that the values may not always be greater in males.</p>

                <p><strong>Vertical craniofacial index: </strong>It showed a significant sexual dimorphism in both of the studied population, though temporal difference were absent for both sexes. On comparing Thai population (M/F:50.84/50.19), [39] South African indigenous population (M/F:50.75/50.43), [68] South African Blacks (M/F: 51.58/51.48), [74] Cretans (M/F: 49.66/48.40), [9], Australians (M/F:49.05/47.88), [73] and Romanians (M/F:52.22/51.79) [72] with Contemporary North Indians (M/F: 49.81/48.56), it was found that there is not much difference in the mean index values.</p>

          <h4>Facial indices</h4>

                <p>The relative face shape is described using three indices: gnathic (the degree of maxillary projection), upper facial and total facial (not included in present study) indices. Nasal and orbital indices also contribute to the morphology of the face.</p>

                <p><strong>Nasal index:</strong> There are characteristic differences among populations in the shape and the size of the nose and hence it is likely that the nasal bone and the piriform aperture also have some differences, which can provide useful insights for the population affinity in anthropology. Green4 reported the highest variability (secular changes) in interorbital and nasal indices and suggested it due to low heritability and large environmental influences. Previously Bhargava and Sharma [75] noted that the variations in the form of the nose are greater than those found in the cranium and much greater than the body variations as a whole. This index distinctively differs from other anthropological indices, in being based upon both bony and cartilaginous landmarks. Some correlation exists between the proportions and diameters of face and nose, for instance a long narrow face is associated with a long nose and a short broad face with a short broad nose Bhargava and Sharma [75]. The variation in nasal aperture is generally attributed to the function of the nasal organ regulating the temperature and moisture of the air before it comes in contact with the delicate lung tissues [52]. Recently Noback <em>et al</em>. [76] reported a high degree of correlations between nasal cavity shape and climatic variables of both temperature and humidity. The bony nasal cavity appears mostly associated with temperature, and the nasopharynx with humidity. Nasal cavities from cold&ndash;dry climates are relatively higher and narrower compared with those of hot&ndash;humid climates [76].&nbsp;The observed climate-related shape changes are functionally consistent with an increase in contact between air and mucosal tissue in cold&ndash;dry climates through greater turbulence during inspiration and a higher surface-to-volume ratio in the upper nasal cavity [76]. The argument is based principally on the observation that nasal height is globally more variable than nasal breadth, with nasal breadth thus contributing little to variation in the index. This argument does not take into account the confounding effect of absolute size of these variables on their variances [77]. As such, in colder and drier climates, the length of the nasal passage is increased and the base is narrowed, thus increasing the surface area and the period of time over which inspired air is warmed and moistened. The Mongoloid people possess short and moderately broad nose; their nasal index takes them to the group of mesorrhine. South Asians have Messorihhine type [39], while northern Chinese displayed &lsquo;relatively narrow and high&rsquo; nasal region [4]. Jheng <em>et al</em>. [45] also found the Chinese Hakka to be of Mesorrhine type. The Caucasoid people of Asia show the characteristics of Leptorrhine nose while the white people of Europe present typical narrow nose. The Negroid ancestry (mainly of African descent) has the Mesorhine or Platyrrhine nose type [78]. The Australian aborigines and Fijians show a clearly marked platyrrhine or hyperplatyrhinae nose. In words of Gabel [46] Platyrrhini is the rule in Fiji, but individual and regional variations are great. There are some leptorrine subjects in every province, and there are some whose noses are broader than long.</p>

                <p>The nasal index of studied north Indian population didn&rsquo;t show any significant sexual or temporal difference. Srivastava <em>et al</em>., [67] also evaluated living population of North India and found dominant nasal form is Leptorhine while Gujarati living population is mesorrhine [79] and South Indian population is hyperchamaerhinae [80].</p>

                <p><strong>Upper facial index:</strong> Albeit the form of face is considered as a paramount criterion but it has a constrained scope in racial allocation. The facial features and indices vary with the increasing age and easily affected by the factors like sex and function, etc. in a population. For example, the females almost invariably show shorter and comparatively broader faces than the males of the same ethnic groups. But after maturation in craniofacial skeleton changes affect very little to its form.</p>

                <p>North Indian population shows Mesen type upper facial index in present study. Gujarati population shows Lepten type upper facial index [79], while Maharashtrian population shows Mesen type upper facial index.64 Prasnna <em>et al.,</em> [81] studied South and North Indian population and found North Indians to be Leptene to Hyperleptene (very long) and South Indian males to be Leptene and females Mesne (Round face). Gabel46 found Fijian population to be Euryen type with a mean index value of 48.2. Ishida and Kondo [40] reported that Siberian and northern Chinese populations both had high upper facial index values indicating &lsquo;high and broad&rsquo; facial skeletons. Thai and Hong Kong population also showed same [39]. Ngeow and Aljunid [41] reported a mesen or medial type face in Malaysian Indian male as indicated by a small index value (53.5 &plusmn; 3.3) while females showed a lepten type face. A broad face is usually associated with a brachycephal and similarly a long face is associated with long head or dolichocephalic. Though this is a harmonic relation between the head and the face, but not universal in occurrence [82]. For example, the Armenoids (a subtype of Caucasoid ancestry) who possess long and relatively narrow faces along with relatively short and broad heads. Again, a broad face with long head is available among the Eskimos. These are the examples of disharmonic relation between head and face.</p>

                <p><strong>Orbital index</strong>: In each orbital cavity, the width is usually greater than the height; the relation between the two is given by the orbital index, which determines the shape of the face. The orbital index has been used for both quantitative and qualitative sex and ancestry estimation from the beginning of anthropometry. A large value of this index reflects a horizontally elongated and vertically shorter orbit (or a more rectangular shape), and a smaller value indicates that the orbit is relatively tall and narrow (a more rounded shape). Usually the Asian populations have larger orbital height and smaller orbital breadth compared to Africans and Europeans, which resulted in Hypsiconch (large) Orbital index (except the Eskimos where the orbital opening is round). On the other hand Africans are characterized by the opposite orbital shape of that of the Asian group. This characteristic is primarily the result of a larger average orbital breadth in the African sample, as average orbital height is nearly identical between this and the European [83]. The researchers have found a great variation in the shape and size of the orbits in both sexes of different populations e.g. Japanese have Microseme type, Malwian and Indonesian have megaseme type, population of Peking province of China showed microseme type while Fushun (83.57) and Hokien population showed a Megaseme type index [84]. Among the Africans, it is usually higher in females than in male [85], and Microseme type [86]. It has been observed that variation in orbital index is also common in Indian population e.g Microseme in Panjabis [87], and population of Delhi [84], Hypsiconch (Megaseme/large) in Mharashtrian (index-86.4), [64] and population of Karnataka (index-97.00) [88]. Contemporary population showed broader orbits in both sexes (Table 1 and 2). But the orbits are on average narrower amongst contemporary males than the subrecent male <em>i.e.,</em> smaller orbital breadth in comparison to orbital height. We found sexual differences in orbital index of only contemporary sample though in both populations greater orbital index was noted for females. Jeremiah <em>et al</em>., [85] sudied Kanyan population and found no sex difference while Ebeye and Otikpo [86] found significant sex difference in this index.</p>

                <p><strong>Interorbital breadth-facial height index</strong>: Cameron [89] studied interorbital breadth- facial height index in Africans (Native Australian and Negros), Whites and Mongolians (Mongol and Eskimos). He found that it was highest in African ancestries and lowest in Mongolian racial types. In present study interorbital breadth-facial height index showed no sexual or temporal differences in both the sexes. But this index is much closer to the mongoloid group. Further Cameron [90] concluded from his craniometric studies on various populations that a high nasal index was associated with a high interorbital width/facial index and vice versa. Highest interorbital breadth was found in Negro crania and lowest in Eskimos [91].</p>

                <p><strong>Nasal breadth-facial height index:</strong> Cameron [92] used this index to the very first time. He studied nine racial groups for this index and found to reach its minimum in the Eskimo (M/F:31/31.11) and its maximum in the Negro ancestries (M/F:38.1/40.1). In Mongols it was (M/F:35.4/36.6). In all his studied groups, without exception, the index was ascertained to be higher in the females than in the males, thus suggesting the existence of a sexual factor. In the present study, the values are similar (contemporary M/F:38.17/38.34) to Australian Aboriginals (M/F:38.68/38.70) [92]. The interpretation of which was that the nasal width is, on the average, 38.25% of the upper facial height in that contemporary group. The mean females index values were little larger than the males but it was non-significant in both population groups. There is also absence of temporal changes in both male and females.</p>

                <p><strong>Gnathic index:</strong> The forward projection of the facial region or protrusion of the upper jaw is known as the facial prognathism. When the face does not show any protrusion, it is known as orthognathism. Prognathism is common among the African ancestry of Africa and Ocenia; it is especially well marked among the Negroes and Australian aborigines. The modern people are generally orthognathous, only a few of them may show a little prognathism. Among the human population, the Mongoloids and some white people show slight or moderate alveolar prognathism but facial prognathism is almost absent in them [82].</p>

                <p>Hanihara [93] reported highest prognathism in the Australian/Melanesian samples (105.4), followed closely by some of the SubSaharan African samples. Eskimos showed a mesognathous alveolar prognathism (98.5) [94] while Chinese population showed an index value of 94.195. Sumati and Patnaik [96] studied Indian crania and found that most of the crania (50 out of 60) having an orthognathic profile (gnathic index&lt;98), which correspond with the present study. In our study sex differences were absent in both population groups though significant temporal increase was present in the contemporary males.</p>

                <p>A low value of gnathic index indicated a relative shortness of the basion-prosthion length compared to basion-nasion length. Wei [95] suggested that a short basion-prosthion distance is more probably a contributing cause to the low gnathic index. The gnathic index of North Indians was found to be orthognathus type without sex differences. Though, females showed slightly higher value of Gnathic Index. This result confirms the findings of Wei [95] and Berett <em>et al.,</em> [97] who previously reported a significant sex difference with greater female values in gnathic indices of the Chinese and Australian aborigines respectively. The linear components of the gnathic index (ba-pr, ba-n) were significantly greater in the males than the females. The contention that female subjects of the same population group tend to be more prognathic than the males appears to be well supported by a number of craniometric studies of various population groups [95,97]. The more prognathism is described as secondary sexual character in females [95]. Baab <em>et al.,</em> [98] investigated [14] geographically widespread human populations and found that crania with more prognathic faces, expanded glabellar and occipital regions, and (slightly) longer skulls were more robust than those with rounder vaults and more orthognathic faces.</p>

                <p><strong>Jugo frontal index:</strong> The relationship between minimum frontal diameter and bizygomatic breadth provides the zygo frontal or jugo-frontal index. This index showed highly significant differences between sexes in both of the population, though temporal variation was absent in both males and females. The lower value of the index in males is due to the narrower forehead than to the unusual larger bizygomatic breadth. This index was also showed significant sexual differences in Thai [39] and Gujrati populations [79]. Gabel [46] found that the Fijian population have narrow zygofrontal index.</p>

                <p>Studies conducted on the craniofacial indices in Indian dry cranium are limited because of the lack of skeletal material. On the contrary, an immensely colossal number of the studies have been conducted on living population (so we have included studies on living and dried skull for the sack of discussion).</p>

          <h4>Ambiguity in nomenclature of indices</h4>

                <p>During the study it was observed that there are conspicuous differences in the cephalofacial indices and craniofacial indices, as the former is calculated on the living as well as on cadavers, whereas the craniofacial indices are an attribute of the cranium. The tissue thickness at the head vault and facial region may affect the cephalofacial measurements and consequently the cephalofacial indices derived from these quantifications. The tissue thickness not only varies in different components of the head vault, but additionally shows individual and population variations. For example, the cephalic index, which is quantified on the heads of the living, is roughly two units higher than cranial index quantified on the dried human skulls. Therefore, the values of the cephalic index cannot be compared with those of the cranial index and hence are not replaceable. In the same way, the range of nasal index in the living subject is higher than on the dried skulls. In case of living subject, the nasal length is to be taken from nasion to the subnasale where the nasal septum physically contacts the upper lip. The nasal breadth is the highest distance between the two alare or two nasal wings in natural condition. In fact, the nasal index calculated on skeleton and the living subjects never correspond to one another. Even the nomenclature and range of the index is also differing for living and skeleton. For example, the nose has been classified into three major groups based on the nasal parameters-Leptorrhine or fine nose (69.9 or less), Mesorrhine or medium nose (79.0-84.9), Platyrrhine or broad nose &sup3; (85.0) [99]. In dried skull, the range drastically changes, as dried skull having an index more than 58 will be hyperchaemerhine (very broad nose). In the same way cephalic and cranial index are different. The orbital index in living is Megaseme (large) &ge; 89, Mesoseme (intermediate) between 89 to 83 and Microseme (small) &le;83 [100]. After literature survey we found that these terms are used interchangeably. The standardization of these terms in different fields of knowledge is essential to facilitate communication between researchers and allow reliable comparisons between different studies.</p>

                <p>Variations and differences in craniofacial morphology (the craniofacial index) between and within populations have been attributed to a complex interaction of genetic and environmental factors with differential growth pattern of male and females. Though the each population mainly has a genetic influence on the morphological features, but expressivity of genes is affected by environmental and epigenetic influence such as socio-cultural background, climate, activity patterns, nutritional status and masticatory functions [39, (2004), pp. 58&ndash;60.101]. Among environmental factors nutritional changes and food habits are considered the most important reason to cause increase or decrease in body height and craniofacial dimensions [39,101-103]. &nbsp;Kasai <em>et al.,</em> [101] studied difference in Japanese and Australian aboriginal crania and found that the masticatory forces due to different food habits are responsible for the difference. Growth disturbances during early development also reflected in the craniofacial dimensions [8].</p>

                <p>The present study characterizes, crania of both North Indian population groups as long, medium, high and narrow skull with medium upper face (round face) and orbits, narrow jugo-frontal profile and broad nose. It is notable that the contemporary population showed a trend toward Brachycephalization (Table 3). Weidenreich [49] opined that brachycephalization is an evolutionary process which involves all mankind. It is not confined to certain ancestries or groups, as is shown by the fact that brachycephalic skulls occur everywhere-only the degree and the number of individuals varies. It obviously makes its appearance as an individual variation, and it may well be that it is more common in certain groups at a certain time than in others. But Kouchi [12] argued that Brachycephalization is due to relative higher increase in the head breadth in comparison with the head length, as a result of improvement in nutrition. Temperature thought to be affects head shape, nose and facial form but it has not proven yet. Sparks and Jantz [104] attributed such changes in craniofacial dimensions to the high heritability and variation among the ethnic groups not the environmental influences. Studies have shown that genetic structure of human populations is still constantly being affected by processes such as urbanization and migration (outbreeding), rapid population expansion, admixture between populations, isolation of specific sub-populations, and rapid non-random abandoning of habitation [105]. Saini <em>et al.,</em> [17] and Saini [18] demonstrated that like other population of the world Indian population also going through a secular change in cranial dimensions. The same has been concluded from the present study in which some craniofacial indices have shown significant deviations from the past population. It is noticeable that temporal differences between craniometric indices of females were much obvious (cranial, vertical, auriculo vertical index and longitudinal craniofacial Index) while males exhibited mere statistical differences (Gnathic, orbital and transverse frontal indices) (as shown by effect size). We supposed that the better living conditions (nutrional status, health, etc) and changing social structure attributed to changing pattern of indices in males and females. In India, after independence (1947) urbanization has increased with admixture of populations to evolving cosmopolitan traits due to large scale migration, and it seems the much better explanation for the differences in craniometric indices.</p>

                <p>Nagesh <em>et al.,</em> [106] reported that sexing by indices is more reliable since the relative growth of bony components are supposed to be proportional to each other. Thus, the ratio between two measurements of skull becomes a significant parameter for identifying sex. Thaper <em>et al.,</em> [63] argued that the linear measurements (cranial length and breadth) are better indicators of sex while cranial index may be superior in determining ethnic variation by defining head shape. In our study sex discrimination using indices is not suggested due to the poor classification (highest classification was 72%) and it showed that sexing by indices is useless in regard to forensic investigation as much higher classification rate can be obtained using single components of indices (Table 4) [18].</p>

<h2 id="jumpmenu7">Conclusion </h2>

                <p>Through the comparison of craniofacial indices with Indian and non-Indian origins&#39; people shows a significant variation in the index values. Discrepancies were also noted in same geographical region which may be attributed to difference in sample size, type (living or skull) or may be the age group. We conclude that these indices may be helpful in estimation of ancestry or regional group due to extreme variability within and between populations but not for sex estimation. Here it is suggested that if a number of indices included for a particular skull than we can identify its regional occurrence. But it will need a collaborative work, in which adequate numbers of skull sample from each regional population group of India include and a reference register maintained for each population group.</p>

                <p>The present study provides a databank for craniometric indices of North Indian population which may guide crime investigators to narrow down the missing persons search and hopefully to a definitive victim identification of North Indian origin. Before applying given indices to the unknown skull, sex and the time frame (from which a skull belongs) should also be keep in mind, as both factors are important in ancestry identification. Further by comparing these results with data from populations worldwide, scientists can evaluate that individual&#39;s relationship to a world group as well as may also be used by surgeons in management of the pathologies concerned in these regions.</p>



<h2 id="jumpmenu8">References</h2>
                
                <ol>
                  <li>Williams PL, Bannister LH, Berry MM, Collins P, Dyson M, et al. (2000) Gray&rsquo;s Anatomy, The anatomical basis of Medicine and surgery. (38<sup>th</sup> Ed). New York, Churchill Livingstone.</li>
                  <li>Shah GV, Jadhav HR (2004) The study of cephalic index in students of Gujarat. <em>J Anat Soc India</em> 53: 25-26.</li>
                  <li>Hanihara T (1994) Craniofacial continuity and discontinuity of Far Easterners in the Late Pleistocene and Holocene. <em>J Hum Evol</em> 27: 417-441.</li>
                  <li>Green H (2007) Cranial variation of contemporary East Asians in a global context. PhD thesis. University of New South Wales, 2007.</li>
                  <li>Stolovitzky JP, Todd NW (1990) Head shape and abnormal appearance of tympanic membranes. <em>Otolaryngol Head Neck Surg </em>102: 322-325. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2113257">[Crossref]</a></li>
                  <li>Cohen MM Jr, Kreiborg S (1994) Cranial size and configuration in the Apert syndrome. <em>J Craniofac Genet Dev Biol </em>14: 153-162. <a href="http://www.ncbi.nlm.nih.gov/pubmed/7852544">[Crossref]</a></li>
                  <li>Ridgway EB, Weiner HL (2004) Skull deformities. <em>Pediatr Clin North Am </em>51: 359-387. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15062675">[Crossref]</a></li>
                  <li>Sutphin R, Ross AH, Jantz RL (2014) Secular trends in Cherokee cranial morphology: Eastern vs Western bands. <em>Ann Hum Biol </em>41: 511-517. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24734845">[Crossref]</a></li>
                  <li>Spradley MK. Biological Anthropological Aspects of the African Diaspora; Geographic Origins, Secular Trends, and Plastic Versus Genetic Influences Utilizing Craniometric Data. PhD Thesis University of Tennessee. 2006.</li>
                  <li>Moore-Jansen PH. A multivariate craniometric analysis of secular change and variation among recent North American populations, Ph.D. Dissertation. University of Tennessee, Knoxville. 1989.</li>
                  <li>Kouchi M (2000) Brachycephalization in Japan has ceased. <em>Am J Phys Anthropol </em>112: 339-347. <a href="http://www.ncbi.nlm.nih.gov/pubmed/10861351">[Crossref]</a></li>
                  <li>Kouchi M (2004) Secular changes in the Japanese head form viewed from somatometric data. <em>Anthropol Sci </em>112: 41&ndash;52</li>
                  <li>Hossain MD, Lestrel PE, Ohtsuki F (2005) Secular changes in head dimensions of Japanese adult male students over eight decades. <em>Homo </em>55: 239-250. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15803769">[Crossref]</a></li>
                  <li>Tomljanovic BA, Ristic S, Milic BB, Ostojic S, Gombac E, et al. (2004) Secular change in body height and cephalic index of Croatian Medical students (University of Rijeka). <em>Am J Phys Anthropol </em>123: 91&ndash;96.</li>
                  <li>Bureti&Auml;-Tomljanovi&Auml; A, Ostoji&Auml; S, Kapovi&Auml; M (2006) Secular change of craniofacial measures in Croatian younger adults. <em>Am J Hum Biol </em>18: 668-675. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16917883">[Crossref]</a></li>
                  <li>Little BB, Buschang PH, Pe&ntilde;a Reyes ME, Tan SK, Malina RM (2006) Craniofacial dimensions in children in rural Oaxaca, southern Mexico: secular change, 1968-2000. <em>Am J Phys Anthropol </em>131: 127-136. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16485300">[Crossref]</a></li>
                  <li>Saini V, Srivastava R, Shamal SN, Singh TB, Kumar V, et al. (2013) Sexual dimorphism in cranial base of two temporally different sample of North India. <em>Int J Legal Med</em>.</li>
                  <li>Saini V (2014) Significance of temporal changes on sexual dimorphism of cranial measurements of Indian population. <em>Forensic Sci Int</em> 242: 300e1-300e8.</li>
                  <li>Wescott DJ, Jantz RL (2005) Assessing craniofacial secular changes in American whites and blacks using geometric morphometry. In, D. Slice (editor). Modern Morphometrics in Physical Anthropology, Volume V, Developments in Primatology, Progress and Prospects. New York, Kluwer Academic Press 231-246.</li>
                  <li>Jantz RL, Meadows Jantz L (2000) Secular change in craniofacial morphology. <em>Am J Hum Biol </em>12: 327-338. <a href="http://www.ncbi.nlm.nih.gov/pubmed/11534023">[Crossref]</a></li>
                  <li>Wescott DJ, Jantz RL (2001) Examining secular change in craniofacial morphology using three-dimensional coordinate data. <em>Am Acad Forensic Sci Proc</em>7: 262&ndash;263</li>
                  <li>Jonke E, Prossinger H, Bookstein FL, Schaefer K, Bernhard M, (2007) Secular trends in the facial skull from the 19th century to the present, analyzed with geometric morphometrics. <em>Am J Orthod Dentofacial Orthop</em> 132: 63-70.</li>
                  <li>Ross AH, Ubelaker DH, Kimmerle EH (2011) Implications of dimorphism, population variation, and secular change in estimating population affinity in the Iberian Peninsula. <em>Forensic Sci Int </em>206: 214.e1&ndash;214.e5.</li>
                  <li>https://en.wikipedia.org/wiki/FORDISC</li>
                  <li>https://en.wikipedia.org/wiki/Cranid</li>
                  <li>Navega D, Coelho C, Vicente R, Ferreira MT, Wasterlain S, et al. (2015) AncesTrees: ancestry estimation with randomized decision trees. <em>Int J Legal Med </em>129: 1145-1153. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25053239">[Crossref]</a></li>
                  <li>Howells WW (1995) Who&rsquo;s Who in Skulls. Ethnic Identification of Crania from Measurements. Cambridge, Mass, Peabody Museum, Papers of the Peabody Museum of Archaeology and Ethnology, pp. 82-108</li>
                  <li>Kallenberger L, Pilbrow V (2012) Using CRANID to test the population affinity of known crania. <em>J Anat </em>221: 459-464. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22924771">[Crossref]</a></li>
                  <li>Elliott M, Collard M (2009) FORDISC and the determination of ancestry from cranial measurements. <em>Biol Lett </em>5: 849-852. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19586965">[Crossref]</a></li>
                  <li>Buikstra JE, Ubelaker DH (1994) Standards for data collection from human skeletal remains. Arkansas Archaeological Survey research series No44.</li>
                  <li>Singh IP, Bhasin MK (2004) A manual of biological anthropology. 1st ed. Delhi, Kamla-Raj Enterprises.</li>
                  <li>Vacha-Haase T, Thompson B (2004) How to estimate and interpret various effect sizes. <em>J Counseling Psychol</em> 51: 473&ndash;481.</li>
                  <li>Fritz CO, Morris PE, Richler JJ (2012) Effect size estimates, current use, calculations, and Interpretation. <em>J Experimental Psychol</em>, American Psychological Association 141: 2&ndash;18.</li>
                  <li>Sullivan GM, Feinn R (2012) Using effect size&mdash;or why the p value is not enough. <em>Editorial J Graduate Medical Education</em> 279-282.</li>
                  <li>Robert, 2002</li>
                  <li>Tobias PV (1359) Studies on the occipital bone in Africa, I Pearson&#39;s occipital index and the chord-arc index in modern crania, means, minimum values, and variability<em>. J Roy Anthropol Inst Great Britain &amp; Ireland</em> 89: 233&ndash;252.</li>
                  <li>Oladipo GS, Olotu EJ (2006) Anthropometric comparison of cephalic indices between the Ijaw and Igbo tribes. <em>Global J Pure Appl Sci</em> 12: 137-138.</li>
                  <li>Oladipo GS, Olotu JE, Suleiman Y (2009) Anthropometric studies of cephalic indices of the Ogonis in Nigeria. <em>Asian J Med Sci</em> 1: 15-17.</li>
                  <li>King CA (1997) Osteometric assessment of 20th century skeletons from Thailand and Hong Kong. MA thesis, Florida Atlantic University.</li>
                  <li>Ishida H, Kondo O (1998) Human cranial variations based on different sets of measurement variables. <em>Anthropological Science</em> 106: 41-60.</li>
                  <li>Ngeow WC, Aljunid ST (2009) Craniofacial anthropometric norms of Malaysian Indians. <em>Indian J Dent Res </em>20: 313-319. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19884715">[Crossref]</a></li>
                  <li>Golalipour MJ (2006) The effect of ethnic factor on cephalic index in 17-20 years old females of North of Iran. <em>Int J Morphol</em> 24: 319-322.</li>
                  <li>Vojdani Z, Bahmanpur S, Momen S, Vasaghi A, Yazdizadeh A Karamifar A (2009) Cephalometry in 14&ndash;18 years old girls and boys of Shiraz-Iran high school. <em>Int J Morphol</em> 27: 101&ndash;104.</li>
                  <li>Ilayperuma I (2011) Evaluation of cephalic indices, a clue for racial and sex diversity. <em>Int J Morphol </em>29: 112-117.</li>
                  <li>Zheng L, Li Y, Lu S, Bao J, Wang Y, et al. (2013) Physical characteristics of Chinese Hakka. <em>Sci China Life Sci </em>56: 541-551. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23576182">[Crossref]</a></li>
                  <li>Gabel NE (1958) A racial study of the Fijians. Anthropological Records. University of California press. Berkeley and Los Angeles.</li>
                  <li>Devi PS, Sugavasi R, Devi BI, Lakshmi KV (2013) A study of cranial, orbital and auriculo vertical index of adult human skulls of north costal Andhra population of south India. <em>IJCRR</em> 5: 83-87</li>
                  <li>Kanchan T, Krishan K, Gupta A, Acharya J (2014) A study of cranial variations based on craniometric indices in a South Indian population. <em>J Craniofac Surg </em>25: 1645-1649. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25162547">[Crossref]</a></li>
                  <li>Weidenreich F. The brachycephalization of recent mankind. <em>Southwestern J Anthropol</em> 1: 1-54.</li>
                  <li>Bharati S, Som S, Bharati P, Vasulu TS (2001) Climate and head form in India. <em>Am J Hum Biol </em>13: 626-634. <a href="http://www.ncbi.nlm.nih.gov/pubmed/11505471">[Crossref]</a></li>
                  <li>Bharati S, Demarchi DA, Mukherji D, Vasulu TS, Bharati P (2005) Spatial patterns of anthropometric variation in India with reference to geographic, climatic, ethnic and linguistic backgrounds. <em>Ann Human Bio</em> 32: 407&ndash;444.</li>
                  <li>Mielke JH, Konigsberg LW, Relethford JH (2005) Human biological variation. Oxford University Press.</li>
                  <li>Adejuwon SA, Salawu OT, Eke CC, Femi-Akinlosotu W, Odaibo AB (2011) A craniometric study of adult human skulls from Southwestern Nigeria. <em>Asian J Medical Sci</em> 3: 23-25.</li>
                  <li>Mahajan A, Khurana BS, Seema, Batra APS (2010) The study of cephalic index in Punjabi students. <em>J Punjab Acad Forensic Med Toxicol </em>10: 24-26.</li>
                  <li>Kumar M, Patnaik VVG (2013) The study of Cephalic Index in Haryanvi population. <em>Int J Pure App Bio Sci</em> 1: 1-.6.</li>
                  <li>Masih WF, Gupta S, Ghulyani T, Saxena V, Saraawat PK (2015) Cranial index, circumference and shape of skull in the central Rajasthan, India, an autopsy study. <em>Int J Sci Stud</em> 2: 44-48.</li>
                  <li>Relethford JH (2009) Race and global patterns of phenotypic variation. <em>Am J Phys Anthropol </em>139: 16-22. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19226639">[Crossref]</a></li>
                  <li>Lobo SW, Chandrasekhar TS, Kumar S (2005) Cephalic index of Gurung community of Nepal--an anthropometric study. <em>Kathmandu Univ Med J (KUMJ) </em>3: 263-265. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18650589">[Crossref]</a></li>
                  <li>Rexhepi A, Meka V (2008) Cephalofacial morphological characteristics of Albanian Kosova population. <em>Int J Morphol </em>26: 935&ndash;940.</li>
                  <li>Salve VM, Chandrashekhar CH (2012) A metric analysis of Mumbai region (India) crania. <em>J Indian Med Assoc </em>110: 690-693. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23738400">[Crossref]</a></li>
                  <li>Singh P, Purkait R (2006) A cephalometric study among sub caste groups Dangi and Ahirwar of Khurai block of Madhya Pradesh. <em>Anthropologist</em> 8: 215-217.</li>
                  <li>Pandey AK (2006) Cephalo&ndash;facial variation among Onges. Kamla &ndash; Raj Anthropologist 8: 245-249.</li>
                  <li>Thapar R, Angadi PV, Hallikerimath S, Kale AD (2012) Sex assessment using odontometry and cranial anthropometry, evaluation in an Indian sample. <em>Forensic Sci Med Pathol</em> 8: 94-100.</li>
                  <li>Howale D, Pradhan R, Jain L, Lekharu R (2012) The calculation of various craniofacial indices in Maharastra population. <em>Int J Curr Res</em> 4: 162&ndash;166.</li>
                  <li>Sangvichien S, Boonkaew K, Chuncharunee A, Komoltri C, Piyawinitwong S, et al. (2007) Sex determination in Thai skulls by using craniometry, multiple logistic regression analysis. <em>Siriraj Med J</em> 59: 216-221.</li>
                  <li>Wilder HH (1920) A laboratory manual of anthropometry. Philadelphia, P. Blakiston&#39;s Son &amp; Co.</li>
                  <li>Srivastava PC, Kapoor AK, Sinha US, Shivkumar BC (2010) Morphological evaluation of head and face in North Indian population. <em>Ind Int J Forensic Med Toxicol</em> 8: 62-79.</li>
                  <li>Franklin D, Freedman L, Milne N (2005) Sexual dimorphism and discriminant function sexing in indigenous South African crania. <em>Homo</em> 55: 213-228.</li>
                  <li>Kranioti EF, I&Aring;&Yuml;can MY, Michalodimitrakis M (2008) Craniometric analysis of the modern Cretan population. <em>Forensic Sci Int </em>180: 110. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18718728">[Crossref]</a></li>
                  <li>Vidya CS, Prashantha B, Gangadhar MR (2012) Anthropometric predictors for sexual dimorphism of skulls of South Indian origin. <em>Int J Scientific Res Pub</em> 2.</li>
                  <li>Fortes de Oliveira O, Lima Ribeiro Tinoco R, Daruge J&uacute;nior E, Silveira Dias Terada AS, Alves da Silva RH, et al. (2012) Sexual dimorphism in Brazilian human skulls: discriminant function analysis. <em>J Forensic Odontostomatol </em>30: 26-33. <a href="http://www.ncbi.nlm.nih.gov/pubmed/23474506">[Crossref]</a></li>
                  <li>Marinescu M, Panaitescu V, Rosu M, Maru N, Punga A (2014) Sexual dimorphism of crania in a Romanian population, Discriminant function analysis approach for sex estimation. <em>Rom J Leg Med</em> 22: 21-26.</li>
                  <li>Dillon (2014) Cranial sexual Dimorphism and the population specificity of anthropological standards. Masters thesis, University of Western Australia.</li>
                  <li>Dayal MR, Spocter MA, Bidmos MA (2008) An assessment of sex using the skull of black South Africans by discriminant function analysis. <em>Homo </em>59: 209-221. <a href="http://www.ncbi.nlm.nih.gov/pubmed/18439606">[Crossref]</a></li>
                  <li>Bhargava I, Sharma JC (1959) The nose in relation to head and face-an anthropometric study. <em>Ind J Otolaryngol Head Neck Surg</em> 11: 213-218.</li>
                  <li>Noback ML, Harvati K, Spoor F (2011) Climate-related variation of the human nasal cavity. <em>Am J Phys Anthropol </em>145: 599-614. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21660932">[Crossref]</a></li>
                  <li>Franciscus RG, Long JC (1991) Variation in human nasal height and breadth. <em>Am J Phys Anthropol </em>85: 419-427. <a href="http://www.ncbi.nlm.nih.gov/pubmed/1928315">[Crossref]</a></li>
                  <li>Oladipo GS, Olabiyi AO, Oremosu AA, Noronha CC (2007) Nasal indices among major ethnic groups in Southern Nigeria. <em>Scientific Research and Essay</em> 2: 20-22.</li>
                  <li>Mehta M, Saini V, Nath S, Patel MN, Menon SK. CT scan images to determine the origin from craniofacial indices for Gujarati population. <em>J Forensic Radiol Imaging.</em></li>
                  <li>Karambelkar R, Shewale A, Karambelkar R, Umarji B (2013) Comparison of nasal morphology between Southern and Northern India. <em>Anat Karnataka</em> 7: 26-30.</li>
                  <li>Prasanna LC, Bhosale S, D&rsquo;Souza AS, Mamatha H, Thomas RH, et al. (2013) Facial indices of North and South Indian adults, reliability in stature estimation and sexual dimorphism. <em>J Clinical Diagnos Res</em>: 540&ndash;1542.</li>
                  <li>Moe SM, Chertow GM, Parfrey PS, Kubo Y, Block GA, et al. (2015) Cinacalcet, Fibroblast Growth Factor-23, and Cardiovascular Disease in Hemodialysis: The Evaluation of Cinacalcet HCl Therapy to Lower Cardiovascular Events (EVOLVE) Trial. <em>Circulation </em>132: 27-39. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26059012">[Crossref]</a></li>
                  <li>Masters MP (2008) Modern variation and evolutionary change in the Hominin eye orbit. PhD thesis The Ohio State University.</li>
                  <li>Kumar A, Nagar M (2014) Morphometry of the orbital region, &ldquo;beauty is bought by judgment of the eyes.&rdquo; <em>Int J Anat Res</em> 2: 566-570.</li>
                  <li>Jeremiah M, Pamela M, Fawzia B (2013) Sex differences in the cranial and orbital indices for a black Kenyan population. <em>Int J Medicine Medical Sci</em> 5: 81-84.</li>
                  <li>Ebeye OA, Otikpo O (2013) Orbital index in Urhobos of Nigeria. <em>J Dental Med Sci</em> 8: 51-53.</li>
                  <li>Kaur J, Yadav S, Singh Z (2012) Orbital dimensions -A direct measurement study using dry skulls. <em>J Acad Indus Res</em> 1: 293-295.</li>
                  <li>Makandar UK, Kulkarni PR (2012) Morphometric study of orbital cavity in the South Indian population. <em>Ind J Forensic Med Toxicol</em> 6: 39- 42.</li>
                  <li>Cameron J (1931) Proportion between Interorbital width and facial height. A new cranial index; its significance in modern and fossil man. <em>Am J Phys Anthropol</em> 16: 237-242.</li>
                  <li>Cameron J (1931) Correlation between Interorbital width/facial height index and nasal index. <em>Am J Phys Anthropol</em> 16: 243-247.</li>
                  <li>Cameron J (1931) The Interorbital width. A new cranial dimension. Its significance n modern and fossil man and in lower mammals. <em>Am J Phys Anthropol</em> 15: 509-519</li>
                  <li>Cameron J Craniometric Memoirs, No I (1929) The proportion between the nasion-alveolar height and the nasal width, a new cranial index, and its significance. <em>J Anat</em> 63: 412-424.</li>
                  <li>Hanihara T (2000) Frontal and facial flatness of major human populations. <em>Am J Phys Anthropol </em>111: 105-134. <a href="http://www.ncbi.nlm.nih.gov/pubmed/10618591">[Crossref]</a></li>
                  <li>Krogstad O (1972) The maxillary prognathism in Greenland Eskimo &mdash; a roentgen craniometric study. <em>Zeitschrift f&uuml;r Morphologie und Anthropologie</em> 64: 279-307.</li>
                  <li>Wei SH (1968) A roentgenographic cephalometric study of prognathism in Chinese males and females. <em>Angle Orthod </em>38: 305-320. <a href="http://www.ncbi.nlm.nih.gov/pubmed/5246793">[Crossref]</a></li>
                  <li>Sumati, Patnaik VVG (2011) Pragnathism as ancestry appraisal criterion in a study of 60 Indian crania of known sex using metric and non metric modes-assesment of methodology. <em>Ind J Forensic Med Toxicol </em>5: 79.</li>
                  <li>Barrett MJ, Brown T, Macdonald MR (1963) Dental observations on Australian aborigines, a roentgenographic study of prognathism. <em>Australian Dental J </em>8: 418-427.</li>
                  <li>Baab KL, Freidline SE, Wang SL, Hanson T (2010) Relationship of cranial robusticity to cranial form, geography and climate in Homo sapiens. <em>Am J Phys Anthropol </em>141: 97-115. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19554616">[Crossref]</a></li>
                  <li>Oladipo G, Fawehinmi H, Suleiman Y (2008) The study of nasal parameters (nasal height, nasal width, nasal index) amongst the Yorubas of Nigeria. <em>Internet J Biol Anthropol</em> 3.</li>
                  <li>Patnaik VVG, Bala S, Singla, RK (2001) Anatomy of the bony orbit-some applied aspects. <em>J Anat Soc India</em> 50: 59-67.</li>
                  <li>Kasai KLC, Richard T, Brown T (1993) Comparative study of craniofacial morphology in Japanese and Australian aboriginal population. <em>Hum Biol</em> 65: 821-832.</li>
                  <li>Miller JP, German RZ (1999) Protein malnutrition affects the growth trajectories of the craniofacial skeleton in rats. <em>J Nutr </em>129: 2061-2069. <a href="http://www.ncbi.nlm.nih.gov/pubmed/10539785">[Crossref]</a></li>
                  <li>Lieberman DE, Krovitz GE, Yates FW, Devlin M, St Claire M (2004) Effects of food processing on masticatory strain and craniofacial growth in a retrognathic face. <em>J Hum Evol </em>46: 655-677. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15183669">[Crossref]</a></li>
                  <li>Sparks CS, Jantz RL (2002) A reassessment of human cranial plasticity: Boas revisited. <em>Proc Natl Acad Sci U S A </em>99: 14636-14639. <a href="http://www.ncbi.nlm.nih.gov/pubmed/12374854">[Crossref]</a></li>
                  <li>Rudan I, Biloglav Z, Vorko-Jovic A, Kujundzic-Tiljak M, Stevanovic R, et al. (2006) Effects of inbreeding, endogamy, genetic admixture, and outbreeding on human health, A &ldquo;1001 Dalmatians&rdquo; study. <em>Croat Med J</em> 47: 601-610.</li>
                  <li>&nbsp;Nagesh KR, Kanchan T, Bastia BK (2007) Sexual dimorphism of acetabulum-pubis index in South-Indian population. <em>Leg Med (Tokyo) </em>9: 305-308. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17616420">[Crossref]</a></li>
                </ol>
                  




</div>

         
          </div>
          <div id="Article_Info" class="content">
            <!--<h4 class="mb10"><span class="">Editorial Information</span></h4>
            <h4 class="mb5"><span class="black-text">Editor-in-Chief</span></h4>-->
   


<h4>Article Type</h4>
<p>Research Article</p>

<h4>Publication history</h4>
<p>Received date: March 10, 2017<br>
Accepted date: April 11, 2017<br>
Published date: April 14, 2017</p>

<h4>Copyright</h4>
<p>&copy; 2017 Saini V. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</p>

<h4>Citation</h4>
<p>Saini V, Mehta M, Saini R, Shamal SN, Singh TB, et al. (2017) Is north Indian population changing it craniofacial form? A study of secular trends in craniometric indices and its relation to sex and ancestry estimation. Forensic Sci Criminol 2: DOI: 10.15761/FSC.1000115</p>

          </div>
          <div id="Author_Info" class="content">
            <h4 class="mb10"><span class="">Corresponding author</span></h4>
            
            
            <h4 class="mb10"><span class="black-text">Vineeta Saini</span></h4>
            <p>Department of Forensic Medicine, Institute of Medical Sciences, BHU, Varanasi, India, 221005</p>





          </div>
          <div id="Figures_Data" class="content">
             
             


 <img src="img/FSC-2-115-g001.gif">
                  <p><b>Figure 1. </b>Graphical presentation of sexual differences and secular changes in craniofacial measurements of both males and females.</p>


<p><strong>Table 1.</strong>&nbsp;Descriptive statistics, t-test and significance of differences between mean of contemporary and subrecent- males.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:91px;"><p align="center"><strong>Variables</strong></p></td>
      <td colspan="3" style="width:245px;"><p align="center"><strong>Contemporary- Males</strong></p></td>
      <td colspan="3" style="width:247px;"><p align="center"><strong>Subrecent- Males</strong></p></td>
      <td rowspan="2" style="width:72px;"><p align="center"><strong>t-value</strong></p></td>
      <td rowspan="2" style="width:62px;"><p align="center"><strong>Sig.</strong></p></td>
    </tr>
    <tr>
      <td style="width:70px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:61px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;"><p align="center"><strong>Min.-Max.</strong></p></td>
      <td style="width:79px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:108px;"><p align="center"><strong>Min.-Max.</strong></p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxCLt</p></td>
      <td style="width:70px;"><p align="right">182.52</p></td>
      <td style="width:61px;"><p align="right">6.59</p></td>
      <td style="width:114px;"><p align="right">160.27-193.8</p></td>
      <td style="width:79px;"><p align="right">179.60</p></td>
      <td style="width:60px;"><p align="right">6.51</p></td>
      <td style="width:108px;"><p align="right">163.0-197.80</p></td>
      <td style="width:72px;"><p align="right">3.780</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxCBr</p></td>
      <td style="width:70px;"><p align="right">128.24</p></td>
      <td style="width:61px;"><p align="right">5.30</p></td>
      <td style="width:114px;"><p align="right">116.0-143.13</p></td>
      <td style="width:79px;"><p align="right">125.97</p></td>
      <td style="width:60px;"><p align="right">4.57</p></td>
      <td style="width:108px;"><p align="right">115.6-138.90</p></td>
      <td style="width:72px;"><p align="right">3.972</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>BBrHt</p></td>
      <td style="width:70px;"><p align="right">134.19</p></td>
      <td style="width:61px;"><p align="right">5.06</p></td>
      <td style="width:114px;"><p align="right">121.17-145.60</p></td>
      <td style="width:79px;"><p align="right">131.59</p></td>
      <td style="width:60px;"><p align="right">4.95</p></td>
      <td style="width:108px;"><p align="right">115.7-147.90</p></td>
      <td style="width:72px;"><p align="right">4.397</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MaxFBr</p></td>
      <td style="width:70px;"><p align="right">114.39</p></td>
      <td style="width:61px;"><p align="right">4.68</p></td>
      <td style="width:114px;"><p align="right">104.6-125.70</p></td>
      <td style="width:79px;"><p align="right">110.77</p></td>
      <td style="width:60px;"><p align="right">4.46</p></td>
      <td style="width:108px;"><p align="right">97.9-123.40</p></td>
      <td style="width:72px;"><p align="right">6.738</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>MinFBr</p></td>
      <td style="width:70px;"><p align="right">93.32</p></td>
      <td style="width:61px;"><p align="right">5.02</p></td>
      <td style="width:114px;"><p align="right">84.16-108.18</p></td>
      <td style="width:79px;"><p align="right">91.68</p></td>
      <td style="width:60px;"><p align="right">3.93</p></td>
      <td style="width:108px;"><p align="right">78.43-102.23</p></td>
      <td style="width:72px;"><p align="right">3.192</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>ABrHt</p></td>
      <td style="width:70px;"><p align="right">126.71</p></td>
      <td style="width:61px;"><p align="right">4.10</p></td>
      <td style="width:114px;"><p align="right">118.9-136.8</p></td>
      <td style="width:79px;"><p align="right">124.28</p></td>
      <td style="width:60px;"><p align="right">4.01</p></td>
      <td style="width:108px;"><p align="right">110.1-135.6</p></td>
      <td style="width:72px;"><p align="right">5.072</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>NHt</p></td>
      <td style="width:70px;"><p align="right">49.83</p></td>
      <td style="width:61px;"><p align="right">2.77</p></td>
      <td style="width:114px;"><p align="right">42.08-56.79</p></td>
      <td style="width:79px;"><p align="right">48.71</p></td>
      <td style="width:60px;"><p align="right">3.31</p></td>
      <td style="width:108px;"><p align="right">39.2-57.25</p></td>
      <td style="width:72px;"><p align="right">3.008</p></td>
      <td style="width:62px;"><p align="right">.003**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>NBr</p></td>
      <td style="width:70px;"><p align="right">25.38</p></td>
      <td style="width:61px;"><p align="right">1.90</p></td>
      <td style="width:114px;"><p align="right">21.58-30.38</p></td>
      <td style="width:79px;"><p align="right">24.63</p></td>
      <td style="width:60px;"><p align="right">2.04</p></td>
      <td style="width:108px;"><p align="right">16.0-29.25</p></td>
      <td style="width:72px;"><p align="right">3.177</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>OHt</p></td>
      <td style="width:70px;"><p align="right">32.63</p></td>
      <td style="width:61px;"><p align="right">1.50</p></td>
      <td style="width:114px;"><p align="right">29.79-36.63</p></td>
      <td style="width:79px;"><p align="right">32.67</p></td>
      <td style="width:60px;"><p align="right">2.07</p></td>
      <td style="width:108px;"><p align="right">26.15-37.33</p></td>
      <td style="width:72px;"><p align="right">-.173</p></td>
      <td style="width:62px;"><p align="right">.863</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>OBr</p></td>
      <td style="width:70px;"><p align="right">&nbsp;39.15</p></td>
      <td style="width:61px;"><p align="right">1.87</p></td>
      <td style="width:114px;"><p align="right">35.08-43.29</p></td>
      <td style="width:79px;"><p align="right">38.30</p></td>
      <td style="width:60px;"><p align="right">1.80</p></td>
      <td style="width:108px;"><p align="right">32.35-42.18</p></td>
      <td style="width:72px;"><p align="right">3.933</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>IOBr</p></td>
      <td style="width:70px;"><p align="right">20.62</p></td>
      <td style="width:61px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">17.26-26.14</p></td>
      <td style="width:79px;"><p align="right">19.727</p></td>
      <td style="width:60px;"><p align="right">2.10</p></td>
      <td style="width:108px;"><p align="right">15.05-25.45</p></td>
      <td style="width:72px;"><p align="right">3.638</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FLt</p></td>
      <td style="width:70px;"><p align="right">96.19</p></td>
      <td style="width:61px;"><p align="right">4.45</p></td>
      <td style="width:114px;"><p align="right">81.24-105.22</p></td>
      <td style="width:79px;"><p align="right">93.58</p></td>
      <td style="width:60px;"><p align="right">5.05</p></td>
      <td style="width:108px;"><p align="right">80.05-111.40</p></td>
      <td style="width:72px;"><p align="right">4.566</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>CBLt</p></td>
      <td style="width:70px;"><p align="right">101.75</p></td>
      <td style="width:61px;"><p align="right">3.69</p></td>
      <td style="width:114px;"><p align="right">91.97-110.53</p></td>
      <td style="width:79px;"><p align="right">99.95</p></td>
      <td style="width:60px;"><p align="right">4.45</p></td>
      <td style="width:108px;"><p align="right">85.0-112.40</p></td>
      <td style="width:72px;"><p align="right">3.629</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FMBr</p></td>
      <td style="width:70px;"><p align="right">28.99</p></td>
      <td style="width:61px;"><p align="right">1.77</p></td>
      <td style="width:114px;"><p align="right">25.0-32.18</p></td>
      <td style="width:79px;"><p align="right">28.43</p></td>
      <td style="width:60px;"><p align="right">2.05</p></td>
      <td style="width:108px;"><p align="right">23.0-33.45</p></td>
      <td style="width:72px;"><p align="right">2.418</p></td>
      <td style="width:62px;"><p align="right">.016*</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>FMLt</p></td>
      <td style="width:70px;"><p align="right">34.24</p></td>
      <td style="width:61px;"><p align="right">2.31</p></td>
      <td style="width:114px;"><p align="right">28.74-39.29</p></td>
      <td style="width:79px;"><p align="right">34.30</p></td>
      <td style="width:60px;"><p align="right">2.52</p></td>
      <td style="width:108px;"><p align="right">25.28-40.05</p></td>
      <td style="width:72px;"><p align="right">-.222</p></td>
      <td style="width:62px;"><p align="right">.824</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>UFHt</p></td>
      <td style="width:70px;"><p align="right">66.75</p></td>
      <td style="width:61px;"><p align="right">3.73</p></td>
      <td style="width:114px;"><p align="right">56.56-75.74</p></td>
      <td style="width:79px;"><p align="right">65.43</p></td>
      <td style="width:60px;"><p align="right">4.67</p></td>
      <td style="width:108px;"><p align="right">52.45-80.25</p></td>
      <td style="width:72px;"><p align="right">2.553</p></td>
      <td style="width:62px;"><p align="right">.011</p></td>
    </tr>
    <tr>
      <td style="width:91px;"><p>&nbsp;BZBr</p></td>
      <td style="width:70px;"><p align="right">127.07</p></td>
      <td style="width:61px;"><p align="right">3.87</p></td>
      <td style="width:114px;"><p align="right">117.38-135.67</p></td>
      <td style="width:79px;"><p align="right">123.99</p></td>
      <td style="width:60px;"><p align="right">4.68</p></td>
      <td style="width:108px;"><p align="right">112.25-140.30</p></td>
      <td style="width:72px;"><p align="right">5.884</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
  </tbody>
</table>

<p><strong>Table<em> 2.</em></strong>&nbsp;Descriptive statistics, t-test and significance of differences between mean of contemporary and subrecent-females.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:77px;"><p align="center"><strong>Variables</strong></p></td>
      <td colspan="3" style="width:246px;"><p align="center"><strong>Contemporary- Females</strong></p></td>
      <td colspan="3" style="width:250px;"><p align="center"><strong>Subrecent- Females</strong></p></td>
      <td rowspan="2" style="width:66px;"><p align="center"><strong>t-value</strong></p></td>
      <td rowspan="2" style="width:62px;"><p align="center"><strong>Sig.</strong></p></td>
    </tr>
    <tr>
      <td style="width:72px;height:22px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;height:22px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;height:22px;"><p align="center"><strong>Min.-Max.</strong></p></td>
      <td style="width:76px;height:22px;"><p align="center"><strong>Mean</strong></p></td>
      <td style="width:60px;height:22px;"><p align="center"><strong>SD</strong></p></td>
      <td style="width:114px;height:22px;"><p align="center"><strong>Min.-Max.</strong></p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxCLt</p></td>
      <td style="width:72px;"><p align="right">171.67</p></td>
      <td style="width:60px;"><p align="right">6.54</p></td>
      <td style="width:114px;"><p align="right">160-183.4</p></td>
      <td style="width:76px;"><p align="right">171.32</p></td>
      <td style="width:60px;"><p align="right">5.65</p></td>
      <td style="width:114px;"><p align="right">152.3-185.60</p></td>
      <td style="width:66px;"><p align="right">.338</p></td>
      <td style="width:62px;"><p align="right">.736</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxCBr</p></td>
      <td style="width:72px;"><p align="right">127.29</p></td>
      <td style="width:60px;"><p align="right">4.11</p></td>
      <td style="width:114px;"><p align="right">120.5-137.13</p></td>
      <td style="width:76px;"><p align="right">123.08</p></td>
      <td style="width:60px;"><p align="right">4.85</p></td>
      <td style="width:114px;"><p align="right">110.0-135.60</p></td>
      <td style="width:66px;"><p align="right">5.286</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>BBrHt</p></td>
      <td style="width:72px;"><p align="right">129.8</p></td>
      <td style="width:60px;"><p align="right">4.33</p></td>
      <td style="width:114px;"><p align="right">121.3-137.90</p></td>
      <td style="width:76px;"><p align="right">125.80</p></td>
      <td style="width:60px;"><p align="right">4.68</p></td>
      <td style="width:114px;"><p align="right">114.5-137.9</p></td>
      <td style="width:66px;"><p align="right">5.098</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MaxFBr</p></td>
      <td style="width:72px;"><p align="right">109.18</p></td>
      <td style="width:60px;"><p align="right">4.37</p></td>
      <td style="width:114px;"><p align="right">102.33-118.97</p></td>
      <td style="width:76px;"><p align="right">106.23</p></td>
      <td style="width:60px;"><p align="right">3.43</p></td>
      <td style="width:114px;"><p align="right">97.9-116.8</p></td>
      <td style="width:66px;"><p align="right">4.636</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>MinFBr</p></td>
      <td style="width:72px;"><p align="right">90.84</p></td>
      <td style="width:60px;"><p align="right">3.50</p></td>
      <td style="width:114px;"><p align="right">82.34-98.33</p></td>
      <td style="width:76px;"><p align="right">87.89</p></td>
      <td style="width:60px;"><p align="right">3.54</p></td>
      <td style="width:114px;"><p align="right">78.45-96.10</p></td>
      <td style="width:66px;"><p align="right">4.872</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>ABrHt</p></td>
      <td style="width:72px;"><p align="right">122.05</p></td>
      <td style="width:60px;"><p align="right">3.06</p></td>
      <td style="width:114px;"><p align="right">115.6-127.53</p></td>
      <td style="width:76px;"><p align="right">118.82</p></td>
      <td style="width:60px;"><p align="right">3.82</p></td>
      <td style="width:114px;"><p align="right">107.90-128.90</p></td>
      <td style="width:66px;"><p align="right">5.207</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>NHt</p></td>
      <td style="width:72px;"><p align="right">46.47</p></td>
      <td style="width:60px;"><p align="right">3.44</p></td>
      <td style="width:114px;"><p align="right">39.21-53.04</p></td>
      <td style="width:76px;"><p align="right">45.61</p></td>
      <td style="width:60px;"><p align="right">2.62</p></td>
      <td style="width:114px;"><p align="right">39.05-53.40</p></td>
      <td style="width:66px;"><p align="right">1.743</p></td>
      <td style="width:62px;"><p align="right">.083</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>NBr</p></td>
      <td style="width:72px;"><p align="right">24.08</p></td>
      <td style="width:60px;"><p align="right">1.34</p></td>
      <td style="width:114px;"><p align="right">21.33-26.92</p></td>
      <td style="width:76px;"><p align="right">22.99</p></td>
      <td style="width:60px;"><p align="right">1.80</p></td>
      <td style="width:114px;"><p align="right">18.40-28.00</p></td>
      <td style="width:66px;"><p align="right">3.763</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>OHt</p></td>
      <td style="width:72px;"><p align="right">32.53</p></td>
      <td style="width:60px;"><p align="right">1.75</p></td>
      <td style="width:114px;"><p align="right">29.43-35.56</p></td>
      <td style="width:76px;"><p align="right">31.72</p></td>
      <td style="width:60px;"><p align="right">2.01</p></td>
      <td style="width:114px;"><p align="right">25.30-36.33</p></td>
      <td style="width:66px;"><p align="right">2.455</p></td>
      <td style="width:62px;"><p align="right">.015*</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>OBr</p></td>
      <td style="width:72px;"><p align="right">37.83</p></td>
      <td style="width:60px;"><p align="right">1.20</p></td>
      <td style="width:114px;"><p align="right">35.06-40.05</p></td>
      <td style="width:76px;"><p align="right">36.91</p></td>
      <td style="width:60px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">31.38-40.40</p></td>
      <td style="width:66px;"><p align="right">2.977</p></td>
      <td style="width:62px;"><p align="right">.003**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>IOBr</p></td>
      <td style="width:72px;"><p align="right">19.54</p></td>
      <td style="width:60px;"><p align="right">1.82</p></td>
      <td style="width:114px;"><p align="right">15.41-22.57</p></td>
      <td style="width:76px;"><p align="right">18.28</p></td>
      <td style="width:60px;"><p align="right">2.00</p></td>
      <td style="width:114px;"><p align="right">14.1-24.25</p></td>
      <td style="width:66px;"><p align="right">3.758</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FLt</p></td>
      <td style="width:72px;"><p align="right">92.56</p></td>
      <td style="width:60px;"><p align="right">4.03</p></td>
      <td style="width:114px;"><p align="right">83.57-101.83</p></td>
      <td style="width:76px;"><p align="right">89.29</p></td>
      <td style="width:60px;"><p align="right">4.60</p></td>
      <td style="width:114px;"><p align="right">77.4-100.45</p></td>
      <td style="width:66px;"><p align="right">4.305</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>CBLt</p></td>
      <td style="width:72px;"><p align="right">96.99</p></td>
      <td style="width:60px;"><p align="right">3.70</p></td>
      <td style="width:114px;"><p align="right">89.07-102.73</p></td>
      <td style="width:76px;"><p align="right">94.69</p></td>
      <td style="width:60px;"><p align="right">4.04</p></td>
      <td style="width:114px;"><p align="right">82.4-104.50</p></td>
      <td style="width:66px;"><p align="right">3.404</p></td>
      <td style="width:62px;"><p align="right">.001**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FMBr</p></td>
      <td style="width:72px;"><p align="right">27.75</p></td>
      <td style="width:60px;"><p align="right">1.68</p></td>
      <td style="width:114px;"><p align="right">24.33-30.21</p></td>
      <td style="width:76px;"><p align="right">27.15</p></td>
      <td style="width:60px;"><p align="right">2.20</p></td>
      <td style="width:114px;"><p align="right">22.45-33.10</p></td>
      <td style="width:66px;"><p align="right">1.704</p></td>
      <td style="width:62px;"><p align="right">.090</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>FMLt</p></td>
      <td style="width:72px;"><p align="right">33.03</p></td>
      <td style="width:60px;"><p align="right">1.88</p></td>
      <td style="width:114px;"><p align="right">30.22-36.43</p></td>
      <td style="width:76px;"><p align="right">32.89</p></td>
      <td style="width:60px;"><p align="right">2.31</p></td>
      <td style="width:114px;"><p align="right">26.3-38.45</p></td>
      <td style="width:66px;"><p align="right">.390</p></td>
      <td style="width:62px;"><p align="right">.697</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>UFHt</p></td>
      <td style="width:72px;"><p align="right">63.02</p></td>
      <td style="width:60px;"><p align="right">4.43</p></td>
      <td style="width:114px;"><p align="right">56.05-72.22</p></td>
      <td style="width:76px;"><p align="right">60.85</p></td>
      <td style="width:60px;"><p align="right">3.93</p></td>
      <td style="width:114px;"><p align="right">53.15-72.10</p></td>
      <td style="width:66px;"><p align="right">3.112</p></td>
      <td style="width:62px;"><p align="right">.002**</p></td>
    </tr>
    <tr>
      <td style="width:77px;"><p>&nbsp;BZBr</p></td>
      <td style="width:72px;"><p align="right">119.99</p></td>
      <td style="width:60px;"><p align="right">4.52</p></td>
      <td style="width:114px;"><p align="right">111.87-130.33</p></td>
      <td style="width:76px;"><p align="right">115.6</p></td>
      <td style="width:60px;"><p align="right">3.88</p></td>
      <td style="width:114px;"><p align="right">107.1-124.40</p></td>
      <td style="width:66px;"><p align="right">6.301</p></td>
      <td style="width:62px;"><p align="right">.000***</p></td>
    </tr>
  </tbody>
</table>

<p><strong>Table 3.</strong>&nbsp;Formulae to calculate respective index, normal range and the classification system according to respective anthropologists and population frequency for contemporary and subrecent samples for each craniofacial index in North Indian population.</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:150px;"><p align="center"><strong>Name of the index</strong></p></td>
      <td style="width:150px;"><p align="center"><strong>Formula of the index</strong></p></td>
      <td style="width:138px;"><p align="center"><strong>Classification I</strong></p></td>
      <td style="width:102px;"><p align="center"><strong>Classification II</strong></p></td>
      <td style="width:126px;"><p align="center"><strong>Normal range variation</strong></p></td>
      <td style="width:59px;"><p align="center"><strong>Scientist found the normal range variation</strong></p></td>
      <td style="width:109px;"><p align="center"><strong>Contemporary population Frequency in %&nbsp; (N=158)</strong></p></td>
      <td style="width:109px;"><p align="center"><strong>Subrecent population Frequency in %&nbsp; (N=325)</strong></p></td>
    </tr>
    <tr>
      <td rowspan="7" style="width:150px;">
      <ol>
        <li align="center">Cranial index or length &ndash; breadth cranial index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="7" style="width:150px;"><p align="center">(max. cranial breadth/max. cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Ultradolichocranial</p></td>
      <td style="width:102px;"><p align="center">Very long skull</p></td>
      <td style="width:126px;"><p align="center">&le;64.99</p></td>
      <td rowspan="7" style="width:59px;"><p align="center">Garson</p></td>
      <td style="width:109px;"><p align="center">03.79</p></td>
      <td style="width:109px;"><p align="center">2.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperdolichocranial</p></td>
      <td style="width:102px;"><p align="center">Very long skull</p></td>
      <td style="width:126px;"><p align="center">65.0-69.99</p></td>
      <td style="width:109px;"><p align="center">36.71</p></td>
      <td style="width:109px;"><p align="center">36.0</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:27px;"><p align="center">Dolichocranial</p></td>
      <td style="width:102px;height:27px;"><p align="center">Long skull</p></td>
      <td style="width:126px;height:27px;"><p align="center">70.0-74.99</p></td>
      <td style="width:109px;height:27px;"><p align="center">40.51</p></td>
      <td style="width:109px;height:27px;"><p align="center">49.23</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">75.0-79.99</p></td>
      <td style="width:109px;"><p align="center">13.92</p></td>
      <td style="width:109px;"><p align="center">10.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Brachycranial</p></td>
      <td style="width:102px;"><p align="center">Broad, short skull</p></td>
      <td style="width:126px;"><p align="center">80.0-84.99</p></td>
      <td style="width:109px;"><p align="center">5.06</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperbrachycranial</p></td>
      <td style="width:102px;"><p align="center">Very broad and short skull</p></td>
      <td style="width:126px;"><p align="center">85.0-89.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:29px;"><p align="center">Ultrabrachycranial</p></td>
      <td style="width:102px;height:29px;"><p align="center">Extremely broad and short skull</p></td>
      <td style="width:126px;height:29px;"><p align="center">&ge;90.0</p></td>
      <td style="width:109px;height:29px;"><p align="center">0</p></td>
      <td style="width:109px;height:29px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td rowspan="3">
      <ol>
        <li align="center" value="2">Length &ndash; height cranial index or vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Basion &ndash; bregma height/maximum cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Chamaecranial</p></td>
      <td style="width:102px;"><p align="center">Low skull</p></td>
      <td style="width:126px;"><p align="center">&le;69.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">10.13</p></td>
      <td style="width:109px;"><p align="center">11.08</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Orthocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">70.0-74.99</p></td>
      <td style="width:109px;"><p align="center">45.57</p></td>
      <td style="width:109px;"><p align="center">58.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:16px;"><p align="center">Hypsicranial</p></td>
      <td style="width:102px;height:16px;"><p align="center">High skull</p></td>
      <td style="width:126px;height:16px;"><p align="center">&ge;75.0</p></td>
      <td style="width:109px;height:16px;"><p align="center">44.3</p></td>
      <td style="width:109px;height:16px;"><p align="center">30.15</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="3">Breadth- height index or transverse vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Basion &ndash; bregma height/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Tapeinocranial</p></td>
      <td style="width:102px;"><p align="center">Low, broad skull</p></td>
      <td style="width:126px;"><p align="center">&le;91.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">92.0-97.99</p></td>
      <td style="width:109px;"><p align="center">8.86</p></td>
      <td style="width:109px;"><p align="center">9.54</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Acrocranial</p></td>
      <td style="width:102px;"><p align="center">High, narrow skull</p></td>
      <td style="width:126px;"><p align="center">&ge;98.0</p></td>
      <td style="width:109px;"><p align="center">91.14</p></td>
      <td style="width:109px;"><p align="center">90.46</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="4">Auriculo vertical index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Auriculo&ndash; bregmatic height/ max cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">Chamaecranial</p></td>
      <td style="width:102px;"><p align="center">Low skull</p></td>
      <td style="width:126px;"><p align="center">&le;57.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Orthocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">58.0-62.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hypsicranial</p></td>
      <td style="width:102px;"><p align="center">High skull</p></td>
      <td style="width:126px;"><p align="center">&ge;63.0</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="5">Auriculo transverse index (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(Auriculo &ndash; bregmatic height/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Tapeinocranial</p></td>
      <td style="width:102px;"><p align="center">Low, broad skull</p></td>
      <td style="width:126px;"><p align="center">&le;79.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Seller</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriocranial</p></td>
      <td style="width:102px;"><p align="center">Mid skull</p></td>
      <td style="width:126px;"><p align="center">80.0-85.99</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Acrocranial</p></td>
      <td style="width:102px;"><p align="center">High, narrow skull</p></td>
      <td style="width:126px;"><p align="center">&ge;86.0</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
      <td style="width:109px;"><p align="center">100</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="6">. Frontal index /Transverse Fronto Prietal index /trans.fronto breadth index/ (Singh and Bhasin, 2004)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(minimum frontal breadth/max. cranial breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Stenometopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&le;65.99</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">2.53</p></td>
      <td style="width:109px;"><p align="center">2.46</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Metriometopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">66.0-68.99</p></td>
      <td style="width:109px;"><p align="center">12.66</p></td>
      <td style="width:109px;"><p align="center">14.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Eurymetopic</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&ge;69.0</p></td>
      <td style="width:109px;"><p align="center">84.81</p></td>
      <td style="width:109px;"><p align="center">82.77</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;height:20px;"><p align="center">7. Foramen magnum index (Singh and Bhasin, 2004)</p></td>
      <td rowspan="3" style="width:150px;height:20px;"><p align="center">(breadth of foramen magnum/ length of foramen magnum)*100</p></td>
      <td style="width:138px;height:20px;"><p align="center">Narrow</p></td>
      <td rowspan="3" style="width:102px;height:20px;"><p align="center">-----</p></td>
      <td style="width:126px;height:20px;"><p align="center">&le;81.99</p></td>
      <td rowspan="3" style="width:59px;height:20px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;height:20px;"><p align="center">&nbsp;37.97.</p></td>
      <td style="width:109px;height:20px;"><p align="center">44.31</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:20px;"><p align="center">Medium</p></td>
      <td style="width:126px;height:20px;"><p align="center">82.0-85.9</p></td>
      <td style="width:109px;height:20px;"><p align="center">24.05</p></td>
      <td style="width:109px;height:20px;"><p align="center">26.77</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:20px;"><p align="center">Broad</p></td>
      <td style="width:126px;height:20px;"><p align="center">&ge;86.0</p></td>
      <td style="width:109px;height:20px;"><p align="center">37.97</p></td>
      <td style="width:109px;height:20px;"><p align="center">28.92</p></td>
    </tr>
    <tr>
      <td rowspan="5" style="width:150px;height:44px;"><p>8. Upper facial index (Krogman and Iscan, 1986)</p></td>
      <td rowspan="5" style="width:150px;height:44px;"><p align="center">(upper facial height/ bizygomatic breadth)*100</p></td>
      <td style="width:138px;height:44px;"><p align="center">Hypereuryen</p></td>
      <td style="width:102px;height:44px;"><p align="center">Very short upper face(very broad face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:44px;"><p align="center">&le;44.99</p></td>
      <td rowspan="5" style="width:59px;height:44px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;height:44px;"><p align="center">01.26</p></td>
      <td style="width:109px;height:44px;"><p align="center">0.6</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Euryen</p></td>
      <td style="width:102px;"><p align="center">Short upper face (Broad face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">45.0-49.99</p></td>
      <td style="width:109px;"><p align="center">18.35</p></td>
      <td style="width:109px;"><p align="center">20.92</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesen</p></td>
      <td style="width:102px;"><p align="center">Medium upper face (Round face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">50.0-54.99</p></td>
      <td style="width:109px;"><p align="center">63.29</p></td>
      <td style="width:109px;"><p align="center">52.31</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Lepten</p></td>
      <td style="width:102px;"><p align="center">Long upper face (long face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">55.0-59.99</p></td>
      <td style="width:109px;"><p align="center">14.56</p></td>
      <td style="width:109px;"><p align="center">24.62</p></td>
    </tr>
    <tr>
      <td style="width:138px;height:47px;"><p align="center">Hyperlepten</p></td>
      <td style="width:102px;height:47px;"><p align="center">Very long upper face (very long face)</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:47px;"><p align="center">&ge;60.0</p></td>
      <td style="width:109px;height:47px;"><p align="center">02.53</p></td>
      <td style="width:109px;height:47px;"><p align="center">1.54</p></td>
    </tr>
    <tr>
      <td rowspan="4" style="width:150px;"><p>9.Nasal index (Singh and Bhasin, 2004)</p></td>
      <td rowspan="4" style="width:150px;"><p align="center">(nasal breadth/ nasal height)*100</p></td>
      <td style="width:138px;"><p align="center">Leptorhinae</p></td>
      <td style="width:102px;"><p align="center">Narrow Nose</p></td>
      <td style="width:126px;"><p align="center">&le;46.99</p></td>
      <td rowspan="4" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">18.99</p></td>
      <td style="width:109px;"><p align="center">22.15</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesorhinae</p></td>
      <td style="width:102px;"><p align="center">Medium nose</p></td>
      <td style="width:126px;"><p align="center">47.0-50.99</p></td>
      <td style="width:109px;"><p align="center">29.11</p></td>
      <td style="width:109px;"><p align="center">31.08</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Chamaerhinae</p></td>
      <td style="width:102px;"><p align="center">Broad nose</p></td>
      <td style="width:126px;"><p align="center">51.0-57.99</p></td>
      <td style="width:109px;"><p align="center">43.04</p></td>
      <td style="width:109px;"><p align="center">39.38</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hyperchamaerhinae</p></td>
      <td style="width:102px;"><p align="center">Very broad nose</p></td>
      <td style="width:126px;"><p align="center">&ge;58.0</p></td>
      <td style="width:109px;"><p align="center">8.86</p></td>
      <td style="width:109px;"><p align="center">7.38</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li value="10">Orbital index (Krogman and Iscan, 1986)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(max. orbital hieght/ maximum orbital breadth)*100</p></td>
      <td style="width:138px;"><p align="center">Chmaeconch</p></td>
      <td style="width:102px;"><p align="center">Low</p></td>
      <td style="width:126px;"><p align="center">&le;82.9</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Broca</p></td>
      <td style="width:109px;"><p align="center">39.24</p></td>
      <td style="width:109px;"><p align="center">29.85</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesoconch</p></td>
      <td style="width:102px;"><p align="center">Medium</p></td>
      <td style="width:126px;"><p align="center">83.0-88.9</p></td>
      <td style="width:109px;"><p align="center">43.04</p></td>
      <td style="width:109px;"><p align="center">43.38</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Hypsiconch</p></td>
      <td style="width:102px;"><p align="center">High</p></td>
      <td style="width:126px;"><p align="center">&ge;89.0</p></td>
      <td style="width:109px;"><p align="center">15.19</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">26.77</p></td>
    </tr>
    <tr>
      <td rowspan="3" style="width:150px;">
      <ol>
        <li align="center" value="11">Gnathic index (Krogman and Iscan, 1986)</li>
      </ol></td>
      <td rowspan="3" style="width:150px;"><p align="center">(basion &ndash; prosthion distance/basion nasion distance)*100</p></td>
      <td style="width:138px;"><p align="center">Orthognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&le;97.9</p></td>
      <td rowspan="3" style="width:59px;"><p align="center">Martin and Seller</p></td>
      <td style="width:109px;"><p align="center">82.28</p></td>
      <td style="width:109px;"><p align="center">84.62</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Mesognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">98.0-102.9</p></td>
      <td style="width:109px;"><p align="center">13.92</p></td>
      <td style="width:109px;"><p align="center">14.46</p></td>
    </tr>
    <tr>
      <td style="width:138px;"><p align="center">Prognathous</p></td>
      <td style="width:102px;"><p align="center">-----</p></td>
      <td style="width:126px;"><p align="center">&ge;103.0</p></td>
      <td style="width:109px;"><p align="center">3.8</p></td>
      <td style="width:109px;"><p align="center">0.92</p></td>
    </tr>
    <tr>
      <td rowspan="5" style="width:150px;height:35px;">
      <ol>
        <li align="center" value="12">Jugo-frontal index (Wilder, 1920)</li>
      </ol></td>
      <td rowspan="5" style="width:150px;height:35px;"><p align="center">(Minimum frontal breadth/ Bizygomatic breadth)* 100</p></td>
      <td rowspan="5" style="width:138px;height:35px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;height:35px;"><p align="center">Very narrow</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;height:35px;"><p align="center">x-69.99, x-71.99</p>

      <p align="center">&nbsp;</p></td>
      <td rowspan="5" style="width:59px;height:35px;"><p align="center">&nbsp;Martin</p></td>
      <td style="width:109px;height:35px;"><p>13.92</p></td>
      <td style="width:109px;height:35px;"><p>8.62</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Narrow</p></td>
      <td style="width:126px;"><p align="center">70-74.99,72-76.99</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>53.16</p></td>
      <td style="width:109px;"><p>54.15</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Moderate</p></td>
      <td style="width:126px;"><p align="center">75-79.99,77-81.99</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>30.38</p></td>
      <td style="width:109px;"><p>&nbsp;34.77</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Wide</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">80-84.99, 82-86.99</p></td>
      <td style="width:109px;"><p>2.53</p></td>
      <td style="width:109px;"><p>2.46</p></td>
    </tr>
    <tr>
      <td style="width:102px;"><p align="center">Very wide</p></td>
      <td style="width:126px;"><p align="center">85-x, 87-x</p>

      <p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">0</p></td>
      <td style="width:109px;"><p>0</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>13. Nasal width &ndash; facial height index (Cameron, 1929)**</p></td>
      <td style="width:150px;"><p align="center">(nasal breadth/upper facial height)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>14.Interorbital breadth &ndash; facial height index&nbsp; (Cameron, 1929)**</p></td>
      <td style="width:150px;"><p align="center">(interorbital breadth/ upper facial height)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>15. Transverse frontal index (Wilder, 1920**)</p></td>
      <td style="width:150px;"><p align="center">(Min frontal breadth/Max frontal breadth)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p>&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>16.Trans. cranio-facial index (Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">Bizygomatic breadth/ Maximum cranial breadth)* 100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>17. Longitudinal craniofacial index(Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">(Basion &ndash;prosthion length/ Max cranial length)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:150px;"><p>18.Vertical craniofacial index (Wilder, 1920)**</p></td>
      <td style="width:150px;"><p align="center">(Nasion &ndash;prosthion/basion-bregma)*100</p></td>
      <td style="width:138px;"><p align="center">&nbsp;</p></td>
      <td style="width:102px;"><p align="center">&nbsp;</p></td>
      <td style="width:126px;"><p align="center">&nbsp;</p></td>
      <td style="width:59px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
      <td style="width:109px;"><p align="center">&nbsp;</p></td>
    </tr>
  </tbody>
</table>

<p># The index classification is slightly modified. The range for indices is modified by adding .09 in higher range of each category. so that the population can be classified according to these classification. ** the classification range was not available for these variables.</p>

<p><strong>Table 4.</strong>&nbsp;Craniometric Indices for Contemporary North Indian sample with significance (P value) and sexing classification accuracy.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:209px;height:22px;"><p align="center"><strong>Indices</strong></p></td>
      <td style="width:82px;height:22px;"><p align="center"><strong>Male</strong></p></td>
      <td style="width:99px;height:22px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:83px;height:22px;"><p align="center"><strong>Female</strong></p></td>
      <td style="width:94px;height:22px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:77px;height:22px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:82px;height:22px;"><p align="center"><strong>Sexing accuracy (%)</strong></p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Cranial index</p></td>
      <td style="width:82px;height:23px;"><p align="center">70.35</p></td>
      <td style="width:99px;height:23px;"><p align="center">63.35-84.65</p></td>
      <td style="width:83px;height:23px;"><p align="center">74.26</p></td>
      <td style="width:94px;height:23px;"><p align="center">68.92-82.23</p></td>
      <td style="width:77px;height:23px;"><p align="center">.000***</p></td>
      <td style="width:82px;height:23px;"><p align="center">72.2</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Vertical index</p></td>
      <td style="width:82px;height:22px;"><p align="center">73.62</p></td>
      <td style="width:99px;height:22px;"><p align="center">65.96-85.09</p></td>
      <td style="width:83px;height:22px;"><p align="center">75.67</p></td>
      <td style="width:94px;height:22px;"><p align="center">70.12-82.03</p></td>
      <td style="width:77px;height:22px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:22px;"><p align="center">60.8</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Transverse vertical Index</p></td>
      <td style="width:82px;height:22px;"><p align="center">104.76</p></td>
      <td style="width:99px;height:22px;"><p align="center">92.34-116.32</p></td>
      <td style="width:83px;height:22px;"><p align="center">102.06</p></td>
      <td style="width:94px;height:22px;"><p align="center">93.7-109.5</p></td>
      <td style="width:77px;height:22px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:22px;"><p align="center">62.0</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Auriculo vertical index</p></td>
      <td style="width:82px;height:23px;"><p align="center">69.49</p></td>
      <td style="width:99px;height:23px;"><p align="center">64.25-79.80</p></td>
      <td style="width:83px;height:23px;"><p align="center">71.17</p></td>
      <td style="width:94px;height:23px;"><p align="center">65.58-75.63</p></td>
      <td style="width:77px;height:23px;"><p align="center">.001**</p></td>
      <td style="width:82px;height:23px;"><p align="center">63.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:19px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:82px;height:19px;"><p align="center">98.91</p></td>
      <td style="width:99px;height:19px;"><p align="center">91.16-110.49</p></td>
      <td style="width:83px;height:19px;"><p align="center">95.98</p></td>
      <td style="width:94px;height:19px;"><p align="center">89.73-104.32</p></td>
      <td style="width:77px;height:19px;"><p align="center">.000***</p></td>
      <td style="width:82px;height:19px;"><p align="center">60.8</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Frontal index</p></td>
      <td style="width:82px;height:23px;"><p align="center">72.82</p></td>
      <td style="width:99px;height:23px;"><p align="center">65.82-83.84</p></td>
      <td style="width:83px;height:23px;"><p align="center">71.41</p></td>
      <td style="width:94px;height:23px;"><p align="center">63.81-78.72</p></td>
      <td style="width:77px;height:23px;"><p align="center">.022*</p></td>
      <td style="width:82px;height:23px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:21px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:82px;height:21px;"><p align="center">84.89</p></td>
      <td style="width:99px;height:21px;"><p align="center">75.09-105.25</p></td>
      <td style="width:83px;height:21px;"><p align="center">84.24</p></td>
      <td style="width:94px;height:21px;"><p align="center">70.24-96.26</p></td>
      <td style="width:77px;height:21px;"><p align="center">.531</p></td>
      <td style="width:82px;height:21px;"><p align="center">44.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Upper Facial index</p></td>
      <td style="width:82px;height:23px;"><p align="center">52.55</p></td>
      <td style="width:99px;height:23px;"><p align="center">44.87-61.97</p></td>
      <td style="width:83px;height:23px;"><p align="center">52.51</p></td>
      <td style="width:94px;height:23px;"><p align="center">46.03-57.62</p></td>
      <td style="width:77px;height:23px;"><p align="center">.929</p></td>
      <td style="width:82px;height:23px;"><p align="center">35.4</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Nasal index</p></td>
      <td style="width:82px;height:23px;"><p align="center">51.10</p></td>
      <td style="width:99px;height:23px;"><p align="center">41.28-64.11</p></td>
      <td style="width:83px;height:23px;"><p align="center">52.06</p></td>
      <td style="width:94px;height:23px;"><p align="center">44.36-61.39</p></td>
      <td style="width:77px;height:23px;"><p align="center">.242</p></td>
      <td style="width:82px;height:23px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:22px;"><p>Posterior Orbital index</p></td>
      <td style="width:82px;height:22px;"><p align="center">83.52</p></td>
      <td style="width:99px;height:22px;"><p align="center">70.74-95.16</p></td>
      <td style="width:83px;height:22px;"><p align="center">86.09</p></td>
      <td style="width:94px;height:22px;"><p align="center">77.8-99.98</p></td>
      <td style="width:77px;height:22px;"><p align="center">.006**</p></td>
      <td style="width:82px;height:22px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Gnathic index</p></td>
      <td style="width:82px;height:23px;"><p align="center">94.58</p></td>
      <td style="width:99px;height:23px;"><p align="center">85.67-106.37</p></td>
      <td style="width:83px;height:23px;"><p align="center">95.5</p></td>
      <td style="width:94px;height:23px;"><p align="center">89.8-104.19</p></td>
      <td style="width:77px;height:23px;"><p align="center">.192</p></td>
      <td style="width:82px;height:23px;"><p align="center">48.1</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Jugo frontal</p></td>
      <td style="width:82px;height:23px;"><p align="center">73.45</p></td>
      <td style="width:99px;height:23px;"><p align="center">64.49-82.69</p></td>
      <td style="width:83px;height:23px;"><p align="center">75.74</p></td>
      <td style="width:94px;height:23px;"><p align="center">70.91-80.46</p></td>
      <td style="width:77px;height:23px;"><p align="center">0.000***</p></td>
      <td style="width:82px;height:23px;"><p align="center">56.3</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:23px;"><p>Nasal breadth-facial ht index</p></td>
      <td style="width:82px;height:23px;"><p align="center">38.17</p></td>
      <td style="width:99px;height:23px;"><p align="center">31.29-49.59</p></td>
      <td style="width:83px;height:23px;"><p align="center">38.34</p></td>
      <td style="width:94px;height:23px;"><p align="center">31.69-43.28</p></td>
      <td style="width:77px;height:23px;"><p align="center">.780</p></td>
      <td style="width:82px;height:23px;"><p align="center">55.7</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Interorbital breadth-facial ht index</p></td>
      <td style="width:82px;height:20px;"><p align="center">30.95</p></td>
      <td style="width:99px;height:20px;"><p align="center">25.45-42.36</p></td>
      <td style="width:83px;height:20px;"><p align="center">31.09</p></td>
      <td style="width:94px;height:20px;"><p align="center">24.09-35.65</p></td>
      <td style="width:77px;height:20px;"><p align="center">.094</p></td>
      <td style="width:82px;height:20px;"><p align="center">50.0</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Transverse frontal index</p></td>
      <td style="width:82px;height:20px;"><p align="center">81.60</p></td>
      <td style="width:99px;height:20px;"><p align="center">74.43-89.19</p></td>
      <td style="width:83px;height:20px;"><p align="center">83.23</p></td>
      <td style="width:94px;height:20px;"><p align="center">79.49-88.19</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.003**</p></td>
      <td style="width:82px;height:20px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Transverse craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">99.24</p></td>
      <td style="width:99px;height:20px;"><p align="center">88.13-111.1</p></td>
      <td style="width:83px;height:20px;"><p align="center">94.35</p></td>
      <td style="width:94px;height:20px;"><p align="center">87.64-104.34</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.000***</p></td>
      <td style="width:82px;height:20px;"><p align="center">58.2</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">52.74</p></td>
      <td style="width:99px;height:20px;"><p align="center">46.74-57.89</p></td>
      <td style="width:83px;height:20px;"><p align="center">53.96</p></td>
      <td style="width:94px;height:20px;"><p align="center">49.56-60.00</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.005**</p></td>
      <td style="width:82px;height:20px;"><p align="center">52.5</p></td>
    </tr>
    <tr>
      <td style="width:209px;height:20px;"><p>Vertical craniofacial index</p></td>
      <td style="width:82px;height:20px;"><p align="center">49.81</p></td>
      <td style="width:99px;height:20px;"><p align="center">48.01-58.71</p></td>
      <td style="width:83px;height:20px;"><p align="center">48.56</p></td>
      <td style="width:94px;height:20px;"><p align="center">42.70-55.49</p></td>
      <td style="width:77px;height:20px;"><p align="center">0.0287*</p></td>
      <td style="width:82px;height:20px;"><p align="center">56.3</p></td>
    </tr>
  </tbody>
</table>

<p><em>*p&lt;.05 Significant, **p&lt;.01Moderate Significant, ***P&lt;.001Highly Significant</em></p>


<p><strong>Table 5.</strong>&nbsp;Craniometric Indices for Subrecent North Indian sample with significance (P value) and sexing classification accuracy.</p>


<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:245px;"><p align="center"><strong>Indices</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>Male</strong></p></td>
      <td style="width:99px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:72px;"><p align="center"><strong>Female</strong></p></td>
      <td style="width:96px;"><p align="center"><strong>Min-Max</strong></p></td>
      <td style="width:60px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:137px;"><p align="center"><strong>Sexing Accuracy (%)</strong></p></td>
    </tr>
    <tr>
      <td style="width:245px;height:19px;"><p>Cranial index</p></td>
      <td style="width:60px;height:19px;"><p align="center">70.20</p></td>
      <td style="width:99px;height:19px;"><p align="center">63.47-77.74</p></td>
      <td style="width:72px;height:19px;"><p align="center">71.89</p></td>
      <td style="width:96px;height:19px;"><p align="center">62.97-79.45</p></td>
      <td style="width:60px;height:19px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:19px;"><p align="center">62.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:24px;"><p>Vertical index</p></td>
      <td style="width:60px;height:24px;"><p align="center">73.32</p></td>
      <td style="width:99px;height:24px;"><p align="center">66.89-79.86</p></td>
      <td style="width:72px;height:24px;"><p align="center">73.46</p></td>
      <td style="width:96px;height:24px;"><p align="center">66.69-78.69</p></td>
      <td style="width:60px;height:24px;"><p align="center">.648</p></td>
      <td style="width:137px;height:24px;"><p align="center">51.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Transverse vertical Index</p></td>
      <td style="width:60px;"><p align="center">104.54</p></td>
      <td style="width:99px;"><p align="center">92.44-115.18</p></td>
      <td style="width:72px;"><p align="center">102.31</p></td>
      <td style="width:96px;"><p align="center">93.26-117.27</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">60.3</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Auriculo vertical index</p></td>
      <td style="width:60px;"><p align="center">69.25</p></td>
      <td style="width:99px;"><p align="center">63.69-75.07</p></td>
      <td style="width:72px;"><p align="center">69.39</p></td>
      <td style="width:96px;"><p align="center">63.38-75.06</p></td>
      <td style="width:60px;"><p align="center">.563</p></td>
      <td style="width:137px;"><p align="center">51.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:60px;"><p align="center">98.73</p></td>
      <td style="width:99px;"><p align="center">90.78-107.27</p></td>
      <td style="width:72px;"><p align="center">96.62</p></td>
      <td style="width:96px;"><p align="center">89.78-107.89</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">61.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Frontal index</p></td>
      <td style="width:60px;"><p align="center">72.85</p></td>
      <td style="width:99px;"><p align="center">62.39-82.50</p></td>
      <td style="width:72px;"><p align="center">71.5</p></td>
      <td style="width:96px;"><p align="center">65.18-82.22</p></td>
      <td style="width:60px;"><p align="center">.001**</p></td>
      <td style="width:137px;"><p align="center">59.4</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:60px;"><p align="center">83.12</p></td>
      <td style="width:99px;"><p align="center">68.95-107.52</p></td>
      <td style="width:72px;"><p align="center">82.64</p></td>
      <td style="width:96px;"><p align="center">70.89-95.68</p></td>
      <td style="width:60px;"><p align="center">.481</p></td>
      <td style="width:137px;"><p align="center">50.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Upper Facial index</p></td>
      <td style="width:60px;"><p align="center">52.79</p></td>
      <td style="width:99px;"><p align="center">44.69-65.17</p></td>
      <td style="width:72px;"><p align="center">52.66</p></td>
      <td style="width:96px;"><p align="center">44.33-61.30</p></td>
      <td style="width:60px;"><p align="center">.737</p></td>
      <td style="width:137px;"><p align="center">49.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Nasal index</p></td>
      <td style="width:60px;"><p align="center">50.85</p></td>
      <td style="width:99px;"><p align="center">38.16-63.88</p></td>
      <td style="width:72px;"><p align="center">50.56</p></td>
      <td style="width:96px;"><p align="center">40.88-64.81</p></td>
      <td style="width:60px;"><p align="center">.616</p></td>
      <td style="width:137px;"><p align="center">50.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Orbital index</p></td>
      <td style="width:60px;"><p align="center">85.41</p></td>
      <td style="width:99px;"><p align="center">69.83-103.19</p></td>
      <td style="width:72px;"><p align="center">86.05</p></td>
      <td style="width:96px;"><p align="center">72.15-100</p></td>
      <td style="width:60px;"><p align="center">.314</p></td>
      <td style="width:137px;"><p align="center">55.7</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Gnathic index</p></td>
      <td style="width:60px;"><p align="center">93.67</p></td>
      <td style="width:99px;"><p align="center">83.13-103.32</p></td>
      <td style="width:72px;"><p align="center">94.35</p></td>
      <td style="width:96px;"><p align="center">83.78-104.55</p></td>
      <td style="width:60px;"><p align="center">.141</p></td>
      <td style="width:137px;"><p align="center">54.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;"><p>Jugo frontal</p></td>
      <td style="width:60px;"><p align="center">73.99</p></td>
      <td style="width:99px;"><p align="center">58.93-82.29</p></td>
      <td style="width:72px;"><p align="center">76.06</p></td>
      <td style="width:96px;"><p align="center">67.39-82.85</p></td>
      <td style="width:60px;"><p align="center">.000***</p></td>
      <td style="width:137px;"><p align="center">53.2</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:23px;"><p>Nasal breadth-facial height index</p></td>
      <td style="width:60px;height:23px;"><p align="center">37.89</p></td>
      <td style="width:99px;height:23px;"><p align="center">27.53-49.60</p></td>
      <td style="width:72px;height:23px;"><p align="center">37.94</p></td>
      <td style="width:96px;height:23px;"><p align="center">29.35-49.12</p></td>
      <td style="width:60px;height:23px;"><p align="center">.925</p></td>
      <td style="width:137px;height:23px;"><p align="center">45.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Interorbital breadth-facial height index</p></td>
      <td style="width:60px;height:21px;"><p align="center">30.27</p></td>
      <td style="width:99px;height:21px;"><p align="center">22.93-41.67</p></td>
      <td style="width:72px;height:21px;"><p align="center">30.16</p></td>
      <td style="width:96px;height:21px;"><p align="center">19.8-42.36</p></td>
      <td style="width:60px;height:21px;"><p align="center">.794</p></td>
      <td style="width:137px;height:21px;"><p align="center">49.4</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Transverse frontal index</p></td>
      <td style="width:60px;height:21px;">
      <table border="0" cellpadding="0" cellspacing="3">
        <tbody>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>

      <p align="center">82.8</p></td>
      <td style="width:99px;height:21px;"><p align="center">75.84-92.01</p></td>
      <td style="width:72px;height:21px;"><p align="center">82.76</p></td>
      <td style="width:96px;height:21px;"><p align="center">76.05-90.45</p></td>
      <td style="width:60px;height:21px;"><p align="center">.884</p></td>
      <td style="width:137px;height:21px;"><p align="center">59.5</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Transverse craniofacial index</p></td>
      <td style="width:60px;height:21px;"><p align="center">98.52</p></td>
      <td style="width:99px;height:21px;"><p align="center">87.89-110.73</p></td>
      <td style="width:72px;height:21px;"><p align="center">94.04</p></td>
      <td style="width:96px;height:21px;"><p align="center">83.73-107.47</p></td>
      <td style="width:60px;height:21px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:21px;"><p align="center">52.0</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:60px;height:21px;"><p align="center">52.14</p></td>
      <td style="width:99px;height:21px;"><p align="center">45.30-58.68</p></td>
      <td style="width:72px;height:21px;"><p align="center">52.15</p></td>
      <td style="width:96px;height:21px;"><p align="center">45.56-61.09</p></td>
      <td style="width:60px;height:21px;"><p align="center">.986</p></td>
      <td style="width:137px;height:21px;"><p align="center">49.8</p></td>
    </tr>
    <tr>
      <td style="width:245px;height:21px;"><p>Vertical craniofacial index</p></td>
      <td style="width:60px;height:21px;">
      <table border="0" cellpadding="0" cellspacing="3">
        <tbody>
          <tr>
            <td>
            <p align="center">49.76</p>
            </td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>

      <p align="center">&nbsp;</p></td>
      <td style="width:99px;height:21px;"><p align="center">40-95-60.05</p></td>
      <td style="width:72px;height:21px;"><p align="center">48.40</p></td>
      <td style="width:96px;height:21px;"><p align="center">40-89-58-19</p></td>
      <td style="width:60px;height:21px;"><p align="center">.000***</p></td>
      <td style="width:137px;height:21px;"><p align="center">55.7</p></td>
    </tr>
  </tbody>
</table>

<p><em>*p&lt;.05 Significant, **p&lt;.01Moderate Significant, ***P&lt;.001Highly Significant</em></p>


<p><strong>Table 6.</strong>&nbsp;Significance of difference between craniometric indices of contemporary and subrecent male sample with effect size.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:207px;height:20px;"><p align="center"><strong>Index</strong></p></td>
      <td colspan="2" style="width:198px;height:20px;"><p align="center"><strong>Males (Mean &plusmn; SD)</strong></p></td>
      <td rowspan="2" style="width:72px;height:20px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:78px;height:20px;"><p align="center"><strong>Effect Size</strong></p></td>
    </tr>
    <tr>
      <td style="width:102px;height:18px;"><p align="center"><strong>Contemporary</strong></p></td>
      <td style="width:96px;height:18px;"><p align="center"><strong>Subrecent</strong></p></td>
      <td style="width:78px;height:18px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:18px;"><p>Cranial index</p></td>
      <td style="width:102px;height:18px;"><p align="center">70.35 &plusmn; 3.83</p></td>
      <td style="width:96px;height:18px;"><p align="center">70.2 &plusmn; 2.91</p></td>
      <td style="width:72px;height:18px;"><p align="center">.697</p></td>
      <td style="width:78px;height:18px;"><p align="center">.044</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:17px;"><p>Vertical index</p></td>
      <td style="width:102px;height:17px;"><p align="center">73.62 &plusmn; 3.85</p></td>
      <td style="width:96px;height:17px;"><p align="center">73.32 &plusmn; 2.80</p></td>
      <td style="width:72px;height:17px;"><p align="center">.437</p></td>
      <td style="width:78px;height:17px;"><p align="center">.087</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:13px;"><p>Transverse vertical Index</p></td>
      <td style="width:102px;height:13px;"><p align="center">104.76 &plusmn; 4.80</p></td>
      <td style="width:96px;height:13px;"><p align="center">104.54 &plusmn; 4.17</p></td>
      <td style="width:72px;height:13px;"><p align="center">.674</p></td>
      <td style="width:78px;height:13px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Auriculo vertical index</p></td>
      <td style="width:102px;"><p align="center">69.49 &plusmn; 2.97</p></td>
      <td style="width:96px;"><p align="center">69.25 &plusmn; 2.20</p></td>
      <td style="width:72px;"><p align="center">.403</p></td>
      <td style="width:78px;"><p align="center">.095</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:102px;"><p align="center">98.91 &plusmn; 3.88</p></td>
      <td style="width:96px;"><p align="center">98.73 &plusmn; 3.32</p></td>
      <td style="width:72px;"><p align="center">.669</p></td>
      <td style="width:78px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Frontal index</p></td>
      <td style="width:102px;"><p align="center">72.82 &plusmn; 3.74</p></td>
      <td style="width:96px;"><p align="center">72.85 &plusmn; 3.62</p></td>
      <td style="width:72px;"><p align="center">.944</p></td>
      <td style="width:78px;"><p align="center">.00</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:102px;"><p align="center">84.89 &plusmn; 5.78</p></td>
      <td style="width:96px;"><p align="center">83.12 &plusmn; 6.28</p></td>
      <td style="width:72px;"><p align="center">.015*</p></td>
      <td style="width:78px;"><p align="center">.294</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Upper Facial index</p></td>
      <td style="width:102px;"><p align="center">52.55 &plusmn; 2.93</p></td>
      <td style="width:96px;"><p align="center">52.79 &plusmn; 3.55</p></td>
      <td style="width:72px;"><p align="center">.546</p></td>
      <td style="width:78px;"><p align="center">.074</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Nasal index</p></td>
      <td style="width:102px;"><p align="center">51.10 &plusmn; 4.87</p></td>
      <td style="width:96px;"><p align="center">50.85 &plusmn; 4.91</p></td>
      <td style="width:72px;"><p align="center">.665</p></td>
      <td style="width:78px;"><p align="center">.051</p></td>
    </tr>
    <tr>
      <td style="width:207px;height:15px;"><p>Orbital index</p></td>
      <td style="width:102px;height:15px;"><p align="center">83.52 &plusmn; 5.21</p></td>
      <td style="width:96px;height:15px;"><p align="center">85.41 &plusmn; 5.67</p></td>
      <td style="width:72px;height:15px;"><p align="center">.004***</p></td>
      <td style="width:78px;height:15px;"><p align="center">.347</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Gnathic index</p></td>
      <td style="width:102px;"><p align="center">94.58 &plusmn; 4.01</p></td>
      <td style="width:96px;"><p align="center">93.67 &plusmn; 3.89</p></td>
      <td style="width:72px;"><p align="center">.049*</p></td>
      <td style="width:78px;"><p align="center">.232</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Jugo frontal</p></td>
      <td style="width:102px;"><p align="center">73.45 &plusmn; 3.36</p></td>
      <td style="width:96px;"><p align="center">73.99 &plusmn; 3.35</p></td>
      <td style="width:72px;"><p align="center">0.1648</p></td>
      <td style="width:78px;"><p align="center">.165</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Nasal breadth-facial ht index</p></td>
      <td style="width:102px;"><p align="center">38.17 &plusmn; 3.90</p></td>
      <td style="width:96px;"><p align="center">37.89 &plusmn; 3.99</p></td>
      <td style="width:72px;"><p align="center">.559</p></td>
      <td style="width:78px;"><p align="center">.069</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Interorbital breadth-facial ht index</p></td>
      <td style="width:102px;"><p align="center">30.95 &plusmn; 3.29</p></td>
      <td style="width:96px;"><p align="center">30.27 &plusmn; 3.60</p></td>
      <td style="width:72px;"><p align="center">.094</p></td>
      <td style="width:78px;"><p align="center">.201</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Transverse frontal index</p></td>
      <td style="width:102px;"><p align="center">81.60 &plusmn; 3.49</p></td>
      <td style="width:96px;"><p align="center">82.81 &plusmn; 2.97</p></td>
      <td style="width:72px;"><p align="center">0.000***</p></td>
      <td style="width:78px;"><p align="center">.372</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Transverse craniofacial index</p></td>
      <td style="width:102px;"><p align="center">99.24 &plusmn; 4.94</p></td>
      <td style="width:96px;"><p align="center">98.52 &plusmn; 4.29</p></td>
      <td style="width:72px;"><p align="center">0.1783</p></td>
      <td style="width:78px;"><p align="center">.156</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:102px;"><p align="center">52.74 &plusmn; 2.52</p></td>
      <td style="width:96px;"><p align="center">52.14 &plusmn; 2.82</p></td>
      <td style="width:72px;"><p align="center">0.0614</p></td>
      <td style="width:78px;"><p align="center">.226</p></td>
    </tr>
    <tr>
      <td style="width:207px;"><p>Vertical craniofacial index</p></td>
      <td style="width:102px;"><p align="center">49.81 &plusmn; 3.28</p></td>
      <td style="width:96px;"><p align="center">49.76 &plusmn; 3.53</p></td>
      <td style="width:72px;"><p align="center">0.9012</p></td>
      <td style="width:78px;"><p align="center">.015</p></td>
    </tr>
  </tbody>
</table>


<p><strong>Table 7. </strong>Significance of difference between craniometric indices of contemporary and subrecent female sample with effect size.</p>

<table align="center" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td rowspan="2" style="width:255px;height:20px;"><p align="center"><strong>Index</strong></p></td>
      <td colspan="2" style="width:228px;height:20px;"><p align="center"><strong>Females (Mean &plusmn; SD)</strong></p></td>
      <td rowspan="2" style="width:69px;height:20px;"><p align="center"><strong>Sig</strong></p></td>
      <td style="width:81px;height:20px;"><p align="center"><strong>Effect Size</strong></p></td>
    </tr>
    <tr>
      <td style="width:99px;"><p align="center"><strong>Contemporary</strong></p></td>
      <td style="width:129px;"><p align="center"><strong>Subrecent</strong></p></td>
      <td style="width:81px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Cranial index</p></td>
      <td style="width:99px;"><p align="center">74.26 &plusmn; 3.90</p></td>
      <td style="width:129px;"><p align="center">71.89 &plusmn; 3.17</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.664</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Vertical index</p></td>
      <td style="width:99px;"><p align="center">75.67 &plusmn; 2.70</p></td>
      <td style="width:129px;"><p align="center">73.46 &plusmn; 2.58</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.829</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse vertical Index</p></td>
      <td style="width:99px;"><p align="center">102.06 &plusmn; 4.3</p></td>
      <td style="width:129px;"><p align="center">102.31 &plusmn; 4.33</p></td>
      <td style="width:69px;"><p align="center">.735</p></td>
      <td style="width:81px;"><p align="center">.058</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Auriculo vertical index</p></td>
      <td style="width:99px;"><p align="center">71.17 &plusmn; 2.64</p></td>
      <td style="width:129px;"><p align="center">69.39 &plusmn; 2.40</p></td>
      <td style="width:69px;"><p align="center">.000***</p></td>
      <td style="width:81px;"><p align="center">.702</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Auriculo Transverse Index</p></td>
      <td style="width:99px;"><p align="center">95.98 &plusmn; 3.77</p></td>
      <td style="width:129px;"><p align="center">96.62 &plusmn; 3.30</p></td>
      <td style="width:69px;"><p align="center">.277</p></td>
      <td style="width:81px;"><p align="center">.181</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Frontal index</p></td>
      <td style="width:99px;"><p align="center">71.41 &plusmn; 2.99</p></td>
      <td style="width:129px;"><p align="center">71.5 &plusmn; 3.57</p></td>
      <td style="width:69px;"><p align="center">.881</p></td>
      <td style="width:81px;"><p align="center">.025</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Foraman &ndash;Magnum Index</p></td>
      <td style="width:99px;"><p align="center">84.24 &plusmn; 6.53</p></td>
      <td style="width:129px;"><p align="center">82.64 &plusmn; 5.10</p></td>
      <td style="width:69px;"><p align="center">.097</p></td>
      <td style="width:81px;"><p align="center">.271</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Upper Facial index</p></td>
      <td style="width:99px;"><p align="center">52.51 &plusmn; 2.90</p></td>
      <td style="width:129px;"><p align="center">52.66 &plusmn; 3.24</p></td>
      <td style="width:69px;"><p align="center">.781</p></td>
      <td style="width:81px;"><p align="center">.049</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Nasal index</p></td>
      <td style="width:99px;"><p align="center">52.06 &plusmn; 4.48</p></td>
      <td style="width:129px;"><p align="center">50.56 &plusmn; 4.84</p></td>
      <td style="width:69px;"><p align="center">.066</p></td>
      <td style="width:81px;"><p align="center">.322</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Orbital index</p></td>
      <td style="width:99px;"><p align="center">86.09 &plusmn; 5.45</p></td>
      <td style="width:129px;"><p align="center">86.05 &plusmn; 5.17</p></td>
      <td style="width:69px;"><p align="center">.966</p></td>
      <td style="width:81px;"><p align="center">.007</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Gnathic index</p></td>
      <td style="width:99px;"><p align="center">95.50 &plusmn; 4.09</p></td>
      <td style="width:129px;"><p align="center">94.35 &plusmn; 4.25</p></td>
      <td style="width:69px;"><p align="center">.112</p></td>
      <td style="width:81px;"><p align="center">.276</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Jugo frontal</p></td>
      <td style="width:99px;"><p align="center">75.74 &plusmn; 2.20</p></td>
      <td style="width:129px;"><p align="center">76.06 &plusmn; 2.73</p></td>
      <td style="width:69px;"><p align="center">0.488</p></td>
      <td style="width:81px;"><p align="center">.119</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Nasal breadth-facial height index</p></td>
      <td style="width:99px;"><p align="center">38.34 &plusmn; 2.85</p></td>
      <td style="width:129px;"><p align="center">37.94 &plusmn; 3.91</p></td>
      <td style="width:69px;"><p align="center">.515</p></td>
      <td style="width:81px;"><p align="center">.117</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Interorbital breadth-facial height index</p></td>
      <td style="width:99px;"><p align="center">31.09 &plusmn; 3.15</p></td>
      <td style="width:129px;"><p align="center">30.16 &plusmn; 3.78</p></td>
      <td style="width:69px;"><p align="center">.129</p></td>
      <td style="width:81px;"><p align="center">.267</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse frontal index</p></td>
      <td style="width:99px;"><p align="center">83.23 &plusmn; 2.29</p></td>
      <td style="width:129px;"><p align="center">82.76 &plusmn; 2.79</p></td>
      <td style="width:69px;"><p align="center">0.304</p></td>
      <td style="width:81px;"><p align="center">.183</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Transverse craniofacial index</p></td>
      <td style="width:99px;"><p align="center">94.35 &plusmn; 3.39</p></td>
      <td style="width:129px;"><p align="center">94.04 &plusmn; 4.36</p></td>
      <td style="width:69px;"><p align="center">0.687</p></td>
      <td style="width:81px;"><p align="center">.070</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>Longitudinal craniofacial index</p></td>
      <td style="width:99px;"><p align="center">53.96 &plusmn; 2.39</p></td>
      <td style="width:129px;"><p align="center">52.15 &plusmn; 2.71</p></td>
      <td style="width:69px;"><p align="center">0.000***</p></td>
      <td style="width:81px;"><p align="center">.709</p></td>
    </tr>
    <tr>
      <td style="width:255px;"><p>&nbsp;Vertical craniofacial index</p></td>
      <td style="width:99px;"><p align="center">48.56 &plusmn; 3.22</p></td>
      <td style="width:129px;"><p align="center">48.40 &plusmn; 3.16</p></td>
      <td style="width:69px;"><p align="center">0.769</p></td>
      <td style="width:81px;"><p align="center">.049</p></td>
    </tr>
  </tbody>
</table>






  
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="inner-right">
      <div class="row">
        <div class="small-12 columns">
          <h4 class="left mr20 mt5">Articles</h4>
          <a href="pdf/FSC-2-115.pdf" target="_blank" class="left mr5"><img src="img/icon-pdf.png" alt=""></a><a href="#" class="left mr5"><img src="img/icon-xml.png" alt=""></a></div>
      </div>
      <hr/>
           <h4><a href="Forensic-Science-and-Criminology-FSC.php">Journal Home</a></h4>

      <hr/>
      <div id="sticky-anchor"></div>
      <div id="sticky">
        <h2>Jump to</h2>
        <hr/>
        <div class="row collapse">
          <div class="large-6 columns">
            <div class="right-set">
              <ul class="main-bar-list">
                <li><a href="#Article" class="active">Article</a></li>
                <li><a href="#Article_Info" >Article Info</a></li>
                <li><a href="#Author_Info" >Author Info</a></li>
                <li><a href="#Figures_Data" >Figures & Data</a></li>
              </ul>
            </div>
          </div>
          <div class="large-6 columns">
            <ul class="right-bar-list">
        		<li><a href="#jumpmenu1">Abstract</a></li>
            <li><a href="#jumpmenu2">Key words</a></li>
            <li><a href="#jumpmenu3">Introduction</a></li>
            <li><a href="#jumpmenu4">Material and method</a></li>
            <li><a href="#jumpmenu5">Results</a></li>
            <li><a href="#jumpmenu6">Discussion</a></li>
            <li><a href="#jumpmenu7">Conclusion</a></li>
            <li><a href="#jumpmenu8">References</a></li>
            
            





            
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns"><p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>

	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script><script>
      $(document).foundation();
    </script>
<!--
<script>
	$('a[href="#"]').click(function(event){

    event.preventDefault();

});
	</script> -->
</body>
</html>
