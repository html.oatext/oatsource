<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="OA Text is an independent open-access scientific publisher showcases innovative research and ideas aimed at improving health by linking research and practice to the benefit of society." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT oatext, oat, open access text" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<title>Combined immunoglobulin G kappa nephropathy: monoclonal immunoglobulin deposition disease and proximal tubulopathy: monoclonal gammopathy of renal significance or smoldering multiple myeloma? Case report and review of literature</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
<div class="small-12 columns">
<hr class="mt0 green-hr-line"/>
</div></div>
<!--banner -->
  <div class="row">
    <div class="medium-12 text-center columns">
<div class="inner-banner">
      <h1>Take a look at the Recent articles</h1>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="row">
    <div class="large-8 columns">
    

    
    
      <div class="inner-left">
        <h2 class="w600">Combined immunoglobulin G kappa nephropathy: monoclonal immunoglobulin deposition disease and proximal tubulopathy: monoclonal gammopathy of renal significance or smoldering multiple myeloma? Case report and review of literature</h2>
        
        
<div data-dropdown="a1" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Elena V Zakharova</p>
</div>
<div id="a1" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Head of Nephrology Department, Botkin Memorial Hospital, Russian Federation</p>
<p><i class="fa  fa-envelope-o"></i> E-mail : <a href="mailto:bhuvaneswari.bibleraaj@uhsm.nhs.uk">bhuvaneswari.bibleraaj@uhsm.nhs.uk</a></p>
</div>
<div data-dropdown="a2" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Ekaterina S Stolyarevich</p>
</div>
<div id="a2" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Head of Nephropathology Department, City Nephrology Centre, Russian Federation</p>
</div>
<div data-dropdown="a3" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Olga A Vorobyeva</p>
</div>
<div id="a3" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Renal Pathologist, National Centre of Clinical Morphology, Russian Federation</p>
</div>
<div data-dropdown="a4" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Eugeny A Nikitin</p>
</div>
<div id="a4" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Head of Hematology Department, City Hematology Centre, Russian Federation</p>
</div>






 
      
      <p class="mt10">DOI: 10.15761/ICST.1000225</p>
      <div class="mt25"></div>
      <div class="custom-horizontal">
        <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs">
          <dd class="active"><a  name="AnchorName"  href="#Article"   class="anchor" >Article</a></dd>
          <dd><a  name="AnchorName"  href="#Article_Info"   class="anchor">Article Info</a></dd>
          <dd><a   name="AnchorName" href="#Author_Info"    class="anchor">Author Info</a></dd>
          <dd><a   name="AnchorName" href="#Figures_Data"   class="anchor">Figures & Data</a></dd>
        </dl>
        <div class="tabs-content">
          <div id="Article" class="content active">
          

<div class="text-justify">

<h2 id="jumpmenu1">Abstract</h2>
                  <p>Multiple myeloma (MM) is consistently preceded by precursor states of monoclonal gammopathy of undetermined significance (MGUS) and smoldering multiple myeloma (SMM). These represent a continuum of progression of the tumor burden from the absence of symptoms or signs of end-organ damage towards full-blown symptomatic disease. MGUS, by definition presenting by monoclonal gammopathy without end organ damage, in fact might be associated with the numerous end organ lesions, first of all pathologic renal conditions. The term monoclonal gammopathy of renal significance (MGRS) was proposed by International Kidney and Monoclonal Gammopathy Research Group in order to discriminate the pathologic nature of these diseases from the truly benign MGUS. Spectrum of MGRS, caused by the deposition of monoclonal immunoglobulin&rsquo;s or fragments thereof as organized and non-organized deposits, includes more commonly AL amyloidosis, monoclonal immunoglobulin deposition disease (MIDD) and light-chain proximal tubulopathy (LCPT). Same variants are seen in patients with MM and SMM. We present a rare case of combined immunoglobulin G kappa nephropathy: MIDD and proximal tubulopathy in a patient, manifested with acute kidney injury and diagnosed with SMM after 6 years of scrutinous repeated evaluation.</p>

<h2 id="jumpmenu2">key words</h2>
                  <p>acute kidney injury, paraproteinemic nephropathy, kidney biopsy, monoclonal light and heavy chains</p>

<h2 id="jumpmenu3">Background</h2>
                  <p>Multiple myeloma (MM) is a plasma-cell dyscrasia presenting with generalized neoplastic changes in bones, accompanied by impaired haematopoiesis and susceptibility to infections. The diagnosis is based on histologic, serologic, and radiographic features: bone marrow clonal plasma cells; monoclonal protein in the serum or urine; and end-organ damage, evidenced by renal impairment, hypercalcemia, anemia, or lytic bone lesions. Monoclonal gammopathy of undermined significance (MGUS) is a pre-malignant disorder, characterized by presence of monoclonal gammopathy without end organ damage. MGUS tend to progress over time to MM, lymphoproliferative disorders and AL amyloidosis with the rate about 1% per year. Asymptomatic or smoldering multiple myeloma (SMM) is a heterogeneous clinical entity where a subset of patients has an indolent course that mimics MGUS, whereas others have a more aggressive course that has been described as &ldquo;early myeloma&rdquo;. MM is consistently preceded by precursor states of MGUS and SMM, these represent a continuum of progression of the tumor burden from the absence of symptoms or signs of end-organ damage towards full-blown symptomatic disease [1-8].</p>

                  <p>Renal damage in MM is mainly caused by the deposition of monoclonal immunoglobulin&rsquo;s (Ig) or fragments thereof as organized (casts, crystals, fibrils, microtubules) and non-organized deposits, involving all compartments of the renal parenchyma: glomeruli, tubules, interstitial space and vessels. Organized deposits induce cast-nephropathy, light-chain proximal tubulopathy (LCPT), AL amyloidosis, glomerulonephritis with organized microtubular monoclonal deposits (GOMMID), and cryoglobulinemic glomerulonephritis; while non-organized deposits lead to the monoclonal immunoglobulin deposition disease (MIDD) or proliferative glomerulonephritis with monoclonal deposits of IgG/IgA (PGNMID). It appears that amino acid sequence of the monoclonal light chains (LC) and other monoclonal proteins, defining inherent biochemical properties, is the primary determinant of the specific pattern of renal parenchymal deposition and clinical disease. Differential diagnostics demands pathology evaluation of kidney tissue, as the above mentioned lesions cannot be differentiated solely on the base of clinical presentation, which vary from acute kidney injury (AKI) and chronic kidney disease (CKD) to nephrotic syndrome (NS) and renal tubular disturbances [9-22].</p>

                  <p>MGUS, by definition presenting by monoclonal gammopathy without end organ damage, in fact might be associated with numerous end organ lesions, first of all pathologic renal conditions. To describe this paradox terms like &ldquo;MIDD with MGUS&rdquo; or &ldquo;Glomerulonephritis with MGUS&rdquo; have been used in the literature. It was shown that such conditions are associated with a high morbidity and mortality [23-33], and the term &ldquo;monoclonal gammopathy of renal significance&rdquo; (MGRS) was proposed by International Kidney and Monoclonal Gammopathy Research Group in order to discriminate the pathologic nature of these diseases from the truly benign MGUS [34].</p>

                  <p>Spectrum of MGRS includes more commonly AL amyloidosis, MIDD and LCPT, and rarely - PGNMID, GOMMID, cryoglobulinemic glomerulonephritis and AH amyloidosis. MGRS conditions are diagnosed by demonstration of monoclonal deposits in the kidney, therefore kidney tissue immunofluorescence/immunohistochemistry study and serum and urine monoclonal protein studies should be performed to match the monoclonal protein in circulation with the monoclonal deposits in the kidney. As MGRS is associated with high morbidity due to the severity of renal and sometimes systemic lesions induced by the monoclonal proteins, early recognition is crucial, as suppression of paraprotein secretion by chemotherapy often improves outcomes [35-39].&nbsp;</p>

                  <p>Here we present a case, manifested with AKI and diagnosed with SMM after 6 years of scrutinous repeated evaluation (summary of work-up and treatment is shown in the table 1).</p>

<h2 id="jumpmenu4">Case presentation</h2>

                  <p>Caucasian lady, 1945 year of birth. Primary admission to Nephrology, Botkin Memorial Hospital in May 2011.</p>

            <h4>Main complains</h4>

                  <p>General weakness.</p>

            <h4>Previous medical history</h4>

                  <p>Breast cancer with mastectomy, hysterectomy, ovariectomy and chemotherapy in 1999, oncologist follow-up for 10 years with no signs or symptoms of cancer recurrence and normal routine labs; mild arterial hypertension.</p>

            <h4>History of present illness</h4>

                  <p>December 2010 she suddenly developed &ldquo;dark&rdquo; urine, within 3 days followed by anuria and was admitted to local hospital with BP 200/100 mm Hg and serum creatinine 800&micro;mol/L. She was diagnosed with AKI and started on hemodialysis (HD), after 4 HD sessions her urine output restored, creatinine decreased to 200&micro;mol/L and she was discharged.</p>

                  <p>Few days later she developed high grade fever with the second episode of anuria, and was admitted to local nephrology unit with creatinine 550&micro;mol/L, Hb 10.0g/dL, proteinuria 5g/24 hours, microhematuria 10-20 RBC/hpf, and re-started on HD. Serum protein electrophoresis revealed M-band, serum and urine immunoelectrophoresis with Freelite assay found monoclonal serum IgG &kappa; 8.4g/L and traces of the monoclonal IgG &kappa; and LC &kappa; in the urine.</p>

                  <p>She was suspected with multiple myeloma and referred to hematology. Her serum calcium was normal, skeletal X-ray did not reveal any destructive lesions, chest and abdomen CT was unremarkable. Bone marrow smear showed normal hematopoietic indices, plasma cells 1.5%, lymphocytes 6%.</p>

<h2 id="jumpmenu5">Bone marrow biopsy</h2>

                  <p><em>Light microscopy:</em> normal ratio of all three hematopoietic lineages with single plasma cells and small lymphoid cells. Congo red staining negative. <em><u>Immunohistochemistry</u>:</em> the small proportion of dispersed lymphoid cells are CD79, CD20 and CD3 positive; single plasma cells, located interstitially and surrounding adipose droplets are CD38, CD138 positive. CD 56 positive cells not found. <em><u>Pathologist&rsquo;s conclusion:</u></em> non-specific changes.</p>

                  <p>Before bone marrow biopsy reading became available patient was started on i.v. cyclophosphamide and oral prednisone. Within 3 weeks her urine output restored, creatinine decreased to 224 &micro;mol/L, HD was discontinued. Her serum IgG &kappa; decreased to 5.3 g/L, paraprotein in the urine was not found.</p>

<h2 id="jumpmenu6">Kidney biopsy</h2>

                  <p><em>Only pathologist&rsquo;s conclusion available:</em> Diffuse immune-complex proliferative glomerulonephritis.</p>

                  <p>The diagnosis of MM was ruled out and chemotherapy (total dose of cyclophosphamide reached 4000 mg) was discontinued. She was diagnosed with &ldquo;Chronic glomerulonephritis with MGUS&rdquo; and referred to our clinic.</p>

            <h4>At admission</h4>

                  <p>Conscious, alert, body temperature 36.7&deg;C, RR 16 per minute, pulse regular 80 per minute, BP 160/90 mm Hg. Obese, skin normally coloured, scars after left mastectomy, hysterectomy. Mild pedal oedema. HEENT and neck otherwise normal. Peripheral lymph nodes not felt. Lungs: clear. Heart:&nbsp; regular rhythm, no murmur. Abdomen: soft, non-tender, bowel sounds normal. Liver, spleen and kidneys not felt. Urine normally coloured, urine output 1600 ml/24 hours.</p>

            <h4>Work-up</h4>

                  <p>Proteinuria 0.2-0.03 g/l, Hb 11.4 g/dL, creatinine 143 &micro;mol/L, uric acid 567 &micro;mol/L, total calcium 2.34 mmol/L. IgG &kappa; secretion 7.7 g/L. Bone marrow smear again sowed normal hematopoietic indices, plasma cells 1.5%, lymphocytes 7.5%.</p>

<h2 id="jumpmenu7">Second kidney biopsy</h2>

                  <p><em>Light microscopy:</em> sections of formalin fixed paraffin-embedded tissue stained with H&amp;E, Masson&rsquo;s trichrome and periodic acid-Shiff. 12 glomeruli, 1 totally sclerotic, the rest are enlarged without any other changes. Interstitial fibrosis and tubular atrophy up to 20% of parenchyma. Few tubules contain РAS-positive casts without giant cell reaction; some of the casts are fragmented. Focal intensive monomorphic lymphoid interstitial infiltration. Arteries are not presented, arterioles without any changes. <em><u>Immunofluorescence on unfixed frozen sections:</u></em> negative for IgG, IgA, IgM, C3, Clq, &kappa; and &lambda; LC, and fibrinogen. <em><u>Pathologist&rsquo;s conclusion:</u></em> Chronic interstitial nephritis. <em><u>Re-evaluation of kidney biopsy in hematopathology unit</u>:</em> interstitial nephritis, no data for specific lymphoid infiltration.</p>

                  <p>Patient was diagnosed with &ldquo;Chronic interstitial nephritis with MGUS&rdquo;.</p>

            <h4>Further course</h4>

                  <p>During next year she was treated for arterial hypertension and hyperuricemia in the outpatient setting. In May 2012 she developed anuria; at admission to our clinic serum creatinine was 621&micro;mol/L, Hb 8.6g/dL, proteinuria 0.4g/24 hours, RBC 10-15 hpf. Further increase of IgG &kappa; secretion to 11.0g/L was found (Table 1), repeated skeletal X-ray and chest CT were unremarkable. She was again started on HD, her urine output recovered, creatinine gradually decreased to 161&micro;mol/L.</p>

                  <p><strong>Table 1. </strong>Serum and urine immunoelectrophoresis, serum creatinine, diagnostic procedures and treatment data for 2010-2017</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:76px;"><p><strong>Date</strong></p></td>
      <td style="width:57px;"><p><strong>Serum IgG &kappa; (g/L)</strong></p></td>
      <td style="width:66px;"><p><strong>Serum LC &kappa; (mg/L)</strong></p></td>
      <td style="width:57px;"><p><strong>Urine IgG &kappa;</strong></p></td>
      <td style="width:57px;"><p><strong>Urine LC &kappa;</strong></p></td>
      <td style="width:85px;"><p><strong>Serum creatinine (&micro;mol/L)</strong></p></td>
      <td style="width:66px;"><p><strong>Kidney biopsy</strong></p></td>
      <td style="width:66px;"><p><strong>Bone marrow biopsy</strong></p></td>
      <td style="width:38px;"><p><strong>HD</strong></p></td>
      <td style="width:110px;"><p><strong>Chemotherapy</strong></p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2010</p></td>
      <td style="width:57px;"><p>8.4</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>800-200</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2011</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>550-224</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>04.2011</p></td>
      <td style="width:57px;"><p>5.3</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>143</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2011</p></td>
      <td style="width:57px;"><p>7.7</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>188</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>05.2012</p></td>
      <td style="width:57px;"><p>11.0</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>621-161</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2012</p></td>
      <td style="width:57px;"><p>9.9</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>160</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2013</p></td>
      <td style="width:57px;"><p>12.5</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>159</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>08.2013</p></td>
      <td style="width:57px;"><p>13.3</p></td>
      <td style="width:66px;"><p>181</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>163</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>10.2013</p></td>
      <td style="width:57px;"><p>14.1</p></td>
      <td style="width:66px;"><p>48</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>148</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>08.2014</p></td>
      <td style="width:57px;"><p>12.7</p></td>
      <td style="width:66px;"><p>49</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>151</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2015</p></td>
      <td style="width:57px;"><p>14.2</p></td>
      <td style="width:66px;"><p>96</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>146</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2015</p></td>
      <td style="width:57px;"><p>15.8</p></td>
      <td style="width:66px;"><p>50</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>152</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2015</p></td>
      <td style="width:57px;"><p>14.3</p></td>
      <td style="width:66px;"><p>69</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>149</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2016</p></td>
      <td style="width:57px;"><p>15.6</p></td>
      <td style="width:66px;"><p>225</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>178</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>11.2016</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>163</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2016</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>154</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2017</p></td>
      <td style="width:57px;"><p>15.0</p></td>
      <td style="width:66px;"><p>114</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>147</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
  </tbody>
</table>

<h2 id="jumpmenu8">Third kidney biopsy</h2>

                  <p><em>Light microscopy: </em>sections of formalin fixed paraffin-embedded tissue stained with H&amp;E, Masson&rsquo;s trichrome, periodic acid-Shiff <em>and </em><em>Jones&rsquo; silver</em><em>.</em> <em>Both cortex and medulla are presented, </em><em>25</em> glomeruli, including 4 (16%) completely sclerotic ones. The rest glomeruli are significantly enlarged, with mainly single-contoured walls; with diffuse and segmental mild mesangial widening due to PAS-positive extracellular matrix and mild mesangial hypercellularity; with focal and segmental minimal endocapillary hypercellularity, presented by few mononuclear cells in the capillary lumen. Masson&rsquo;s trichrome stain shows diffuse irregular fuchsinophilic staining of mesangial matrix, Jones&rsquo; stain reveals focal and segmental double-contoured glomerular basement membrane (GBM). 2 glomeruli (8%) have nonspecific segmental sclerosis. There is no sign of crescentic lesion. (Figures 1-4). Tubular epithelial cytoplasm diffusely loaded with PAS-negative, Jones-negative (non-argyrophylic) protein droplets. Diffuse and focal acute tubular epithelial injury presented by loss of brush border and flattening of cell lining. Tubular lumens contain single small casts without specific tinctorial properties. Tubular basement membranes (TBM) of preserved non-atrophic tubules strongly PAS-positive, emphasized or focally moderately thickened, without multilayering or wrinkling. Masson&rsquo;s trichrome stain shows multifocal fuchsinophilic segments in thickened tubular basement membranes. (Figures 5-10). Mild focal (20%) tubular atrophy with thickening, wrinkling and multilayering of TBM. Focal mild interstitial fibrosis (20%). Small size artery and arteriole walls severely thickened due to smooth muscle cell hypertrophy. Middle size artery walls are significantly thickened due to intimal fibrosis. <em><u>Immunofluorescence on pronase-digested paraffin-embedded tissue fixed in formalin with FITC-conjugated anti IgA, IgG, IgM, C1q, C3, fibrinogen, &kappa; and &lambda; LC antibodies (using pronase antigen retrieval protocol)</u></em><u>:</u> diffuse linear expression of IgG++/+++ and &kappa;++/+++ along TBMs and GBMs. Fine granular confluent IgG++ mesangial expression. Diffuse expression of IgG++/+++ and &kappa;+++ expression in the tubular epithelial cytoplasmic droplets. (Figures 11-16). Presented small cast sow equal expression of both &kappa;+++ and &lambda;+++. <em><u>Electron microscopy</u></em><u>:</u> Single segments of subendothelial fine granular increase of GBM electron density and segmental subepithelial increase of TBM electron density (Figures 17,18). <em><u>Pathologist&rsquo;s conclusion:</u></em> Combined monoclonal (IgG/&kappa;) paraproteinemic nephropathy &ndash; MIDD with glomerulomegaly and mild mesangial hypercellulatity; and proximal paraproteinemic tubulopathy with acute tubular necrosis. Nonspecific secondary global (16%) and focal segmental (8%) glomerulosclerosis, mild interstitial fibrosis (20%); prominent arteriole-arteriosclerosis.</p>

                  <img src="img/ICST-4-225-g001.gif">
<p><b>Figure 1. </b>Enlarged glomeruli; mild mesangial widening and minimal endocapillary hypercellularity; acute tubular epithelial injury. PAS x 100.</p>
 
 <img src="img/ICST-4-225-g002.gif">
<p><b>Figure 2. </b>Enlarged glomeruli; mild mesangial widening and hypercellularity; segmental double-contoured capillary walls. PAS x 200.</p>

<img src="img/ICST-4-225-g003.gif">
<p><b>Figure 3. </b>Enlarged glomeruli; mild segmental mesangial widening and hypercellularity; glomerular basement membrane single-contoured, regularly argyrophilic. Jones’ silver x 200.</p>

<img src="img/ICST-4-225-g004.gif">
<p><b>Figure 4. </b>Fuchsinophilic staining of mesangial matrix. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g005.gif">
<p><b>Figure 5. </b>Convolute tubules epithelial cytoplasm with mainly preserved brush border, loaded with PAS-negative protein droplets. PAS x 400.</p>

<img src="img/ICST-4-225-g006.gif">
<p><b>Figure 6. </b>Convoluted tubules epithelial cytoplasm with mainly preserved brush border, filled with Jones-negative protein droplets. Jones’ silver x 400.</p>

<img src="img/ICST-4-225-g007.gif">
<p><b>Figure 7. </b>Thickened, irregular fuchsinopilic basement membranes of convoluted and straight tubules. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g008.gif">
<p><b>Figure 8. </b>Thickened fuchsinopilic basement membranes of straight tubules. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g009.gif">
<p><b>Figure 9. </b>Thickened, irregular fuchsinopilic basement membranes of convoluted and straight tubules. Masson’s trichrome х 600.</p>

<img src="img/ICST-4-225-g010.gif">
<p><b>Figure 10. </b>Thickened fuchsinopilic basement membranes of straight tubules. Masson’s trichrome х 600.</p>

<img src="img/ICST-4-225-g011.gif">
<p><b>Figure 11. </b>Diffuse linear expression of IgG along tubular and glomerular basement membranes. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 100.</p>

<img src="img/ICST-4-225-g012.gif">
<p><b>Figure 12. </b>Diffuse linear expression of light chain κ along tubular and glomerular basement membranes. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 100.</p>

<img src="img/ICST-4-225-g013.gif">
<p><b>Figure 13. </b>IgG expression in the glomerulus: linear along glomerular basement membrane and irregular granular-confluent in mesangium. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g014.gif">
<p><b>Figure 14. </b>Diffuse IgG expression in the protein droplets in the tubular epithelial cytoplasm. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g015.gif">
<p><b>Figure 15. </b>Diffuse light chain κ expression in the protein droplets in the tubular epithelial cytoplasm. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g016.gif">
<p><b>Figure 16. </b>Negative light chain λ in any compartment of kidney tissue. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g017.gif">
<p><b>Figure 17. </b>Segmental subendothelial fine granular increase of electron density of glomerular basement membrane. Electron microscopy x 12000</p>

<img src="img/ICST-4-225-g018.gif">
<p><b>Figure 18. </b>Segmental subepithelial increase of electron density of tubular basement membrane. Electron microscopy x 12000</p>

<h2 id="jumpmenu9">Second bone marrow biopsy</h2>

                  <p><em>Light microscopy:</em> bone trabecules with focal resorption.&nbsp; Stromal repletion with focal hemorrhages. Bone marrow cavities are wide, bone marrow moderately cellular (with respect to normal for patient&rsquo;s age). Megakaryocyte lineage is sufficient, megakaryocytes of relatively small size with hypolobular nuclei. Erythroid lineage is sufficient, erythrokariocytes of normoblastic type. Granulocytic lineage is sufficient, granulocytes of different degree of maturation, predominantly mature. Discrete small lymphoid cells are located interstitially; mature plasma cells are located perivascular. <em><u>Pathologist&rsquo;s conclusion:</u> </em>non-specific changes.</p>

                  <p>Patient was diagnosed with MGRS (MIDD IgG &kappa; and proximal tubulopathy IgG &kappa;), and July 2012 re-started on chemotherapy with i.v. cyclophosphamide plus dexamethasone - 5 day courses every 3 months (total dose of cyclophosphamide 8000 mg), under regular control of monoclonal secretion. 4-th course of chemotherapy was completed August 2013; serum creatinine was 148&micro;mol/L, proteinuria 0.2g/24 hours. IgG &kappa; secretion remained 14.1g/L; LC &kappa; secretion 181mg/L, which appeared before the last chemotherapy course, decreased 48mg/L. Moderate decrease of polyclonal IgG was found.</p>

<h2 id="jumpmenu10">Third bone marrow biopsy</h2>

                  <p><em>Pathologist&rsquo;s conclusion: </em>No data in favor of lymphoproliferative disease.</p>

                  <p>She was doing well and followed-up in the outpatient setting for next 3 years. September 2016 her IgG &kappa; and LC &kappa; serum levels increased to 15.6g/L and 225mg/L respectively, and LC &kappa; appeared in the urine. She was re-admitted to our clinic with Hb 12.3g/dL, creatinine 178&micro;mol/L, uric acid 484&micro;mol/L, total protein 8.6g/dL, total serum calcium 2.31mmol/L, proteinuria 0.2g/24 hours and urine RBC 8-10 hpf. &nbsp;&nbsp;</p>

<h2 id="jumpmenu11">Forth bone marrow biopsy</h2>

                  <p><em>Light microscopy:</em> Bone trabecules with focal resorption.&nbsp; Bone marrow cavities are wide, bone marrow is moderately cellular (with respect to normal for patient&rsquo;s age). Granulocytic lineage is moderate, represented by cells of different maturation degree, predominantly mature. Erythroid lineage is moderate, represented by clusters of normoblastic erythrokariocytes, focally rejuvenated. Sufficient number of megakaryocytes is located discretely, represented by of relatively small size cells with hypersegmented normochromic nuclei. Discrete small lymphoid cells and mature plasma cells are located interstitially. <em><u>Immunohistochemistry</u></em><u> <em>with anti CD3, CD19, CD20, CD56, CD138, IgA, IgG, light chains &kappa;, light chains &lambda; and CyclinD antibodies.</em></u> Increased number of mature plasma cells (CD138+), located discreetly and in clusters perivascular, intra- and para-trabecular, and expressing CD56 (membrane reaction) and IgG (cytoplasmic reaction). They are predominantly &kappa;-positive with only single of them &lambda;-positive. Small B-cells are CD19 and CD20 positive, some plasma cells, located discreetly intra-trabecular are CD19 positive. Small B-cells (CD20+) and small T-cells (CD3+) are dispersed interstitially. Histiocytes are CyclinD positive. <em><u>Pathologist&rsquo;s conclusion:</u></em> bone marrow pathology demonstrates moderate plasma cell infiltration with aberrant plasma cell immunophenotype IgG &kappa;+, which can be characterized as minimal features of multiple myeloma.</p>

            <h4>Final diagnosis</h4>

                  <p>Smoldering multiple myeloma with combined paraproteinemic kidney damage: MIDD IgG &kappa; and proximal tubulopathy IgG &kappa;, CKD stage 3b.</p>

            <h4>Current treatment and follow-up</h4>

                  <p>Patient was referred to the hematology unit and November 2016 was started on bortezomib-cyclophosphamide-dexamethasone. At the latest follow-up visit January 25 2017, after 3 courses of chemotherapy she is doing well, her LC &kappa; secretion decreased to 114mg/L, and serum creatinine - to 147&micro;mol/L. Next course is scheduled on February 2017.</p>

<h2 id="jumpmenu12">Discussion</h2>

                  <p>First clinical manifestation in our patient was anuric AKI of unexplained origin, partially resolved after few HD sessions. During the second episode of anuria, which developed shortly and demanded HD again, M-band was found by serum protein electrophoresis. Serum and urine immunoelectrophoresis revealed moderate monoclonal Ig &kappa; secretion, and IgG &kappa; and LC &kappa; urinary excretion, therefore MM was suspected. Re-introduction of HD, followed by cyclophosphamide and prednisone treatment, lead to the restoration of kidney function, decrease of paraproteinemia and vanishing of paraproteinuria. However, her anemia was mild, serum calcium was normal with no evidence of lytic bone lesions, and bone marrow smear, as well as bone marrow biopsy did not confirm plasma cell or lymphocyte infiltration, and kidney biopsy did not show paraproteinemic kidney damage.</p>

                  <p>At that point MM was ruled out, and the diagnosis of &ldquo;Chronic glomerulonephritis with MGUS&rdquo; appeared on the basis of kidney biopsy conclusion. Of notice, 1-st biopsy was read by external pathologist and hardly can be interpreted. As this diagnosis was not compatible with two episodes of AKI, and monoclonal serum IgG &kappa; increased again after chemotherapy discontinuation, we performed second kidney biopsy in search of LCPT, cast-nephropathy or specific lymphoid infiltration, missed by the first biopsy. Light microscopy showed interstitial nephritis with few РAS-positive casts without giant cell reaction, which could not prove cast-nephropathy, and just one area of intensive monomorphic lymphoid interstitial infiltration, which was not considered as specific. Immunofluorescence on unfixed frozen sections was totally negative; therefore, LCPT was also not confirmed. Hence, we did not find any glomerular lesions but very mild global glomerulosclerosis and moderate glomerulomegaly, and failed to prove any paraproteinemic kidney damage or lymphoid infiltration, we came to the formal diagnosis of &ldquo;Interstitial nephritis with MGUS&rdquo; and kept &ldquo;watch and wait&rdquo; strategy.</p>

                  <p>New episode of AKI happened after a year of follow-up, and further increase of monoclonal IgG &kappa; secretion, forced us to perform the third kidney biopsy. Light microscopy showed glomerulomegaly, mild mesangial hypercellulatity and acute tubular necrosis. However, this time we used immunofluorescence on pronase-digested paraffin-embedded tissue, fixed in formalin [40], and found diffuse linear IgG++/+++ and &kappa;++/+++ expression along TBM and GBM, fine granular confluent IgG++ mesangial expression, and diffuse IgG+++ and &kappa;+++ expression in tubular epithelial cytoplasmic droplets. These findings confirmed paraproteinemic origin of renal damage and matched the monoclonal protein in circulation with the monoclonal deposits in the kidney. That led us to the diagnosis of combined paraproteinemic IgG &kappa; nephropathy &ndash; MIDD and proximal paraproteinemic tubulopathy with acute tubular necrosis, the latter explained AKI episodes. The evidence for MM was still pending - repeated skeletal X-ray and bone marrow biopsy failed to confirm lytic bone changes and clonal plasma cells, therefore we arrived to the diagnosis of MGRS, the concept of which was just introduced [34].</p>

                  <p>Second block of chemotherapy resulted in the stabilization of kidney function &ndash; no new episodes of AKI occurred during next 3 years. However, IgG &kappa; secretion was gradually increasing, and LC &kappa; secretion appeared, but the third bone marrow biopsy again failed to give evidence for MM. Only substantial increase of LC &kappa; secretion 3 years after the second chemotherapy guided us to perform forth bone marrow biopsy and order immunohistochemistry, which demonstrated moderate plasma cell infiltration with aberrant plasma cell immunophenotype IgG &kappa;+ and allowed to end up with the diagnosis of SMM with combined paraproteinemic kidney damage. That was the sixth diagnosis during 6 years of follow-up and treatment (Table 2).</p>

                  <p><strong>Table 2. </strong>List of diagnoses 2010-2016</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:76px;"><p><strong>Date </strong></p></td>
      <td style="width:539px;"><p><strong>Diagnosis</strong></p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2010</p></td>
      <td style="width:539px;"><p>Acute kidney injury of unknown origin</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2011</p></td>
      <td style="width:539px;"><p>Multiple myeloma</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>04.2011</p></td>
      <td style="width:539px;"><p>Glomerulonephritis with MGUS</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2011</p></td>
      <td style="width:539px;"><p>Interstitial nephritis with MGUS</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>05.2012</p></td>
      <td style="width:539px;"><p>MGRS: monoclonal immunoglobulin deposition disease IgG &kappa; and proximal tubulopathy IgG &kappa;</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2016</p></td>
      <td style="width:539px;"><p>Smoldering multiple myeloma with combined paraproteinemic kidney damage: monoclonal immunoglobulin deposition disease IgG &kappa; and proximal tubulopathy IgG &kappa;</p></td>
    </tr>
  </tbody>
</table>

                  <p>Analyzing the clinical course and pathology data we presume that if immunofluorescence on pronase-digested paraffin-embedded tissue had been available for the second kidney biopsy processing, and immunohistochemistry with the whole panel of antibodies had been done for the second or third bone marrow biopsy, the diagnosis of SMM with paraproteinemic kidney damage might have been already proven in 2011.</p>

                  <p>The coexistence of two and more types of paraproteinemic kidney damage, like amyloidosis and MIDD, cast-nephropathy and MIDD, cast-nephropathy, amyloidosis and MIDD, is described in the literature [41-43]; however, we did not find cases of combination of MIDD and LCPT.</p>

                  <p>On the other hand, paraproteinemic proximal tubular injury, including crystal-storing hystiocytosis and LCPT with or without Fanconi syndrome, and causative for acute tubular necrosis and AKI [13,18,22,37], is almost invariably associated with monoclonal LC &kappa;, and only rarely with Ig heavy chains [44], like in our case. As no crystals were found by electron microscopy, and the patient did not demonstrate clinical features of Fanconi syndrome, we presume that she has non-crystalline form of proximal heavy and light chain tubulopathy without Fanconi syndrome, causing repeated episodes of AKI. Again, we couldn&rsquo;t find such a pattern of injury in the available literature.</p>

                  <p>Of interest, at the time of the third biopsy, which was critical for the diagnosis, our patient had only monoclonal IgG &kappa; secretion. LC &kappa; were not detectable in the serum or urine either, which is compatible with the pathology findings, showing that the paraproteinemic kidney damage was IgG &kappa; driven. We suppose that repeated kidney biopsy, if performed at the time when LC &kappa; secretion appeared, might show also LC &kappa; associated additional damage.</p>

                  <p>And finally, we believe that the chemotherapy with i.v. cyclophosphamide, which our patient received twice, even it was guided by incorrect diagnoses, was beneficial for our patient and delayed the progression both of blood disorder and CKD.</p>

<h2 id="jumpmenu13">Conclusions</h2>

                  <p>Unexplained kidney function disturbances in patients with monoclonal gammopathy demand usage of diagnostic algorithm, which include kidney biopsy with full-panel immunofluorescence, and bone marrow biopsy with full-panel imunohistochemistry. We consider very useful to apply immunofluorescent duplication on pronase-digested paraffin-embedded tissue especially in cases of discrepancy between clinical evidence of monoclonal secretion and confusing pathology findings. Patients with MGRS or SMM, diagnosed on the basis of above-mentioned work-up findings benefit from MM-protocols chemotherapy.</p>

<h2 id="jumpmenu14">Acknowledgements</h2>

                  <p>We have to thank doctors Olga Vinogradova, Vladimir Lunin, Sergey Semochkin, Alla Kovrigina, Salya Maryina and Arthur Cohen for their help in the diagnostics and treatment of our patient.&nbsp;</p>


<h2 id="jumpmenu15">References</h2>
                  <ol>
  <li>International Myeloma Working Group (2003) Criteria for the classification of monoclonal gammopathies, multiple myeloma and related disorders: A report of the International Myeloma Working Group. <em>Br J Haematol</em> 121: 749-757. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/12780789">Crossref</a>]</li>
  <li>Rajkumar SV, Kyle RA (2005) Multiple myeloma: diagnosis and treatment.&nbsp;<em>Mayo Clin Proc&nbsp;</em>80: 1371-1382. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16212152">[Crossref]</a>&nbsp;</li>
  <li>Korbet SM, Schwartz MM (2006) Multiple myeloma.&nbsp;<em>J Am Soc Nephrol&nbsp;</em>17: 2533-2545. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16885408">[Crossref]</a>&nbsp;</li>
  <li>Kyle RA, Therneau TM, Rajkumar SV, Larson DR, Plevak MF, et al. (2006) Prevalence of monoclonal gammopathy of undetermined significance.&nbsp;<em>N Engl J Med&nbsp;</em>354: 1362-1369. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16571879">[Crossref]</a>&nbsp;</li>
  <li>Landgren O, Kyle RA, Pfeiffer RM, Katzmann JA, Caporaso NE, et al (2009) Monoclonal gammopathy of undetermined significance (MGUS) consistently precedes multiple myeloma: a prospective study. <em>Blood</em> 113: 5412-5417. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/19179464">Crossref</a>]</li>
  <li>Kyle RA, Durie BG, Rajkumar SV, Landgren O, Blade J, et al (2010) Monoclonal gammopathy of undetermined significance (MGUS) and smoldering (asymptomatic) multiple myeloma: IMWG consensus perspectives risk factors for progression and guidelines for monitoring and management. <em>Leukemia</em> 24: 1121-1127. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20410922">Crossref</a>]</li>
  <li>Landgren O (2013) Monoclonal gammopathy of undetermined significance and smoldering multiple myeloma: biological insights and early treatment strategies. <em>Hematology Am Soc Hematol Educ Program</em> 2013: 478-487. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/24319222">Crossref</a>]</li>
  <li>Ghobrial IM, Landgren O (2014) How I treat smoldering multiple myeloma.&nbsp;<em>Blood&nbsp;</em>124: 3380-3388. <a href="http://www.ncbi.nlm.nih.gov/pubmed/25298034">[Crossref]</a>&nbsp;</li>
  <li>Haas M, Spargo BH, Wit EJ, Meehan SM (2000) Etiologies and outcome of acute renal insufficiency in older adults: a renal biopsy study of 259 cases.&nbsp;<em>Am J Kidney Dis&nbsp;</em>35: 433-447. <a href="http://www.ncbi.nlm.nih.gov/pubmed/10692269">[Crossref]</a>&nbsp;</li>
  <li>Pozzi C, D&rsquo;Amico M, Fogazzi GB, Curioni S, Ferrario F,&nbsp; et al (2003) Light chain deposition disease with renal involvement: clinical characteristics and prognostic factors. <em>Am J Kidney Dis</em> 42: 1154-1163. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/14655186">Crossref</a>]</li>
  <li>Markowitz GS (2004) Dysproteinemia and the kidney.&nbsp;<em>Adv Anat Pathol&nbsp;</em>11: 49-63. <a href="http://www.ncbi.nlm.nih.gov/pubmed/14676640">[Crossref]</a>&nbsp;</li>
  <li>Herrera GA, Joseph L, Gu X, Hough A, Barlogie B (2004) Renal pathologic spectrum in an autopsy series of patients with plasma cell dyscrasia.&nbsp;<em>Arch Pathol Lab Med&nbsp;</em>128: 875-879. <a href="http://www.ncbi.nlm.nih.gov/pubmed/15270616">[Crossref]</a>&nbsp;</li>
  <li>Herrera GA, Sanders PW (2007) Paraproteinemic renal diseases that involve the tubule-interstitium. In: Herrera GA (ed): The Kidney in Plasma Cell Dyscraisas. Contrib Nephrol. Basel, Karger 153: 105-115.</li>
  <li>Merlini G, Pozzi C (2007) Mechanisms of renal damage in plasma cell dyscrasias: an overview.&nbsp;<em>Contrib Nephrol&nbsp;</em>153: 66-86. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17075224">[Crossref]</a>&nbsp;</li>
  <li>Batuman V (2007) Proximal tubular injury in myeloma.&nbsp;<em>Contrib Nephrol&nbsp;</em>153: 87-104. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17075225">[Crossref]</a>&nbsp;</li>
  <li>Ronco P, Plaisier E, Aucouturier P (2010) Ig-related renal disease in lymphoplasmacytic disorders: an update.&nbsp;<em>Semin Nephrol&nbsp;</em>30: 557-569. <a href="http://www.ncbi.nlm.nih.gov/pubmed/21146121">[Crossref]</a>&nbsp;</li>
  <li>Heher EC1, Goes NB, Spitzer TR, Raje NS, Humphreys BD, et al. (2010) Kidney disease associated with plasma cell dyscrasias.&nbsp;<em>Blood&nbsp;</em>116: 1397-1404. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20462963">[Crossref]</a>&nbsp;</li>
  <li>El Hanel C, Thierry A, Trouillas P, Bridoux F, Carrion C, et al. (2010) Crystal-storing histiocytosis with renal Fanconi syndrome: pathological and molecular characteristics compared with classical myeloma-associated Fanconi syndrome. <em>Nephrol Dial Transplant</em> 25: 2982-2990. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20356978">Crossref</a>]</li>
  <li>Tsakiris DJ, Stel VS, Finne P, Fraser E, Heaf J, et al (2010). Incidence and outcome of patients starting renal replacement therapy for end stage of renal disease due to multiple myeloma or light chain depositon disease: an ERA-EDTA Registry study. <em>Nephrol Dial Transplant</em> 25: 1200-1206. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20037169">Crossref</a>]</li>
  <li>Ronco P (2010) Disease classification: a pitfall of the ERA/EDTA registry?&nbsp;<em>Nephrol Dial Transplant&nbsp;</em>25: 1022-1024. <a href="http://www.ncbi.nlm.nih.gov/pubmed/20139408">[Crossref]</a>&nbsp;</li>
  <li>Davenport A, Merlini G (2012) Myeloma kidney: advances in molecular mechanisms of acute kidney injury open novel therapeutic opportunities. <em>Nephrol Dial Transplant</em> 27: 3713-3187. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/23114897">Crossref</a>]</li>
  <li>Cambier JF, Ronco P (2012) Onco-nephrology: glomerular diseases with cancer.&nbsp;<em>Clin J Am Soc Nephrol&nbsp;</em>7: 1701-1712. <a href="http://www.ncbi.nlm.nih.gov/pubmed/22904123">[Crossref]</a>&nbsp;</li>
  <li>Lin J, Markowitz GS, Valeri AM, Kambham N, Sherman WH, et al. (2001). Renal monoclonal immunoglobulin deposition disease: the disease spectrum. <em>J Am Soc Nephrol</em> 12: 1482-1492. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/11423577">Crossref</a>]</li>
  <li>Paueksakon P, Revelo MP, Horn RG, Shappell S, Fogo AB (2003) Monoclonal gammopathy: significance and possible causality in renal disease.&nbsp;<em>Am J Kidney Dis&nbsp;</em>42: 87-95. <a href="http://www.ncbi.nlm.nih.gov/pubmed/12830460">[Crossref]</a>&nbsp;</li>
  <li>Merlini G, Stone MJ (2006) Dangerous small B-cell clones.&nbsp;<em>Blood&nbsp;</em>108: 2520-2530. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16794250">[Crossref]</a>&nbsp;</li>
  <li>Nasr SH, Markowitz GS, Stokes MB, Seshan SV, Valderrama E, et al. (2004). Proliferative glomerulonephritis with monoclonal IgG deposits; a distinct entity mimicking immune-complex glomerulonephritis. <em>Kidney Int</em> 65: 85-96. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/14675039">Crossref</a>]</li>
  <li>Soares SM, Lager DJ, Leung N, Haugen EN, Fervenza FC (2006). A proliferative glomerulonephritis secondary to a monoclonal IgA. <em>Am J Kidney Dis</em> 47: 342-349. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/16431264">Crossref</a>]</li>
  <li>Nasr SH, Satoskar A, Markowitz GS, Valeri AM, Appel GB, et al. (2009) Proliferative glomerulonephritis with monoclonal IgG deposits.&nbsp;<em>J Am Soc Nephrol&nbsp;</em>20: 2055-2064. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19470674">[Crossref]</a>&nbsp;</li>
  <li>Bridoux F, Hugue V, Coldefy O, Goujon JM, Bauwens M, et al (2002) Fibrillary glomerulonephritis and immunotactoid (microtubular) glomerulopathy are associated with distinct immunologic features. <em>Kidney Int</em> 62: 1764-1775. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/12371978">Crossref</a>]</li>
  <li>Nagao T, Okura T, Miyoshi K, Watanabe S, Manabe S, et al. (2005) Fibrillary glomerulonephritis associated with monoclonal gammopathy of undetermined significance showing lambda-type Bence Jones protein. <em>Clin Exp Nephrol</em> 9: 247-251. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/16189635">Crossref</a>]</li>
  <li>Sethi S, Zand L, Leung N, Smith RJ, Jevremonic D, et al. (2010) Membranoproliferative glomerulonephritis secondary to monoclonal gammopathy. <em>Clin J Am Soc Nephrol</em> 5: 770-782. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20185597">Crossref</a>]</li>
  <li>Gertz MA, Leung N, Lacy MQ, Dispenzieri A, Zeldenrust SR, et al. (2009) Clinical outcome of immunoglobulin light chain amyloidosis affecting the kidney.&nbsp;<em>Nephrol Dial Transplant&nbsp;</em>24: 3132-3137. <a href="http://www.ncbi.nlm.nih.gov/pubmed/19403931">[Crossref]</a>&nbsp;</li>
  <li>Leung N, Buadi F, Song KW, Magil AB, Cornell LD, et al. (2010) A case of bilateral renal arterial thrombosis associated with cryocrystalglobulinaemia. <em>Nephrol Dial Transplant Plus</em> 3: 74-77. [<a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4421549/">Crossref</a>]</li>
  <li>Leung N, Bridoux F, Hutchison CA, Nasr SH, Cockwell P, et al. (2012) Monoclonal gammopathy of renal significance: when MGUS is no longer undetermined or insignificant. <em>Blood</em> 120: 4292-4295. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/23047823">Crossref</a>]</li>
  <li>Fermand JP, Bridoux F, Kyle RA, Kastritis E, Weiss BM, et al. (2013) How I treat monoclonal gammopathy of renal significance (MGRS).&nbsp;<em>Blood&nbsp;</em>122: 3583-3590. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24108460">[Crossref]</a>&nbsp;</li>
  <li>Cabrita A (2014) Monoclonal gammopathy of renal significance: why is it significant. <em>J Blood Disord</em> 1: 2-3.</li>
  <li>Paueksakon P, Fogo AB (2014) More light shed on light chains.&nbsp;<em>Nephrol Dial Transplant&nbsp;</em>29: 1799-1801. <a href="http://www.ncbi.nlm.nih.gov/pubmed/24895438">[Crossref]</a>&nbsp;</li>
  <li>Bridoux F, Leung N, Hutchison CA, Touchard G, Sethi S, et al (2015). International Kidney and Monoclonal Gammopathy Research Group. Diagnosis of monoclonal gammopathy of renal significance. <em>Kidney Int</em> 87: 698-711. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/25607108">Crossref</a>]</li>
  <li>Leung N, Nasr SH (2016) A Patient with Abnormal Kidney Function and a Monoclonal Light Chain in the Urine.&nbsp;<em>Clin J Am Soc Nephrol&nbsp;</em>11: 1073-1082. <a href="http://www.ncbi.nlm.nih.gov/pubmed/26992418">[Crossref]</a>&nbsp;</li>
  <li>Nasr SH, Galgano SJ, Markowitz GS, Stokes MB, D&#39;Agati VD (2006) Immunofluorescence on pronase-digested paraffin sections: a valuable salvage technique for renal biopsies. <em>Kidney Int</em> 70: 2148-2151. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/17063172">Crossref</a>]</li>
  <li>Gokden N, Cetin N, Colakoglu N, Kumar J, Abul-Ezz S, et al. (2007) Morphologic manifestations of combined light-chain deposition disease and light-chain cast nephropathy. <em>Ultrastruct Pathol</em> 31: 141-149. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/17613994">Crossref</a>]</li>
  <li>Qian Q, Leung N, Theis JD, Dogan A, Sethi S (2010) Coexistence of myeloma cast nephropathy, light chain deposition disease, and nonamyloid fibrils in a patient with multiple myeloma. <em>Am J Kidney Dis</em> 56: 971-976. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20870327">Crossref</a>]</li>
  <li>Lorenz EC, Sethi S, Poshusta TL, Ramirez-Alvarado M, Kumar S, et al. (2010) Renal failure due to combined cast nephropathy, amyloidosis and light-chain deposition disease. <em>Nephrol Dial Transplant</em> 25: 1340-1343. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/20061318">Crossref</a>]</li>
  <li>Lebeau A, Zeindl-Eberhart E, Muller EC, Muller-Hocker J, Jundblut PR, et al. (2002) Generalized crystal-storing hystiocytosis associated with monoclonal gammopathy: molecular analysis of a disorder with rapid clinical course and review of literature. <em>Blood</em> 100: 1817-1827. [<a href="https://www.ncbi.nlm.nih.gov/pubmed/12176905">Crossref</a>]</li>
</ol>






</div>

         
          </div>
          <div id="Article_Info" class="content">
            <h4 class="mb10"><span class="">Editorial Information</span></h4>
            <h4 class="mb5"><span class="black-text">Editor-in-Chief</span></h4>
   
   <p>Vicente Notario<br>
   Georgetown University Medical Center</p>


<h4>Article Type</h4>
<p>Case Report</p>

<h4>Publication history</h4>
<p>Received date: January 29, 2017<br>
Accepted date: February 14, 2017<br>
Published date: February 17, 2017</p>

<h4>Copyright</h4>
<p>&copy; 2017 Zakharova EV. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</p>

<h4>Citation</h4>
<p>Zakharova EV, Stolyarevich ES, Vorobyeva OA, Nikitin EA (2017) Combined immunoglobulin G kappa nephropathy: Monoclonal immunoglobulin deposition disease and proximal tubulopathy: monoclonal gammopathy of renal significance or smoldering multiple myeloma? Case report and review of literature. Integr Cancer Sci Therap. 4: DOI: 10.15761/ICST.1000225</p>

          </div>
          <div id="Author_Info" class="content">
            <h4 class="mb10"><span class="">Corresponding author</span></h4>
            
            
            <h4 class="mb10"><span class="black-text">Elena V Zakharova</span></h4>
            <p>Nephrology Department, Head, Botkin Memorial Hospital, 125284, 2-nd Botkinsky proezd, 5, Moscow, Russian Federation, Tel: +7 967 134 6936, Fax: +7 495 945 1756.</p>





          </div>
          <div id="Figures_Data" class="content">
             
             
<img src="img/ICST-4-225-g001.gif">
<p><b>Figure 1. </b>Enlarged glomeruli; mild mesangial widening and minimal endocapillary hypercellularity; acute tubular epithelial injury. PAS x 100.</p>
 
 <img src="img/ICST-4-225-g002.gif">
<p><b>Figure 2. </b>Enlarged glomeruli; mild mesangial widening and hypercellularity; segmental double-contoured capillary walls. PAS x 200.</p>

<img src="img/ICST-4-225-g003.gif">
<p><b>Figure 3. </b>Enlarged glomeruli; mild segmental mesangial widening and hypercellularity; glomerular basement membrane single-contoured, regularly argyrophilic. Jones’ silver x 200.</p>

<img src="img/ICST-4-225-g004.gif">
<p><b>Figure 4. </b>Fuchsinophilic staining of mesangial matrix. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g005.gif">
<p><b>Figure 5. </b>Convolute tubules epithelial cytoplasm with mainly preserved brush border, loaded with PAS-negative protein droplets. PAS x 400.</p>

<img src="img/ICST-4-225-g006.gif">
<p><b>Figure 6. </b>Convoluted tubules epithelial cytoplasm with mainly preserved brush border, filled with Jones-negative protein droplets. Jones’ silver x 400.</p>

<img src="img/ICST-4-225-g007.gif">
<p><b>Figure 7. </b>Thickened, irregular fuchsinopilic basement membranes of convoluted and straight tubules. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g008.gif">
<p><b>Figure 8. </b>Thickened fuchsinopilic basement membranes of straight tubules. Masson’s trichrome х 400.</p>

<img src="img/ICST-4-225-g009.gif">
<p><b>Figure 9. </b>Thickened, irregular fuchsinopilic basement membranes of convoluted and straight tubules. Masson’s trichrome х 600.</p>

<img src="img/ICST-4-225-g010.gif">
<p><b>Figure 10. </b>Thickened fuchsinopilic basement membranes of straight tubules. Masson’s trichrome х 600.</p>

<img src="img/ICST-4-225-g011.gif">
<p><b>Figure 11. </b>Diffuse linear expression of IgG along tubular and glomerular basement membranes. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 100.</p>

<img src="img/ICST-4-225-g012.gif">
<p><b>Figure 12. </b>Diffuse linear expression of light chain κ along tubular and glomerular basement membranes. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 100.</p>

<img src="img/ICST-4-225-g013.gif">
<p><b>Figure 13. </b>IgG expression in the glomerulus: linear along glomerular basement membrane and irregular granular-confluent in mesangium. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g014.gif">
<p><b>Figure 14. </b>Diffuse IgG expression in the protein droplets in the tubular epithelial cytoplasm. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g015.gif">
<p><b>Figure 15. </b>Diffuse light chain κ expression in the protein droplets in the tubular epithelial cytoplasm. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g016.gif">
<p><b>Figure 16. </b>Negative light chain λ in any compartment of kidney tissue. Immunofluorescence on formalin fixed paraffin-embedded sections (using pronase antigen retrieval protocol) х 200.</p>

<img src="img/ICST-4-225-g017.gif">
<p><b>Figure 17. </b>Segmental subendothelial fine granular increase of electron density of glomerular basement membrane. Electron microscopy x 12000</p>

<img src="img/ICST-4-225-g018.gif">
<p><b>Figure 18. </b>Segmental subepithelial increase of electron density of tubular basement membrane. Electron microscopy x 12000</p>


<p><strong>Table 1. </strong>Serum and urine immunoelectrophoresis, serum creatinine, diagnostic procedures and treatment data for 2010-2017</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:76px;"><p><strong>Date</strong></p></td>
      <td style="width:57px;"><p><strong>Serum IgG &kappa; (g/L)</strong></p></td>
      <td style="width:66px;"><p><strong>Serum LC &kappa; (mg/L)</strong></p></td>
      <td style="width:57px;"><p><strong>Urine IgG &kappa;</strong></p></td>
      <td style="width:57px;"><p><strong>Urine LC &kappa;</strong></p></td>
      <td style="width:85px;"><p><strong>Serum creatinine (&micro;mol/L)</strong></p></td>
      <td style="width:66px;"><p><strong>Kidney biopsy</strong></p></td>
      <td style="width:66px;"><p><strong>Bone marrow biopsy</strong></p></td>
      <td style="width:38px;"><p><strong>HD</strong></p></td>
      <td style="width:110px;"><p><strong>Chemotherapy</strong></p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2010</p></td>
      <td style="width:57px;"><p>8.4</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>800-200</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2011</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>550-224</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>04.2011</p></td>
      <td style="width:57px;"><p>5.3</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>143</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2011</p></td>
      <td style="width:57px;"><p>7.7</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>188</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>05.2012</p></td>
      <td style="width:57px;"><p>11.0</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>621-161</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>+</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2012</p></td>
      <td style="width:57px;"><p>9.9</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>160</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2013</p></td>
      <td style="width:57px;"><p>12.5</p></td>
      <td style="width:66px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>159</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>08.2013</p></td>
      <td style="width:57px;"><p>13.3</p></td>
      <td style="width:66px;"><p>181</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>163</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>10.2013</p></td>
      <td style="width:57px;"><p>14.1</p></td>
      <td style="width:66px;"><p>48</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>148</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>08.2014</p></td>
      <td style="width:57px;"><p>12.7</p></td>
      <td style="width:66px;"><p>49</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>151</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2015</p></td>
      <td style="width:57px;"><p>14.2</p></td>
      <td style="width:66px;"><p>96</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>146</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2015</p></td>
      <td style="width:57px;"><p>15.8</p></td>
      <td style="width:66px;"><p>50</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>152</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2015</p></td>
      <td style="width:57px;"><p>14.3</p></td>
      <td style="width:66px;"><p>69</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:85px;"><p>149</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2016</p></td>
      <td style="width:57px;"><p>15.6</p></td>
      <td style="width:66px;"><p>225</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>178</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>+</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>-</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>11.2016</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>163</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2016</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:57px;"><p>&nbsp;</p></td>
      <td style="width:85px;"><p>154</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2017</p></td>
      <td style="width:57px;"><p>15.0</p></td>
      <td style="width:66px;"><p>114</p></td>
      <td style="width:57px;"><p>-</p></td>
      <td style="width:57px;"><p>+</p></td>
      <td style="width:85px;"><p>147</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:66px;"><p>&nbsp;</p></td>
      <td style="width:38px;"><p>&nbsp;</p></td>
      <td style="width:110px;"><p>+</p></td>
    </tr>
  </tbody>
</table>

<p><strong>Table 2. </strong>List of diagnoses 2010-2016</p>

<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:76px;"><p><strong>Date </strong></p></td>
      <td style="width:539px;"><p><strong>Diagnosis</strong></p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>12.2010</p></td>
      <td style="width:539px;"><p>Acute kidney injury of unknown origin</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>01.2011</p></td>
      <td style="width:539px;"><p>Multiple myeloma</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>04.2011</p></td>
      <td style="width:539px;"><p>Glomerulonephritis with MGUS</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>07.2011</p></td>
      <td style="width:539px;"><p>Interstitial nephritis with MGUS</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>05.2012</p></td>
      <td style="width:539px;"><p>MGRS: monoclonal immunoglobulin deposition disease IgG &kappa; and proximal tubulopathy IgG &kappa;</p></td>
    </tr>
    <tr>
      <td style="width:76px;"><p>09.2016</p></td>
      <td style="width:539px;"><p>Smoldering multiple myeloma with combined paraproteinemic kidney damage: monoclonal immunoglobulin deposition disease IgG &kappa; and proximal tubulopathy IgG &kappa;</p></td>
    </tr>
  </tbody>
</table>






  
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="inner-right">
      <div class="row">
        <div class="small-12 columns">
          <h4 class="left mr20 mt5">Articles</h4>
          <a href="pdf/ICST-4-225.pdf" target="_blank" class="left mr5"><img src="img/icon-pdf.png" alt=""></a><a href="#" class="left mr5"><img src="img/icon-xml.png" alt=""></a></div>
      </div>
      <hr/>
           <h4><a href="Integrative-Cancer-Science-and-Therapeutics-ICST.php">Journal Home</a></h4>

      <hr/>
      <div id="sticky-anchor"></div>
      <div id="sticky">
        <h2>Jump to</h2>
        <hr/>
        <div class="row collapse">
          <div class="large-6 columns">
            <div class="right-set">
              <ul class="main-bar-list">
                <li><a href="#Article" class="active">Article</a></li>
                <li><a href="#Article_Info" >Article Info</a></li>
                <li><a href="#Author_Info" >Author Info</a></li>
                <li><a href="#Figures_Data" >Figures & Data</a></li>
              </ul>
            </div>
          </div>
          <div class="large-6 columns">
            <ul class="right-bar-list">
        		<li><a href="#jumpmenu1">Abstract</a></li>
            <li><a href="#jumpmenu2">Key words</a></li>
            <li><a href="#jumpmenu3">Background</a></li>
            <li><a href="#jumpmenu4">Case presentation</a></li>
            <li><a href="#jumpmenu5">Bone marrow biopsy</a></li>
            <li><a href="#jumpmenu6">Kidney biopsy</a></li>
            <li><a href="#jumpmenu7">Second kidney biopsy</a></li>
            <li><a href="#jumpmenu8">Third kidney biopsy</a></li>
            <li><a href="#jumpmenu9">Second bone marrow biopsy</a></li>
            <li><a href="#jumpmenu10">Third bone marrow biopsy</a></li>
            <li><a href="#jumpmenu11">Forth bone marrow biopsy</a></li>
            <li><a href="#jumpmenu12">Discussion</a></li>
            <li><a href="#jumpmenu13">Conclusions</a></li>
            <li><a href="#jumpmenu14">Acknowledgements</a></li>
            <li><a href="#jumpmenu15">References</a></li>
            





            
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns"><p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>

	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script><script>
      $(document).foundation();
    </script>
<!--
<script>
	$('a[href="#"]').click(function(event){

    event.preventDefault();

});
	</script> -->
</body>
</html>
