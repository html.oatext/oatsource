<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Clinical and Medical Investigations (CMI) - OATEXT</title>
<meta name="description" content=" Clinical and Medical Investigations (CMI) is an open access journal with comprehensive peer review policy and a rapid publication process on oatext." />
<meta name="keywords" content="scientific journals, medical journals, journals, oatext" />
<meta name="robots" content=" ALL, index, follow"/>
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For All" />
<meta name="language" content="English" />
<meta http-equiv="window-target" content="_top"/>
<meta http-equiv="pics-label" content="for all ages"/>
<meta name="rating" content="general"/>
<meta content="All, FOLLOW" name="GOOGLEBOTS"/>
<meta content="All, FOLLOW" name="YAHOOBOTS"/>
<meta content="All, FOLLOW" name="MSNBOTS"/>
<meta content="All, FOLLOW" name="BINGBOTS"/>
<meta content="all" name="Googlebot-Image"/>
<meta content="all" name="Slurp"/>
<meta content="all" name="Scooter"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Clinical and Medical Investigations (CMI)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName"  href="#Review_Board"   class="anchor">Review Board </a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes"    class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
            <dd><a  name="AnchorName" href="#Special_Issues"    class="anchor">Special Issues</a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Clinical and Medical Investigations (CMI)</h2>
                <h4>Online ISSN: 2398-5763</h4>
                <h2 class="mb5"><a href="#Editor-in-Chief">Dr. Uwe Wollina</a><span class="f14"> (Editor-in-Chief)</span> </h2>
                <span class="black-text"><i class="fa fa-university"></i> Academic Teaching Hospital Dresden-Friedrichstadt, Dresden</span>
                <hr/>
                <p  class="text-justify"> <a href="img/cmi-cover.jpg" target="_blank"><img src="img/cmi-cover.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Clinical and Medical Investigations (CMI) is an open access journal with comprehensive peer review policy and a rapid publication process.&nbsp; CMI is a novel journal that &nbsp;will consider for publication manuscripts of original investigations including clinical and experimental researches, case reports and high quality of review articles in medicine. The journal will cover technical and clinical studies related to health, ethical and social issues in field of Medical Practice &amp; Investigations And Allied Sciences.&nbsp;</p>
                <p>CMI will feature original research, review papers, clinical studies, editorials, expert opinion and perspective papers, rapid communications, letters, commentaries, and book reviews.</p>
                <p>CMI welcomes direct submissions from authors: Attach your word file with e- mail and send it to <a href="mailto:submissions@oatext.com" target="_blank">submissions@oatext.com</a> alternatively to <a href="mailto:editor.cmi@oatext.com" target="_blank">editor.cmi@oatext.com</a></p>
                <p>Please, follow the Instructions for Authors. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by CMI, is properly cited</p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Dr. Uwe Wollina</h2>
              <hr/>
              <p><img src="img/CMI_DamingWei.jpg" align="right" class="profile-pic"> Dr. Uwe Wollina is a distinguished dermatologist and certified venereologist. Dr. Uwe Wollina graduated from Friedrich-Schiller-University Jena. He worked as Head of the Department of Dermatology and Allergology at Academic Teaching Hospital Dresden-Friedrichstadt and Served as President for Several Societies like European Society of Cosmetic and Aesthetic Dermatology and Saxonian Society of Dermatology.</p>
              <p>He was an active member in European Academy of Dermatology and Venereology, German Society of Dermatology, German Cancer Society, German Society of Aesthetic Surgeons, International Academy of Cosmetic Dermatology, German Society for Botulinum Toxin Therapy. He was monitoring several journals as Editor in chief or Editorial Board Member. Some of them are Cosmetic Medicine/ Kosmetische Medizin and Psoriasis: Targets and Therapies, : Journal of the German Society of Dermatology (JDDG), Indian Journal of Dermatology, Clinics in Dermatology, Der Hautarzt, Journal of Cutaneous and Aesthetic Medicine etc,.</p>
              <p>Research Interests: Alllergology, Dermatohistopathology, Environmental Medicine.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Neli T. Basheska</h4>
                  <p>Department of Histopathology and Clinical Cytology<br>
                    University Clinic of Radiotherapy and Oncology<br>
                    Republic of Macedonia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ryuya Yamanaka</h4>
                  <p>Professor<br>
                    Department of Medical Science <br>
                    Graduate School for Health Care Science<br>
                    Kyoto Prefectural University of Medicine<br>
                    Japan </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Daming Wei</h4>
                  <p>President, EKG Technology Lab, Inc.<br>
                    Professor Emeritus <br>
                    University of Aizu<br>
                    Japan </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Carlota Saldanha</h4>
                  <p>Microvascular Biology and Inflammation Unit<br>
                    Institute of Molecular Medicine Medicina,<br>
                    University of Lisbon<br>
                    Portugal </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sarit Larisch</h4>
                  <p>Department of Biology<br>
                    Faculty of Natural Sciences<br>
                    University of Haifa<br>
                    Israel </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ming-Jen Fan</h4>
                  <p>Professor and Director <br>
                    Department of Biotechnology<br>
                    Asia University<br>
                    Taiwan </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Carlos Camps Herrero</h4>
                  <p>Department of Medicine<br>
                    University of Valencia<br>
                    Spain </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Vishwa Jeet Amatya</h4>
                  <p>Associate professor<br>
                    Institute of Biomedical & Health Sciences<br>
                    Hiroshima University<br>
                    Japan </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Fausto J. Pinto</h4>
                  <p>Dean of the Faculty of Medicine<br>
                    University of Lisbon<br>
                    Portugal </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>CHUA Ka-Kit, Tony</h4>
                  <p>HKBU – School of Chinese Medicine<br>
                    China </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Margit Zeher</h4>
                  <p>University of Debrecen<br>
                    Hungary </p>
                </div>
                <div class="medium-4 columns">
                  <h4> Gyanendra Singh</h4>
                  <p>Scientist <br>
                    National Institute of Occupational Health (ICMR)<br>
                    India</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Massimo Tusconi</h4>
                  <p>Department of Public Health<br>
                    Clinical and Molecular Medicine<br>
                    University of Cagliari, Italy<br>
                    Dep. of Psychiatry and Behavioral Sciences<br>
                    University of Texas<br>
                    U.S.A.</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dennis R.A. Mans</h4>
                  <p>Medical Sciences<br>
                    Anton de Kom University of Suriname<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dr.Saurabh Gupta</h4>
                  <p>Oral and Maxillofacial Surgeon<br>
                    Implantologist, Laser and Cosmetic Surgeon<br>
                    Academic and Research Scientist<br>
                    India</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ayman Al-Hendy</h4>
                  <p>Professor (Tenure) & Director<br>
                    Division of Translational Research<br>
                    Department of Obstetrics and Gynecology<br>
                    Medical College of Georgia<br>
                    Georgia Regents University<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Mohammed Rachidi</h4>
                  <p>Molecular Genetics of Human Diseases<br>
                    University Paris Denis Diderot<br>
                    France</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>

              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>



            </div>
            <div id="Review_Board" class="content">
              <h2>Review Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Rabiul Ahasan</h4>
                  <p>Associate Professor<br>
                    Institute for Community Developemnt & Quality of Life 
                    Faculty of Medicine, Universiti Sultan Zainul Adidin
                    21300 Kuala Terengganu, Malaysia.</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Robert Skalik</h4>
                  <p>consultant in cardiology<br>
                    Medical University of Wrocław, Poland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Amgad Mohamed Mahmoud Hazza</h4>
                  <p>Meet Ghamr Hospital, Egypt</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Dr. Abdul Aziz</h4>
                  <p>Magdy Abdelhamid, MD, FSCAI<br>
                    Professor of Cardiovascular Medicine<br>
                    Cairo University</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dr.Reda Elbadawy</h4>
                  <p>Professor Gastroenterology,Hepatology<br>
                    Banha University,Egypt</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ram Jagannathan</h4>
                  <p>Postdoctoral Fellow – Center for Healthful Behavior Change<br>
                    New York</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Arun Beeman </h4>
                  <p> Great Ormond street children's hospital, London</p>
                </div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 2, Issue 3</h4>
              <hr/>
              <!------CMI.1000132 ------>
                <div class="article">
                  <h4><a href="Osteomyelitis-A-manifestation-of-sickle-cell-anemia.php">Osteomyelitis: A manifestation of sickle cell anemia</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Salma M. AlDallal</p>
                  <p class="mb5">Mini Review-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 24, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000132  x------>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 2, Issue 2</h4>
              <hr/>
              <!------CMI.1000126 ------>
                <div class="article">
                  <h4><a href="Association-between-psoriasis-and-ocular-disorders-A-narrative-review.php"> Association between psoriasis and ocular disorders: A narrative review </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abdullah Algarni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdullah Almuqrin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdulaziz Alarwan </p>
                  <p class="mb5">Review  Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 28, 2017 </p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000126  x------>
              <!------CMI.1000127------>
                <div class="article">
                  <h4><a href="A-possible-etiology-of-Kawasaki-Disease.php"> A possible etiology of Kawasaki Disease </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kimihiko Okazaki </p>
                  <p class="mb5">Short Communication-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February28, 2017 </p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000127 x------>
              <!------CMI.1000128------>
                <div class="article">
                  <h4><a href="Minimizing-metabolic-competition-between-the-tumor-and-the-tumor-bearing-using-the-amino-acids-at-doses-comparable-to-their-physiological-levels.php">Minimizing metabolic competition between the tumor and the tumor-bearing: using the amino acids at doses comparable to their physiological levels</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nefyodov L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karavay A</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 11, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000128 x------>
              <!------CMI.1000129------>
                <div class="article">
                  <h4><a href="Theraworx-Skin-Care-Formulation-Reduces-Nosocomial-Associated-CAUTI-Rates-When-Used-for-Urinary-Catheter-Insertion-and-Maintenance.php">Theraworx Skin Care Formulation Reduces Nosocomial Associated CAUTI Rates When Used for Urinary Catheter Insertion and Maintenance</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Joseph F Renzulli</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 10, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000129 x------>
              <!------CMI.1000130------>
                <div class="article">
                  <h4><a href="Biomarkers-of-endothelial-function-and-insulin-resistance-response-to-aerobic-exercise-versus-resisted-exercises-in-obese-type-2-diabetic-patients.php">Biomarkers of endothelial function and insulin resistance response to aerobic exercise versus resisted exercises in obese type 2 diabetic patients</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shehab M. Abd El-Kader <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fadwa M Al-Shreef</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000130 x------>
              <!------CMI.1000131------>
                <div class="article">
                  <h4><a href="Chemotherapy-and-hearing-loss.php">Chemotherapy and hearing loss</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Renato Cunha <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lilian Felipe</p>
                  <p class="mb5">Short Communication-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 20, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000131 x------>
              
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 2, Issue 1</h4>
              <hr/>
              <!------CMI.1000120 ------>
              <div class="article">
                <h4><a href="Being-a-homosexual-to-bisexual-Overlapping-sexual-risks-among-men-who-have-sex-with-men-as-well-as-women-in-South-Asian-countries.php">Being a homosexual to bisexual: Overlapping sexual risks among men who have sex with men as well as women in South Asian countries</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Singh SK <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Deepanjali Vishwakarma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santosh Kumar Sharma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bhawana Sharma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alankar Malviya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tshering N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rahman A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khan S</p>
                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 22, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000120  x------>
              <!------CMI.1000121 ---->
              <div class="article">
                <h4><a href="Nature-green-in-leaf-and-stem-Research-on-plants-with-medicinal-properties-in-Suriname.php">“Nature, green in leaf and stem”. Research on plants with medicinal properties in Suriname</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis R.A. Mans</p>
                <p class="mb5">Review Article-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 26, 2016</p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000121 x------>
              <!------CMI.1000122 ---->
              <div class="article">
                <h4><a href="May-preablative-thyroglobulin-level-be-a-prognostic-indicator-for-papillary-thyroid-cancer.php">May preablative thyroglobulin level be a prognostic indicator for papillary thyroid cancer?</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elgin Ozkan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cigdem Soydal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mine Araz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasemin Genc <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nuriye Ozlem Kucuk </p>
                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 09, 2017</p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000122 x------>
              <!------CMI.1000123 ---->
              <div class="article">
                <h4><a href="The-new-indexes-comparing-the-radicality-of-tumor-removal-and-the-extent-of-post-operative-defects-after-treatment-of-basal-cell-cancer-by-mean-of-Mohs-Surgery-and-Classical-Excision.php">The new indexes comparing the radicality of tumor removal and the extent of post-operative defects after treatment of basal cell cancer by mean of Mohs Surgery and Classical Excision</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrzej Bieniek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Łukasz Matusiak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zdzisław Woźniak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karolona Wójcicka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria Kozioł </p>
                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 13, 2017</p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000123 x------>
              <!------CMI.1000124 ---->
              <div class="article">
                <h4><a href="The-robust-solution-for-epidemiology.php">The robust solution for epidemiology</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paul T E Cusack</p>
                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 21, 2017</p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000124 x------>
              <!------CMI.1000125 ---->
              <div class="article">
                <h4><a href="Surgical-resection-for-bilateral-giant-emphysematous-bullae.php">Surgical resection for bilateral giant emphysematous bullae</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shinsuke Kitazawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yusuke Saeki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shinji Kikuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukinobu Goto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukio Sato</p>
                <p class="mb5">Case Report-Clinical and Medical Investigations (CMI)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 24, 2017</p>
                </em>
                <hr/>
              </div>
              <!------CMI.1000125 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion">
                  <dd> <a href="#panel1c" class="accordian-active">Volume 2<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>April 2017</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>February 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------CMI.1000126 ------>
                <div class="article">
                  <h4><a href="Association-between-psoriasis-and-ocular-disorders-A-narrative-review.php"> Association between psoriasis and ocular disorders: A narrative review </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abdullah Algarni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdullah Almuqrin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdulaziz Alarwan </p>
                  <p class="mb5">Review  Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 28, 2017 </p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000126  x------>
              <!------CMI.1000127------>
                <div class="article">
                  <h4><a href="A-possible-etiology-of-Kawasaki-Disease.php"> A possible etiology of Kawasaki Disease </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kimihiko Okazaki </p>
                  <p class="mb5">Short Communication-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February28, 2017 </p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000127 x------>
              <!------CMI.1000128------>
                <div class="article">
                  <h4><a href="Minimizing-metabolic-competition-between-the-tumor-and-the-tumor-bearing-using-the-amino-acids-at-doses-comparable-to-their-physiological-levels.php">Minimizing metabolic competition between the tumor and the tumor-bearing: using the amino acids at doses comparable to their physiological levels</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nefyodov L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karavay A</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 11, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000128 x------>
              <!------CMI.1000129------>
                <div class="article">
                  <h4><a href="Theraworx-Skin-Care-Formulation-Reduces-Nosocomial-Associated-CAUTI-Rates-When-Used-for-Urinary-Catheter-Insertion-and-Maintenance.php">Theraworx Skin Care Formulation Reduces Nosocomial Associated CAUTI Rates When Used for Urinary Catheter Insertion and Maintenance</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Joseph F Renzulli</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 10, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000129 x------>
              <!------CMI.1000130------>
                <div class="article">
                  <h4><a href="Biomarkers-of-endothelial-function-and-insulin-resistance-response-to-aerobic-exercise-versus-resisted-exercises-in-obese-type-2-diabetic-patients.php">Biomarkers of endothelial function and insulin resistance response to aerobic exercise versus resisted exercises in obese type 2 diabetic patients</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shehab M. Abd El-Kader <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fadwa M Al-Shreef</p>
                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000130 x------>
              <!------CMI.1000131------>
                <div class="article">
                  <h4><a href="Chemotherapy-and-hearing-loss.php">Chemotherapy and hearing loss</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Renato Cunha <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lilian Felipe</p>
                  <p class="mb5">Short Communication-Clinical and Medical Investigations (CMI)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 20, 2017</p>
                  </em>
                  <hr/>
                </div>
              <!------CMI.1000131 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------CMI.1000120 ------>
                                <div class="article">
                                  <h4><a href="Being-a-homosexual-to-bisexual-Overlapping-sexual-risks-among-men-who-have-sex-with-men-as-well-as-women-in-South-Asian-countries.php">Being a homosexual to bisexual: Overlapping sexual risks among men who have sex with men as well as women in South Asian countries</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Singh SK <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Deepanjali Vishwakarma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santosh Kumar Sharma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bhawana Sharma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alankar Malviya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tshering N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rahman A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khan S</p>
                                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000120  x------>
                                <!------CMI.1000121 ---->
                                <div class="article">
                                  <h4><a href="Nature-green-in-leaf-and-stem-Research-on-plants-with-medicinal-properties-in-Suriname.php">“Nature, green in leaf and stem”. Research on plants with medicinal properties in Suriname</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis R.A. Mans</p>
                                  <p class="mb5">Review Article-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 26, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000121 x------>
                                <!------CMI.1000122 ---->
                                <div class="article">
                                  <h4><a href="May-preablative-thyroglobulin-level-be-a-prognostic-indicator-for-papillary-thyroid-cancer.php">May preablative thyroglobulin level be a prognostic indicator for papillary thyroid cancer?</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elgin Ozkan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cigdem Soydal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mine Araz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasemin Genc <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nuriye Ozlem Kucuk </p>
                                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 09, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000122 x------>
                                <!------CMI.1000123 ---->
                                <div class="article">
                                  <h4><a href="The-new-indexes-comparing-the-radicality-of-tumor-removal-and-the-extent-of-post-operative-defects-after-treatment-of-basal-cell-cancer-by-mean-of-Mohs-Surgery-and-Classical-Excision.php">The new indexes comparing the radicality of tumor removal and the extent of post-operative defects after treatment of basal cell cancer by mean of Mohs Surgery and Classical Excision</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrzej Bieniek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Łukasz Matusiak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zdzisław Woźniak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Karolona Wójcicka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria Kozioł </p>
                                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 13, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000123 x------>
                                <!------CMI.1000124 ---->
                                <div class="article">
                                  <h4><a href="The-robust-solution-for-epidemiology.php">The robust solution for epidemiology</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paul T E Cusack</p>
                                  <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 21, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000124 x------>
                                <!------CMI.1000125 ---->
                                <div class="article">
                                  <h4><a href="Surgical-resection-for-bilateral-giant-emphysematous-bullae.php">Surgical resection for bilateral giant emphysematous bullae</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shinsuke Kitazawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yusuke Saeki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shinji Kikuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukinobu Goto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukio Sato</p>
                                  <p class="mb5">Case Report-Clinical and Medical Investigations (CMI)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 24, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------CMI.1000125 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                    <div class="hr-tabs">
                      <ul class="tabs" data-tab>
                        <li class="tab-title active"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec 2016</span> <span class="mt3 grey-text">Issue 3</a></li>
                        <li class="tab-title"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>Oct 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                        <li class="tab-title "><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span> <span class="mt3 grey-text">Issue 1</a></li>
                      </ul>
                      <div class="tabs-content">
                        <div class="content active" id="panelc">
                          <div class="row pt10">
                            <div class="small-12 columns">
                              <!------CMI.1000112------>
                              <div class="article">
                                <h4><a href="Prevalenve-of-Human-Papilloma-Virus-in-anal-duct-in-patients-with-HIV.php">Prevalenve of Human Papilloma Virus in anal duct in patients with H.I.V.</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carreño Lomeli MA <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Medina Andrade LA <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruiz Flores BM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anota Rivera M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ortiz Contreras F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robledo Madrid Paul <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santiago Ramirez NI <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fuentes Duran S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gallaga Rojas Marco A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Selene Noemi Montoya Valdez </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 19, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000112 x------>
                              <!------CMI.1000114------>
                              <div class="article">
                                <h4><a href="Suggestive-or-inexclusive-linkage-of-Childhood-Cerebral-Malaria-to-3q27-q29-region-in-a-Sudanese-consanguineous-family-from-Central-Sudan.php">Suggestive or inexclusive linkage of Childhood Cerebral Malaria to 3q27-q29 region in a Sudanese consanguineous family from Central Sudan</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adil Mergani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ammar H. Khamis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>EL fatih Hashim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Gumma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bella Awadelseed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali Babikir Haboor <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nasr EL din M. A. Elwali</p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000114 x------>
                              <!------CMI.1000115------>
                              <div class="article">
                                <h4><a href="Antimalarial-in-dermatology-62-cases.php">Antimalarial in dermatology: 62 cases</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>A Lahlou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>S Elloudi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>H Baybay <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>FZ Mernissi</p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 03, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000115 x------>
                              <!------CMI.1000116------>
                              <div class="article">
                                <h4><a href="In-vitro-susceptibility-testing-of-yeasts-to-nystatin–low-minimum-inhibitory-concentrations-suggest-no-indication-of-in-vitro-resistance-of-Candida-albicans-Candida-species-or-non-Candida-yeast-species-to-nystatin.php">In vitro susceptibility testing of yeasts to nystatin – low minimum inhibitory concentrations suggest no indication of in vitro resistance of Candida albicans, Candida species or non-Candida yeast species to nystatin</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pietro Nenoff <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Constanze Krüger <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Claudia Neumeister <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ulrich Schwantes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daniela Koch</p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 20, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000116 x------>
                              <!------CMI.1000117----
              <div class="article">
              <h4><a href="Assessment-of-Genetics-Mutation-NOTCH3-Gene-in-Patients-with-Migraine-Disease-in-Tabriz-Iran.php">Assessment of Genetics Mutation NOTCH3 Gene in Patients with Migraine Disease in Tabriz, Iran</a></h4>
              <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shahin Asadi
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mahsa Jamali
              <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ali Nazizadeh</p>
              <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
              <em>
              <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>  November 30, 2016   </p>
              </em>
              <hr/>
              </div>
              ------CMI.1000117 x------>
                              <!------CMI.1000119---->
                              <div class="article">
                                <h4><a href="Evaluation-of-the-topical-antiperspirant-effects-of-a-simple-herbal-formula.php">Evaluation of the topical antiperspirant effects of a simple herbal formula</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ping-Chung LEUNG <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Patrick CL HUI <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Frency SF NG <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Clara BS Lau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>King-Fai CHENG <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Winnie Wing-Man LO <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ping CHOOK <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ping CHOOK</p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000119 x------>
                              <!------CMI.1000120 ---->
                              <div class="article">
                                <h4><a href="Occlusal-analysis-and-management-of-a-patient-with-vertigo-a-case-report.php">Occlusal analysis and management of a patient with vertigo: a case report</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kengo Torii</p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 16, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000120 x------>
                            </div>
                          </div>
                        </div>
                        <div class="content" id="panelb">
                          <div class="row pt10">
                            <div class="small-12 columns">
                              <!------CMI.1000105------>
                              <div class="article">
                                <h4><a href="Delivery-of-pain-education-through-picture-telephone-videoconferencing-for-veterans-with-chronic-non-cancer-pain.php"> Delivery of pain education through picture-telephone videoconferencing for veterans with chronic, non-cancer pain </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> David Cosio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Erica HL </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 16, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000105 x------>
                              <!------CMI.1000106 ------>
                              <div class="article">
                                <h4><a href="New-medical-approach-for-rejuvenation-of-the-periorbital-area.php"> New medical approach for rejuvenation of the periorbital area</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Evgeniya Ranneva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriel Siquier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Olga Liplavk </p>
                                <p class="mb5">Research Article-Medical Devices and Diagnostic Engineering (MDDE)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 26, 2016</p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000106  x------>
                              <!------CMI.1000107------>
                              <div class="article">
                                <h4><a href="Infections-in-IBD-Patients-on-Immunomodulators-Corticosteroids-and-Anti-TNF-therapy-Is-Elderly-Age-a-Predictor.php"> Infections in IBD Patients on Immunomodulators, Corticosteroids, and Anti-TNF therapy: Is Elderly Age a Predictor? </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Christina Tofani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Faten Aberra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ann Tierney <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lichtenstein GR </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 23, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000107 x------>
                              <!------CMI.1000108------>
                              <div class="article">
                                <h4><a href="Recurrent-ectopic-pregnancy-at-the-ipsilateral-tubal-stump-following-total-salpingectomy-Case-report-and-Review-of-Literature.php"> Recurrent ectopic pregnancy at the ipsilateral tubal stump following total salpingectomy Case report and Review of Literature </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lakhotia S, Yussof SM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Aggarwal I </p>
                                <p class="mb5">Case Report-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 26, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000108 x------>
                              <!------CMI.1000111------>
                              <div class="article">
                                <h4><a href="Is-NerBloc-the-botulinum-toxin-type-B-formulation-an-effective-therapy-for-cervical-dystonia.php">Is NerBloc, the botulinum toxin type B formulation, an effective therapy for cervical dystonia?</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masahiro Horiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akira Endo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mika Ishii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryuji Kaji </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 17, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000111 x------>
                              <!------CMI.1000113------>
                              <div class="article">
                                <h4><a href="A-clinical-analysis-of-an-intrahepatic-lesion-detected-in-a-65-year-old-lady-who-admitted-for-severe-epigastric-pain.php">A clinical analysis of an intrahepatic lesion detected in a 65-year-old lady who admitted for severe epigastric pain</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Bolognesi </p>
                                <p class="mb5">Case Report-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000113 x------>
                            </div>
                          </div>
                        </div>
                        <div class="content" id="panela">
                          <div class="row pt10">
                            <div class="small-12 columns">
                              <!------CMI.1000101------>
                              <div class="article">
                                <h4><a href="Identification-of-serum-APOA1-and-APOC3-as-potential-biomarkers-of-aplastic-anemia.php"> Identification of serum APOA1 and APOC3 as potential biomarkers of aplastic anemia </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Edita Hamzic <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Karen Whiting <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ruth Pettengell </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 27, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000101 x------>
                              <!------CMI.1000102 ------>
                              <div class="article">
                                <h4><a href="The-role-of-poly-ADP-ribose-polymerase-inhibitors-in-the-treatment-of-endometrial-cancer-a-scoping-review-of-the-current-literature.php"> The role of poly(ADP-ribose) polymerase inhibitors in the treatment of endometrial cancer: a scoping review of the current literature </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Samantha Ricci <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Laura Parisi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amy Knehans <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rebecca Phaeton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joshua P Kesterson </p>
                                <p class="mb5"> Review  Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 03, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000102  x------>
                              <!------CMI.1000103------>
                              <div class="article">
                                <h4><a href="Initial-outcomes-from-an-autism-treatment-demonstration.php"> Initial outcomes from an autism treatment demonstration </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Steven Evans <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Debra Jude Fuller </p>
                                <p class="mb5">Research Article-Clinical and Medical Investigations (CMI)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 10, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------CMI.1000103 x------>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>




            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr/>
              <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border:0px;">
                <tr>
                  <td align="Center" style="border:0px;"><a href="http://road.issn.org/"><img src="img/IMM_ROAD.JPG"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://www.worldcat.org/"><img src="img/IMM_WorldCat.JPG"></a></td>
                  <td align="Center" style="border:0px;"><a href="http://www.icmje.org/"><img src="img/MDDE-icmje.gif"></a></td>
                </tr>
                <tr>
                  <td align="Center" style="border:0px;"><a href="https://www.researchbib.com/"><img src="img/IFNM-Researcch-Bib.png"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://doaj.org/"><img src="img/MDDE-cropped.jpg"></a></td>
                  <td align="Center" style="border:0px;"><a href="https://publons.com/home/"><img src="img/MDD-squarespace.com.png"></a></td>
                </tr>
              </table>
              <p>Note : DOAJ under Progress</p>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>CMI welcomes direct submissions from authors: Attach your word file with e- mail and send it to <a href="mailto:submissions@oatext.com" target="_blank">submissions@oatext.com</a> alternatively to <a href="mailto:editor.cmi@oatext.com" target="_blank">editor.cmi@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>

            <div id="Special_Issues" class="content">
              <h2>Special Issues</h2>
              <hr/>
              <h4>The use of herbs for cosmetic purposes throughout the world</h4>
              <br>
              <p><b>Editor Affiliation:</b><br>
              Dennis R.A. Mans<br>
      				Medical Sciences<br>
      				Anton de Kom University of Suriname<br>
      				Paramaribo<br>
              Suriname<br><br>

      				Submission date: June 30, 2017<br> 
      				Publication date: July 30, 2017</p>
                  
      				<p>Human beings have used plants, plant constituents, herbal preparations, and finished herbal products for centuries and for a variety of purposes, among others as foods; for clothing and shelter; as medicinal agents; as stimulants, narcotics, and hallucinogens; to prepare poisonous arrow- and spearheads for warfare and hunting; and as dyes, aromatics, and cosmetics. The latter use dates back to ancient times, and virtually all civilizations have extensively used herbs to enhance appearance and attractiveness, increase virility and fertility, prolong life and delay aging, enhance mental performance, improve hygiene, and preserve general health.</p>

              <p>Herbal cosmetics are considered highly effective in beauty treatments with few side effects, and the demand for these products is growing all over the world. A few well-known examples are preparations based on <em>Aloe vera</em>, avocado, almond, henna, sandalwood, saffron, neem, and lavender. These products have been fairly well investigated with regards to their pharmacologically active principle(s) and safety. Various societies throughout the world use many other herbal cosmetics that are less known but are now, due to increasing globalization, rapidly finding their way to mainstream markets.</p>

              <p>The Journal of Clinical and Medical Investigations wishes to dedicate a special issue to herbal cosmetics that are traditionally used in various parts of the world and that have the potential to penetrate world markets. Topics that would fit the special include:</p>


              <ul>
              	<li>the use of herbal cosmetics by traditional peoples (for instance, those of South America, Africa, Australia, and North America)</li>
              	<li>the use of herbal cosmetics in association with traditional medicinal systems (for instance with Traditional Chinese Medicine, Indian Ayurveda, or Indonesian Jamu)</li>
              	<li>the production of herbal cosmetics</li>
              	<li>the chemical composition of herbal cosmetics</li>
              	<li>pharmacological effects of herbal cosmetics</li>
              	<li>possible side- or undesired effects of herbal cosmetics</li>
              	<li>the market for herbal cosmetics (for instance, the world market or that of Europe, North America, etc.)</li>
              	<li>the use of herbal cosmetics to enhance appearance and attractiveness</li>
              	<li>the use of herbal products to increase virility and fertility</li>
              	<li>the use of herbal products to prolong life and delay aging</li>
              	<li>the use of herbal products to enhance mental performance</li>
              	<li>the use of herbal products to improve hygiene</li>
              	<li>the use of herbal products preserve general health</li>
              </ul>

              <p>In addition to this brief list of possible topics, we welcome submissions addressing other aspects of herbal cosmetics. We welcome research articles, research reports and research reviews. Articles for this special issue should be submitted by June 30, 2017. The manuscripts should be prepared according to the guidelines and the format of the journal.</p>

              <p><b>Submit manuscript to:</b> <a href="editor.cmi@oatext.com">editor.cmi@oatext.com</a></p>
            </div>





          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
