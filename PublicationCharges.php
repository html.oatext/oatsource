<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="OA Text is an independent open-access scientific publisher showcases innovative research and ideas aimed at improving health by linking research and practice to the benefit of society." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT, oatext, oat, oatext,  open access text" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <h1>Paying for Open Access</h1>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="row">
    <div class="large-9 columns">
      <div class="inner-left">
        <h2>Publication Charges</h2>
        <div id="publication-charge-set">
          <h4 class="mt10">Article Publication Charges (APC)</h4>
          <p>OA Text journals are open access, we do not charge the end user when accessing a manuscript or any article. This allows the scientific community to view, download, distribution of an article in any medium, provided that the original work is properly cited, under the term of<a href="http://creativecommons.org/licenses/by/4.0/" target="_blank">&quot;Creative Commons Attribution License&quot;</a>. In line with other open access journals we provide a flat fee submission structure (dependent upon the journal) on the acceptance of a peer-reviewed article which covers in part the entirety of the publication pathway (the article processing charge). The process includes our maintenance, submission and peer review systems and international editing, publication and submission to global indexing and tracking organisations and archiving to allow instant access to the whole article and associated supplementary documents. We also have to ensure enough investment to secure a sustainable model which ethically, legally and financially stable.</p>
          <ul>
            <li><a href="#set-F1">1. What are your publication charges?</a></li>
            <li><a href="#set-F2">2. Why are your charges set at these levels?</a></li>
            <li><a href="#set-F3">3. When do I pay Article Publication Charges?</a></li>
          </ul>
          <div id="set-F1">
            <h4>1. What are your publication charges?</h4>
            <p>The costs of producing and maintaining OA Text are recovered by charging a publication fee to authors or research sponsors for each article that they publish.</p>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
              <colgroup>
              <col />
              <col />
              </colgroup>
              <tbody>
                <tr height="28">
                  <td height="28"><strong>Journal</strong></td>
                  <td align="right" class="text-right"><strong>Publication Fee (GBP / USD)</strong></td>
                </tr>
                <tr>
                  <td>Integrative Molecular Medicine (IMM)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Global Dermatology (GOD)</td>
                  <td align="right"> £1600 / $2000</td>
                </tr>
                <tr>
                  <td>Integrative Cancer Science and Therapeutics (ICST)</td>
                  <td align="right"> £1760 / $2190</td>
                </tr>
                <tr>
                  <td>Journal of Integrative Cardiology (JIC)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Integrative Pharmacology, Toxicology and Genotoxicology (IPTG)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Integrative Food, Nutrition and Metabolism (IFNM)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Dental, Oral and Craniofacial Research (DOCR)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Clinical Research and Trials (CRT)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Integrative Obesity and Diabetes (IOD)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Global Anesthesia and Perioperative Medicine (GAPM)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Contemporary Behavioral Health Care (CBHC)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Fractal Geometry and Nonlinear Analysis in Medicine and Biology (FGNAMB)</td>
                  <td align="right"> £0</td>
                </tr>
                <tr>
                  <td>Clinical Case Reports and Reviews (CCRR)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Clinical Obstetrics, Gynecology and Reproductive Medicine (COGRM)</td>
                  <td align="right"> £1030 / $1290</td>
                </tr>
                <tr>
                  <td>Global Surgery (GOS)</td>
                  <td align="right"> £900 / $1100</td>
                </tr>
                <tr>
                  <td>Cardiovascular Disorders and Medicine (CDM)</td>
                  <td align="right"> £780 / $980</td>
                </tr>
                <tr>
                  <td>Molecular Biophysics and Biochemistry (MBB)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Biology, Engineering and Medicine (BEM)</td>
                  <td align="right"> £690/ $890 </td>
                </tr>
                <tr>
                  <td> Journal of Translational Science (JTS) </td>
                  <td align="right"> £1600 / $2000</td>
                </tr>
                <tr>
                  <td>New Frontiers in Ophthalmology (NFO)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Clinical Proteomics and Bioinformatics (CPB)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Journal of Systems and Integrative Neuroscience (JSIN)</td>
                  <td align="right"> £1030 / $1290</td>
                </tr>
                <tr>
                  <td> General Internal Medicine and Clinical Innovations (GIMCI) </td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td> Frontiers in Nanoscience and Nanotechnology (FNN) </td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td> Global Vaccines and Immunology (GVI) </td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Global Hormonal Health (GHH)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Pediatric Dimensions (PD) </td><td align="right"> £1760 / $2190</td>
                </tr> 
                <tr>
                  <td>Pulmonary and Critical Care Medicine (PCCM)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Translational Brain Rhythmicity (TBR) </td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Biomedical Research and Clinical Practice (BRCP)</td>
                  <td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Biomaterials and Tissue Technology (BTT)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Trauma and Emergency Care (TEC)</td>
                  <td align="right"> £1030 / $1290</td>
                </tr>
                <tr>
                  <td>Nursing and Palliative Care (NPC)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Frontiers in Women’s Health (FWH)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Advanced Material Science (AMS)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Clinical Microbiology and Infectious Diseases (CMID)</td>
                  <td align="right"> £780 / $980</td>
                </tr>
                <tr>
                  <td>Interdisciplinary Journal of Chemistry (IJC)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Mental Health and Addiction Research (MHAR)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Biomedical Genetics and Genomics (BGG)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Energy Research and Applications (ERA)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Otorhinolaryngology Head and Neck Surgery (OHNS)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Nuclear Medicine and Biomedical Imaging (NMBI)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Journal of Stem Cell Research and Medicine (JSCRM)</td>
                  <td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                  <td>Physical Medicine and Rehabilitation Research (PMRR)</td>
                  <td align="right"> £1200 / $1500</td>
                </tr>
                <tr>
                  <td>Integrative Clinical Medicine (ICM)</td>
                  <td align="right"> £1200 / $1500</td>
                </tr>
                <tr>
		        <td>Journal of Medicine and Therapeutics (JMT)</td><td align="right"> £1110	 / $1390</td>
		        </tr>
            <tr>
                <td>Integrative Endocrinology and Metabolism (IEM)</td><td align="right"> £1110 / $1390</td>
                </tr>
                <tr>
                  <td>Gastroenterology Hepatology and Endoscopy (GHE)</td>
                  <td align="right"> £690 / $890</td>
                </tr>
                <tr>
                  <td>Alzheimer's, Dementia & Cognitive Neurology (ADCN)</td>
                  <td align="right"> £1200 / $1500</td>
                </tr>
                <tr><td>Clinical and Medical Investigations (CMI)</td><td align="right"> £960 / $1190</td>
				</tr>
				<tr><td>Medical Devices and Diagnostic Engineering (MDDE)</td><td align="right"> £960 / $1190</td>
				</tr>
                </tr>
                <tr>
                <td>Vascular Diseases and Therapeutics (VDT)</td><td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                <td>Global Imaging Insights (GII)</td><td align="right"> £205 / $250</td>
                </tr>
                <tr>
                <td>Hematology & Medical Oncology (HMO)</td><td align="right"> £800 / $990</td>
                </tr>
                <tr>
                <td>Oral Health and Care (OHC)</td><td align="right"> £960 / $1190</td>
                </tr>
                <tr>
                <td>Hypertension Current Concepts and Therapeutics (HCCT) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Nephrology and Renal Diseases (NRD) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Clinical and Diagnostic Pathology (CDP) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Journal of Spine Care (JSC) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Neurological Disorders and Therapeutics (NDT) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Rheumatology and Orthopedic Medicine (ROM) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Cancer Reports and Reviews (CRR) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Surgery and Rehabilitation (SRJ) </td><td align="right">£1030 / $1290</td>
                </tr>
                <tr>
                <td>Journal of Psychology and Psychiatry (JPP) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>Medical research and innovations (MRI) </td><td align="right"> £1440 / $1800</td>
                </tr> 
                <tr>
                <td>Radiology and Diagnostic Imaging (RDI) </td><td align="right"> £690 / $890</td>
                </tr>
                 <tr>
                <td>Animal Husbandry, Dairy and Veterinary Science (AHDVS) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
               <tr>
                <td>Research and Review Insights (RRI) </td><td align="right"> £810 / $990</td>
                </tr>
                <tr>
                <td>Medical and Clinical Archives (MCA) </td><td align="right"> £810 / $990</td>
                </tr>
                <tr>
                <td>Blood, Heart and Circulation (BHC) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
        				<td>Transplantation Open (TO) </td><td align="right"> £960 / $1190</td>
        				</tr>
        				<tr>
        				<td>Global Drugs and Therapeutics (GDT) </td><td align="right"> £960 / $1190</td>
        				</tr>
        				<tr>
        				<td>Pharmacology, Drug Development & Therapeutics (PDDT) </td><td align="right"> £690 / $890</td>
        				</tr>
		            <tr>
		            <td>Health and Primary Care (HPC) </td><td align="right"> £800 / $990</td>
		            </tr>
		            <tr>
		            <td>Virology: Research and Reviews (VRR) </td><td align="right"> £690 / $890</td>
		            </tr>
                <tr>
                <td>Forensic Science and Criminology (FSC) </td><td align="right"> £690 / $890</td>
                </tr>
                <tr>
                <td>General Medicine-Open (GMO) </td><td align="right"> £690 / $890</td>
                </tr>
                

                
              </tbody>
            </table>
            <p>* Authors who lack the funds to cover publication fees may request a discount. In order to keep publication charges as low as possible, fee discounts are not automatically given but must be approved on a case-by-case basis, and authors from developing and transitional countries are given priority.</p>
            <p><strong>Note:<em>Fractal Geometry and Nonlinear Analysis in Medicine and Biology (FGNAMB) Journal doesn&rsquo;t have any publication charges</em>.</strong></p>
          </div>
          <div id="set-F2">
            <h4>2. Why are your charges set at these levels?</h4>
            <p>All articles published in OA Text are open access. Open Access publishing implies that all readers, anywhere in the world, are allowed unrestricted to full text of articles, immediately on publication in OA Text Journals. The Article Publication Charges pay for the editorial and production costs of the journal, for hosting the website, publishing articles online, preparing HTML , PDF and XML versions of the articles and submitting the articles in electronic citation database like CrossRef. </p>
            <p>Our financial goals are to:</p>
            <ul>
              <li> Recover capitalization costs;</li>
              <li> Produce sufficient revenue to allow for a sustainable and scalable publishing program, under continuous development;</li>
              <li> Bend the publication-charge cost downward over time.</li>
            </ul>
          </div>
          <div id="set-F3">
            <h4>3. When do I pay Article Publication Charges?</h4>
            <p>Article Publication Charges will have to be paid after the manuscript has been peer reviewed and accepted for publication. The corresponding author will be responsible for arranging the payments. </p>
          </div>
        </div>
      </div>
    </div>
    <div class="large-3 columns">
      <div class="inner-right">
        <h2 class="mb10"><a href="GuidelinesforAuthors.php">Guidelines for Authors</a></h2>
        <ul>
          <li><a href="ArticleTypes-Format.php">Article Types &amp; Format</a></li>
          <li><a href="Supplemental-Material.php">Supplemental Material</a></li>
          <li><a href="Submission-Review-Decision.php">Submission, Review, &amp; Decision</a></li>
          <li><a href="PublicationCharges.php" class="active">Publication Charges</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
