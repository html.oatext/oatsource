<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="OA Text is an independent open-access scientific publisher showcases innovative research and ideas aimed at improving health by linking research and practice to the benefit of society." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT oatext, oat, open access text" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<title>Electrical Left Ventricular Mapping in Patients without CKD and with different stages of CKD</title>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
<div class="small-12 columns">
<hr class="mt0 green-hr-line"/>
</div></div>
<!--banner -->
  <div class="row">
    <div class="medium-12 text-center columns">
<div class="inner-banner">
      <h1>Take a look at the Recent articles</h1>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="row">
    <div class="large-8 columns">
    

    
    
      <div class="inner-left">
        <h2 class="w600">Electrical Left Ventricular Mapping in Patients without CKD and with different stages of CKD</h2>
        
        
<div data-dropdown="a1" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi</p>
</div>
<div id="a1" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Cardiac Surgery and Artificial Cardiac Stimulation Division, Department of Medicine, Hospital e Clínica São Gonçalo, São Gonçalo, RJ, Brazil</p>
<p><i class="fa  fa-envelope-o"></i> E-mail : <a href="mailto:bhuvaneswari.bibleraaj@uhsm.nhs.uk">bhuvaneswari.bibleraaj@uhsm.nhs.uk</a></p>
</div>

<div data-dropdown="a2" data-options="is_hover:false" class="custom-tootip-head">
<p class="pt5 mb10 f14 author-title"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Shaojie Chen</p>
</div>
<div id="a2" class="tool-tip f-dropdown custom-tooltip-content" data-dropdown-content >
<p class="mb5">Department of Cardiology, Shanghai First People’s Hospital, Shanghai Jiao Tong University School of Medicine, Shanghai, China</p>
</div>



 
      
      <p class="mt10">DOI: 10.15761/JIC.1000218</p>
      <div class="mt25"></div>
      <div class="custom-horizontal">
        <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs">
          <dd class="active"><a  name="AnchorName"  href="#Article"   class="anchor" >Article</a></dd>
          <dd><a  name="AnchorName"  href="#Article_Info"   class="anchor">Article Info</a></dd>
          <dd><a   name="AnchorName" href="#Author_Info"    class="anchor">Author Info</a></dd>
          <dd><a   name="AnchorName" href="#Figures_Data"   class="anchor">Figures & Data</a></dd>
        </dl>
        <div class="tabs-content">
          <div id="Article" class="content active">
          

<div class="text-justify">

<h2 id="jumpmenu1">Letter to Editor</h2>
                  <p>Recently, we evaluated patients who had received a dual chamber pacemaker implant due to sinus node disease or 3<sup>rd</sup>/2<sup>nd</sup> degree type 2 atrioventricular block in chronic kidney disease (CKD) stages 2, 3 and 4. We observed that the sustained ventricular tachycardia episodes only occurred in patients with CKD stage 4, suggesting that the most advanced the stage of CKD, greater the incidence of malignant arrhythmias [1]. Kidney disease induces cardiac remodeling including left ventricular hypertrophy (LVH) and heart fibrosis. Several clinical studies, including those who recruited participants with mild-to-moderate reduction in estimated glomerular filtration rate (eGFR), showed an independent association between CKD and LVH [2-5]. Specifically, there is a progressive increase in the prevalence of LVH, and left ventricular mass increased when the eGFR decreases. In addition, among participants with more advanced kidney disease on dialysis, magnetic resonance imaging (MRI) with contrast demonstrates a diffuse pattern image with gadolinium uptake suggestive of fibrosis and non-ischemic cardiomyopathy [6]. The pathogenesis of these conditions is considered multifactorial, and the presence of commonly associated comorbidities, such as hypertension, diabetes mellitus, and anemia, explain only part of the left ventricular remodeling [7-9]. The molecular basis for these changes includes activation of growth factors, proto-oncogenes, plasma norepinephrine, cytokines, and angiotensin II. These factors regulate intracellular processes that accelerate cardiac hypertrophy, myocardial fibrosis, and apoptosis [10,11]. Any LVH and cardiac fibrosis have been linked to increased risk of sustained ventricular arrhythmias and predisposition to SCD [12-16]. Structural changes can alter the electrophysiological properties of the myocardium. The myocardial fibrosis disrupts the normal architecture and results in a decrease in conduction velocity through the diseased tissue [17]. This condition can form heterogeneous areas of conduction and depolarization that can sustain a re-entrant arrhythmia, such as ventricular tachycardia [14,15,18]. These structural changes in cardiac conduction delay ventricular activation and create late potentials in the terminal portion of the QRS complex. Furthermore, these low amplitude signals, which may be detected using a high-resolution electrocardiogram, were identified in 25% of patients on dialysis [19].</p>

                  <p>This transversal study involved 40 patients with CKD stage 4 and without CKD, all of them having a history of symptomatic paroxysmal AF (PAF). The study was piloted in agreement with the Helsinki declaration and approved by the ethics committee of our institution. All patients signed the informed consent term before inclusion. This study was conducted at the Hospital e Cl&iacute;nica S&atilde;o Gon&ccedil;alo, Rio de Janeiro, Brazil. Patients were recruited from July 2015 till July 2016 from the Arrhythmias and Artificial Cardiac Pacing Service of the same hospital. Enrolled patients met the following criteria: (i) a heart with an ejection fraction of &gt;50% as measured by echocardiography (Simpson&rsquo;s method), (ii) PAF (defined as AF episodes lasting &lt;7 days with spontaneous termination) registered on ECG or 24-h Holter monitoring, (iii) aged 18 to 80 years, (iv) patients with CKD stage 4: estimated glomerular filtration rate (eGFR) between 15 and 29 mL/min/1.73 m<sup>2</sup>, CKD stage 3: eGFR between 30 and 59 mL/min/1.73 m<sup>2</sup>, CKD stage 2: eGFR between 60 and 89 mL/min/1.73 m<sup>2</sup> with microalbuminuria (albumin:creatinine ratio &gt; 30mg/g) , CKD stage 1: eGFR &gt;90 mL/min/1.73 m<sup>2</sup> with microalbuminuria (albumin:creatinine ratio &gt; 30mg/g), and no CKD patients: eGFR &gt;60 mL/min/1.73 m<sup>2</sup> calculated using the Chronic Kidney Disease Epidemiology Collaboration (CKD-EPI) equation [20] (without microalbuminuria), and (v) the capacity to read, comprehend and sign the informed consent form, and attend the study. Patients with any of the following were excluded: (i) pregnancy; (ii) valvular disease with significant adverse sequelae; (iii) unstable angina, myocardial infarction, transient ischemic attack or stroke previously; (iv) psychiatric disease; (v) the inability to be monitored clinically after the procedure; (vi) a known addiction to alcohol or drugs that affects the intellect; (vii) congestive heart failure (symptoms of functional class II to IV heart failure on the New York Heart Association scale).</p>

                  <p>The subjects were divided into five groups according to their KDIGO CKD status, and the procedure performed: CKD stage 4, 3, 2 and 1 patients underwent pulmonary veins isolation (PVI) (n=20, 17, 13 and 18 respectively), and no CKD patients underwent PVI (n=20). After the PVI, a left ventricular voltage map was constructed, and zones without voltage were compared between groups.</p>

                  <p>The AF ablation procedure has been described in detail previously [21]. All patients underwent complete PVI using a three-dimensional mapping system (EnSite Velocity; St. Jude Medical) without additional ablation lesion sets or lines. Patients still in AF at the end of the procedure were converted to sinus rhythm by cardioversion. The left ventricular voltage map was constructed also using the EnSite Velocity system, and the left ventricle was divided into 4 parts: septum, lateral free wall, anterior wall, and posterior wall, aiming to find areas without voltage (zones of scar).</p>

                  <p>The results are expressed as a mean and standard deviation for normally distributed data and as median with interquartile range otherwise. Comparisons between two-paired values were performed with the paired t-test in cases of a Gaussian distribution and by the Wilcoxon test otherwise. For normality of distribution, D&rsquo;Agostino-Pearson test was used. Comparisons between more than two-paired values were made by repeated-measures analysis of variance or by Kruskal&ndash;Wallis analysis of variance as appropriate, complemented by the post-hoc Tukey test. Categorical variables were compared with Chi-square test. A two-tailed P-value&lt;0.05 was used as a criterion for statistical significance. All statistical analyses were performed using the program Graphpad Prism v 7.0 (Graphpad Software, La Jolla, CA, USA).</p>

                  <p>The general features and the baseline echocardiographic parameters of the five groups of patients are listed in Table 1. Comparing patients with CKD <em>vs</em>. patients without CKD, we noted that the number of the absence of voltage is higher in the CKD stages 4 group, making this the most susceptible group to present ventricular arrhythmias, as shown in Table 2.</p>


<p><strong>Table 1. General features of patients at baseline</strong></p>

<table align="left" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:262px;height:31px;"><p align="center">Parameters</p></td>
      <td style="width:161px;height:31px;"><p align="center">No CKD</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 1</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 2</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 3</p></td>
      <td style="width:151px;height:31px;"><p align="center">CKD stage 4</p></td>
      <td style="width:113px;height:31px;"><p align="center">Overall</p>

      <p align="center">P value</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>N</p></td>
      <td style="width:161px;height:31px;"><p align="center">20</p></td>
      <td style="width:132px;height:31px;"><p align="center">18</p></td>
      <td style="width:132px;height:31px;"><p align="center">13</p></td>
      <td style="width:132px;height:31px;"><p align="center">17</p></td>
      <td style="width:151px;height:31px;"><p align="center">20</p></td>
      <td style="width:113px;height:31px;"><p align="center">---</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Age, years</p></td>
      <td style="width:161px;height:31px;"><p align="center">61.4&plusmn;17.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">59.2&plusmn;20.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">58.0&plusmn;17.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.8&plusmn;12.2</p></td>
      <td style="width:151px;height:31px;"><p align="center">65.6&plusmn;14.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7051</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Body mass index, kg/m<sup>2</sup></p></td>
      <td style="width:161px;height:31px;"><p align="center">28.7&plusmn;6.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">30.0&plusmn;8.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">29.3&plusmn;10.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">28.8&plusmn;6.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">26.9&plusmn;4.8</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7279</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Male sex (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">12 (60%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (72%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">8 (62%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">10 (59%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7622</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>White ethnicity (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">17 (85%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (61%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (69%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (65%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.5111</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Paroxysmal AF</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Hypertension</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Type 2 Diabetes <em>Mellitus</em></p></td>
      <td style="width:161px;height:31px;"><p align="center">8 (40%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">7 (39%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">5 (38%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">6 (35%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">10 (50%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.9136</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Creatinine, mg/dL</p></td>
      <td style="width:161px;height:31px;"><p align="center">0.80&plusmn;0.25</p></td>
      <td style="width:132px;height:31px;"><p align="center">0.90&plusmn;0.54</p></td>
      <td style="width:132px;height:31px;"><p align="center">1.23&plusmn;0.33</p></td>
      <td style="width:132px;height:31px;"><p align="center">1.45&plusmn;0.38</p></td>
      <td style="width:151px;height:31px;"><p align="center">2.80&plusmn;0.31</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>eGFR, mL/min/1.73 m<sup>2</sup></p></td>
      <td style="width:161px;height:31px;"><p align="center">96.7&plusmn;18.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">93.6&plusmn;28.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">64.7&plusmn;14.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">44.6&plusmn;13.0</p></td>
      <td style="width:151px;height:31px;"><p align="center">22.3&plusmn;4.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Albumin:creatinine ratio, mg/g</p></td>
      <td style="width:161px;height:31px;"><p align="center">16.4&plusmn;3.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">66.0&plusmn;15.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">77.5&plusmn;20.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">74.2&plusmn;18.8</p></td>
      <td style="width:151px;height:31px;"><p align="center">83.3&plusmn;12.5</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Antihypertensive</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> ACE-inhibitors/ARB</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Diuretics</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> DHP Ca<sup>++</sup> channel blockers</p></td>
      <td style="width:161px;height:31px;"><p align="center">8 (40%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">8 (44%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">5 (38%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (53%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">16 (80%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0660</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> &beta;-blockers</p></td>
      <td style="width:161px;height:31px;"><p align="center">13 (65%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">12 (67%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (69%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (65%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.9583</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Mean 24-hour &nbsp;ABPM, mmHg</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Systolic</p></td>
      <td style="width:161px;height:31px;"><p align="center">121.4&plusmn;7.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">120.0&plusmn;8.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">123.3&plusmn;3.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">125.2&plusmn;4.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">124.5&plusmn;12.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0665</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Diastolic</p></td>
      <td style="width:161px;height:31px;"><p align="center">72.3&plusmn;4.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">70.8&plusmn;6.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">73.3&plusmn;5.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">72.8&plusmn;5.4</p></td>
      <td style="width:151px;height:31px;"><p align="center">75.4&plusmn;4.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.1034</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Echocardiographic parameters</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:35px;"><p> Indexed left atrial volume (mL/m<sup>2</sup>)</p></td>
      <td style="width:161px;height:35px;"><p align="center">30.4&plusmn;4.1</p></td>
      <td style="width:132px;height:35px;"><p align="center">29.8&plusmn;5.0</p></td>
      <td style="width:132px;height:35px;"><p align="center">31.3&plusmn;3.8</p></td>
      <td style="width:132px;height:35px;"><p align="center">31.9&plusmn;4.2</p></td>
      <td style="width:151px;height:35px;"><p align="center">32.2&plusmn;3.9</p></td>
      <td style="width:113px;height:35px;"><p align="center">0.3833</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:25px;"><p> IST (mm)</p></td>
      <td style="width:161px;height:25px;"><p align="center">9.0&plusmn;1.8</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.2&plusmn;1.3</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.6&plusmn;0.9</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.2&plusmn;1.6</p></td>
      <td style="width:151px;height:25px;"><p align="center">9.5&plusmn;1.3</p></td>
      <td style="width:113px;height:25px;"><p align="center">0.7427</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVPWT (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">10.5&plusmn;0.9</p></td>
      <td style="width:132px;height:31px;"><p align="center">11.0&plusmn;1.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">10.6&plusmn;1.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">11.1&plusmn;1.4</p></td>
      <td style="width:151px;height:31px;"><p align="center">10.8&plusmn;1.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.5047</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVEF, Simpson (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">62.4&plusmn;8.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.0&plusmn;6.7</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.5&plusmn;9.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">62.3&plusmn;8.8</p></td>
      <td style="width:151px;height:31px;"><p align="center">64.1&plusmn;9.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.6064</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVEDD (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">47.0&plusmn;6.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">48.0&plusmn;4.4</p></td>
      <td style="width:132px;height:31px;"><p align="center">45.6&plusmn;5.4</p></td>
      <td style="width:132px;height:31px;"><p align="center">49.4&plusmn;3.3</p></td>
      <td style="width:151px;height:31px;"><p align="center">54.4&plusmn;5.8</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVESD (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">32.7&plusmn;8.6</p></td>
      <td style="width:132px;height:31px;"><p align="center">33.1&plusmn;6.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">32.9&plusmn;7.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">35.5&plusmn;6.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">38.8&plusmn;7.5</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0530</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:25px;"><p> LV mass index (g/m<sup>2</sup>)</p></td>
      <td style="width:161px;height:25px;"><p align="center">88.7&plusmn;19.4</p></td>
      <td style="width:132px;height:25px;"><p align="center">93.0&plusmn;14.5</p></td>
      <td style="width:132px;height:25px;"><p align="center">95.4&plusmn;13.8</p></td>
      <td style="width:132px;height:25px;"><p align="center">108.2&plusmn;10.6</p></td>
      <td style="width:151px;height:25px;"><p align="center">126.0&plusmn;16.3</p></td>
      <td style="width:113px;height:25px;"><p align="center">&lt;0.0001</p></td>
    </tr>
  </tbody>
</table>
<p>Values are expressed as mean &plusmn; SD; ABPM, ambulatory blood pressure measurements; ACE, angiotensin-converting enzyme; AF, atrial fibrillation; ARB, angiotensin receptor blocker; CKD, chronic kidney disease; DHP, dihydropyridine; eGFR, estimated glomerular filtration rate; &nbsp;LV, left ventricular; LVEDD, left ventricular &nbsp;end-diastolic diameter; LVEF, left ventricular ejection fraction; LVESD, left ventricular &nbsp;end-systolic diameter; LVPWT, left ventricular posterior wall thickness.</p>


<p><strong>Table 2. Absence of voltage in the left ventricle in patients with and without CKD.</strong></p>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:234px;"><p align="center"><strong>Absence of voltage</strong></p></td>
      <td style="width:113px;"><p align="center">No CKD</p></td>
      <td style="width:135px;"><p align="center">CKD stage 1</p></td>
      <td style="width:119px;"><p align="center">CKD stage 2</p></td>
      <td style="width:124px;"><p align="center">CKD stage 3</p></td>
      <td style="width:113px;"><p align="center">CKD stage 4</p></td>
      <td style="width:104px;"><p align="center">&nbsp;P value</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular septum</p></td>
      <td style="width:113px;"><p align="center">10%</p></td>
      <td style="width:135px;"><p align="center">17%</p></td>
      <td style="width:119px;"><p align="center">54%</p></td>
      <td style="width:124px;"><p align="center">53%</p></td>
      <td style="width:113px;"><p align="center">80%</p></td>
      <td style="width:104px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular lateral free wall</p></td>
      <td style="width:113px;"><p align="center">0%</p></td>
      <td style="width:135px;"><p align="center">6%</p></td>
      <td style="width:119px;"><p align="center">38%</p></td>
      <td style="width:124px;"><p align="center">42%</p></td>
      <td style="width:113px;"><p align="center">60%</p></td>
      <td style="width:104px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular anterior wall</p></td>
      <td style="width:113px;"><p align="center">20%</p></td>
      <td style="width:135px;"><p align="center">23%</p></td>
      <td style="width:119px;"><p align="center">46%</p></td>
      <td style="width:124px;"><p align="center">47%</p></td>
      <td style="width:113px;"><p align="center">70%</p></td>
      <td style="width:104px;"><p align="center">0.0038</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular posterior wall</p></td>
      <td style="width:113px;"><p align="center">15%</p></td>
      <td style="width:135px;"><p align="center">17%</p></td>
      <td style="width:119px;"><p align="center">31%</p></td>
      <td style="width:124px;"><p align="center">35%</p></td>
      <td style="width:113px;"><p align="center">75%</p></td>
      <td style="width:104px;"><p align="center">0.0004</p></td>
    </tr>
    <tr>
      <td colspan="7" style="width:943px;"><p>CKD, chronic kidney disease.</p></td>
    </tr>
  </tbody>
</table>

                  <p>The presence of AF hampers measurement of LV ejection fraction because of tachycardia and beat-to-beat (i.e., R-to-R) LV filling variability. Our measurements could have been less precise because we did not use a three-dimensional single-beat ultrasound system. The inability to perform cardiac gadolinium-enhanced magnetic resonance image in patients with CKD, due to the nephrotoxicity of a component of this substance is an important limitation because it could confirm exactly the zones of scar detected by the voltage our map.</p>

                  <p>Patients with CKD stage 4 seem to be more susceptible to ventricular arrhythmias than patients without CKD, due to a larger absence of voltage in the left ventricular walls of the first ones.&nbsp;</p>

<h2 id="jumpmenu2">Conflict of interest</h2>

                  <p>The authors report no relationships that could be construed as a conflict of interest.</p>

<h2 id="jumpmenu3">Acknowledgements</h2>
                  <p>We would like to thank Pacemed for the technical support.</p>

<h2 id="jumpmenu4">References</h2>
                  <ol>
                    <li>Kiuchi MG, Chen S, e Silva GR, Paz LMR, Souto GLL (2016) Register of arrhythmias in patients with pacemakers and mild to moderate chronic kidney disease (RYCKE): results from an observational cohort study. <em>Latin-American Journal of Pacemaker and Arrhythmia</em> 29:49-56.</li>
                    <li>Cerasola G, Nardi E, Mule G, Palermo A, Cusimano P, et al. (2010) Left ventricular mass in hypertensive patients with a mild-to-moderate reduction of renal function. <em>Nephrology (Carlton)</em> 15:203e210.</li>
                    <li>Levin A, Thompson CR, Ethier J, Carlisle EJ, Tobe S, et al. (1999) Left ventricular mass index increase in early renal disease: impact of the decline in hemoglobin. <em>Am J Kidney Dis</em> 34:125e134.</li>
                    <li>Paoletti E, Bellino D, Cassottana P, Rolla D, Cannella G (2005) Left ventricular hypertrophy in nondiabetic predialysis CKD. <em>Am J Kidney Dis </em>46: 320-327. <a href="http://www.ncbi.nlm.nih.gov/pubmed/16112052">[crossref]</a></li>
                    <li>Moran A, Katz R, Jenny NS, Astor B, Bluemke DA, Lima JA, et al. (2008) Left ventricular hypertrophy n mild and moderate reduction in kidney function determined using cardiac magnetic resonance imaging and Cystatin C: the multi-ethnic study of atherosclerosis (MESA). <em>Am J Kidney Dis</em> 52: 839-848.</li>
                    <li>Mark PB, Johnston N, Groenning BA, Foster JE, Blyth KG, et al. (2006) Redefinition of uremic cardiomyopathy by contrast-enhanced cardiac magnetic resonance imaging. <em>Kidney Int</em> 69:1839-1845.</li>
                    <li>Cioffi G, Tarantini L, Frizzi R, Stefenelli C, Russo TE, et al. (2011) Chronic kidney disease elicits an excessive increase in left ventricular mass growth in patients at increased risk for cardiovascular events <em>J Hypertens</em> 565-573, 2011</li>
                    <li>Schroeder AP, Kristensen BO, Nielsen CB, Pedersen EB (1997) Heart function in patients with chronic glomerulonephritis and mildly to moderately impaired renal function. An echocardiographic study. <em>Blood Press</em> 6:286-293.</li>
                    <li>Hunter JJ, Chien KR (1999) Signaling pathways for cardiac hypertrophy and failure. <em>N Engl J Med </em>341: 1276-1283. <a href="http://www.ncbi.nlm.nih.gov/pubmed/10528039">[crossref]</a></li>
                    <li>Amann K, Kronenberg G, Gehlen F, Wessels S, Orth S, et al. (1998) Cardiac remodelling in experimental renal failure - an immunohistochemical study. <em>Nephrol Dial Transplant</em> 13:1958-1966.</li>
                    <li>Mall G, Huther W, Schneider J, Lundin P, Ritz E (1990) Diffuse intermyocardiocytic fibrosis in uraemic patients. <em>Nephrol Dial Transplant </em>5: 39-44. <a href="http://www.ncbi.nlm.nih.gov/pubmed/2109283">[crossref]</a></li>
                    <li>Haider AW, Larson MG, Benjamin EJ, Levy D (1998) Increased left ventricular mass and hypertrophy are associated with increased risk for sudden death. <em>J Am Coll Cardiol </em>32: 1454-1459. <a href="http://www.ncbi.nlm.nih.gov/pubmed/9809962">[crossref]</a></li>
                    <li>Reinier K, Dervan C, Singh T, Uy-Evanado A, Lai S, et al. (2011) Increased left ventricular mass and decreased left ventricular systolic function have independent pathways to ventricular arrhythmogenesis in coronary artery disease. <em>Heart Rhythm</em> 8: 1177-1182.</li>
                    <li>Yan AT, Shayne AJ, Brown KA, Gupta SN, Chan CW, et al. (2006) Characterization of the peri-infarct zone by contrast enhanced cardiac magnetic resonance imaging is a powerful predictor of postmyocardial infarction mortality. <em>Circulation</em> 114:32-39.</li>
                    <li>Schmieder RE1, Hilgers KF, Schlaich MP, Schmidt BM (2007) Renin-angiotensin system and cardiovascular risk. <em>Lancet </em>369: 1208-1219. <a href="http://www.ncbi.nlm.nih.gov/pubmed/17416265">[crossref]</a></li>
                    <li>Roes SD, Borleffs CJ, van der Geest RJ, Westenberg JJ, Marsan NA, et al. (2009) &nbsp;Infarct tissue heterogeneity assessed with contrast-enhanced MRI predicts spontaneous ventricular arrhythmia in patients with ischemic cardiomyopathy and implantable cardioverterdefibrillator. Circ Cardiovasc Imaging 2:183-190.</li>
                    <li>Waldo AL, Plumb VJ, Arciniegas JG, MacLean WA, Cooper TB, Priest MF, et al. (1983) Transient entrainment and interruption of the atrioventricular bypass pathway type of paroxysmal atrial tachycardia. A model for understanding and identifying reentrant arrhythmias. <em>Circulation</em> 67:73-83.</li>
                    <li>Schmidt A, Azevedo CF, Cheng A, Gupta SN, Bluemke DA, et al. (2007) Infarct tissue heterogeneity by magnetic resonance imaging identifies enhanced cardiac arrhythmia susceptibility in patients with left ventricular dysfunction. <em>Circulation</em> 115: 2006-2014.</li>
                    <li>Morales MA, Gremigni C, Dattolo P, Piacenti M, Cerrai T, et al. (1998) Signal-averaged ECG abnormalities in haemodialysis patients. Role of dialysis. <em>Nephrol Dial Transplant </em>13: 668-673. <a href="http://www.ncbi.nlm.nih.gov/pubmed/9550645">[crossref]</a></li>
                    <li>Levey AS, Stevens LA, Schmid CH, Zhang YL, Castro AF III, et al. (2009) CKD-EPI (Chronic Kidney Disease Epidemiology Collaboration): A new equation to estimate glomerular filtration rate. <em>Ann Intern Med </em>150: 604&ndash;612.</li>
                    <li>Pokushalov E, Romanov A, Corbucci G, Artyomenko S, Turov A, et al. (2011) Ablation of paroxysmal and persistent atrial fibrillation: 1-year follow-up through continuous subcutaneous monitoring. <em>J Cardiovasc Electrophysiol</em> 22: 369&ndash;75.</li>
                  </ol>





</div>

         
          </div>
          <div id="Article_Info" class="content">
            <h4 class="mb10"><span class="">Editorial Information</span></h4>
            <h4 class="mb5"><span class="black-text">Editor-in-Chief</span></h4>
   
<p>Massimo Fioranelli<br>
Guglielmo Marconi University</p>

<h4>Article Type</h4>
<p>Short Communication</p>

<h4>Publication history</h4>
<p>Received date: March 27, 2017<br>
Accepted date: April 27, 2017<br>
Published date: April 30, 2017</p>

<h4>Copyright</h4>
<p>&copy; 2017 Márcio Galindo Kiuch. This is an open-access article distributed under the terms of the Creative Commons Attribution License, which permits unrestricted use, distribution, and reproduction in any medium, provided the original author and source are credited.</p>

<h4>Citation</h4>
<p>Kiuchi MG, Chen S (2016) Electrical left ventricular mapping in patients without CKD and with different stages of CKD. J Integr Cardiol 3: DOI: 10.15761/JIC.1000218</p>

          </div>
          <div id="Author_Info" class="content">
            <h4 class="mb10"><span class="">Corresponding author</span></h4>
            
            
            <h4 class="mb10"><span class="black-text">Márcio Galindo Kiuchi</span></h4>
            <p>Rua Cel. Moreira César, 138 - Centro, São Gonçalo – Rio de Janeiro – Brazil. ZIP-CODE: 24440-400.Tel and Fax: +55 (21) 26047744</p>





          </div>
          <div id="Figures_Data" class="content">
             
             
<p><strong>Table 1. General features of patients at baseline</strong></p>

<table align="left" border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:262px;height:31px;"><p align="center">Parameters</p></td>
      <td style="width:161px;height:31px;"><p align="center">No CKD</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 1</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 2</p></td>
      <td style="width:132px;height:31px;"><p align="center">CKD stage 3</p></td>
      <td style="width:151px;height:31px;"><p align="center">CKD stage 4</p></td>
      <td style="width:113px;height:31px;"><p align="center">Overall</p>

      <p align="center">P value</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>N</p></td>
      <td style="width:161px;height:31px;"><p align="center">20</p></td>
      <td style="width:132px;height:31px;"><p align="center">18</p></td>
      <td style="width:132px;height:31px;"><p align="center">13</p></td>
      <td style="width:132px;height:31px;"><p align="center">17</p></td>
      <td style="width:151px;height:31px;"><p align="center">20</p></td>
      <td style="width:113px;height:31px;"><p align="center">---</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Age, years</p></td>
      <td style="width:161px;height:31px;"><p align="center">61.4&plusmn;17.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">59.2&plusmn;20.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">58.0&plusmn;17.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.8&plusmn;12.2</p></td>
      <td style="width:151px;height:31px;"><p align="center">65.6&plusmn;14.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7051</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Body mass index, kg/m<sup>2</sup></p></td>
      <td style="width:161px;height:31px;"><p align="center">28.7&plusmn;6.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">30.0&plusmn;8.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">29.3&plusmn;10.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">28.8&plusmn;6.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">26.9&plusmn;4.8</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7279</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Male sex (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">12 (60%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (72%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">8 (62%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">10 (59%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.7622</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>White ethnicity (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">17 (85%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (61%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (69%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (65%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.5111</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Paroxysmal AF</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Hypertension</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Type 2 Diabetes <em>Mellitus</em></p></td>
      <td style="width:161px;height:31px;"><p align="center">8 (40%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">7 (39%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">5 (38%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">6 (35%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">10 (50%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.9136</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Creatinine, mg/dL</p></td>
      <td style="width:161px;height:31px;"><p align="center">0.80&plusmn;0.25</p></td>
      <td style="width:132px;height:31px;"><p align="center">0.90&plusmn;0.54</p></td>
      <td style="width:132px;height:31px;"><p align="center">1.23&plusmn;0.33</p></td>
      <td style="width:132px;height:31px;"><p align="center">1.45&plusmn;0.38</p></td>
      <td style="width:151px;height:31px;"><p align="center">2.80&plusmn;0.31</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>eGFR, mL/min/1.73 m<sup>2</sup></p></td>
      <td style="width:161px;height:31px;"><p align="center">96.7&plusmn;18.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">93.6&plusmn;28.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">64.7&plusmn;14.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">44.6&plusmn;13.0</p></td>
      <td style="width:151px;height:31px;"><p align="center">22.3&plusmn;4.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Albumin:creatinine ratio, mg/g</p></td>
      <td style="width:161px;height:31px;"><p align="center">16.4&plusmn;3.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">66.0&plusmn;15.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">77.5&plusmn;20.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">74.2&plusmn;18.8</p></td>
      <td style="width:151px;height:31px;"><p align="center">83.3&plusmn;12.5</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Antihypertensive</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> ACE-inhibitors/ARB</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Diuretics</p></td>
      <td style="width:161px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">18 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">13 (100%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">17 (100%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">20 (100%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">1.0000</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> DHP Ca<sup>++</sup> channel blockers</p></td>
      <td style="width:161px;height:31px;"><p align="center">8 (40%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">8 (44%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">5 (38%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (53%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">16 (80%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0660</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> &beta;-blockers</p></td>
      <td style="width:161px;height:31px;"><p align="center">13 (65%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">12 (67%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">9 (69%)</p></td>
      <td style="width:132px;height:31px;"><p align="center">11 (65%)</p></td>
      <td style="width:151px;height:31px;"><p align="center">15 (75%)</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.9583</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Mean 24-hour &nbsp;ABPM, mmHg</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Systolic</p></td>
      <td style="width:161px;height:31px;"><p align="center">121.4&plusmn;7.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">120.0&plusmn;8.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">123.3&plusmn;3.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">125.2&plusmn;4.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">124.5&plusmn;12.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0665</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> Diastolic</p></td>
      <td style="width:161px;height:31px;"><p align="center">72.3&plusmn;4.8</p></td>
      <td style="width:132px;height:31px;"><p align="center">70.8&plusmn;6.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">73.3&plusmn;5.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">72.8&plusmn;5.4</p></td>
      <td style="width:151px;height:31px;"><p align="center">75.4&plusmn;4.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.1034</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p>Echocardiographic parameters</p></td>
      <td colspan="6" style="width:822px;height:31px;"><p align="center">&nbsp;</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:35px;"><p> Indexed left atrial volume (mL/m<sup>2</sup>)</p></td>
      <td style="width:161px;height:35px;"><p align="center">30.4&plusmn;4.1</p></td>
      <td style="width:132px;height:35px;"><p align="center">29.8&plusmn;5.0</p></td>
      <td style="width:132px;height:35px;"><p align="center">31.3&plusmn;3.8</p></td>
      <td style="width:132px;height:35px;"><p align="center">31.9&plusmn;4.2</p></td>
      <td style="width:151px;height:35px;"><p align="center">32.2&plusmn;3.9</p></td>
      <td style="width:113px;height:35px;"><p align="center">0.3833</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:25px;"><p> IST (mm)</p></td>
      <td style="width:161px;height:25px;"><p align="center">9.0&plusmn;1.8</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.2&plusmn;1.3</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.6&plusmn;0.9</p></td>
      <td style="width:132px;height:25px;"><p align="center">9.2&plusmn;1.6</p></td>
      <td style="width:151px;height:25px;"><p align="center">9.5&plusmn;1.3</p></td>
      <td style="width:113px;height:25px;"><p align="center">0.7427</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVPWT (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">10.5&plusmn;0.9</p></td>
      <td style="width:132px;height:31px;"><p align="center">11.0&plusmn;1.2</p></td>
      <td style="width:132px;height:31px;"><p align="center">10.6&plusmn;1.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">11.1&plusmn;1.4</p></td>
      <td style="width:151px;height:31px;"><p align="center">10.8&plusmn;1.2</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.5047</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVEF, Simpson (%)</p></td>
      <td style="width:161px;height:31px;"><p align="center">62.4&plusmn;8.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.0&plusmn;6.7</p></td>
      <td style="width:132px;height:31px;"><p align="center">60.5&plusmn;9.1</p></td>
      <td style="width:132px;height:31px;"><p align="center">62.3&plusmn;8.8</p></td>
      <td style="width:151px;height:31px;"><p align="center">64.1&plusmn;9.0</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.6064</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVEDD (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">47.0&plusmn;6.3</p></td>
      <td style="width:132px;height:31px;"><p align="center">48.0&plusmn;4.4</p></td>
      <td style="width:132px;height:31px;"><p align="center">45.6&plusmn;5.4</p></td>
      <td style="width:132px;height:31px;"><p align="center">49.4&plusmn;3.3</p></td>
      <td style="width:151px;height:31px;"><p align="center">54.4&plusmn;5.8</p></td>
      <td style="width:113px;height:31px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:31px;"><p> LVESD (mm)</p></td>
      <td style="width:161px;height:31px;"><p align="center">32.7&plusmn;8.6</p></td>
      <td style="width:132px;height:31px;"><p align="center">33.1&plusmn;6.5</p></td>
      <td style="width:132px;height:31px;"><p align="center">32.9&plusmn;7.0</p></td>
      <td style="width:132px;height:31px;"><p align="center">35.5&plusmn;6.1</p></td>
      <td style="width:151px;height:31px;"><p align="center">38.8&plusmn;7.5</p></td>
      <td style="width:113px;height:31px;"><p align="center">0.0530</p></td>
    </tr>
    <tr>
      <td style="width:262px;height:25px;"><p> LV mass index (g/m<sup>2</sup>)</p></td>
      <td style="width:161px;height:25px;"><p align="center">88.7&plusmn;19.4</p></td>
      <td style="width:132px;height:25px;"><p align="center">93.0&plusmn;14.5</p></td>
      <td style="width:132px;height:25px;"><p align="center">95.4&plusmn;13.8</p></td>
      <td style="width:132px;height:25px;"><p align="center">108.2&plusmn;10.6</p></td>
      <td style="width:151px;height:25px;"><p align="center">126.0&plusmn;16.3</p></td>
      <td style="width:113px;height:25px;"><p align="center">&lt;0.0001</p></td>
    </tr>
  </tbody>
</table>
<p>Values are expressed as mean &plusmn; SD; ABPM, ambulatory blood pressure measurements; ACE, angiotensin-converting enzyme; AF, atrial fibrillation; ARB, angiotensin receptor blocker; CKD, chronic kidney disease; DHP, dihydropyridine; eGFR, estimated glomerular filtration rate; &nbsp;LV, left ventricular; LVEDD, left ventricular &nbsp;end-diastolic diameter; LVEF, left ventricular ejection fraction; LVESD, left ventricular &nbsp;end-systolic diameter; LVPWT, left ventricular posterior wall thickness.</p>


<p><strong>Table 2. Absence of voltage in the left ventricle in patients with and without CKD.</strong></p>
<table border="1" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td style="width:234px;"><p align="center"><strong>Absence of voltage</strong></p></td>
      <td style="width:113px;"><p align="center">No CKD</p></td>
      <td style="width:135px;"><p align="center">CKD stage 1</p></td>
      <td style="width:119px;"><p align="center">CKD stage 2</p></td>
      <td style="width:124px;"><p align="center">CKD stage 3</p></td>
      <td style="width:113px;"><p align="center">CKD stage 4</p></td>
      <td style="width:104px;"><p align="center">&nbsp;P value</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular septum</p></td>
      <td style="width:113px;"><p align="center">10%</p></td>
      <td style="width:135px;"><p align="center">17%</p></td>
      <td style="width:119px;"><p align="center">54%</p></td>
      <td style="width:124px;"><p align="center">53%</p></td>
      <td style="width:113px;"><p align="center">80%</p></td>
      <td style="width:104px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular lateral free wall</p></td>
      <td style="width:113px;"><p align="center">0%</p></td>
      <td style="width:135px;"><p align="center">6%</p></td>
      <td style="width:119px;"><p align="center">38%</p></td>
      <td style="width:124px;"><p align="center">42%</p></td>
      <td style="width:113px;"><p align="center">60%</p></td>
      <td style="width:104px;"><p align="center">&lt;0.0001</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular anterior wall</p></td>
      <td style="width:113px;"><p align="center">20%</p></td>
      <td style="width:135px;"><p align="center">23%</p></td>
      <td style="width:119px;"><p align="center">46%</p></td>
      <td style="width:124px;"><p align="center">47%</p></td>
      <td style="width:113px;"><p align="center">70%</p></td>
      <td style="width:104px;"><p align="center">0.0038</p></td>
    </tr>
    <tr>
      <td style="width:234px;"><p>Left ventricular posterior wall</p></td>
      <td style="width:113px;"><p align="center">15%</p></td>
      <td style="width:135px;"><p align="center">17%</p></td>
      <td style="width:119px;"><p align="center">31%</p></td>
      <td style="width:124px;"><p align="center">35%</p></td>
      <td style="width:113px;"><p align="center">75%</p></td>
      <td style="width:104px;"><p align="center">0.0004</p></td>
    </tr>
    <tr>
      <td colspan="7" style="width:943px;"><p>CKD, chronic kidney disease.</p></td>
    </tr>
  </tbody>
</table>





  
          
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="large-4 columns">
    <div class="inner-right">
      <div class="row">
        <div class="small-12 columns">
          <h4 class="left mr20 mt5">Articles</h4>
          <a href="pdf/JIC-3-218.pdf" target="_blank" class="left mr5"><img src="img/icon-pdf.png" alt=""></a><a href="#" class="left mr5"><img src="img/icon-xml.png" alt=""></a></div>
      </div>
      <hr/>
           <h4><a href="Journal-of-Integrative-Cardiology-JIC.php">Journal Home</a></h4>

      <hr/>
      <div id="sticky-anchor"></div>
      <div id="sticky">
        <h2>Jump to</h2>
        <hr/>
        <div class="row collapse">
          <div class="large-6 columns">
            <div class="right-set">
              <ul class="main-bar-list">
                <li><a href="#Article" class="active">Article</a></li>
                <li><a href="#Article_Info" >Article Info</a></li>
                <li><a href="#Author_Info" >Author Info</a></li>
                <li><a href="#Figures_Data" >Figures & Data</a></li>
              </ul>
            </div>
          </div>
          <div class="large-6 columns">
            <ul class="right-bar-list">
        		<li><a href="#jumpmenu1">Letter to Editor</a></li>
            <li><a href="#jumpmenu2">Conflict of interest</a></li>
            <li><a href="#jumpmenu3">Acknowledgements</a></li>
            <li><a href="#jumpmenu4">References</a></li>
            
            





            
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns"><p class="text-right white-text">© 2016 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>

	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script><script>
      $(document).foundation();
    </script>
<!--
<script>
	$('a[href="#"]').click(function(event){

    event.preventDefault();

});
	</script> -->
</body>
</html>
