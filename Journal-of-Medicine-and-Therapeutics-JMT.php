<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Journal of Medicine and Therapeutics (JMT)</title>
<meta name="description" content="" />
<meta name="keywords" content="scientific journals, medical journals, journals" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Journal of Medicine and Therapeutics (JMT)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes"    class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges </a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Journal of Medicine and Therapeutics (JMT)</h2>
                <h4>Online ISSN: 2399-9799 </h4>
                <!--<h2 class="mb5"><a href="#Editor-in-Chief">Ciro Rinaldi</a><span class="f14"> (Editor-in-Chief)</span> </h2>  -->
                <!-- <span class="black-text"><i class="fa fa-university"></i>    University of Lincoln<br><br></span> -->
                <!--United Lincolnshire Hospital NHS Trust  -->
                <hr/>
                <p><a href="img/Cover-JMT.jpg" target="_blank"><img src="img/Cover-JMT.jpg" alt=""  align="right"  class="pl20 pb10"  width="20%"/> </a> Journal of Medicine and Therapeutics&nbsp;</em>is an international, peer-reviewed, open access journal that focuses on publishing novel topics in the field of medical sciences. Areas covered by Journal of Medicine and Therapeutics includes Clinical Medicine, Emergency Care, Family Practice, Hypertension, Pulmonary Diseases, Cardiac Diseases, Hepatology, Trauma, Diabetes, Rheumatic Diseases, HIV, Tumours, Critical Care, Microbial Pathology, Preventive Care, Thrombophilia, Body Mass Index, Asthma, Neurology, Endoscopy, Infectious Diseases, Gastrointestinal Cancer, Gastrointestinal Surgery, Immunology on bimonthly basis.</p>
                <p>Manuscripts may take the form of original empirical research, critical reviews of the literature, brief commentaries, meeting reports, case reports, innovations in clinical practice, letters and drug clinical trial studies.</p>
                <p>JMT welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.jmt@oatext.com">editor.jmt@oatext.com</a></p>
                <p>Please, follow the <a href="http://oatext.com/GuidelinesforAuthors.php">Instructions for Authors</a>. In the cover letter add the name and e-mail address of 5 proposed reviewers (we can choose them or not).</p>
                <p>Copyright is retained by the authors and articles can be freely used and distributed by others. Articles are distributed under the terms of the Creative Commons Attribution License (<a href="http://creativecommons.org/licenses/by/4.0/">http://creativecommons.org/licenses/by/4.0/</a>), which permits unrestricted use, distribution, and reproduction in any medium, provided the original work, first published by JMT, is properly cited.</p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Editor in chief</h2>
              <hr/>
              <p class="text-justify"> <img src="" class="profile-pic" align="right"> </p>
            </div>
            <div id="Editorial_Board" class="content">
              <div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10">Regional Editor in Chief</h2>
                </div>
              </div>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Jinyong Peng </h4>
                  <p>Professor<br>
                    College of Pharmacy<br>
                    Dalian Medical University<br>
                    China</p>
                </div>
              </div>
              <div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10">Editorial Board</h2>
                </div>
              </div>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ahmet Eroglu</h4>
                  <p>Professor<br>
                    Department of Anesthesiology and Reanimation<br>
                    Karadeniz Technical University<br>
                    Turkey</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yaming Li</h4>
                  <p>Professor<br>
                    Department of Nuclear Medicine<br>
                    The First Hospital China Medical University<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Antonio G Tristano</h4>
                  <p>Rheumatologist<br>
                    Centro Medico Carpetana<br>
                    Spain</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Paula M. Mazur</h4>
                  <p>Associate Professor<br>
                    Pediatric Emergency Medicine<br>
                    University at Buffalo<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Andrew Chi Kin</h4>
                  <p>Professor<br>
                    Department of Psychiatry<br>
                    Perdana University<br>
                    Malaysia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Emad Tawfik Mahmoud Daif</h4>
                  <p>Professor<br>
                    Faculty of Oral & Dental Medicine<br>
                    Cairo University<br>
                    Egypt</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Paul J. Higgins</h4>
                  <p>Chair<br>
                    Department of Regenerative & Cancer Cell Biology<br>
                    Albany Medical College<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alpana Ray</h4>
                  <p>Research Professor<br>
                    Department of Veterinary Pathobiology<br>
                    University of Missouri<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Masoud Neghab</h4>
                  <p>professor<br>
                    School of Health<br>
                    Shiraz University of Medical Sciences<br>
                    Iran </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Prasanta K. Bag</h4>
                  <p>Professor<br>
                    Department of Biochemistry<br>
                    University of Calcutta<br>
                    India </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Seung-Yub Ku</h4>
                  <p>Associate Professor<br>
                    Department of Obstetrics and Gynecology<br>
                    Seoul National University<br>
                    Korea</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Valquiria Bueno</h4>
                  <p>Associate Professor<br>
                    Department of Microbiology, Immunology and Parasitology<br>
                    Federal University of São Paulo<br>
                    Brazil</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Juei-Tang Cheng</h4>
                  <p>Chair & Professor<br>
                    Institute of Medical Research<br>
                    Chang Jung Christian University<br>
                    Taiwan</p>
                </div>
                <div class="medium-4 columns">
                  <h4>A. C. Matin</h4>
                  <p>Professor<br>
                    Department of Microbiology & Immunology<br>
                    Stanford University<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Michel Goldberg</h4>
                  <p>Professeur Emerite<br>
                    Biomédicale des Saints Pères<br>
                    Université Paris Descartes<br>
                    France</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 1, Issue 2</h4>
              <hr/>
              <!------JMT.1000107 ------>
              <div class="article">
                <h4><a href="Minute-walk-test-and-idiopathic-pulmonary-fibrosis-distance-or-desaturation-A-prospective-observational-study.php"> 6 Minute walk test and idiopathic pulmonary fibrosis: distance or desaturation? A prospective observational study </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Neeraj Gupta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mukesh Goyal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Harish VK <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arjun Chandran <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amit Sharma </p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 27, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000107  x------>
              <!------JMT.1000108 ------>
              <div class="article">
                <h4><a href="Exploration-of-gene-variations-in-the-transcytosis-system-as-a-policy-proposal-for-the-personalized-therapy-in-type-2-diabetes-mellitus.php"> Exploration of gene variations in the transcytosis system as a policy proposal for the personalized therapy in type 2 diabetes mellitus </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Luis J. Flores-Alvarado <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>José R. Villafán-Bernal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carlos E. Cabrera-Pivaral <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carlos J. Castro-Juárez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sergio A. Ramirez-Garcia </p>
                <p class="mb5">Short Communication-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 31, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000108  x------>
              <!------JMT.1000109------>
              <div class="article">
                <h4><a href="Isolated-periaortitis-in-a-53-year-old-male.php"> Isolated periaortitis in a 53-year-old male </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rakul Nambiar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kripesh Kannoth <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dalus Dae </p>
                <p class="mb5">Case Report-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 22, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000109 x------>
              <!------JMT.1000110------>
              <div class="article">
                <h4><a href="Investigation-on-life-events-social-support-and-coping-style-of-patients-with-breast-cancer-in-Wuhan-city-China-a-case-control-study.php"> Investigation on life events, social support and coping style of patients with breast cancer in Wuhan city, China: a case-control study </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Qiong Dai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jianqiong Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bei Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jiangshan Cao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yukai Du </p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 28, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000110 x------>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 1, Issue 1</h4>
              <hr/>
              <!------JMT.1000101 ------>
              <div class="article">
                <h4><a href="Elevated-mitochondrial-and-heme-function-as-hallmarks-for-Non-small-cell-lung-cancers.php"> Elevated mitochondrial and heme function as hallmarks for Non-small cell lung cancers </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Chantal Vidal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sagar Sohoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Li Zhang </p>
                <p class="mb5">Short Communication-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 28, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000101  x------>
              <!------JMT.1000102------>
              <div class="article">
                <h4><a href="Estimating-the-disease-burden-of-MECFS-in-the-United-States-and-its-relation-to-research-funding.php"> Estimating the disease burden of ME/CFS in the United States and its relation to research funding </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Mary. E. Dimmock <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arthur A. Mirin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leonard A. Jason </p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000102 x------>
              <!------JMT.1000103------>
              <div class="article">
                <h4><a href="Water-activity-as-related-to-microorganisms-in-the-manufacturing-environment.php">Water activity as related to microorganisms in the manufacturing environment</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Clyde Schultz</p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 13, 2017</p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000103 x------>
              <!------JMT.1000104------>
              <div class="article">
                <h4><a href="Moving-targets-in-sodium-channel-blocker-development-the-case-of-raxatrigine-from-a-central-NaV1-3-blocker-via-a-peripheral-NaV1-7-blocker-to-a-less-selective-sodium-channel-blocker.php">Moving targets in sodium channel blocker development: the case of raxatrigine: from a central NaV1.3 blocker via a peripheral NaV1.7 blocker to a less selective sodium channel blocker</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan M. Keppel Hesselink</p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 19, 2017</p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000104 x------>
              <!------JMT.1000105------>
              <div class="article">
                <h4><a href="Dental-health-in-sickle-cell-disease.php">Dental health in sickle cell disease</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>S. M. AlDallal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>M. M. AlKathemi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>W. H. Haj <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>N. M. AlDallal</p>
                <p class="mb5">Mini Review-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017</p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000105 x------>
              <!------JMT.1000106------>
              <div class="article">
                <h4><a href="Superb-microvascular-imaging-Evaluating-the-intraplacental-blood-flow-formation.php">Superb microvascular imaging: Evaluating the intraplacental blood flow formation</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Julia E. Dobrokhotova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrei R. Zubarev <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sofya A. Zalesskaya</p>
                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 23, 2017</p>
                </em>
                <hr/>
              </div>
              <!------JMT.1000106 x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <!--<h4>Volume 1, Issue 4</h4> -->
              <hr/>
              <p>Will be updated soon.</p>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion" >
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                    <div class="hr-tabs">
                      <ul class="tabs" data-tab>
                        <li class="tab-title active"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>February 2017</span> <span class="mt3 grey-text">Issue 1</a></li>
                        <!--<li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2016</span> <span class="mt3 grey-text">Issue 2</a></a></li>
                      <li class="tab-title "><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2016</span> <span class="mt3 grey-text">Issue 1</a></li>-->
                      </ul>
                      <div class="tabs-content">
                        <div class="content active" id="panela">
                          <div class="row pt10">
                            <div class="small-12 columns">
                              <!------JMT.1000101 ------>
                              <div class="article">
                                <h4><a href="Elevated-mitochondrial-and-heme-function-as-hallmarks-for-Non-small-cell-lung-cancers.php"> Elevated mitochondrial and heme function as hallmarks for Non-small cell lung cancers </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Chantal Vidal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sagar Sohoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Li Zhang </p>
                                <p class="mb5">Short Communication-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 28, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000101  x------>
                              <!------JMT.1000102------>
                              <div class="article">
                                <h4><a href="Estimating-the-disease-burden-of-MECFS-in-the-United-States-and-its-relation-to-research-funding.php"> Estimating the disease burden of ME/CFS in the United States and its relation to research funding </a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Mary. E. Dimmock <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arthur A. Mirin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leonard A. Jason </p>
                                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000102 x------>
                              <!------JMT.1000103------>
                              <div class="article">
                                <h4><a href="Water-activity-as-related-to-microorganisms-in-the-manufacturing-environment.php">Water activity as related to microorganisms in the manufacturing environment</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Clyde Schultz</p>
                                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 13, 2017</p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000103 x------>
                              <!------JMT.1000104------>
                              <div class="article">
                                <h4><a href="Moving-targets-in-sodium-channel-blocker-development-the-case-of-raxatrigine-from-a-central-NaV1-3-blocker-via-a-peripheral-NaV1-7-blocker-to-a-less-selective-sodium-channel-blocker.php">Moving targets in sodium channel blocker development: the case of raxatrigine: from a central NaV1.3 blocker via a peripheral NaV1.7 blocker to a less selective sodium channel blocker</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jan M. Keppel Hesselink</p>
                                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 19, 2017</p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000104 x------>
                              <!------JMT.1000105------>
                              <div class="article">
                                <h4><a href="Dental-health-in-sickle-cell-disease.php">Dental health in sickle cell disease</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>S. M. AlDallal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>M. M. AlKathemi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>W. H. Haj <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>N. M. AlDallal</p>
                                <p class="mb5">Mini Review-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2017</p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000105 x------>
                              <!------JMT.1000106------>
                              <div class="article">
                                <h4><a href="Superb-microvascular-imaging-Evaluating-the-intraplacental-blood-flow-formation.php">Superb microvascular imaging: Evaluating the intraplacental blood flow formation</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Julia E. Dobrokhotova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andrei R. Zubarev <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sofya A. Zalesskaya</p>
                                <p class="mb5">Research Article-Journal of Medicine and Therapeutics (JMT)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 23, 2017</p>
                                </em>
                                <hr/>
                              </div>
                              <!------JMT.1000106 x------>
                            </div>
                          </div>
                        </div>
                        <div class="content" id="panelb">B</div>
                        <div class="content" id="panelc">C</div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>

            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr/>
              
              <table cellspacing="0" cellpadding="0" border="0" style="border: 0px;">
                <tr>
                  <td align="center" style="border: 0px;"><a href="http://www.icmje.org/journals-following-the-icmje-recommendations/" target="_blank"><img src="img/BRCP-icmje.png"></a></td>
                  <td align="center" style="border: 0px;"><a href="http://journalseeker.researchbib.com/view/issn/2399-9799" target="_blank"><img src="img/IFNM-Researcch-Bib.png"></a></td>
                  <td align="center" style="border: 0px;"></td>
                </tr>
              </table>

 </div>





            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p>JMT welcomes direct submissions from authors: Attach your word file with e-mail and send it to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>&nbsp;alternatively to:&nbsp;<a href="mailto:editor.jmt@oatext.com">editor.jmt@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
