<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Journal of Integrative Cardiology (JIC) - OATEXT</title>
<meta name="description" content="Journal of Integrative Cardiology (JIC) is an open access, peer-reviewed online journal interested in attracting high-quality original research and reviews that present or highlight significant advances involving cardiovascular system and related regulatory organ systems on oatext." />
<meta name="keywords" content="OA Text, OAT, Open Access Text, OAtext, OATEXT, oatext, oat, oatext,  open access text, oatext." />
<meta name="robots" content=" ALL, index, follow"/>
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For All" />
<meta name="language" content="English" />
<meta http-equiv="window-target" content="_top"/>
<meta http-equiv="pics-label" content="for all ages"/>
<meta name="rating" content="general"/>
<meta content="All, FOLLOW" name="GOOGLEBOTS"/>
<meta content="All, FOLLOW" name="YAHOOBOTS"/>
<meta content="All, FOLLOW" name="MSNBOTS"/>
<meta content="All, FOLLOW" name="BINGBOTS"/>
<meta content="all" name="Googlebot-Image"/>
<meta content="all" name="Slurp"/>
<meta content="all" name="Scooter"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Journal of Integrative Cardiology (JIC)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges</a></dd>
          </dl>
          <h4 class="mt10">In Collaboration With</h4>
          <a href="http://www.regeneragroup.it/" target="_blank"><img src="img/LOGO_REGENERA.jpg" class="mt5" alt="oatext" title="oatext"></a> <a href="http://www.global-antiaging-medicine.com/" target="_blank"><img src="img/gam.jpg" class="mt5" alt="oatext" title="oatext"></a> <a href="http://scienzedellavita.unimarconi.it/SDV/?page_id=5" target="_blank"><img src="img/css.jpg" class="mt5" alt="oatext" title="oatext"></a> <a href="http://www.artoi.it/en/" target="_blank"><img src="img/artoi.jpg" class="mt5" alt="oatext" title="oatext"></a> </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Journal of Integrative Cardiology (JIC)</h2>
                <h4>Online ISSN: 2058-3702</h4>
                <h2 class="mb5"><a href="#Editor-in-Chief">Massimo Fioranelli</a><span class="f14"> (Founding Editor in Chief)</span></h2>
                <span class="black-text"><i class="fa fa-university"></i>Guglielmo Marconi University</span>
                <div class="hide">
                  <h2 class="mb5"><a href="#Editor-in-Chief">Marcelo L. Larramendy</a> <span class="f14"> (Founding Editor in Chief)</span></h2>
                  <span class="black-text"><i class="fa fa-university"></i> The National Scientific and Technical Research Council (CONICET)</span></div>
                <hr/>
                <!--<a target="_blank" href="img/CRA.jpg" ><img width="24%" align="right" class="pl20 pb10" alt="" src="img/CRA.jpg"></a>
 -->
                <p class="text-justify"> <a target="_blank" href="img/Journal-of-Integrative-Cardiology.jpg"><img align="right" width="20%" alt="" class="pl20" src="img/Journal-of-Integrative-Cardiology.jpg"></a>Journal of Integrative Cardiology (JIC) is an open access, peer-reviewed online journal interested in attracting high-quality original research and reviews that present or highlight significant advances involving cardiovascular system and related regulatory organ systems. The Journal is concerned with basic, translational and clinical research, across different disciplines and areas, enhancing insight in understanding, prevention, diagnosis and treatment of cardiovascular diseases. Journal of Integrative Cardiology (JIC) welcomes submission of papers both at the molecular, subcellular, cellular, organ, and organism level, and of clinical proof-of-concept and translational studies. The main commitment of the Journal is to encourage research in non conventional therapies, like nutrition, low dose therapy, botanicals, metabolic cardiology, acupuncture, Psycho-Neuro-Endocrine-Immunology (PNEI), immunomodulation through the intervention on microbioma, complementary therapy of low grade chronic inflammation. </p>
                <p class="text-justify">The Psycho-Neuro-Endocrine-Immunology concepts result in a change of perspective, from a separatist point of view to an unifying one, relating to the interpretation of the biological functions of the body. A key point is the recognition of the importance of continuous cross-talk between cells, organs and systems in both physiological and pathological conditions based on the fine regulation of the levels of a large number of messenger molecules.</p>
                <p class="text-justify"> Interpreting the pathological phenomenon as an imbalance in intercellular signaling, the administration of low physiological doses of messenger molecules (which act as homeostatic modulating agents) can be considered an intriguing and innovative approach in order to restore the correct intracellular signaling and consequently to restore healthy conditions; these concepts are the milestones of Low Dose Medicine. </p>
                <p class="text-justify">Integrative cardiology emphasizes the mind-body connection, and the evidence that depression, anxiety, and stress are not only risk factors for the development of cardiovascular disease, but lead to adverse outcomes, including cardiac death and disease progression. Techniques that deals with depression, anxiety, and stress using stress management programs, relaxation therapy, and physical activity, are therapies that can be as effective as drugs in some patients. As prevention is the cornerstone of integrative medicine we  highlights powerful opportunities afforded by nutritional approaches, lifestyle changes, and supplements, combined with conservative use of medication. The importance of evaluation for inherited risk factors that go beyond traditional ones is a corner stone of integrated cardiology. 
                  Journal of Integrative Cardiology (JIC) provides an important forum for exchange of ideas, concepts and findings in any area of cardiovascular system and related regulatory organ systems in a vision in which many medical conditions like hypertension, coronary artery disease, congestive heart failure, arrhythmias, and cardiac surgery can benefit from an olistic approach. </p>
                <p class="text-justify">Manuscripts may be submitted as original articles, rapid and short communications, or reviews, mini-review, case reports, opinion, letters to the editor and editorials. Journal of Integrative Cardiology (JIC) is proud of its fast Reviewing and Editorial Decision system. Manuscripts are normally evaluated by three members from an international panel of reviewers and, in most cases; we provide a first editorial decision within 21 days of receipt.</p>
                <p class="text-justify">JIC welcomes direct submissions of manuscripts from authors. You can submit your manuscript to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a> alternatively to: <a href="mailto:editor.jic@oatext.com">editor.jic@oatext.com</a></p>
              </div>
            </div>
            <div id="Editor-in-Chief" class="content">
              <h2>Massimo Fioranelli</h2>
              <hr/>
              <p class="text-justify"> <img align="right" src="img/Massimo-Fioranelli.jpg" alt="" class="profile-pic">Born in Rome July 09, 1958. Graduated in 1984 at Medical School, University of Rome “La Sapienza”. Massimo did his Fellowship in Internal Medicine, University Tor Vergata, Rome in 1989 and in 1993 he did his Fellowship in Cardiology from Catholic  University of the Sacred Heart, Rome. In 1989-2001 he worked as Staff  Member Cardiologist at Division of Cardiology S.Giovanni Calibita Fatebenefratelli Hospital, Isola Tiberina, Rome. He was Chief of Cardiology Department  and Interventional Cardiology Laboratory, Casa di Cura  Villa Flaminia, Rome during 2002-2006. From 2006 he is a Chief at Heart Center, Casa di Cura Mater Dei, Rome, Italy. In 2008 he worked as Full Professor of History of Medicine and Bioethics at Guglielmo Marconi University, Rome. He was Scientific Director "Centro Studi Scienze della Vita", ' Guglielmo Marconi University, Rome in 2010 and worked as Associate Professor of Physiology University B.I.S. group of institutions, Punjab Technical University, Punjab, India. From 2013 he is Member  of Scientific Comitee  Regenera Group and Scientific Director  ARTOI (Associazione Ricerca Terapie Oncologiche Integrate). From 2014 he started working as Associate Professor of Physiology at Guglielmo Marconi University in Rome.</p>
              <p class="text-justify">Massimo Fioranelli has written several books and papers in the field of Cardiology and History of Medicine and gave many invited presentations and session chairman in scientific meetings.</p>
              <p class="mb5">FELLOWSHIP ESC, European Society of Cardiology :</p>
              <p>FELLOWSHIP SCAI, Society for Cardiovascular Angiography and Interventions <br>
                FELLOWSHIP ACCP, American College Chest Physician <br>
                FELLOWSHIP GISE, Societa' Italiana di Cardiologia Invasiva FELLOWSHIP ANMCO, Associazione Nazionale Cardiologi Ospedalieri </p>
              <p>Interventional Cardiology Activity: More than 2.500 diagnostic and interventional cardiovascular precedures.</p>
            </div>
            <div id="Editorial_Board" class="content">
              <h2>Editorial Board</h2>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Paolo Angelini</h4>
                  <p>Clinical Professor of Medicine<br>
                    Baylor College of Medicine<br>
                    Houston, Texas,  USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Luigi M Biasucci</h4>
                  <p>Catholic University of the Sacred Heart<br>
                    Department of Cardiovascular Medicine<br>
                    Rome, Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Takuya Watanabe</h4>
                  <p>Professor of Medical Science
                    Laboratory of Cardiovascular Medicine
                    Tokyo University of Pharmacy and Life Sciences
                    Tokyo, Japan</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Leonello Milani</h4>
                  <p>Vice President of A.I.O.T.<br>
                    Professor, Honoris Causa of the <br>
                    Advanced Institute of Health Studies<br>
                    Rome, Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Daniel Pella</h4>
                  <p>Professor of Medicine <br>
                    Dean of Pavol Jozef Šafárik University<br>
                    Slovakia</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ascanio Polimen</h4>
                  <p>co-chief Regenera Research Group<br>
                    professor at the International Master of <br>
                    Anti-Aging Medicine</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Wilbert S Aronow</h4>
                  <p>Professor of Medicine <br>
                    Divisions of Cardiology, <br>
                    Geriatrics, and Pulmonary/Critical Medicine <br>
                    Westchester Medical Center and<br>
                    New York Medical College </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dipak K Dube </h4>
                  <p>Professor<br>
                    Department of Medicine <br>
                    SUNY Upstate Medical University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Maria Grazia Roccia</h4>
                  <p>Full Professor<br>
                    History of Medicine<br>
                    University B.I.S. group of institutions<br>
                    Punjab Technical University, India </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Giuseppe Speziale</h4>
                  <p>Cardiac Surgery Unit<br>
                    Anthea Hospital<br>
                    GVM Care & Research</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Torello Lotti</h4>
                  <p>Professor <br>
                    Department of the <br>
                    Dermatology and Venereology <br>
                    University of Rome "G.Marconi" </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ali Dahhan</h4>
                  <p>Department of Internal Medicine<br>
                    Carver College of Medicine<br>
                    University of Iowa</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Florian Lang</h4>
                  <p>Professor<br>
                    Physiological Institute <br>
                    University of Tübingen<br>
                    Germany </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Fumimaro Takatsu</h4>
                  <p>Department of Cardiology<br>
                    Anjo Kosei Hospital </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yin Ruixing </h4>
                  <p>Department of Cardiology<br>
                    Institute of Cardiovascular Diseases <br>
                    The First Affiliated Hospital <br>
                    Guangxi Medical University <br>
                    China </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Dan goldman </h4>
                  <p>Cardiology Center <br>
                    Delray Beach <br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pitt Bertram </h4>
                  <p>Professor of Medicine Emeritus <br>
                    University of Michigan School of Medicine <br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Carlo Maria Rotella </h4>
                  <p>Professor<br>
                    Department of Clinical Pathophysiology <br>
                    University of Florence<br>
                    Italy </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Chaddha Ashish </h4>
                  <p>University of Wisconsin Hospital and Clinics <br>
                    Department of Internal Medicine <br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Praxis Cocco </h4>
                  <p>Cardiology Office <br>
                    Rheinfelden <br>
                    Switzerland </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Seth Baum</h4>
                  <p>University of Miami Miller School of Medicine <br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Servet Altay</h4>
                  <p>Department of Cardiology <br>
                    Siyami Ersek Thoracic and Cardiovascular Surgery Center <br>
                    Training and Research Hospital <br>
                    Turkey</p>
                </div>
                <div class="medium-4 columns">
                  <h4>D.A. Spandidos </h4>
                  <p>Professor of Virology<br>
                    Department of Clinical Virology <br>
                    University of Crete <br>
                    Greece </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Cagdas Can </h4>
                  <p>Department of Emergency Medicine <br>
                    Merkezefendi State Hospital <br>
                    Turkey </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Xiang Zhou</h4>
                  <p>Department of Cardiology<br>
                    The Second Affiliated Hospital of Soochow University<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Andrea Cardona</h4>
                  <p>Department Cardiology and Cardiovascular Pathophysiology<br>
                    University of Perugia <br>
                    Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Kevin Shah</h4>
                  <p>Department of Internal Medicine <br>
                    University of California <br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Alberto M. Marra</h4>
                  <p>MD Internal Medicine Unit<br>
                    Villa dei Fiori Private Hospital<br>
                    European Union of Private Hospitals<br>
                    Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pachon M JC</h4>
                  <p>Pacemaker and Arrhythmias <br>
                    Dante Pazzanese Cardiology Institute <br>
                    and Sao Paulo Heart Hospital<br>
                    Brazil </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ertan Yetkin</h4>
                  <p>Professor of Cardiology <br>
                    Yenisehir Hospital <br>
                    Division of Cardiology <br>
                    Turkey </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Jennifer Hall </h4>
                  <p>Associate Professor of Medicine<br>
                    Cardiovascular Division<br>
                    University of Minnesota<br>
                    USA </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Sevket balta </h4>
                  <p>Gulhane Military Medical Academy<br>
                    School of Medicine <br>
                    Department of Cardiology<br>
                    Turkey </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jalal K. Ghali </h4>
                  <p>Department of Internal Medicine <br>
                    Division of Cardiology <br>
                    Mercer University School of Medicine<br>
                    USA </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Alessandro Della Corte </h4>
                  <p>Associate Professor of Cardiac Surgery<br>
                    Department of Cardiothoracic Sciences<br>
                    University of Naples II<br>
                    Italy </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ugur Canpolat </h4>
                  <p>Department of Cardiology <br>
                    Hacettepe University Faculty of Medicine<br>
                    Turkey </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Maria Radu</h4>
                  <p>The Heart Centre <br>
                    Rigshospitalet<br>
                    Copenhagen University Hospital<br>
                    Department of Cardiology <br>
                    Denmark </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Enrico Ferrari </h4>
                  <p>Cardiac Surgery and Research Unit<br>
                    University Hospital of Lausanne <br>
                    Switzerland</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jorge Belardi </h4>
                  <p>Cardiology Department<br>
                    Instituto Cardiovascular de Buenos Aires [ICBA]<br>
                    Argentina </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Muhammed Keskin </h4>
                  <p>Siyami Ersek Thoracic and Cardiovascular Surgery Center<br>
                    Training and Research Hospital Department of Cardiology <br>
                    Turkey </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Leonardo Calo</h4>
                  <p>Catholic University of the Sacred Heart<br>
                    Faculty of Medicine and Surgery<br>
                    Rome, Italy</p>
                </div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 3, Issue 3</h4>
              <hr/>
                <!------JIC.1000212------>
                <div class="article">
                  <h4><a href="Frequency-dispersion-of-the-surface-wave-on-the-vessel-wall-Primary-reason-of-atherosclerosis.php">Frequency dispersion of the surface wave on the vessel wall - Primary reason of atherosclerosis</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Guram Beraia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Merab Beraia</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 20, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000212 x------>
                <!------JIC.1000213------>
                <div class="article">
                  <h4><a href="Beta-blockers-and-aerobic-training-on-peripheral-adrenoceptor-numbers-in-CAD-patients.php">Beta-blockers and aerobic training on peripheral adrenoceptor numbers in CAD patients</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Moran Saghiv <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ehud Goldhammer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David Ben-Sira <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jill Nustad</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 07, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000213 x------>
                <!------JIC.1000214------>
                <div class="article">
                  <h4><a href="The-effect-of-the-Wingate-Anaerobic-Test-on-NTproBNP-lactate-and-blood-pressure-in-wrestlers-and-untrained-young-healthy-males.php">The effect of the Wingate Anaerobic Test on NTproBNP, lactate and blood pressure in wrestlers and untrained young healthy males</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Saghiv M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cummings K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kusser K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kvislen K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roemmich L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dinkel J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Knoll C</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 07, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000214 x------>
                <!------JIC.1000215------>
                <div class="article">
                  <h4><a href="Subcutaneous-ICD-and-ventricular-tachycardia-degeneration.php">Subcutaneous ICD and ventricular tachycardia degeneration</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anna Mengoni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Erberto Carluccio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gianluca Zingarini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Giuseppe Ambrosio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alessandra Tordini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paolo Biagioli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David Giannandrea <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Claudio Cavallini</p>
                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 07, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000215 x------>
                <!------JIC.1000216------>
                <div class="article">
                  <h4><a href="Right-mammary-artery-and-saphenous-vein-non-touch-a-combination-for-complete-off-pump-revascularization-in-coronary-surgery-without-touching-the-aorta.php">Right mammary artery and saphenous vein non touch a combination for complete off pump revascularization in coronary surgery without touching the aorta</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Federico J Benetti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Natalia Scialacomo</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 10, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000216 x------>
                <!------JIC.1000217------>
                <div class="article">
                  <h4><a href="Pulmonary-Vein-Isolation-by-the-Helical-Approach.php">Pulmonary Vein Isolation by the Helical Approach</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guilherme Miglioli Lobato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 10, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000217 x------>
                <!------JIC.1000218------>
                <div class="article">
                  <h4><a href="Electrical-Left-Ventricular-Mapping-in-Patients-without-CKD-and-with-different-stages-of-CKD.php">Electrical Left Ventricular Mapping in Patients without CKD and with different stages of CKD</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 30, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000218 x------>




              
             
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue </h2>
              <h4>Volume 3, Issue 2</h4>
              <hr/>
               <!------JIC.1000204 ------>
                <div class="article">
                  <h4><a href="Technical-feasibility-of-a-care-management-system-for-follow-up-of-patients-hospitalized-for-acute-coronary-syndrome.php"> Technical feasibility of a care management system for follow-up of patients hospitalized for acute coronary syndrome </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Martin Juneau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Malorie Chabot-Blanchet <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sylvie Cossette <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marie-Claude Guertin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert F. DeBusk </p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 15, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000204  x------>
                <!------JIC.1000205 ------>
                <div class="article">
                  <h4><a href="Novel-extracorporeal-membrane-oxygenation-support-strategy-as-a-treatment-for-acute-lung-injury-following-ventricular-assist-device-implantation.php"> Novel extracorporeal membrane oxygenation support strategy as a treatment for acute lung injury following ventricular assist device implantation </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vander Pluym CJ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daly KP <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thiagarajan R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fynn-Thompson F </p>
                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 19, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000205  x------>
                <!------JIC.1000206 ------>
                <div class="article">
                  <h4><a href="ChitoHem-hemostatic-powder-compared-with-electro-cautery-anorectal-surgery-A-randomized-controlled-trial.php"> ChitoHem hemostatic powder compared with electro-cautery anorectal surgery: A randomized controlled trial </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rezvan Mirzaei <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bahar Mahjoobi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Soheila S Kordestani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farzaneh NayebHabib <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saman Mohammadi pour </p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000206  x------>
                <!------JIC.1000207------>
                <div class="article">
                  <h4><a href="Atrial-fibrilation-in-patients-with-systolic-heart-dysfunction-and-central-vs-obstructive-sleep-apnea.php"> Atrial fibrilation in patients with systolic heart dysfunction and central <em>vs.</em> obstructive sleep apnea </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 26, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000207 x------>
                <!------JIC.1000208------>
                <div class="article">
                  <h4><a href="Acute-epinephrine-test-after-renal-sympathetic-denervation-in-uncontrolled-hypertensive-patients.php">Acute epinephrine test after renal sympathetic denervation in uncontrolled hypertensive patients</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 20, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000208 x------>
                <!------JIC.1000209------>
                <div class="article">
                  <h4><a href="Acute-effects-of-renal-sympathetic-denervation-guided-by-renal-nerve-stimulation-in-CKD-patients-with-ICD.php">Acute effects of renal sympathetic denervation guided by renal nerve stimulation in CKD patients with ICD</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000209 x------>
                <!------JIC.1000210------>
                <div class="article">
                  <h4><a href="Effect-of-nicotine-caffeine-interaction-on-rats-coronaries-and-cardiac-muscle.php">Effect of nicotine-caffeine interaction on rat’s coronaries and cardiac muscle</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Soad Shaker Ali <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Nabil Alama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Etedal Abbas Huwait <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amal Saeed Abed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gamal Karrouf</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 03, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000210 x------>
                <!------JIC.1000211------>
                <div class="article">
                  <h4><a href="PCSK9-AB-and-risk-of-neurocognitive-events-comments-to-Robinsons-meta-analysis.php">PCSK9-AB and risk of neurocognitive events – comments to Robinson’s meta-analysis</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alessandro Battaggia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrea Scalisi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alessandro Schivalocchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alberto Donzelli</p>
                  <p class="mb5">Letter to Editor-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000211 x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 3, Issue 1</h4>
              <hr/>
             <!------JIC.1000194------>
              <div class="article">
                <h4><a href="Effects-of-renal-sympathetic-denervation-in-hypertensive-response-and-trigger-of-atrial-fibrillation-in-treadmill-test-in-patients-without-hypertension.php">Effects of renal sympathetic denervation in hypertensive response and trigger of atrial fibrillation in treadmill test in patients without hypertension</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Short communication-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000194 x------>
              <!------JIC.1000195------>
              <div class="article">
                <h4><a href="Semi-automated-evaluation-of-right-ventricular-volumes-using-gradient-cine-magnetic-resonance-data-sets-an-in-vitro-validation-study.php">Semi-automated evaluation of right ventricular volumes using gradient cine magnetic resonance data sets: an in vitro validation study</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muhammad Ashraf <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Matthew Martinez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William Woodward <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shelby Kutty <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thomas Balbach <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thuan Nguyen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jeong Y Lim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David J Sahn</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000195 x------>
              <!------JIC.1000197------>
              <div class="article">
                <h4><a href="Acute-carotid-sinus-stimulation-in-uncontrolled-hypertensive-patients.php">Acute carotid sinus stimulation in uncontrolled hypertensive patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 14, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000197 x------>
              <!------JIC.1000198------>
              <div class="article">
                <h4><a href="Adverse-cardiovascular-outcomes-in-patients-with-hand-osteoarthritis.php">Adverse cardiovascular outcomes in patients with hand osteoarthritis</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Sharaf Eldeen Shazly Abdallah <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Osama SD Mohamed</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 21, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000198 x------>
              <!------JIC.1000199------>
              <div class="article">
                <h4><a href="Acute-carotid-sinus-stimulation-vs.-renal-sympathetic-stimulation-in-uncontrolled-hypertensive-patients.php">Acute carotid sinus stimulation vs. renal sympathetic stimulation in uncontrolled hypertensive patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000199 x------>
              <!------JIC.1000200------>
              <div class="article">
                <h4><a href="Mediastinal-mass-an-extremely-rare-incidental-finding-saccular-superior-vena-cava-aneurism.php">Mediastinal mass, an extremely rare incidental finding: saccular superior vena cava aneurism</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Pedro María Azcarate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jose Antonio Arrieta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Uxua Idiazabal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fernando Idoate</p>
                <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 26, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000200 x------>
              <!------JIC.1000201 x------>
              <div class="article">
                <h4><a href="Renal-sympathetic-denervation-in-mild-chronic-kidney-disease-patients-with-chronic-heart-failure-without-indication-of-cardiac-resynchronization-therapy-a-bridge-to-cardiac-transplantation.php">Renal sympathetic denervation in mild chronic kidney disease patients with chronic heart failure without indication of cardiac resynchronization therapy: a bridge to cardiac transplantation</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Helmut Pürerfellner</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 28, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000201 x------>
              <!------JIC.1000202------>
              <div class="article">
                <h4><a href="Renal-sympathetic-denervation-for-normotensive-patients-with-recurrent-mixed-reflex-syncope-the-pilot-RELAX-study.php"> Renal sympathetic denervation for normotensive patients with recurrent mixed reflex syncope (the pilot RELAX study) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000202 x------>
              <!------JIC.1000203 ------>
                <div class="article">
                  <h4><a href="Carcinoid-heart-disease-and-recurrent-hypertensive-crises-in-a-patient-with-primary-high-grade-ovarian-neuroendocrine-carcinoma.php"> Carcinoid heart disease and recurrent hypertensive crises in a patient with primary high-grade ovarian neuroendocrine carcinoma </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adam M Brouillard <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John M Kasznica <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jennifer K Lang </p>
                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2016 </p>
                  </em>
                  <hr/>
                </div>
              <!------JIC.1000203  x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr/>
              <div class="lee">
                <dl data-accordion="" class="accordion" >
                  <dd> <a href="#panel1d" class="accordian-active">Volume 3 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1d">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>March 2017</span> <span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>January 2017</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                          
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                    <!------JIC.1000204 ------>
                <div class="article">
                  <h4><a href="Technical-feasibility-of-a-care-management-system-for-follow-up-of-patients-hospitalized-for-acute-coronary-syndrome.php"> Technical feasibility of a care management system for follow-up of patients hospitalized for acute coronary syndrome </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Martin Juneau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Malorie Chabot-Blanchet <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sylvie Cossette <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marie-Claude Guertin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert F. DeBusk </p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 15, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000204  x------>
                <!------JIC.1000205 ------>
                <div class="article">
                  <h4><a href="Novel-extracorporeal-membrane-oxygenation-support-strategy-as-a-treatment-for-acute-lung-injury-following-ventricular-assist-device-implantation.php"> Novel extracorporeal membrane oxygenation support strategy as a treatment for acute lung injury following ventricular assist device implantation </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vander Pluym CJ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daly KP <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thiagarajan R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fynn-Thompson F </p>
                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 19, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000205  x------>
                <!------JIC.1000206 ------>
                <div class="article">
                  <h4><a href="ChitoHem-hemostatic-powder-compared-with-electro-cautery-anorectal-surgery-A-randomized-controlled-trial.php"> ChitoHem hemostatic powder compared with electro-cautery anorectal surgery: A randomized controlled trial </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rezvan Mirzaei <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bahar Mahjoobi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Soheila S Kordestani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farzaneh NayebHabib <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saman Mohammadi pour </p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000206  x------>
                <!------JIC.1000207------>
                <div class="article">
                  <h4><a href="Atrial-fibrilation-in-patients-with-systolic-heart-dysfunction-and-central-vs-obstructive-sleep-apnea.php"> Atrial fibrilation in patients with systolic heart dysfunction and central <em>vs.</em> obstructive sleep apnea </a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 26, 2016 </p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000207 x------>
                <!------JIC.1000208------>
                <div class="article">
                  <h4><a href="Acute-epinephrine-test-after-renal-sympathetic-denervation-in-uncontrolled-hypertensive-patients.php">Acute epinephrine test after renal sympathetic denervation in uncontrolled hypertensive patients</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 20, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000208 x------>
                <!------JIC.1000209------>
                <div class="article">
                  <h4><a href="Acute-effects-of-renal-sympathetic-denervation-guided-by-renal-nerve-stimulation-in-CKD-patients-with-ICD.php">Acute effects of renal sympathetic denervation guided by renal nerve stimulation in CKD patients with ICD</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000209 x------>
                <!------JIC.1000210------>
                <div class="article">
                  <h4><a href="Effect-of-nicotine-caffeine-interaction-on-rats-coronaries-and-cardiac-muscle.php">Effect of nicotine-caffeine interaction on rat’s coronaries and cardiac muscle</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Soad Shaker Ali <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Nabil Alama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Etedal Abbas Huwait <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amal Saeed Abed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gamal Karrouf</p>
                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 03, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000210 x------>
                <!------JIC.1000211------>
                <div class="article">
                  <h4><a href="PCSK9-AB-and-risk-of-neurocognitive-events-comments-to-Robinsons-meta-analysis.php">PCSK9-AB and risk of neurocognitive events – comments to Robinson’s meta-analysis</a></h4>
                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alessandro Battaggia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrea Scalisi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alessandro Schivalocchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alberto Donzelli</p>
                  <p class="mb5">Letter to Editor-Journal of Integrative Cardiology (JIC)</p>
                  <em>
                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2017</p>
                  </em>
                  <hr/>
                </div>
                <!------JIC.1000211 x------>  
                              
                              </div>
                            </div>
                          </div>    



                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                 <!------JIC.1000194------>
              <div class="article">
                <h4><a href="Effects-of-renal-sympathetic-denervation-in-hypertensive-response-and-trigger-of-atrial-fibrillation-in-treadmill-test-in-patients-without-hypertension.php">Effects of renal sympathetic denervation in hypertensive response and trigger of atrial fibrillation in treadmill test in patients without hypertension</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Short communication-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000194 x------>
              <!------JIC.1000195------>
              <div class="article">
                <h4><a href="Semi-automated-evaluation-of-right-ventricular-volumes-using-gradient-cine-magnetic-resonance-data-sets-an-in-vitro-validation-study.php">Semi-automated evaluation of right ventricular volumes using gradient cine magnetic resonance data sets: an in vitro validation study</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Muhammad Ashraf <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Matthew Martinez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William Woodward <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shelby Kutty <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thomas Balbach <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thuan Nguyen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jeong Y Lim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David J Sahn</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000195 x------>
              <!------JIC.1000197------>
              <div class="article">
                <h4><a href="Acute-carotid-sinus-stimulation-in-uncontrolled-hypertensive-patients.php">Acute carotid sinus stimulation in uncontrolled hypertensive patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 14, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000197 x------>
              <!------JIC.1000198------>
              <div class="article">
                <h4><a href="Adverse-cardiovascular-outcomes-in-patients-with-hand-osteoarthritis.php">Adverse cardiovascular outcomes in patients with hand osteoarthritis</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Sharaf Eldeen Shazly Abdallah <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Osama SD Mohamed</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 21, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000198 x------>
              <!------JIC.1000199------>
              <div class="article">
                <h4><a href="Acute-carotid-sinus-stimulation-vs.-renal-sympathetic-stimulation-in-uncontrolled-hypertensive-patients.php">Acute carotid sinus stimulation vs. renal sympathetic stimulation in uncontrolled hypertensive patients</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 25, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000199 x------>
              <!------JIC.1000200------>
              <div class="article">
                <h4><a href="Mediastinal-mass-an-extremely-rare-incidental-finding-saccular-superior-vena-cava-aneurism.php">Mediastinal mass, an extremely rare incidental finding: saccular superior vena cava aneurism</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Pedro María Azcarate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jose Antonio Arrieta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Uxua Idiazabal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fernando Idoate</p>
                <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 26, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000200 x------>
              <!------JIC.1000201 x------>
              <div class="article">
                <h4><a href="Renal-sympathetic-denervation-in-mild-chronic-kidney-disease-patients-with-chronic-heart-failure-without-indication-of-cardiac-resynchronization-therapy-a-bridge-to-cardiac-transplantation.php">Renal sympathetic denervation in mild chronic kidney disease patients with chronic heart failure without indication of cardiac resynchronization therapy: a bridge to cardiac transplantation</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Helmut Pürerfellner</p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Novembe 28, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000201 x------>
              <!------JIC.1000202------>
              <div class="article">
                <h4><a href="Renal-sympathetic-denervation-for-normotensive-patients-with-recurrent-mixed-reflex-syncope-the-pilot-RELAX-study.php"> Renal sympathetic denervation for normotensive patients with recurrent mixed reflex syncope (the pilot RELAX study) </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 09, 2016 </p>
                </em>
                <hr/>
              </div>
              <!------JIC.1000202 x------>
                 <!------JIC.1000203 ------>
      <div class="article">
        <h4><a href="Carcinoid-heart-disease-and-recurrent-hypertensive-crises-in-a-patient-with-primary-high-grade-ovarian-neuroendocrine-carcinoma.php"> Carcinoid heart disease and recurrent hypertensive crises in a patient with primary high-grade ovarian neuroendocrine carcinoma </a></h4>
        <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adam M Brouillard <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John M Kasznica <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jennifer K Lang </p>
        <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
        <em>
        <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 12, 2016 </p>
        </em>
        <hr/>
      </div>
      <!------JIC.1000203  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1c" class="accordian">Volume 2 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Nov 2016</span> <span class="mt3 grey-text">Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Sep 2016</span> <span class="mt3 grey-text">Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2016</span> <span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2016</span> <span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Mar 2016</span> <span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Jan 2016</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000183 ------>
                                <div class="article">
                                  <h4><a href="Ventricular-arrhythmias-in-patients-with-controlled-hypertension-and-obstructive-sleep-apnea.php">Ventricular arrhythmias in patients with controlled hypertension and obstructive sleep apnea</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000183  x------>
                                <!------JIC.1000184------>
                                <div class="article">
                                  <h4><a href="Lenalidomide-induced-stroke-in-multiple-myeloma.php">Lenalidomide induced stroke in multiple myeloma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shridhar P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khalil R <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lasorda D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chun Y </p>
                                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 20, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000184 x------>
                                <!------JIC.1000185------>
                                <div class="article">
                                  <h4><a href="The-shape-of-a-volume.php">The shape of a volume</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marco Cirillo</p>
                                  <p class="mb5">Editorial-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 20, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000185 x------>
                                <!------JIC.1000186------>
                                <div class="article">
                                  <h4><a href="Novelty-of-continuous-intravenous-infusion-of-bumetanide-in-severe-congestive-heart-failure-associated-with-renal-failure.php">Novelty of continuous intravenous infusion of bumetanide in severe congestive heart failure associated with renal failure</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anil K Mandal </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000186 x------>
                                <!------JIC.1000187------>
                                <div class="article">
                                  <h4><a href="Different-pathologies-without-coronary-artery-disease-may-induce-typical-chest-pain.php">Different pathologies without coronary artery disease may induce typical chest pain </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Giuseppe Cocco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Philipp Amiet <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul Jerie </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000187 x------>
                                <!------JIC.1000188------>
                                <div class="article">
                                  <h4><a href="The-importance-of-the-lymphatic-system-in-vascular-disease.php">The importance of the lymphatic system in vascular disease</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lemole GM </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000188 x------>
                                <!------JIC.1000189------>
                                <div class="article">
                                  <h4><a href="Trichinosis-complicated-with-hepatitis-and-myopericarditis.php">Trichinosis complicated with hepatitis and myopericarditis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Boulus M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saad E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Budman D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lisitsin S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Barhoum m <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sbeit W <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Assy N</p>
                                  <p class="mb5">Case report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000189 x------>
                                <!------JIC.1000191------>
                                <div class="article">
                                  <h4><a href="Right-atrial-pacing-in-patients-with-sinus-node-dysfunction-results-from-an-observational-cohort-study.php">Right atrial pacing in patients with sinus node dysfunction results from an observational cohort study</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>ShaojieChen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gustavo Ramalho e Silva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luis Marcelo Rodrigues Paz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gladyston Luiz Lima Souto</p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 07, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000191 x------>
                                <!------JIC.1000193------>
                                <div class="article">
                                  <h4><a href="The-bald-right-atrium-an-unexpected-approach.php">The “bald” right atrium: an unexpected approach</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaojie Chen</p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000193 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000174 ------>
                                <div class="article">
                                  <h4><a href="Coronary-artery-bypass-grafting-with-adjunctive-endarterectomy-A-mandatory-procedure-in-complex-revascularizations-current-results-and-postoperative-considerations.php"> Coronary artery bypass grafting with adjunctive endarterectomy: A mandatory procedure in complex revascularizations. current results and postoperative considerations </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marco Russo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paolo Nardi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Guglielmo Saitto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Emanuele Bovio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Antonio Pellegrino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Giovanni Ruvolo </p>
                                  <p class="mb5">Editorial -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000174  x------>
                                <!------JIC.1000175------>
                                <div class="article">
                                  <h4><a href="Adult-onset-stills-disease-presenting-as-recurrent-fever-with-ARDS-A-case-report.php"> Adult onset still's disease presenting as recurrent fever with ARDS: A case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Prabhakaran Gopalakrishnan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Smita Subramaniam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Asha Thomas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Andrey Manov </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000175 x------>
                                <!------JIC.1000177 ------>
                                <div class="article">
                                  <h4><a href="NADPH-oxidase-uncoupled-endothelial-nitric-oxide-synthase-and-NF-KappaB-are-key-mediators-of-the-pathogenic-impact-of-obstructive-sleep-apnea-Therapeutic-implications.php"> NADPH oxidase, uncoupled endothelial nitric oxide synthase, and NF-KappaB are key mediators of the pathogenic impact of obstructive sleep apnea – Therapeutic implications </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mark F McCarty <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James J DiNicolantonio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>James H. O'Keefe c </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000177  x------>
                                <!------JIC.1000178 ------>
                                <div class="article">
                                  <h4><a href="Cardiac-torsion-after-excision-of-a-huge-mature-cystic-teratoma.php"> Cardiac torsion after excision of a huge mature cystic teratoma </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Kathleen S Cruz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jaime F Esquivel </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000178  x------>
                                <!------JIC.1000179 ------>
                                <div class="article">
                                  <h4><a href=" Pulmonary-vein-isolation-and-renal-sympathetic-denervation-in-CKD-patients-with-refractory-AF.php"> Pulmonary vein isolation and renal sympathetic denervation in CKD patients with refractory AF </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Márcio Galindo Kiuchi </p>
                                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000179  x------>
                                <!------JIC.1000180 ------>
                                <div class="article">
                                  <h4><a href="Function-of-renin-angiotensin-system-on-heart-failure.php"> Function of renin angiotensin system on heart failure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Flores-Monroy Jazmín <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lezama-Martínez Diego <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martínez-Aguilar Luisa </p>
                                  <p class="mb5">Review Article -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 26,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000180  x------>
                                <!------JIC.1000181 ------>
                                <div class="article">
                                  <h4><a href="Le-Compte-Maneuver-in-surgical-correction-of-absent-pulmonary-valve-Does-it-improve-severe-bronchial-compression.php "> Le-Compte Maneuver in surgical correction of absent pulmonary valve. Does it improve severe bronchial compression? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Bulent Saritas <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emre Ozker <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ozlem Sarisoy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Murat Sahin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ismail Caymaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Burcak Gumus <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Canan Ayabakan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sait Aslamaci </p>
                                  <p class="mb5">Case report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000181  x------>
                                <!------JIC.1000182------>
                                <div class="article">
                                  <h4><a href="New-perspective-on-atrial-fibrillation-A-theoretical--assumption.php"> New perspective on atrial fibrillation: A theoretical  assumption </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vladimir Tilman </p>
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000182 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000167------>
                                <div class="article">
                                  <h4><a href="Lipomatous-hypertrophy-of-the-interatrial-septum-and-arrhythmia-A-case-report.php"> Lipomatous hypertrophy of the interatrial septum and arrhythmia: A case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Bolognesi </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000167 x------>
                                <!------JIC.1000168------>
                                <div class="article">
                                  <h4><a href="Misdiagnose-of-acute-aortic-dissection-Case-report-acute-aortic-dissection.php"> Misdiagnose of acute aortic dissection: Case report acute aortic dissection </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Humberto Morais <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Valdano Manuel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fidel M Cáceres-Loriga</p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 04, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000168 x------>
                                <!------JIC.1000169------>
                                <div class="article">
                                  <h4><a href="Resource-utilization-after-thoracic-aortic-aneurysm-repair-An-examination-of-the-endovascular-approach-in-the-United-States.php">Resource utilization after thoracic aortic aneurysm repair: An examination of the endovascular approach in the United States</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander Iribarne <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ashwin Karanam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dhaval Chauhan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Aurelie Merlo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noah Bressner <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ross Milner <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mark J Russo</p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000169 x------>
                                <!------JIC.1000170------>
                                <div class="article">
                                  <h4><a href="Factors-necessitating-tolvaptan-continuation-in-the-outpatient-setting-for-heart-failure.php">Factors necessitating tolvaptan continuation in the outpatient setting for heart failure</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Keiichi Tsuchida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryohei Sakai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kota Nishida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jiro Hiroki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Asami Kashiwa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Norihito Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuki Fujihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yukio Hosaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazuyoshi Takahashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hirotaka Oda</p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000170 x------>
                                <!------JIC.1000171------>
                                <div class="article">
                                  <h4><a href="Trends-and-advances-of-teleconsultation-in-developing-countries.php">Trends and advances of teleconsultation in developing countries</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bartolo M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ferrari F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Erba F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Petrolati S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Leone M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Enrico Fraschetti</p>
                                  <p class="mb5">Perspective-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000171 x------>
                                <!------JIC.1000172------>
                                <div class="article">
                                  <h4><a href="Can-healthy-diet-and-lifestyle-of-father-and-mother-prevent-cardio-metabolic-risk-in-the-offspring.php">Can healthy diet and lifestyle of father and mother prevent cardio-metabolic risk in the offspring?</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ram B Singh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sergey Shastun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Poonam Singh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>GarimaTomar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Banshi Saboo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anuj Maheshwari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Narsing Verma <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Galal Elkilany <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>DW Wilson</p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000172 x------>
                                <!------JIC.1000173------>
                                <div class="article">
                                  <h4><a href="The-diagnostic-value-of-exercise-stress-testing-for-cardiovascular-disease-is-more-than-just-st-segment-changes-A-review.php">The diagnostic value of exercise stress testing for cardiovascular disease is more than just st segment changes: A review</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Salaheddin Sharif <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Stephen E Alway</p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000173 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000159------>
                                <div class="article">
                                  <h4><a href="Evaluating-ejection-fraction-and-obesity-paradox-in-a-heart-failure-program.php"> Evaluating ejection fraction and obesity paradox in a heart failure program </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hong Seok Lee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Belen Nunez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ferdinand Visco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Savi Mushiyev <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerald Pekler </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 14, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000159 x------>
                                <!------JIC.1000160------>
                                <div class="article">
                                  <h4><a href="Alcohol-and-cardiovascular-mortality-crisis-in-Russia.php"> Alcohol and cardiovascular mortality crisis in Russia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Razvodovsky YE </p>
                                  <p class="mb5">Mini Review -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000160 x------>
                                <!------JIC.1000161------>
                                <div class="article">
                                  <h4><a href="Endothelial-function-evaluation-in-patients-with-anorexia-nervosa.php"> Endothelial function evaluation in patients with anorexia nervosa </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Patcharapong Suntharos <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Myriam E Almeida Jones <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Howard S Seiden <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martin Fisher <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dorota Gruber <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lisa M. Rosen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew D Blaufox <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rubin S. Cooper </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000161 x------>
                                <!------JIC.1000162 ------>
                                <div class="article">
                                  <h4><a href="Cannabis-induced-myocardial-infarction-underlying-mechanism-and-the-place-of-glycoprotein-IIb-IIIa-inhibitors-in-its-management-Two-illustrative-cases-of-acute-anterior-myocardial-infarction-related-to-cannabis.php"> Cannabis induced myocardial infarction underlying mechanism and the place of glycoprotein IIb/IIIa inhibitors in its management: Two illustrative cases of acute anterior myocardial infarction related to cannabis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noamen A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hajlaoui N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mehdi G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Haouala H </p>
                                  <p class="mb5">Case Report -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000162  x------>
                                <!------JIC.1000163 ------>
                                <div class="article">
                                  <h4><a href="An-integrative-approach-to-the-Marfan-aortic-root-between-the-patient-the-physician-and-basic-scientists-A-case-study-in-personalised-external-aortic-root-support-PEARS.php"> An integrative approach to the Marfan aortic root between the patient, the physician and basic scientists: A case study in personalised external aortic root support (PEARS) </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tal Golesworthy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Peter Verbrugghe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cemil Izgi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shelly Singh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tom Treasure </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000163  x------>
                                <!------JIC.1000164------>
                                <div class="article">
                                  <h4><a href="Role-of-preventive-national-program-in-combating-diabetes-mellitus-and-cardiovascular-diseases.php"> Role of preventive national program in combating diabetes mellitus and cardiovascular diseases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Osman El-Labban </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000164 x------>
                                <!------JIC.1000165 ------>
                                <div class="article">
                                  <h4><a href="A-case-of-reversible-cardiomyopathy-left-ventricular-noncompaction.php"> A case of reversible cardiomyopathy & left ventricular noncompaction </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Oshi Amira <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdelbasit <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Seidahmed MZ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hussein K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miqdad A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Samadi M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alyousef S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Manie W <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdullah M </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 31, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000165  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000151------>
                                <div class="article">
                                  <h4><a href="What-should-the-target-systolic-blood-pressure-goal-be-in-older-diabetics-with-hypertension.php"> What should the target systolic blood pressure goal be in older diabetics with hypertension? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wilbert S. Aronow </p>
                                  <p class="mb5">Editorial-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February11, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000151 x------>
                                <!------JIC.1000152------>
                                <div class="article">
                                  <h4><a href="Attitudes-of-inner-city-patients-with-cardiovascular-disease-towards-meditation.php"> Attitudes of inner city patients with cardiovascular disease towards meditation </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Amit J. Shah <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Robert J. Ostfeld </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 10,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000152 x------>
                                <!------JIC.1000153------>
                                <div class="article">
                                  <h4><a href="Biomarkers-in-heart-failure.php"> Biomarkers in heart failure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kuan-Jen Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chih-Hui Chin </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 14,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000153 x------>
                                <!------JIC.1000154------>
                                <div class="article">
                                  <h4><a href="Cardiovascular-primary-prevention-in-2016.php"> Cardiovascular primary prevention in 2016 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>KinsaraAbdulhalim J </p>
                                  <p class="mb5">Opinion Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 18,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000154 x------>
                                <!------JIC.1000155 ------>
                                <div class="article">
                                  <h4><a href="Heart-rate-regulation-and-stress-resistence-of-elite-athletes.php"> Heart rate regulation and stress resistence of elite athletes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Korobeynikov G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Korobeinikova L </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000155  x------>
                                <!------JIC.1000156------>
                                <div class="article">
                                  <h4><a href="A-43-year-old-female-post-PDA-ligation-with-ALCAPA-presenting-with-chest-pain-case-report.php"> A 43 year old female post PDA ligation with ALCAPA presenting with chest pain:  case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Francis Carl L. Catalan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Francis Charles L. Fernandez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Lala Ann F. Bambico </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000156 x------>
                                <!------JIC.1000157------>
                                <div class="article">
                                  <h4><a href="Cardiac-Surgery-and-Potential-Biomarkers.php"> Cardiac Surgery and Potential Biomarkers </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alireza Mohammadzadeh </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 29,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000157 x------>
                                <!------JIC.1000158 ------>
                                <div class="article">
                                  <h4><a href="Rare-unexpected-anatomy-of-the-coronary-artery-combination-of-two-coronary-anomalies-case-report.php"> Rare unexpected anatomy of the coronary artery: combination of two coronary anomalies case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ayman Shaalan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eman E. El Wakeel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed A Alassal <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hany H. Ebaid </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 31,2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000158  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000145 ------>
                                <div class="article">
                                  <h4><a href="A-review-of-myasthenia-gravis-in-cardiac-surgery.php"> A review of myasthenia gravis in cardiac surgery </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marzia Cottini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marco Picichè <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>AmedeoPergolini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tania Dominici <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Francesco Musumeci </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 18, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000145  x------>
                                <!------JIC.1000146 ------>
                                <div class="article">
                                  <h4><a href="The-transition-from-the-anatomical-to-the-physiological-angiographic-assessment-fractional-flow-reserve-or-instantaneous-wave-free-ratio.php"> The transition from the anatomical to the physiological angiographic assessment: fractional flow reserve or instantaneous wave-free ratio </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Truscelli G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bianchi M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fioranelli M </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 02, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000146  x------>
                                <!------JIC.1000147 ------>
                                <div class="article">
                                  <h4><a href="Clinical-and-genetic-diagnosis-of-Marfan-syndrome-in-childhood-are-Equal--FBN1-mutation-should-not-influence-handling-of-pediatric-patients-with-confirmed-Marfansyndrome.php"> Clinical and genetic diagnosis of Marfan syndrome in childhood are Equal- FBN1 mutation should not influence handling of pediatric patients with confirmed Marfansyndrome </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Veronika C. Stark <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Florian Arndt <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gesa Harring <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Britta Keyser <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yskert von Kodolitsch <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rainer Kozlik-Feldmann <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kerstin Kutsche <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Goetz C. Mueller <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Meike Rybczynski <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Thomas S. Mir </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000147  x------>
                                <!------JIC.1000148------>
                                <div class="article">
                                  <h4><a href="Preoperative-cardiovascular-risk-evaluation-before-liver-transplantation-a-retrospective-single-center-study.php"> Preoperative cardiovascular risk evaluation before liver transplantation: a retrospective single center study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Donato Barile <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Émile Saliba <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ying Sia </p>
                                  <p class="mb5">Retrospective Study-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Janaury18, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000148 x------>
                                <!------JIC.1000149------>
                                <div class="article">
                                  <h4><a href="Laboratory-diagnosis-of-acute-myocardial-infarction-by-optical-aggregometry-of-platelets.php"> Laboratory diagnosis of acute myocardial infarction by optical aggregometry of platelets </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Debipriya Banerjee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sarbashri Bank <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gausal Azam Khan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santanu Guha <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Asru Kumar Sinha </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Janaury22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000149 x------>
                                <!------JIC.1000150 ------>
                                <div class="article">
                                  <h4><a href="Prognostic-value-of-area-flow-index-in-patients-with-acute-ST-elevation-myocardial-infarction-clinical-and-echocardiographic-outcomes.php"> Prognostic value of area flow index in patients with acute ST elevation myocardial infarction: clinical and echocardiographic outcomes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sherif W Ayad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed A Sobhy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed A Sadaka </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Janaury25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000150  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Nov 2015</span> <span class="mt3 grey-text">Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Sep 2015</span> <span class="mt3 grey-text">Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2015</span> <span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>May 2015</span> <span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title "><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Mar 2015</span> <span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title "><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Jan 2015</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000138 ------>
                                <div class="article">
                                  <h4><a href="Anectodal-report-of-acute-gastric-anisakiasis-and-severe-chest-discomfort.php"> Anectodal report of acute gastric anisakiasis and severe chest discomfort </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Enrica Mariano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Fioranelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Grazia Roccia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maurizio Onorato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Veronica Di Nardo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Bianchi </p>
                                  <p class="mb5">Cas Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000138  x------>
                                <!------JIC.1000139------>
                                <div class="article">
                                  <h4><a href="Visualization-of-an-innovative-approach-for-mitral-isthmus-ablation.php"> Visualization of an innovative approach for mitral isthmus ablation </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mark A. Benscoter <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Boaz Avitall <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul A. Iaizzo </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 13, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000139 x------>
                                <!------JIC.1000140------>
                                <div class="article">
                                  <h4><a href="The-risk-stratification-in-heart-failure-patients-The-controversial-role-of-high-sensitive-ST2.php"> The risk stratification in heart failure patients: The controversial role of high-sensitive ST2 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander E. Berezin </p>
                                  <p class="mb5">Commentary-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000140 x------>
                                <!------JIC.1000141------>
                                <div class="article">
                                  <h4><a href="Pericardial-effusion-in-the-setting-of-Takosubo-Cardiomyopathy.php"> Pericardial effusion in the setting of Takosubo Cardiomyopathy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Perry Fisher <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rajbir Sidhu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Supreeti Behuria <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maurice Rachko </p>
                                  <p class="mb5">Case Report -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000141 x------>
                                <!------JIC.1000142------>
                                <div class="article">
                                  <h4><a href="Pre-participation-screening-of-competitive-of-middle-aged-athletes-Certainties-and-doubts-in-daily-practice.php"> Pre-participation screening of competitive of middle-aged athletes: Certainties and doubts in daily practice </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Bolognesi </p>
                                  <p class="mb5">Case Report -Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000142 x------>
                                <!------JIC.1000143 ------>
                                <div class="article">
                                  <h4><a href="Targeting-Insulin-Like-Growth-Factor-I-receptor-signaling-pathways-improve-compromised-function-during-cardiac-hypertrophy.php"> Targeting Insulin-Like Growth Factor-I receptor signaling pathways improve compromised function during cardiac hypertrophy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Subhalakshmi Ganguly <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emeli Chatterjee <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sagartirtha Sarkar </p>
                                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 18, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000143  x------>
                                <!------JIC.1000144 ------>
                                <div class="article">
                                  <h4><a href="Spontaneous-triple-vessel-coronary-artery-dissection-in-a-patient-with-Ehlers-Danlos-Type-IV.php"> Spontaneous triple-vessel coronary artery dissection in a patient with Ehlers-Danlos Type IV </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Andres E. Carmona-Rubio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Narasa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jennifer Lang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Susan Graham <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Umesh Sharma </p>
                                  <p class="mb5">Mini Review-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000144  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------JIC.1000130------>
                                <div class="article">
                                  <h4><a href="The-importance-of-vessel-factors-for-stent-deployment-in-diseased-arteries.php"> The importance of vessel factors for stent deployment in diseased arteries </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alessandro Schiavone <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Liguo Zhao </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)<strong></strong></p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000130 x------>
                                <!------JIC.1000131------>
                                <div class="article">
                                  <h4><a href="Dilated-coronary-sinus-due-to-persistent-left-superior-vena-cava-in-a-healthy-athlete-A-case-report-with-brief-review.php"> Dilated coronary sinus due to persistent left superior vena cava in a healthy athlete: A case report with brief review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Massimo Bolognesi </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 07, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000131 x------>
                                <!------JIC.1000132------>
                                <div class="article">
                                  <h4><a href="Role-of-plaque-characterization-by-64-slice-multi-detector-computed-tomography-in-prediction-of-complexity-of-percutaneous-coronary-interventions.php"> Role of plaque characterization by 64-slice multi detector computed tomography in prediction of complexity of percutaneous coronary interventions </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sherif Wagdy Ayad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Ahmed Sobhy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eman Mohamed El-Sharkawy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mahmoud Shehata Abd-Elhamid </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 07, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000132 x------>
                                <!------JIC.1000133 ------>
                                <div class="article">
                                  <h4><a href="Electrocardiomatrix-A-new-method-for-beat-by-beat-visualization-and-inspection-of-cardiac-signals.php"> Electrocardiomatrix: A new method for beat-by-beat visualization and inspection of cardiac signals </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Duan Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fangyun Tian <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santiago Rengifo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gang Xu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michael M. Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jimo Borjigin </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 19, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000133  x------>
                                <!------JIC.1000134------>
                                <div class="article">
                                  <h4><a href="An-unusual-case-of-isolate-obstructive-thrombi-in-the-right-atrium.php"> An unusual case of isolate obstructive thrombi in the right atrium </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hassane Abdallah <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>François-Pierre Mongeon <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Philippe Demers </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000134 x------>
                                <!------JIC.1000135 ------>
                                <div class="article">
                                  <h4><a href="Coronary-microvascular-and-endothelial-function-regulation-Crossroads-of-psychoneuroendocrine-immunitary-signals-and-quantum-physics-Part-A.php"> Coronary microvascular and endothelial function regulation: Crossroads of psychoneuroendocrine immunitary signals and quantum physics [Part A] </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carlo Dal Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anna Poretto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marta Scodro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Martina Perazzolo Marra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sabino Iliceto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Francesco Tona <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000135  x------>
                                <!------JIC.1000136 ------>
                                <div class="article">
                                  <h4><a href="Coronary-microvascular-and-endothelial-function-regulation-Crossroads-of-psychoneuroendocrine-immunitary-signals-and-quantum-physics-Part-B.php"> Coronary microvascular and endothelial function regulation: Crossroads of psychoneuroendocrine immunitary signals and quantum physics [Part B] </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carlo Dal Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Poretto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marta Scodro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martina Perazzolo Marra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sabino Iliceto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Francesco Tona </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000136  x------>
                                <!------JIC.1000137 ------>
                                <div class="article">
                                  <h4><a href="Coronary-microvascular-and-endothelial-function-regulation-Crossroads-of-psychoneuroendocrine-immunitary-signals-and-quantum-physics-Part-C.php"> Coronary microvascular and endothelial function regulation: Crossroads of psychoneuroendocrine immunitary signals and quantum physics [Part C] </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Carlo Dal Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Poretto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marta Scodro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martina Perazzolo Marra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sabino Iliceto<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Francesco Tona </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000137  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="Journal-of-Integrative-Cardiology.php">Journal of Integrative Cardiology</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Fioranelli </p>
                                  <p class="mb5">Editorial-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 05, 2014 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Twenty-five-years-of-studies-and-trials-for-the-therapeutic-application-of-IL-10-immunomodulating-properties-From-high-doses-administration-to-low-dose-medicine-new-paradigm.php">Twenty-five years of studies and trials for the therapeutic application of IL-10 immunomodulating properties. From high doses administration to low dose medicine new paradigm</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Fioranelli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Roccia Maria Grazia </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2014</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Clinicians-have-not-to-use-trials-like-drunks-do-with-street-lamps.php">Clinicians have not to use trials like drunks do with street lamps</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Enrico Giustiniano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Meco </p>
                                  <p class="mb5">Commentry-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 30, 2014</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Subclinical-atherosclerosis-in-non-dialysis-chronic-renal-patients.php">Subclinical atherosclerosis in non-dialysis chronic renal patients</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Borges, Wagner Ramos <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fernandes Andre Mauricio Souza <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Duraes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andre Rodrigues <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Aras Junior Roque <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lima João </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 10, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Review-of-pericardial-diseases-Etiopathology-diagnosis-and-treatment-options.php">Review of pericardial diseases: Etiopathology, diagnosis and treatment options</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Balakrishnan Mahesh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Samer A M Nashef </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 11, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Atrial-myxoma-leading-to-intraparenchymal-hemorrhage.php">Atrial myxoma leading to intraparenchymal hemorrhagee</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Santoshi Billakota <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vani Chilukuri </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 13, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Gender-differences-in-cardiovascular-disease.php">Gender differences in cardiovascular disease</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Leonarda Galiuto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriella Locorotondo </p>
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 13, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Percutaneous-closure-of-spontaneous-pseudoaneurysm-of-ascending-aorta.php">Percutaneous closure of spontaneous pseudoaneurysm of ascending aorta</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vijairam Selvaraj <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ashequl Islam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shanmugam Uthamalingam <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Evan Lau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joseph E Flack </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>January 14, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="Contrast-induced-nephropathy.php"> Contrast-induced nephropathy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Bruno Pironi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Grazia Roccia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Bianchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carolina Aracena Jahaira <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Massimo Fioranelli
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 14, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Screening-program-in-the-athlete-in-the-suspicion-of-HCM.php"> Screening program in the athlete in the suspicion of HCM </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Maddalena Piro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Grazia Roccia <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carolina Jahaira Aracena <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Bianchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fioranelli Massimo
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Case-report-of-domiciliary-treatment-with-Tolvaptan-in-patients-with-advanced-heart-failure-and-persistent-hyponatremia.php"> Case report of domiciliary treatment with Tolvaptan in patients with advanced heart failure and persistent hyponatremia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Alberto Esteban-Fernández <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nahikari Salterain-Gonzalez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Juan J Gavira-Gómez
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 07, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Transluminalendovascular-aortic-repair-and-pregnancy-a-case-report.php"> Transluminalendovascular-aortic-repair-and-pregnancy-a-case-report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Duiella SF <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gaia G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bonanomi C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ossola MW <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rossi G <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fedele L
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 11, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Effects-of-exercise-training-on-the-hemodynamics-in-patients-with-chronic-heart-failure.php"> Effects of exercise training on the hemodynamics in patients with chronic heart failure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Shinji Satoh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miyuki Kamo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomoko Ohtsuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koji Hiyamuta
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 14, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Aesthetic-complaints-as-clue-to-Pseudoxanthoma-elasticum.php"> Cardiac amyloidosis: a case review series </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Asher Edwards <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark Paulsen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Baia Lasky <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vivek Ramanathan
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 15, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Interrelationship-between-insulin-resistance-and-impaired-immune-phenotype-of-circulating-endothelial-derived-microparticles-in-none-diabetic-patients-with-chronic-heart-failure.php"> Interrelationship between insulin resistance and impaired immune phenotype of circulating endothelial-derived microparticles in none-diabetic patients with chronic heart failure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander E Berezin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alexander A Kremzer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatyana A Samura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yulia V Martovitskaya <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatyana A Berezina </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 22, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="content " id="panelc">
                            <div class="row">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="Advances-in-chronotherapeutic-applications-for-prevention-and-treatment-of-cardiovascular-diseases.php	"> Advances in chronotherapeutic applications for prevention and treatment of cardiovascular diseases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hannah Cooke-Ariel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khalique Ahmed </p>
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 14, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------/JIC.1000117 ------>
                                <div class="article">
                                  <h4><a href="Why-measure-troponin-after-a-fall-in-the-elderly-Results-of-a-prospective-study-in-internal-medicine.php"> Why measure troponin after a fall in the elderly? Results of a prospective study in internal medicine </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Caroline Nguyen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sandrine Pruvot <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jean François. Bergmann <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guy Simoneau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Isabelle Mahé </p>
                                  <p class="mb5">Case Study-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 18, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------/JIC.1000117  x------>
                                <!------JIC.1000118------>
                                <div class="article">
                                  <h4><a href="Chronobiology-chronotherapy-and-timing-of-aspirin-ingestion.php"> Chronobiology, chronotherapy and timing of aspirin ingestion </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hannah Cooke-Ariel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sarah Wood <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Charles H. Hennekens </p>
                                  <p class="mb5">Editorial-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 21, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000118 x------>
                                <!------JIC.1000119------>
                                <div class="article">
                                  <h4><a href="Cocaine-inflicts-Silent-damage-to-the-heart-a-case-report.php"> Cocaine inflicts 'Silent' damage to the heart: a case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Massimo Bolognesi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Diletta Bolognesi </p>
                                  <p class="mb5">Case Report-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000119 x------>
                                <!------JIC.1000120------>
                                <div class="article">
                                  <h4><a href="Angiotensin-II-type-1-receptor-A1166C-gene-polymorphism-and-Essential-hypertension-in-Egypt.php"> Angiotensin II type 1 receptor (A1166C) gene polymorphism and Essential hypertension in Egypt </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marium M. Shamaa </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000120 x------>
                                <!------JIC.1000121 ------>
                                <div class="article">
                                  <h4><a href="The-association-of-serum-osteoprotegerin-sRANKL-complex-C-reactive-protein-and-adiponectin-with-number-of-circulating-endothelial-progenitor-cells-in-patients-with-metabolic-syndrome-and-diabetes-mellitus.php"> The association of serum osteoprotegerin/ sRANKL complex, C-reactive protein and adiponectin with number of circulating endothelial progenitor cells in patients with metabolic syndrome and diabetes mellitus </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alexander E. Berezin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alexander A. Kremzer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tatyana A Berezina <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Martovitskaya Yu.V. </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000121  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="paneld">
                            <div class="row">
                              <div class="small-12 columns">
                                <!------JIC.1000122------>
                                <div class="article">
                                  <h4><a href="Cardiovascular-disease-and-healthy-ageing.php"> Cardiovascular disease and healthy ageing </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kathleen M. Mooney<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Mark T. Mc Auley </p>
                                  <p class="mb5">Short Communication-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 16, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000122 x------>
                                <!------JIC.1000123 ------>
                                <div class="article">
                                  <h4><a href="An-insight-into-pathogenesis-of-cardiovascular-diseases.php"> An insight into pathogenesis of cardiovascular diseases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rakhshinda Parvin Zafar </p>
                                  <p class="mb5">Update-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 18, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000123  x------>
                                <!------JIC.1000124 ------>
                                <div class="article">
                                  <h4><a href="The-novel-mutations-of-GATA4-gene-in-Chinese-patients-with-sporadic congenital-heart-defects.php"> The novel mutations of GATA4 gene in Chinese patients with sporadic congenital heart defects </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jing-dong Ding<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Jiayi Tong<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Kairu Li<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Xiang Fang<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Hao Li<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Xiaoli Zhang<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> YuyuYao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Genshan Ma </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 29, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000124  x------>
                                <!------JIC.1000125 ------>
                                <div class="article">
                                  <h4><a href="Enhancement-of-cardiac-contractility-by-refractory-period-stimulation-in-conjunction-with-cardiac-resynchronization-therapy.php"> Enhancement of cardiac contractility by refractory period stimulation in conjunction with cardiac resynchronization therapy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Farong Shen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuanwei Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hongfeng Jin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gong Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Xiaohong Zhou </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000125  x------>
                                <!------JIC.1000126 ------>
                                <div class="article">
                                  <h4><a href="Dual-antiplatelet-therapy-after-coronary-artery-bypass-grafting-Do-we-have-a-consensus.php"> Dual antiplatelet therapy after coronary artery bypass grafting: Do we have a consensus? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emad Al Jaaly <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Zakkar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maria Pufulete <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Franco Ciulli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gianni D Angelini </p>
                                  <p class="mb5">Review Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000126  x------>
                                <!------JIC.1000127 ------>
                                <div class="article">
                                  <h4><a href="Immunonutrition-in-perioperative-phase-An-insight-into-the-integrated-treatment-of-cardiac-surgery-patients.php"> Immunonutrition in perioperative phase: An insight into the integrated treatment of cardiac surgery patients </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>De Santis L <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Maio D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cavone M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Serlenga E <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Minelli M </p>
                                  <p class="mb5">Research Article-
                                  <p>De Santis L, Maio D, Cavone M, Serlenga E, Minelli M (2015) Immunonutrition in perioperative phase: An insight into the integrated treatment of cardiac surgery patients. J Integr Cardiol, 1: DOI: 10.15761/JIC.1000127 </p>
                                  </p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 25, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000127  x------>
                                <!------JIC.1000128------>
                                <div class="article">
                                  <h4><a href="The-effect-of-Kolkolis-in-reducing-blood-pressure.php"> The effect of Kolkolis in reducing blood pressure </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dennis Glen Ramos <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sheena Vie Alimboyao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alhazel Dianne Dompiles <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emerald Gem Nazarro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nicole Mathews Pascua <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kristine Joy Pawid <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zandrex Reyes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Albert Vera </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 26, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000128 x------>
                                <!------JIC.1000129 ------>
                                <div class="article">
                                  <h4><a href="Predictive-factors-of-in-hospital-major-adverse-cardiac-events-and-no-reflow-phenomenon-in-patients-with-ST-elevation-myocardial-infarction-undergoing-primary-percutaneous-coronary-intervention.php"> Predictive factors of in hospital major adverse cardiac events and no reflow phenomenon in patients with ST elevation myocardial infarction undergoing primary percutaneous coronary intervention </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sherif Wagdy Ayad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Sobhy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amr Zaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amr Elkammash </p>
                                  <p class="mb5">Research Article-Journal of Integrative Cardiology (JIC)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------JIC.1000129  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr/>
              <p class="text-justify">JIC welcomes direct submissions of manuscripts from authors. You can submit your manuscript to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a> alternatively to: <a href="mailto:editor.jic@oatext.com">editor.jic@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <div id="publication-charge-set" class="pull-publication-charge-set"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<script src="js/vendor/jquery.js"></script>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
