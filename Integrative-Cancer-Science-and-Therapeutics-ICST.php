<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="google-site-verification" content="GRZVwJKdrXMzh98mqJZ25yBoTeCLw2OA05Pj4hC8o4s" />
<title>Cancer Journal | Cancer Journals | Cancer Journals in India - OATEXT</title>
<meta name="description" content="The cancer journal  is an important and reliable source of current information on developments in the field, cancer journal research updates is a peer reviewed journals on oatext " />
<meta name="keywords" content="cancer journal, cancer journals, cancer journals in india, cancer journals impact factor, cancer journals impact factor ranking, cancer journals impact factor list, cancer journal instructions for authors, cancer cell journal instructions for authors, cancer research journal, oatext">
<meta name="robots" content=" ALL, index, follow"/>
<meta name="distribution" content="Global" />
<meta name="rating" content="Safe For All" />
<meta name="language" content="English" />
<meta http-equiv="window-target" content="_top"/>
<meta http-equiv="pics-label" content="for all ages"/>
<meta name="rating" content="general"/>
<meta content="All, FOLLOW" name="GOOGLEBOTS"/>
<meta content="All, FOLLOW" name="YAHOOBOTS"/>
<meta content="All, FOLLOW" name="MSNBOTS"/>
<meta content="All, FOLLOW" name="BINGBOTS"/>
<meta content="all" name="Googlebot-Image"/>
<meta content="all" name="Slurp"/>
<meta content="all" name="Scooter"/>
<meta content="ALL" name="WEBCRAWLERS"/>
<link rel="canonical" href="Integrative-Cancer-Science-and-Therapeutics-ICST.php" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,400italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
<link rel="stylesheet" href="css/component.css" />
<link rel="stylesheet" href="css/foundation.css" />
<link rel="stylesheet" href="css/custom.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
<?php include("header.php"); ?>
<div class="row">
  <div class="small-12 columns">
    <hr class="mt0 green-hr-line"/>
  </div>
</div>
<!--banner -->
<div class="row">
  <div class="medium-12 text-center columns">
    <div class="inner-banner">
      <div id="typer"></div>
      <script src='js/jquery.typer.js'></script>
      <script>
            var win = $(window),
                foo = $('#typer');

            foo.typer(['Integrative Cancer Science and Therapeutics (ICST)']);

            // unneeded...
            win.resize(function(){
                var fontSize = Math.max(Math.min(win.width() / (1 * 10), parseFloat(Number.POSITIVE_INFINITY)), parseFloat(Number.NEGATIVE_INFINITY));

                foo.css({
                 //   fontSize: fontSize * .3 + 'px'
                });
            }).resize();
        </script>
    </div>
  </div>
</div>
<!--banner x -->
<!--CONTENT -->
<div class="inner-page">
  <div class="">
    <div class="custom-vertical">
      <div class="row">
        <div class="medium-3 columns">
          <dl data-options="deep_linking: true; scroll_to_content: false" data-tab="" class="tabs vertical">
            <dd class="active"><a  name="AnchorName"  href="#About_Journal"   class="anchor" >About Journal</a></dd>
            <dd ><a  name="AnchorName"  href="#Editor-in-Chief"   class="anchor" >Editor-in-Chief</a></dd>
            <dd><a  name="AnchorName"  href="#Editorial_Board"   class="anchor">Editorial Board</a></dd>
            <dd class="no-visible no-height no-border" ><a  name="AnchorName"  href="#a-1"   class="anchor "  >xxx</a></dd>
            <dd  class="no-visible no-height no-border"><a  name="AnchorName"  href="#a-2"   class="anchor" >yyy</a></dd>
            <dd><a  name="AnchorName" href="#Early_View"    class="anchor">Early View</a></dd>
            <dd><a  name="AnchorName" href="#Current_Issue"   class="anchor">Current Issue</a></dd>
            <dd><a  name="AnchorName" href="#Previous_Issue"   class="anchor" >Previous Issue</a></dd>
            <dd><a  name="AnchorName" href="#Archive"    class="anchor">Archive</a></dd>
            <dd><a  name="AnchorName" href="#External_Databases_Indexes" class="anchor">External Databases & Indexes</a></dd>
            <dd><a  name="AnchorName" href="#Submit_Manuscript"    class="anchor">Submit Manuscript</a></dd>
            <dd><a  name="AnchorName" href="#Publication_Charges"    class="anchor">Publication Charges</a></dd>
            <dd><a  name="AnchorName" href="#Special_Issues"    class="anchor">Special Issues</a></dd>
          </dl>
        </div>
        <div class="medium-9 columns">
          <div class="tabs-content">
            <div id="About_Journal" class="content active">
              <div class="vertical-tab-content">
                <h2 class="w600">Integrative Cancer Science and Therapeutics (ICST)</h2>
                <h4>Online ISSN: 2056-4546</h4>

                <h2 class="mb5"><a href="#Editor-in-Chief">(Founding Editors)</a></h2>
                <div class="row mt10">
                  <div class="medium-6 columns">
                    <h2 class="mb5"><a href="#a-1">Hiroshi Miyamoto</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> University of Rochester Medical Center</span> </div>
                  <!--<div class="medium-6 columns">
                    <h2 class="mb5"><a href="#a-2">Hiroshi Miyamoto</a></h2>
                    <span class="black-text"><i class="fa fa-university"></i> University of Rochester Medical Center</span> </div>-->
                </div>

                <hr>
                <p class="text-justify"><a target="_blank" href="img/unnamed-1.jpg"><img width="20%" align="right" alt="" class="pl20 pb10" src="img/unnamed-1.jpg"></a>Integrative Cancer Science and Therapeutics (ICST) is an open access, peer-reviewed online journal interested in attracting high-quality original research and reviews that present or highlight significant advances in all areas of cancer and related biomedical science. The Journal is concerned with basic, translational and clinical research, across different disciplines and areas, enhancing insight in understanding, prevention, diagnosis and treatment of cancer. The journal also prioritizes clinical trials evaluating new treatments, accompanied by research on pharmacology and molecular alterations or biomarkers that predict response or resistance to treatment.</p>
                <p class="text-justify">Integrative Cancer Science and Therapeutics welcomes submission of papers both at the molecular, subcellular, cellular, organ, and organism level, and of clinical proof-of-concept and translational studies. Topics of interest include, but are not limited to: novel basic cancer research discoveries, cell and tumor biology, animal models, metastasis, cancer antigens and the immune response to them, cellular signalling and molecular biology, epidemiology, genetic and molecular profiling of cancer and molecular targets, cancer stem cells, cancer-associated pathways (including cell-cycle regulation; cell death; chromatin regulation; DNA damage and repair; gene and RNA regulation; genomics; oncogenes and tumor suppressors; and signal transduction), molecular virology and vaccine- and antibody-based cancer therapies, and other cancer-related research.</p>
                <p class="text-justify">Integrative Cancer Science and Therapeutics provides an important forum for exchange of ideas, concepts and findings in any area of cancer and related biomedical science. The online appearance of 
                  Integrative Cancer Science and Therapeutics
                  allows the immediate publication of accepted articles and the presentation of large amounts of data and supplemental information to ensure that new research is disseminated as efficiently and quickly as possible to the scientific community.</p>
                <p class="text-justify">Manuscripts may be submitted as Original Articles, Rapid and Short Communications, or Reviews, Mini-review, Case Reports, Opinion, Letters to the Editor and Editorials. Integrative Cancer Science and Therapeutics is proud of its fast Reviewing and Editorial Decision system. Manuscripts are normally evaluated by three members from an international panel of reviewers and, in most cases, we provide a first editorial decision within 21 days of receipt.</p>
                <p>ICST welcomes direct submissions of manuscripts from authors. You can submit your manuscript to : <a href="mailto:submissions@oatext.com">submissions@oatext.com</a>; alternatively to : <a href="mailto:masayoshi@oatext.com">masayoshi@oatext.com</a></p>
              </div>
             </div>
            <div id="Editor-in-Chief" class="content">
                <h2>Hiroshi Miyamoto</h2>
              <hr/>
              <p class="text-justify"> <img align="right" class="profile-pic" alt="" src="img/ICST-Hiroshi-Miyamoto.jpg">Hiroshi Miyamoto, MD, PhD, completed his medical school and urology residency training followed by clinical urology practice and translational research in genitourinary cancers in Japan. In 1996, he moved to the United States to conduct postdoctoral research at University of Wisconsin - Madison and University of Rochester. He then completed anatomic pathology residency training at University of Rochester Medical Center and clinical fellowship in urologic pathology at The Johns Hopkins Hospital. Since 2009, he has been the faculty as a surgical pathologist as well as an independent investigator at University of Rochester School of Medicine and Dentistry (2009-2013 &amp; 2016-present) and Johns Hopkins University School of Medicine (2013-2016). He is currently the Director of Genitourinary Pathology at University of Rochester Medical Center.</p>

              <p class="text-justify"><a name="_GoBack"></a>Dr. Miyamoto&rsquo;s primary research interest includes molecular biology of steroid hormone receptors in genitourinary tumors. Specifically, he has been investigating the role of androgen receptor signals in the development and progression of urothelial cancer. Epidemiological and clinical data indicate that men have a substantially higher risk of bladder cancer, whereas women tend to present with more aggressive tumors. The underlying mechanisms of how androgens regulate urothelial tumorigenesis and tumor outgrowth will offer explanations for these gender-specific differences in cancer incidence and aggressiveness. He has also assessed the effects of new classes of androgen receptor antagonists on prostate cancer progression and has characterized novel androgen receptor co-regulators in prostate cancer cells. He has published more than 160 peer-reviewed articles and 17 book chapters.</p>

            </div>
            

               <div id="a-1" class="content">                 
                 <h2>Hiroshi Miyamoto</h2>
              <hr/>
              <p class="text-justify"> <img align="right" class="profile-pic" alt="" src="img/ICST-Hiroshi-Miyamoto.jpg">Hiroshi Miyamoto, MD, PhD, completed his medical school and urology residency training followed by clinical urology practice and translational research in genitourinary cancers in Japan. In 1996, he moved to the United States to conduct postdoctoral research at University of Wisconsin - Madison and University of Rochester. He then completed anatomic pathology residency training at University of Rochester Medical Center and clinical fellowship in urologic pathology at The Johns Hopkins Hospital. Since 2009, he has been the faculty as a surgical pathologist as well as an independent investigator at University of Rochester School of Medicine and Dentistry (2009-2013 &amp; 2016-present) and Johns Hopkins University School of Medicine (2013-2016). He is currently the Director of Genitourinary Pathology at University of Rochester Medical Center.</p>

              <p class="text-justify"><a name="_GoBack"></a>Dr. Miyamoto&rsquo;s primary research interest includes molecular biology of steroid hormone receptors in genitourinary tumors. Specifically, he has been investigating the role of androgen receptor signals in the development and progression of urothelial cancer. Epidemiological and clinical data indicate that men have a substantially higher risk of bladder cancer, whereas women tend to present with more aggressive tumors. The underlying mechanisms of how androgens regulate urothelial tumorigenesis and tumor outgrowth will offer explanations for these gender-specific differences in cancer incidence and aggressiveness. He has also assessed the effects of new classes of androgen receptor antagonists on prostate cancer progression and has characterized novel androgen receptor co-regulators in prostate cancer cells. He has published more than 160 peer-reviewed articles and 17 book chapters.</p>
              </div>

              <div id="a-2" class="content"></div>



            <div id="Editorial_Board" class="content">
              <div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10">Editorial Board</h2>
                </div>
              </div>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Yu-Jui Yvonne Wan</h4>
                  <p>Professor and Vice Chair of Research<br>
                    Department of Medical Pathology & Laboratory Medicine<br>
                    University of California, Davis Medical Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Glen J. Weiss</h4>
                  <p>Clinical Associate Professor<br>
                    Translational Genomics Research Institute (TGen)<br>
                    Clinical Assistant Professor of Medicine<br>
                    The University of Arizona College of Medicine-Phoenix<br>
                    Director of Clinical Research<br>
                    Cancer Treatment Centers of America<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ben Warner</h4>
                  <p>Clinical Associate Professor<br>
                    Department of General Practice and Dental Public Health<br>
                    The University of Texas</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Rajagopal Ramesh</h4>
                  <p>Professor of Pathology<br>
                    Director, Experimental Therapeutics and Translational Cancer Medicine<br>
                    University of Oklahoma Health Sciences Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Donald J. Alcendor</h4>
                  <p>Assistant Professor of Cancer Biology<br>
                    Meharry Medical College<br>
                    Adjunct Assistant Professor of Cancer Biology<br>
                    Vanderbilt School of Medicine</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Toru Ouchi</h4>
                  <p>Professor of Oncology<br>
                    Department of Cancer Genetics<br>
                    Roswell Park Cancer Institute</p>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Arun K. Rishi</h4>
                  <p>Professor<br>
                    Department of Oncology and Internal Medicine<br>
                    Karmanos Cancer Institute<br>
                    Wayne State University<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Moshe Talpaz</h4>
                  <p>Associate Chief<br>
                    Division of Hematology/Oncology<br>
                    University of Michigan Health System</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Ming Zhang</h4>
                  <p>Associate Professor<br>
                    Northwestern University Feinberg School of Medicine<br>
                    Department of Biochemistry<br>
                    Robert H. Lurie Comprehensive Cancer Center, and Center for Genetic Medicine</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Vicente Notario</h4>
                  <p>Professor<br>
                    Department of Radiation Medicine<br>
                    Leader, Molecular Oncology Program<br>
                    Lombardi Comprehensive Cancer Center<br>
                    Georgetown University Medical Center<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Charlie Ma</h4>
                  <p>Professor
                    Vice Chairman, Department of Radiation Oncology<br>
                    Director, Radiation Physics<br>
                    Fox Chase Cancer Center<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Pranela Rameshwar</h4>
                  <p>Professor<br>
                    Medicine-Hematology/Oncology<br>
                    Rutgers-New Jersey Medical School</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Awad Shamma</h4>
                  <p>Division of Oncology and Molecular Biology, <br>
                    Cancer and Stem Cell Research Program <br>
                    Kanazawa University Cancer Research Institute </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Rebecca B. Riggins</h4>
                  <p>Assistant Professor<br>
                    Breast Cancer Program<br>
                    Lombardi Comprehensive Cancer Center<br>
                    Georgetown University Medical Center</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Lucio Miele</h4>
                  <p>Professor of Medicine and Director for Inter-Institutional Programs<br>
                    LSU Stanley Scott Cancer Center and Louisiana Cancer Research Consortium<br>
                  </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Leonidas G. Koniaris</h4>
                  <p>Professor of Surgery (Surgical Oncology)<br>
                    Vice Chair of Research<br>
                    IU Health University Hospital, Surgery OP Clinic<br>
                    Indiana University </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Alfio Ferlito </h4>
                  <p>Director of the Department of Surgical Sciences<br>
                    Professor and Chairman of the ENT Clinic <br>
                    University of Udine School of Medicine </p>
                </div>
                <div class="medium-4 columns">
                  <h4>
                  Masakazu Toi
                  <h4>
                  <p>Professor<br>
                    Breast Surgery<br>
                    Kyoto University</p>
                </div>
              </div>
              <!--<div class="row">
                <div class="medium-12 columns">
                  <h2 class="mb30 blue-bg white-text p10">Section Editors- Lung Cancer</h2>
                </div>
              </div> -->
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Kazuya Fukuoka</h4>
                  <p>Associate Professor
                    Department of Medical Oncology, Sakai Hospital, 
                    Kinki University Faculty of Medicine, Japan<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Makoto Takahama</h4>
                  <p>Director and Chairman
                    Department of General Thoracic Surgery
                    Osaka City General Hospital, Japan<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Jitsuo Usuda</h4>
                  <p>Professor, Department of Thoracic Surgery
                    Nippon Medical School, Japan<br>
                  </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Yoshitane Tsukamoto</h4>
                  <p>Associate Professor<br>
                    Department of Surgical Pathology<br>
                    Hyogo College of Medicine, Japan<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Yasuhiro Koh</h4>
                  <p>Assistant Professor<br>
                    Third Department of Internal Medicine<br>
                    Wakayama Medical University, Japan<br>
                  </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Akito Hata</h4>
                  <p>Deputy Head Physician<br>
                    Division of Integrated Oncology<br>
                    Institute of Biomedical Research and Innovation,  Japan<br>
                  </p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Kazushi Inoue</h4>
                  <p>Associate Professor<br>
                    Pathology - Tumor Biology<br>
                    Wake Forest University School of Medicine<br>
                    USA</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Michael J Gonzalez</h4>
                  <p>Professor<br>
                    University of Puerto Rico Medical Sciences Campus<br>
                    School of Public Health<br>
                    Department of Human Development<br>
                    Puerto Rico</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Canan Hascicek </h4>
                  <p>Associate Professor<br>
                    Department of Pharmaceutical Technology<br>
                    Ankara University<br>
                    Turkey</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Yang Xueqin</h4>
                  <p>Cancer Center, Institute of Surgery Research and Daping Hospital<br>
                    Third Military Medical University<br>
                    China</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Dr. Tanja Khosrawipour</h4>
                  <p>Department for General and special visceral surgery<br>
                    Center for Peritoneal Carcinomatosis<br>
                    University Hospital of the Ruhr-University Bochum<br>
                    Germany</p>
                </div>
                <div class="medium-4 columns">
                  <h4>Antonio Herreros Martínez</h4>
                  <p>Associate Professor<br>
                    Department of Basic Clinical Practice<br>
                    Faculty of Medicine<br>
                    Universitat de Barcelona<br>
                    Spain</p>
                </div>
              </div>
              <hr/>
              <div class="row">
                <div class="medium-4 columns">
                  <h4>Ying-Yu Cui</h4>
                  <p>Associte Professor<br>
                    Department of Regenerative Medicine<br>
                    Tongji University School of Medicine<br>
                    China </p>
                </div>
                <div class="medium-4 columns">
                  <h4>Hiroshi Miyamoto</h4>
                  <p>Director<br> 
                      Genitourinary Section<br>
                      University of Rochester Medical Center<br>
                      USA</p>
                </div>
                <div class="medium-4 columns"></div>
              </div>
              <div class="row">
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
                <div class="medium-4 columns"></div>
              </div>
            </div>
            <div id="Early_View" class="content">
              <h2>Early View</h2>
              <h4>Volume 4, Issue 3</h4>
              <hr>
              <p>Wil be updated soon.</p>
            </div>
            <div id="Current_Issue" class="content">
              <h2>Current Issue</h2>
              <h4>Volume 4, Issue 2</h4>
              <hr>
              <!------ICST.1000231------>
              <div class="article">
                <h4><a href="Renin-angiotensin-system-and-cancer-A-review.php">Renin-angiotensin system and cancer: A review</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Matthew J Munro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Agadha C Wickremesekera
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul F Davis
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Reginald Marsh
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Swee T Tan
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tinte Itinteang</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 29, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000231 x------>
              <!------ICST.1000232------>
              <div class="article">
                <h4><a href="The-expression-of-HMGB1-and-the-receptor-for-advanced-glycation-end-products-RAGE-in-epithelial-ovarian-cancer.php"> The expression of HMGB1 and the receptor for advanced glycation end products (RAGE) in epithelial ovarian cancer </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Jiheum Paek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Young Tae Kim </p>
                <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 03, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000232 x------>
              <!------ICST.1000233.------>
              <div class="article">
                <h4><a href="Lifestyle-factors-and-the-risk-of-kidney-and-bladder-cancer-in-the-Japanese-population-A-mini-review.php"> Lifestyle factors and the risk of kidney and bladder cancer in the Japanese population: A mini-review </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masakazu Washio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuru Mori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akiko Tamakoshi </p>
                <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 24, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000233. x------>
              <!------ICST.1000234.------>
              <div class="article">
                <h4><a href="Current-experience-in-intraocular-fine-needle-aspiration-biopsy-in-Mexican-Mestizo-population.php">Current experience in intraocular fine needle aspiration biopsy in Mexican-Mestizo population</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Daniel Moreno-Páramo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marcelo Baizabal Castro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fernando Pérez-Pérez
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ivette Hernández-Ayuso
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dolores Ríos y Valles-Valles
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leonora Chávez-Mercado
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abelardo A. Rodríguez-Reyes </p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 24, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000234. x------>
              <!------ICST.1000235.------>
              <div class="article">
                <h4><a href="Nano-drug-delivery-systems-for-ovarian-cancer-therapy.php">Nano drug delivery systems for ovarian cancer therapy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Canan Hascicek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ozge Gun</p>
                <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 25, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000235. x------>
              <!------ICST.1000236.------>
              <div class="article">
                <h4><a href="Tumor-heterogeneity-An-overlooked-issue-in-cancer-therapeutics.php">Tumor heterogeneity: An overlooked issue in cancer therapeutics</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomas Koltai</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 25, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000236. x------>
              <!------ICST.1000237.------>
              <div class="article">
                <h4><a href="Oxazaphosphorine-cytostatics-A-good-way-to-create-better-is-to-study-old-oxazaphosphorines.php">Oxazaphosphorine cytostatics: A good way to create better is to study old oxazaphosphorines</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Georg Voelcker</p>
                <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 26, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000237. x------>
              <!------ICST.1000238.------>
              <div class="article">
                <h4><a href="RIG-I-A-double-edged-sword-between-inflammation-and-cancer.php">RIG-I: A double-edged sword between inflammation and cancer</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying-Yu Cui</p>
                <p class="mb5">Editorial-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 28, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000238. x------>
            </div>
            <div id="Previous_Issue" class="content">
              <h2>Previous Issue</h2>
              <h4>Volume 4, Issue 1</h4>
              <hr>
              <!------ICST.1000222.------>
              <div class="article">
                <h4><a href="Cancer-caused-by-UV-radiation-from-nanoparticles-in-GM-food.php">Cancer caused by UV radiation from nanoparticles in GM food</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Thomas Prevenslik</p>
                <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 14, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000222. x------>
              <!------ICST.1000223.------>
              <div class="article">
                <h4><a href="A-review-of-the-impact-of-obesity-on-common-gastrointestinal-malignancies.php">A review of the impact of obesity on common gastrointestinal malignancies</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Somashekar G. Krishna <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hisham Hussan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zobeida Cruz-Monserrate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lanla F. Conteh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khalid Mumtaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Darwin L. Conwell</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 18, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000223. x------>
              <!------ICST.1000224.------>
              <div class="article">
                <h4><a href="Molecularly-confirmed-Li-Fraumeni-like-syndrome-in-a-patient-with-breast-cancer-and-a-low-pre-test-probability-for-harboring-a-germline-CHEK2-truncation.php">Molecularly confirmed Li-Fraumeni-like syndrome in a patient with breast cancer and a low pre-test probability for harboring a germline CHEK2 truncation</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Steven Sorscher</p>
                <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000224. x------>
              <!------ICST.1000225.------>
              <div class="article">
                <h4><a href="Combined-immunoglobulin-G-kappa-nephropathy-monoclonal-immunoglobulin-deposition-disease-and-proximal-tubulopathy-monoclonal-gammopathy-of-renal-significance-or-smoldering-multiple-myeloma-Case-report-and-review-of-lit.php">Combined immunoglobulin G kappa nephropathy: monoclonal immunoglobulin deposition disease and proximal tubulopathy: monoclonal gammopathy of renal significance or smoldering multiple myeloma? Case report and review of literature</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elena V Zakharova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ekaterina S Stolyarevich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Olga A Vorobyeva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Eugeny A Nikitin</p>
                <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000225. x------>
              <!------ICST.1000226------>
              <div class="article">
                <h4><a href="Long-term-administration-of-Active-Hexose-Correlated-Compound-as-a-dietary-supplement-to-a-patient-after-breast-cancer-surgery-and-chemotherapy-A-case-report.php">Long-term administration of Active Hexose Correlated Compound as a dietary supplement to a patient after breast cancer surgery and chemotherapy: A case report</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Wakame <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Takanari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Atsuya Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Satomi Shirakawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ken-ichi Komatsu</p>
                <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000226 x------>
              <!------ICST.1000227------>
              <div class="article">
                <h4><a href="Ewing-sarcoma-of-the-testis-A-case-report-and-review-of-the-literature.php">Ewing sarcoma of the testis: A case report and review of the literature</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Javier Peinado-Serrano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>David Chinchón-Espino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michele Biscuola <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria José Ortiz-Gordillo</p>
                <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000227 x------>
              <!------ICST.1000228------>
              <div class="article">
                <h4><a href="BRCA1-and-BRCA2-mutations-and-treatment-strategies-for-breast-cancer.php">BRCA1 and BRCA2 mutations and treatment strategies for breast cancer</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Inês Godet <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Daniele M. Gilkes</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000228 x------>
              <!------ICST.1000229------>
              <div class="article">
                <h4><a href="Pure-nongestational-ovarian-choriocarcinoma-A-scoping-review.php">Pure nongestational ovarian choriocarcinoma: A scoping review</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alison Garrett <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Amy Knehans <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rebecca Phaeton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Joshua P. Kesterson</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000229 x------>
              <!------ICST.1000230------>
              <div class="article">
                <h4><a href="Cavernous-hemangioma-arising-in-an-intramural-leiomyoma.php">Cavernous hemangioma arising in an intramural leiomyoma</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zeliha Esin Celik <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Senem Bastoklu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tolgay Tuyan Ilhan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cetin Celik</p>
                <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000230 x------>
            </div>
            <div id="Archive" class="content">
              <h2>Archive</h2>
              <hr>
              <div class="lee">
                <dl data-accordion="" class="accordion">
                  <dd> <a href="#panel1e" class="accordian-active">Volume 4<span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1e">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <!--<li class="tab-title"><a href="#panelc"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>July 2017</span><span class="mt3 grey-text">Issue 3</span> </a></li>-->
                          <li class="tab-title active"><a href="#panelb"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>April 2017</span><span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"><span><i class="fa small-font pr5 fa-calendar lite-text"></i>February 2017</span><span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <!--<div class="content" id="panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                CCC
                              </div>
                            </div>
                          </div>-->

                          <div class="content active" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000231------>
              <div class="article">
                <h4><a href="Renin-angiotensin-system-and-cancer-A-review.php">Renin-angiotensin system and cancer: A review</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Matthew J Munro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Agadha C Wickremesekera
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul F Davis
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Reginald Marsh
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Swee T Tan
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tinte Itinteang</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 29, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000231 x------>
              <!------ICST.1000232------>
              <div class="article">
                <h4><a href="The-expression-of-HMGB1-and-the-receptor-for-advanced-glycation-end-products-RAGE-in-epithelial-ovarian-cancer.php"> The expression of HMGB1 and the receptor for advanced glycation end products (RAGE) in epithelial ovarian cancer </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Jiheum Paek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Young Tae Kim </p>
                <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 03, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000232 x------>
              <!------ICST.1000233.------>
              <div class="article">
                <h4><a href="Lifestyle-factors-and-the-risk-of-kidney-and-bladder-cancer-in-the-Japanese-population-A-mini-review.php"> Lifestyle factors and the risk of kidney and bladder cancer in the Japanese population: A mini-review </a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masakazu Washio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mitsuru Mori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Akiko Tamakoshi </p>
                <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 24, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000233. x------>
              <!------ICST.1000234.------>
              <div class="article">
                <h4><a href="Current-experience-in-intraocular-fine-needle-aspiration-biopsy-in-Mexican-Mestizo-population.php">Current experience in intraocular fine needle aspiration biopsy in Mexican-Mestizo population</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Daniel Moreno-Páramo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marcelo Baizabal Castro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fernando Pérez-Pérez
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ivette Hernández-Ayuso
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dolores Ríos y Valles-Valles
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leonora Chávez-Mercado
                <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abelardo A. Rodríguez-Reyes </p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 24, 2017 </p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000234. x------>
              <!------ICST.1000235.------>
              <div class="article">
                <h4><a href="Nano-drug-delivery-systems-for-ovarian-cancer-therapy.php">Nano drug delivery systems for ovarian cancer therapy</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Canan Hascicek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ozge Gun</p>
                <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 25, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000235. x------>
              <!------ICST.1000236.------>
              <div class="article">
                <h4><a href="Tumor-heterogeneity-An-overlooked-issue-in-cancer-therapeutics.php">Tumor heterogeneity: An overlooked issue in cancer therapeutics</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomas Koltai</p>
                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 25, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000236. x------>
              <!------ICST.1000237.------>
              <div class="article">
                <h4><a href="Oxazaphosphorine-cytostatics-A-good-way-to-create-better-is-to-study-old-oxazaphosphorines.php">Oxazaphosphorine cytostatics: A good way to create better is to study old oxazaphosphorines</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Georg Voelcker</p>
                <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 26, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000237. x------>
              <!------ICST.1000238.------>
              <div class="article">
                <h4><a href="RIG-I-A-double-edged-sword-between-inflammation-and-cancer.php">RIG-I: A double-edged sword between inflammation and cancer</a></h4>
                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ying-Yu Cui</p>
                <p class="mb5">Editorial-Integrative Cancer Science and Therapeutics (ICST)</p>
                <em>
                <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 28, 2017</p>
                </em>
                <hr/>
              </div>
              <!------ICST.1000238. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content " id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000222.------>
                                <div class="article">
                                  <h4><a href="Cancer-caused-by-UV-radiation-from-nanoparticles-in-GM-food.php">Cancer caused by UV radiation from nanoparticles in GM food</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Thomas Prevenslik</p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 14, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000222. x------>
                                <!------ICST.1000223.------>
                                <div class="article">
                                  <h4><a href="A-review-of-the-impact-of-obesity-on-common-gastrointestinal-malignancies.php">A review of the impact of obesity on common gastrointestinal malignancies</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Somashekar G. Krishna <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hisham Hussan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zobeida Cruz-Monserrate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lanla F. Conteh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khalid Mumtaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Darwin L. Conwell</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 18, 2017 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000223. x------>
                                <!------ICST.1000224.------>
                                <div class="article">
                                  <h4><a href="Molecularly-confirmed-Li-Fraumeni-like-syndrome-in-a-patient-with-breast-cancer-and-a-low-pre-test-probability-for-harboring-a-germline-CHEK2-truncation.php">Molecularly confirmed Li-Fraumeni-like syndrome in a patient with breast cancer and a low pre-test probability for harboring a germline CHEK2 truncation</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Steven Sorscher</p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000224. x------>
                                <!------ICST.1000225.------>
                                <div class="article">
                                  <h4><a href="Combined-immunoglobulin-G-kappa-nephropathy-monoclonal-immunoglobulin-deposition-disease-and-proximal-tubulopathy-monoclonal-gammopathy-of-renal-significance-or-smoldering-multiple-myeloma-Case-report-and-review-of-lit.php">Combined immunoglobulin G kappa nephropathy: monoclonal immunoglobulin deposition disease and proximal tubulopathy: monoclonal gammopathy of renal significance or smoldering multiple myeloma? Case report and review of literature</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elena V Zakharova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ekaterina S Stolyarevich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Olga A Vorobyeva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Eugeny A Nikitin</p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 17, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000225. x------>
                                <!------ICST.1000226------>
                                <div class="article">
                                  <h4><a href="Long-term-administration-of-Active-Hexose-Correlated-Compound-as-a-dietary-supplement-to-a-patient-after-breast-cancer-surgery-and-chemotherapy-A-case-report.php">Long-term administration of Active Hexose Correlated Compound as a dietary supplement to a patient after breast cancer surgery and chemotherapy: A case report</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Koji Wakame <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jun Takanari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Atsuya Sato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Satomi Shirakawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ken-ichi Komatsu</p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000226 x------>
                                <!------ICST.1000227------>
                                <div class="article">
                                  <h4><a href="Ewing-sarcoma-of-the-testis-A-case-report-and-review-of-the-literature.php">Ewing sarcoma of the testis: A case report and review of the literature</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Javier Peinado-Serrano <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>David Chinchón-Espino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michele Biscuola <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Maria José Ortiz-Gordillo</p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000227 x------>
                                <!------ICST.1000228------>
                                <div class="article">
                                  <h4><a href="BRCA1-and-BRCA2-mutations-and-treatment-strategies-for-breast-cancer.php">BRCA1 and BRCA2 mutations and treatment strategies for breast cancer</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Inês Godet <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Daniele M. Gilkes</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 27, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000228 x------>
                                <!------ICST.1000229------>
                                <div class="article">
                                  <h4><a href="Pure-nongestational-ovarian-choriocarcinoma-A-scoping-review.php">Pure nongestational ovarian choriocarcinoma: A scoping review</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Alison Garrett <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Amy Knehans <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rebecca Phaeton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Joshua P. Kesterson</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000229 x------>
                                <!------ICST.1000230------>
                                <div class="article">
                                  <h4><a href="Cavernous-hemangioma-arising-in-an-intramural-leiomyoma.php">Cavernous hemangioma arising in an intramural leiomyoma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zeliha Esin Celik <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Senem Bastoklu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tolgay Tuyan Ilhan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Cetin Celik</p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>February 28, 2017</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000230 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1d" class="accordian-active">Volume 3 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1d">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 2016</span> <span class="mt3 grey-text">Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Oct 2016</span> <span class="mt3 grey-text">Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 2016</span> <span class="mt3 grey-text">Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> June 2016</span> <span class="mt3 grey-text">Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Apr 2016</span> <span class="mt3 grey-text">Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Feb 2016</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000212.------>
                                <div class="article">
                                  <h4><a href="Electrical-impedance-spectroscopy-for-breast-cancer-diagnosis-Clinical-study.php">Electrical impedance spectroscopy for breast cancer diagnosis: Clinical study</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Z. Haeri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>M. Shokoufi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>M. Jenab <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>R. Janzen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>F.Golnaraghi</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 16, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000212. x------>
                                <!------ICST.1000213.------>
                                <div class="article">
                                  <h4><a href="Historical-highlights-on-cancer-invading-the-portal-vein.php">Historical highlights on cancer invading the portal vein</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wilson I. B. Onuigbo</p>
                                  <p class="mb5">Opinion-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 19, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000213. x------>
                                <!------ICST.1000214.------>
                                <div class="article">
                                  <h4><a href="Cytokines-and-immune-response-in-vasculitis-of-peripheral-nervous-system-Tumor-necrosis-factor-alpha-and-pro-inflammatory-cytokines-in-vaculitis-of-peripheral-nervous-system-A-comparative-immunohistochemical-study-on-s.php">Cytokines and immune response in vasculitis of peripheral nervous system: Tumor necrosis factor-alpha and pro-inflammatory cytokines in vaculitis of peripheral nervous system: A comparative immunohistochemical study on systemic and non-systemic vasculitic neuropathy</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Giovanni Antioco Putzu</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000214. x------>
                                <!------ICST.1000215.------>
                                <div class="article">
                                  <h4><a href="Cardiac-toxicities-associated-with-cancer-therapy.php">Cardiac toxicities associated with cancer therapy</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Eric Chang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sanjay R Jain</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000215. x------>
                                <!------ICST.1000216.------>
                                <div class="article">
                                  <h4><a href="Exploration-of-food-materials-and-components-showing-the-suppressing-effect-on-absorption-of-strontium-and-the-promoting-effect-on-excretion-of-cesium-in-vivo.php">Exploration of food materials and components showing the suppressing effect on absorption of strontium and the promoting effect on excretion of cesium in vivo</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuya Uehara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kana Ishizuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuko Shimamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michiko Yasuda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kayoko Shimoi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shuichi Masuda</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000216. x------>
                                <!------ICST.1000217.------>
                                <div class="article">
                                  <h4><a href="Large-cell-neuroendocrine-carcinoma-LCNEC-of-the-ovary-A-case-report-and-review-of-the-literature.php"> Large cell neuroendocrine carcinoma (LCNEC) of the ovary: A case report and review of the literature </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Allyson Jang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jordan M. Newell <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rebecca Phaeton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joshua P. Kesterson </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000217. x------>
                                <!------ICST.1000218. ------>
                                <div class="article">
                                  <h4><a href="An-intracranial-dermoid-tumour-with-large-right-parietal-infarct-A-rare-case-report.php"> An intracranial dermoid tumour with large right parietal infarct- A rare case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sarkar S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Islam MM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Avijit D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hossen MK <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dasgupta A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Saha S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sazib SMF <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Khan MN </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000218.  x------>
                                <!------ICST.1000219.------>
                                <div class="article">
                                  <h4><a href="High-dose-intravenous-vitamin-c-and-metastatic-pancreatic-cancer-Two-cases.php"> High dose intravenous vitamin c and metastatic pancreatic cancer: Two cases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Michael J. González <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miguel J. Berdiel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jorge R Miranda-Massari <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Desireé López <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jorge Duconge <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Joshua L. Rodriguez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pedro Adrover c </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000219. x------>
                                <!------ICST.1000220.------>
                                <div class="article">
                                  <h4><a href="Thin-melanoma-and-sentinel-lymph-node-biopsy-A-difficult-relationship-between-them.php"> Thin melanoma and sentinel lymph node biopsy: A difficult relationship between them </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>JC Rodriguez Otero <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R Fernández Bussy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>A Bergero <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>M Gorosito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>G Salerni <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>MS Dagatti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R Villavicencio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R Staffieri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>SM Batallés <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>V Milatich <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>J Rodriguez Otero <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>SM Pezzotto </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000220. x------>
                                <!------ICST.1000221.------>
                                <div class="article">
                                  <h4><a href="Microenvironment-and-its-role-in-acquiring-drug-resistance-of-leukemia-treatment.php"> Microenvironment and its role in acquiring drug resistance of leukemia treatment </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Malak Yahia Mohammed Qattan </p>
                                  <p class="mb5">Mini- Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Dec 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000221. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000204.------>
                                <div class="article">
                                  <h4><a href="M2-like-macrophages-and-tumor-associated-macrophages-overlapping-and-distinguishing-properties-en-route-to-a-safe-therapeutic-potential.php">M2-like macrophages and tumor-associated macrophages: overlapping and distinguishing properties en route to a safe therapeutic potential</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ofer Guttman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Eli C. Lewis</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000204. x------>
                                <!------ICST.1000205.------>
                                <div class="article">
                                  <h4><a href="Multicentral-meningothelial-meningioma-a-case-of-death.php">Multicentral meningothelial meningioma: a case of death</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mansueto Gelsomina <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emanuele Capasso <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Antonio Lombardi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pieri Maria <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Claudio Buccelli</p>
                                  <p class="mb5">Case Report-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000205. x------>
                                <!------ICST.1000206.------>
                                <div class="article">
                                  <h4><a href="Should-we-continue-intra-peritoneal-chemotherapy-in-advanced-ovarian-cancer-patients.php">Should we continue intra-peritoneal chemotherapy in advanced ovarian cancer patients?</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gary Altwerger <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gulden Menderes <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jonathan D. Black <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Masoud Azodi</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000206. x------>
                                <!------ICST.1000207.------>
                                <div class="article">
                                  <h4><a href="The-clinical-characteristics-of-oral-squamous-cell-carcinoma-in-patients-attending-the-Medunsa-Oral-Health-Centre,-South-Africa.php">The clinical characteristics of oral squamous cell carcinoma in patients attending the Medunsa Oral Health Centre, South Africa</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bouckaert M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Munzhelele TI <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lemmer J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Khammissa RAG</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 12, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000207. x------>
                                <!------ICST.1000208.------>
                                <div class="article">
                                  <h4><a href="Distinguishing-malignant-from-benign-prostate-using-content-of-17-chemical-elements-in-prostatic-tissue.php">Distinguishing malignant from benign prostate using content of 17 chemical elements in prostatic tissue</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vladimir Zaichick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sofia Zaichick</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 22, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000208. x------>
                                <!------ICST.1000209.------>
                                <div class="article">
                                  <h4><a href="Choledochal-cysts-Classification-physiopathology,-and-clinical-course.php">Choledochal cysts- Classification, physiopathology, and clinical course</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Fatih Altıntoprak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mustafa Yener Uzunoğlu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Enis Dikicier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>İsmail Zengin</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 24, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000209. x------>
                                <!------ICST.1000210.------>
                                <div class="article">
                                  <h4><a href="Clinical-applications-of-mouse-models-for-breast-cancer-engaging-HER2-neu.php">Clinical applications of mouse models for breast cancer engaging HER2/neu</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Elizabeth A. Fry <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Pankaj Taneja <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kazushi Inoue</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000210. x------>
                                <!------ICST.1000211.------>
                                <div class="article">
                                  <h4><a href="A-review-of-invasive-lobular-carcinoma-of-the-breast-Should-it-be-treated-like-invasive-ductal-carcinoma.php">A review of invasive lobular carcinoma of the breast: Should it be treated like invasive ductal carcinoma?</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lee Joycelyn Jie Xin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lee Guek Eng </p>
                                  <p class="mb5">Mini Review-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000211. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000194.------>
                                <div class="article">
                                  <h4><a href="Lymphorrhea-in-gastric-cancer-patients.php"> Lymphorrhea in gastric cancer patients</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Bandar Ali</p>
                                  <p class="mb5">Short Communication-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000194. x------>
                                <!------ICST.1000195.------>
                                <div class="article">
                                  <h4><a href="Application-of-molecular-targeted-therapy-for-refractory-metastatic-thyroid-cancers.php"> Application of molecular targeted therapy for refractory metastatic thyroid cancers</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Timothy Allen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lee Meller</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 17, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000195. x------>
                                <!------ICST.1000196.------>
                                <div class="article">
                                  <h4><a href="The-role-of-myeloid-derived-suppressor-cells-in-the-relationship-between-chronic-obstructive-pulmonary-disease-and-lung-cancer.php"> The role of myeloid-derived suppressor cells in the relationship between chronic obstructive pulmonary disease and lung cancer</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sergio Scrimini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jaume Pons <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jaume Sauleda</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000196. x------>
                                <!------ICST.1000197.------>
                                <div class="article">
                                  <h4><a href="Characterizing-heterogeneic-tumor-profiles-with-an-integrated-companion-diagnostics-approach.php"> Characterizing heterogeneic tumor profiles with an integrated companion diagnostics approach</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sergio Scrimini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jaume Pons <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jaume Sauleda</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 01, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000197. x------>
                                <!------ICST.1000198.------>
                                <div class="article">
                                  <h4><a href="Cytoreductive-surgery-and-heated-intrathoracic-chemotherapy-for-thoracic-extension-of-Pseudomyxoma-peritonei.php"> Cytoreductive surgery and heated intrathoracic chemotherapy for thoracic extension of Pseudomyxoma peritonei</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mathew P. Doyle <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Claudia I. Villanueva <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Samuel Davies <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gary G. Fermanis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Matthew D. Horton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>David L. Morris</p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 03, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000198. x------>
                                <!------ICST.1000199.------>
                                <div class="article">
                                  <h4><a href="Peri-tumoral-neural-niche-in-brain-metastasis-from-breast-cancer.php"> Peri-tumoral neural niche in brain metastasis from breast cancer</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Rahul Jandial <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Khairul I Ansari</p>
                                  <p class="mb5">Image Articl-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 04, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000199. x------>
                                <!------ICST.1000200.------>
                                <div class="article">
                                  <h4><a href="Microorganisms-and-cancer-of-the-oral-cavity.php"> Microorganisms and cancer of the oral cavity</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kaoru Kusama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Harumi Inoue <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yuji Miyazaki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kentaro Kikuchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hideaki Sakashita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kuniyasu Ochiai</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 08, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000200. x------>
                                <!------ICST.1000201.------>
                                <div class="article">
                                  <h4><a href="Genetic-determinants-of-psychic-resilience-after-a-diagnosis-of-cancer.php"> Genetic determinants of psychic resilience after a diagnosis of cancer</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mette Christensen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Antonio Drago</p>
                                  <p class="mb5">Review Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000201. x------>
                                <!------ICST.1000202.------>
                                <div class="article">
                                  <h4><a href="Acetogenins-of-Annona-muricata-leaves-Characterization-and-potential-anticancer-study.php">Acetogenins of Annona muricata leaves: Characterization and potential anticancer study</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nik Nurul Najihah Nik Mat Daud <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Harisun Ya’akob <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mohamad Norisham Mohamad Rosdi</p>
                                  <p class="mb5">Research Article-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 14, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000202. x------>
                                <!------ICST.1000203.------>
                                <div class="article">
                                  <h4><a href="The-rosebud-twenty-five-years-of-tumor-dormancy-mimicks-locally-advanced-non-small-cell-lung-cancer.php">The rosebud- twenty-five years of tumor dormancy mimicks locally advanced non-small cell lung cancer</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>D. Staudenmann <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> M. Gugger <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ph. Dumont <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>St. Zimmerman</p>
                                  <p class="mb5">Case Report-Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> Aug 23, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000203. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000185.------>
                                <div class="article">
                                  <h4><a href="BH3-mimetics-Their-action-and-efficacy-in-cancer-chemotherapy.php"> BH3 mimetics: Their action and efficacy in cancer chemotherapy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Wataru Nakajima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nobuyuki Tanaka </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 26, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000184. x------>
                                <!------ICST.1000185.------>
                                <div class="article">
                                  <h4><a href="11-hydroxysteroid-dehydrogenase-type-II-a-Potential-target-for-prevention-of-colonic-tumorigenesis.php"> 11β-hydroxysteroid dehydrogenase type II: a Potential target for prevention of colonic tumorigenesis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xin Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raymond C. Harris <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ming-Zhi Zhang </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000185. x------>
                                <!------ICST.1000186.------>
                                <div class="article">
                                  <h4><a href="Discussion-on-the-tumor-treatment-of-traditional-chinese-medicine-Discover-the-new-therapeutic-strategy.php"> Discussion on the tumor treatment of traditional chinese medicine：Discover the new therapeutic strategy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xiaojuan Sun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fucheng Hu </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000186. x------>
                                <!------ICST.1000187. ------>
                                <div class="article">
                                  <h4><a href="Modulation-of-5-Aminolevulinic-acid-mediated-photodynamic-therapy-induced-cell-death-in-a-human-lung-adenocarcinoma-cell-line.php"> Modulation of 5-Aminolevulinic acid mediated photodynamic therapy induced cell death in a human lung adenocarcinoma cell line </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Teijo MJ <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Diez B <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Batlle A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Haydée Fukuda </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 29, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000187.  x------>
                                <!------ICST.1000188.------>
                                <div class="article">
                                  <h4><a href="Integrative-approaches-in-breast-cancer-patients-A-mini-review.php"> Integrative approaches in breast cancer patients: A mini-review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Stefano Magno <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Filippone Alessio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Assunta Scaldaferri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Virgilio Sacchini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Federica Chiesa </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000188. x------>
                                <!------ICST.1000189. ------>
                                <div class="article">
                                  <h4><a href="Macrophages-directed-approaches-are-paramount-for-effective-cancer-immunotherapies.php"> Macrophages directed approaches are paramount for effective cancer immunotherapies </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vinod Nadella <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sandhya Singh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hridayesh Prakash </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 05, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000189.  x------>
                                <!------ICST.1000190. ------>
                                <div class="article">
                                  <h4><a href="Primary-vaginal-melanoma-in-the-setting-of-mucosal-melanosis-A-case-report-and-literature-review.php"> Primary vaginal melanoma in the setting of mucosal melanosis - A case report and literature review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Deirdre Kelly <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jack Gleeson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Geoffrey Watson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Graham Woods <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Connor O’Keane <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>John Mc Caffrey </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 07, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000190.  x------>
                                <!------ICST.1000191.------>
                                <div class="article">
                                  <h4><a href="Kikuchi-Fujimoto-necrotizing-lymphadenitis-A-rare-case-of-generalized-lymphadenopathy.php"> Kikuchi-Fujimoto necrotizing lymphadenitis: A rare case of generalized lymphadenopathy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Charlotte Lansky <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hasaam Sheikh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamad Lazkani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cheryl O’Malley </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000191. x------>
                                <!------ICST.1000192.------>
                                <div class="article">
                                  <h4><a href="Placenta-specific-protein-1-PLAC1-is-a-unique-onco-fetal-placental-protein-and-an-underappreciated-therapeutic-target-in-cancer.php"> Placenta-specific protein 1 (PLAC1) is a unique onco-fetal-placental protein and an underappreciated therapeutic target in cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Eric J. Devor </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 09, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000192. x------>
                                <!------ICST.1000193.------>
                                <div class="article">
                                  <h4><a href="An-overview-of-integrative-analysis-in-cancer-studies.php"> An overview of integrative analysis in cancer studies </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shinuk Kim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Taesung Park </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 10, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000193. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000174. ------>
                                <div class="article">
                                  <h4><a href="A-single-patient-study-of-high-dose-isoflavones-to-enhance-gemzar-chemotherapy-in-post-resection-and-post-chemoradiation-adjuvant-treatment-of-pancreatic-adenocarcinoma.php"> A single patient study of high dose isoflavones to enhance gemzar chemotherapy in post resection and post chemoradiation adjuvant treatment of pancreatic adenocarcinoma </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Adam J. Nehr III <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Barbara J. Nehr </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000174.  x------>
                                <!------ICST.1000175.------>
                                <div class="article">
                                  <h4><a href="Adrenomedullin-in-pancreatic-carcinoma-A-case-control-study-of-22-patients.php"> Adrenomedullin in pancreatic carcinoma: A case-control study of 22 patients </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>D'Angelo Francesco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Letizia Claudio <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antolino Laura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>La Rocca Mara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Aurello Paolo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ramacciato Giovanni </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 14, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000175. x------>
                                <!------ICST.1000176.------>
                                <div class="article">
                                  <h4><a href="The-high-effect-of-chemomobilization-with-high-dose-topside-and-G-CSF-in-autologous-hematopoietic-peripheral-blood-stem-cell-transplantation-Single-centre-experience.php"> The high effect of chemomobilization with high-dose topside and G-CSF in autologous hematopoietic peripheral blood stem cell transplantation (Single centre experience) </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sebnem Izmir Guner <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Teoman Yanmaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmet Selvi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cıgdem Usul </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 21, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000176. x------>
                                <!------ICST.1000177. ------>
                                <div class="article">
                                  <h4><a href="An-importance-of-clinical-proteomics-for-personalized-medicine-of-lung-cancer-subtypes.php"> An importance of clinical proteomics for personalized medicine of lung cancer subtypes </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yasufumi Kato <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Toshihide Nishimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jun Matsubayashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kinya Furukawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masaharu Nomura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kiyonaga Fujii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Haruhiko Nakamura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Harubumi Kato </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000177.  x------>
                                <!------ICST.1000178. ------>
                                <div class="article">
                                  <h4><a href="Erosion-of-bowel-by-catheter-of-peritoneal-access-device-during-intraperitoneal-chemotherapy-A-case-report-and-PRISMA-driven-systematic-review.php"> Erosion of bowel by catheter of peritoneal access device during intraperitoneal chemotherapy: A case report and PRISMA-driven systematic review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hung-Hsin Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hou-Hsuan Cheng <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yi Chang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yen-Hou Chang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chi-Mu Chuang </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 28, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000178.  x------>
                                <!------ICST.1000179.------>
                                <div class="article">
                                  <h4><a href="Immunotherapyandcoloncancer.php"> Immunotherapy and colon cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Timothy Allen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdullateef Al-Hadeethi </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April04, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000179. x------>
                                <!------ICST.1000181.------>
                                <div class="article">
                                  <h4><a href="MRI-guided-radiotherapy-Opening-our-eyes-to-the-future.php"> MRI-guided radiotherapy: Opening our eyes to the future </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Amar U. Kishan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Percy Lee </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 06, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000181. x------>
                                <!------ICST.1000182.------>
                                <div class="article">
                                  <h4><a href="Imaging-in-animal-models.php"> Imaging in animal models </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Eika Webb <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ming Yuan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nicholas R Lemoine <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yaohe Wang </p>
                                  <p class="mb5"> Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 08, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000182. x------>
                                <!------ICST.1000183.------>
                                <div class="article">
                                  <h4><a href="Toll-like-receptors-and-breast-cancer.php"> Toll-like receptors and breast cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lijie Sun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qiuli Jiang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yifan Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hongbin Liang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Huan Ren <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dekai Zhang </p>
                                  <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> April 25, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000183. x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000164 ------>
                                <div class="article">
                                  <h4><a href="The-possible-role-of-embryonic-polarity-axes-for-the-normalization-of-tissue-function-induced-by-the-interaction-between-human-bilateral-parts.php"> The possible role of embryonic polarity axes for the normalization of tissue function induced by the interaction between human bilateral parts </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ming Cheh Ou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Dennis Ou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chung Chu Pang </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 21, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000164  x------>
                                <!------ICST.1000165 ------>
                                <div class="article">
                                  <h4><a href="A-short-term-retrospective-analysis-of-the-clinical-histopathological-and-immunohistochemical-aspects-in-endometrial-malignancy.php"> A short-term retrospective analysis of the clinical, histopathological and immunohistochemical aspects in endometrial malignancy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tiberiu Augustin Georgescu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Monica Cîrstoiu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mariana Costache <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anca Lăzăroiu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adrian Dumitru </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 11, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000165  x------>
                                <!------ICST.1000166 ------>
                                <div class="article">
                                  <h4><a href="A-case-of-advanced-hepatocellular-carcinoma-cured-with-antiviral-therapy-and-sorafenib.php"> A case of advanced hepatocellular carcinoma cured with antiviral therapy and sorafenib </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Steven Krawitz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Flavius Guglielmo <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hie-Won Hann </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 15, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000166  x------>
                                <!------ICST.1000167 ------>
                                <div class="article">
                                  <h4><a href="Corticosteroid-induced-case-of-a-lightning-pheochromocytoma-crisisInsight-into-glucocorticoid-receptor-expression.php"> Corticosteroid-induced case of a lightning pheochromocytoma crisis:Insight into glucocorticoid receptor expression </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Marie-France Dupont <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marie-Claude Battista <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Émilie Comeau <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Myrna Atallah-Chababi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Patrice Perron <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>5andMartine Chamberland </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 23, 2016</p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000167  x------>
                                <!------ICST.1000168 ------>
                                <div class="article">
                                  <h4><a href="Subjective-and-objective-evaluation-of-voice-and-pulmonary-function-in-partial-laryngectomised-patients.php"> Subjective and objective evaluation of voice and pulmonary function in partial laryngectomised patients </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mehmet Gökhan Demir <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mustafa Paksoy <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arif Şanlı <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sevtap Akbulut <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sedat Aydın <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hüseyin Baki Yılmaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mehmet Bilgin Eser </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000168  x------>
                                <!------ICST.1000169 ------>
                                <div class="article">
                                  <h4><a href="Comparison-of-exposure-levels-of-carotid-artery-in-T1-2-glottic-cancer-patients-undergoing-radiotherapy-with-different-modalities-A-simulation-based-dosimetric-study.php"> Comparison of exposure levels of carotid artery in T1-2 glottic cancer patients undergoing radiotherapy with different modalities: A simulation-based dosimetric study </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gonca Hanedan Uslu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emine Canyilmaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Fatma Colak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Burcin Hazeral <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmet Yasar Zengin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hüseyin Göçmez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adnan Yoney </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 30, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000169  x------>
                                <!------ICST.1000170------>
                                <div class="article">
                                  <h4><a href="Management-of-hypercalcemia-of-malignancy.php"> Management of hypercalcemia of malignancy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sonia Amin Thomas (Sonia Patel) <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Soo-Hwan Chung </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 06, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000170 x------>
                                <!------ICST.1000171 ------>
                                <div class="article">
                                  <h4><a href="Metformin-associates-with-lower-on-colorectal-neoplastic-risk-in-African-American-diabetic-patients.php"> Metformin associates with lower on colorectal neoplastic risk in African American diabetic patients </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>AnahitaShahnazi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Armana Saeed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gail Nunlee-Bland <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yasin Mustafa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hassan Brim <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mehdi Nouraie <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hassan Ashktorab </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 08, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000171  x------>
                                <!------ICST.1000172------>
                                <div class="article">
                                  <h4><a href="Obesity-as-a-risk-factor-for-prostate-cancer-A-clinical-review.php"> Obesity as a risk factor for prostate cancer: A clinical review </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tushar Trivedi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marsha Samson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Olubunmi Orekoya </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 11, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000172 x------>
                                <!------ICST.1000173------>
                                <div class="article">
                                  <h4><a href="Diabetes-associated-dysregulated-cytokines-and-cancer.php"> Diabetes-associated dysregulated cytokines and cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yong Wu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yunzhou Dong <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yanjun Liu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jay Vadgama </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 15, 2016 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000173 x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1c" class="accordian-active">Volume 2 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1c">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#v2-panelf"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Dec 2015</span> <span class="mt3 grey-text"> Issue 6</span> </a></li>
                          <li class="tab-title"><a href="#v2-panele"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Oct 2015</span> <span class="mt3 grey-text"> Issue 5</span> </a></li>
                          <li class="tab-title"><a href="#v2-paneld"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Aug 2015</span> <span class="mt3 grey-text"> Issue 4</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>June 2015</span> <span class="mt3 grey-text"> Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#v2-panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>Apr 2015</span> <span class="mt3 grey-text"> Issue 2</span> </a></li>
                          <li class="tab-title"><a href="#v2-panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> Feb 2015</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content active" id="v2-panelf">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000154 ------>
                                <div class="article">
                                  <h4><a href="Alpha1-antitrypsin-an-endogenous-immunoregulatory-molecule-distinction-between-local-and-systemic-effects-on-tumor-immunology.php"> Alpha1-antitrypsin, an endogenous immunoregulatory molecule: distinction between local and systemic effects on tumor immunology </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ofer Guttman <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Gabriella S Freixo-Lima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eli C Lewis </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000154  x------>
                                <!------ICST.1000155 ------>
                                <div class="article">
                                  <h4><a href="Evaluation-of-the-impact-of-treatment-delays-and-dose-reductions-in-human-ovarian-cancer-orthotopic-mouse-models.php"> Evaluation of the impact of treatment delays and dose reductions in human ovarian cancer orthotopic mouse models </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Scott Mosley <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jing-Hong Chen <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anjali Gaikwad <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Elizabeth K. Nugent <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Judith A. Smith </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000155  x------>
                                <!------ICST.1000156 ------>
                                <div class="article">
                                  <h4><a href="Comparing-Chinese-and-American-oncology-practice-patterns-using-precision-therapy-for-non-small-cell-lung-cancer.php"> Comparing Chinese and American oncology practice patterns using precision therapy for non-small cell lung cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Zhang Ming <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>HaoXinbao <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Han LiangFu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hu Honglin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>AoRui <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Zhu Xueqiang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>He Yangke <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chen Lin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ming Zeng </p>
                                  <p class="mb5">Clinical Survey-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 31, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000156  x------>
                                <!------ICST.1000157 ------>
                                <div class="article">
                                  <h4><a href="Monitoring-Bip-promoter-activation-during-cancer-cell-growth-by-bioluminescence-imaging-at-the-single-cell-level.php"> Monitoring Bip promoter activation during cancer cell growth by bioluminescence imaging at the single-cell level </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tomohisa Horibe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Aya Torisawa <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryohsuke Kurihara <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ryutaro Akiyoshi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yoko Hatta-Ohashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hirobumi Suzuki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Koji Kawakami </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000157  x------>
                                <!------ICST.1000158------>
                                <div class="article">
                                  <h4><a href="Risk-factors-for-cost-related-medication-non-adherence-among-older-patients-with-cancer.php"> Risk factors for cost-related medication non-adherence among older patients with cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>James X. Zhang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David O. Meltzer </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> November 06, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000158 x------>
                                <!------ICST.1000159------>
                                <div class="article">
                                  <h4><a href="Then-there-were-none.php"> Then there were none! </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Veronica J. James <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Judith M. O'Malley Ford <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jason Buttigieg </p>
                                  <p class="mb5">Letter to Editor-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December01, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000159 x------>
                                <!------ICST.1000160 ------>
                                <div class="article">
                                  <h4><a href="Pseudomyxoma-peritonei-a-rare-tumors-of-the-abdomen-Palliative-treatment-and-response-to-low-dose-capecitabine-and-or-bevacizumab.php"> Pseudomyxoma peritonei, a rare tumors of the abdomen: Palliative treatment and response to low-dose capecitabine and/or bevacizumab </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Stefanis Aristotelis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Albertsson Maria </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000160  x------>
                                <!------ICST.1000161 
                                <div class="article">
                                  <h4><a href="The-incidence-and-risk-of-biochemical-recurrence-following-radical-radiotherapy-for-prostate-cancer-in-men-on-Angiotensin-Converting-Enzyme-Inhibitors-ACEIs-or-Angiotensin-Receptor-Blockers-ARB.php"> The incidence and risk of biochemical recurrence following radical radiotherapy for prostate cancer in men on Angiotensin-Converting Enzyme Inhibitors (ACEIs) or Angiotensin Receptor Blockers (ARB) </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Abduelmenem Alashkham <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Catherine Paterson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Phyllis Windsor <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Allan Struthers <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Petra Rauchhaus <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ghulam Nab </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December05, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                ICST.1000161  x------>
                                <!------ICST.1000162 ------>
                                <div class="article">
                                  <h4><a href="Interprofessional-care-to-improve-patient-centered-approach-in-breast-cancer-a-case-report-regarding-the-management-of-concomitant-psychiatric-symptomatology.php"> Interprofessional care to improve patient-centered approach in breast cancer: a case-report regarding the management of concomitant psychiatric symptomatology </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Paola Arnaboldi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Serena Oliveri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gabriella Pravettoni </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 10, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000162  x------>
                                <!------ICST.1000163 ------>
                                <div class="article">
                                  <h4><a href="Systemic-inflammation-in-malignant-pleural-mesothelioma-Is-neutrophyl-to-lymphocyte-ratio-a-prognostic-index.php"> Systemic inflammation in malignant pleural mesothelioma: Is neutrophyl-to-lymphocyte ratio a prognostic index? </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Felice MucilliFelice Mucilli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Pierpaolo Camplese <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Luigi Guetti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Barbara Maggi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marco Prioletta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mirko Barone <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antonia Rizzuto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rosario Sacco </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> December 15, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000163  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="oncogenic-mutational-epigenetic-events-deterministic-stochastic-or-random-implications-for-hypoxic-cancer-cells-and-tumor-heterogeneity.php">Oncogenic mutational/epigenetic events: Deterministic, stochastic or random: Implications for hypoxic cancer cells and tumor heterogeneity </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Anderson KM <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rubenstein M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Guinan P <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Patel MK </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 05, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Fatigue-in-patients-with-colorectal-cancer-treated-with-capecitabine-and-oxaliplatin.php">Fatigue in patients with colorectal cancer treated with capecitabine and oxaliplatin </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Sá Lilian A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Vettori Josiane C <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Moreira Cláudia LR <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leandro Miraya S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tirapelli Daniela PC <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Perdoná Gleici SC <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Souza Hayala CC <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Oliveira Harley F <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Santos Fabiana N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rapatoni Liane <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ribeiro Karen B <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Almeida Thiago CL <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Feres Omar <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rocha José JRR <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Peria Fernanda M </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Expression-and-regulation-of-bone-morphogenetic-protein-4-in-hepatocellular-carcinoma.php">Expression and regulation of bone morphogenetic protein 4 in hepatocellular carcinoma </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Xiaotong Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Qingguo Wang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weisi Kong <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerald Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Minuk <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Frank J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Burczynski <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emmanuel Ho <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yuewen Gong </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> January 29, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="The-origin-of-circumscribed-necroses-and-perinecrotic-niches-in-glioblastoma-multiforme-An-additional-hypothesis.php">The origin of circumscribed necroses and perinecrotic niches in glioblastoma multiforme: An additional hypothesis</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Davide Schiffer <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Laura Annovazzi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marta Mazzucco <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marta Mellai </p>
                                  <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February  03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Shedding-by-ADAM10-and-ADAM17-is-associated-with-progression-of-adult-T-cell-leukemia-lymphoma.php">Shedding by ADAM10 and ADAM17 is associated with progression of adult T-cell leukemia/lymphoma</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shigeki Takemoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tomohiro Kozako <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ratiorn Pornkuna </p>
                                  <p class="mb5">Short Communication-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February  05, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Delphinidin-and-cyanidin-exhibit-antiproliferative-and-apoptotic-effects-in-MCF7-human-breast-cancer-cells.php">Delphinidin and cyanidin exhibit antiproliferative and apoptotic effects in MCF7 human breast cancer cells</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Jessica Tang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emin Oroudjev <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Leslie Wilson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>George Ayoub </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 09, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="effects-of-chk1-inhibitor-or-paclitaxel-on-cisplatin-induced-cell-cycle-kinetics-and-survival-in-parental-and-cisplatin-resistant-HeLa-cells-expressing-fluorescent-ubiquitination-based-cell-cycle-indicator-Fucci.php"> Effects of Chk1 inhibitor or paclitaxel on cisplatin-induced cell-cycle kinetics and survival in parental and cisplatin-resistant HeLa cells expressing fluorescent ubiquitination-based cell cycle indicator (Fucci)</a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Chisato Yamada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Atsushi Kaida <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Okuyama <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kiyoshi Harada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Masahiko Miura </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 09, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="The-endometrial-cancer-in-Southern-Tunisia-Investigation-about-52-cases.php"> The endometrial cancer in Southern Tunisia: Investigation about 52 cases </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kebaili Sahbi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Trigui K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Louati D <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Charfi S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ben Hlima S <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daoud J <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chaabane K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Amouri H </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 12, 2015 </p>
                                  </em> </div>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panelb">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <div class="article">
                                  <h4><a href="SERCA-as-a-target-for-cancer-therapies.php"> SERCA-as-a-target-for-cancer-therapies </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Denise Casemore <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chengguo Xing </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> February 26, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="An-audit-of-inter-observer-variability-in-Gleason-grading-of-prostate-cancer-biopsies-The-experience-of-central-pathology-review-in-the-North-West-of-England.php"> An audit of inter-observer variability in Gleason grading of prostate cancer biopsies: The experience of central pathology review in the North West of England </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emile N Salmo </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> March 02, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Quality-of-life-among-Brazilian-women-having-undergone-surgery-for-breast-cancer-Validity-and-reliability-of-the-Quality-of-life-Questionnaire-EORTC-QLQ-C30-and-Breast-Cancer-Module-QLQ-BR-23.php"> Quality of life  among Brazilian women having undergone surgery for breast cancer: Validity and reliability of the <em>Quality of life</em> <em>Questionnaire</em> (EORTC QLQ-C30) and <em>Breast Cancer Module</em> (QLQ BR-23) </a> </h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Ana Silvia Diniz Makluf <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alexandre de Almeida Barra <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rosângela Corrêa Dias <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Cristovão Pinheiro de Barros
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 08, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Towards-an-integrated-model-of-defense-in-gynecological-cancer-Psychoneuroimmunological-and-psychological-factors-between-risk-and-protection-in-cancer-A-review.php"> Towards an integrated model of defense in gynecological cancer. Psychoneuroimmunological and psychological factors, between risk and protection in cancer: A review </a> </h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Chiara Cosentino <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Carlo A. Pruneti
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 09, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Effect-of-glycosylated-vitamin-D-binding-protein-complexed-with-oleic-acid-on-human-myeloma-and-Hodgkins-lymphoma-cultures.php"> Effect of glycosylated vitamin D binding protein complexed with oleic acid on human myeloma and Hodgkin’s lymphoma cultures </a> </h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Jacopo J.V. Branca <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rodney J Smith <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emma Ward <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Marco Ruggiero </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>March 17, 2015</p>
                                  </em>
                                  <hr/>
                                </div>
                                <div class="article">
                                  <h4><a href="Cytotoxicity-of-gemcitabine-loaded-thermosensitive-liposomes-in-pancreatic-cancer-cell-lines.php"> Cytotoxicity of gemcitabine-loaded thermosensitive liposomes in pancreatic cancer cell lines </a> </h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Kevin Affram <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ofonime Udofot <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Edward Agyare </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>April 16, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panelc">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000129 ------>
                                <div class="article">
                                  <h4><a href="Retrospective-review-on-Local-breast-therapy-in-addition-to-systemic-therapy-in-de-novo-metastatic-breast-cancer.php"> Retrospective review on: Local breast therapy in addition to systemic therapy in de-novo metastatic breast cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Tahani Nageeti <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Raid Jastania </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> May 02, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000129  x------>
                                <!------ICST.1000130------>
                                <div class="article">
                                  <h4><a href="Feasibility-of-liposuction-for-treatment-of-arm-lymphedema-from-breast-cancer-A-prospective-study-and-review-of-the-literature.php"> Feasibility of liposuction for treatment of arm lymphedema from breast cancer: A prospective study and review of the literature </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Erin Louise Doren <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul David Smith <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Weihong Sun <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>MensuraLacevic <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>William Fulp <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Rebecca Reid <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Christine Laronga </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000130 x------>
                                <!------ICST.1000131 ------>
                                <div class="article">
                                  <h4><a href="Rethinking-NM23-An-extracellular-role-for-NDPKinase-in-breast-cancer.php"> Rethinking NM23: An extracellular role for NDPKinase in breast cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Iain L. O. Buxton <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Senny Nordmeier </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 03, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000131  x------>
                                <!------ICST.1000132 ------>
                                <div class="article">
                                  <h4><a href="Antioxidants-therapy-An-alternative-for-androgen-deprivation-therapy-ADT-to-decrease-prostate-specific-antigen-PSA-level.php"> Antioxidants therapy: An alternative for androgen deprivation therapy (ADT) to decrease prostate-specific antigen (PSA) level </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Mohammad Reza Naghii <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mehdi Hedayati <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mahmood Mofid </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 06, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000132  x------>
                                <!------ICST.1000133 ------>
                                <div class="article">
                                  <h4><a href="The-cytogenetic-and-molecular-analysis-of-chronic-myeloid-leukemia-in-a-tertiary-care-hospital-of-Sindh-Pakistan.php"> The cytogenetic and molecular analysis of chronic myeloid leukemia in a tertiary care hospital of Sindh, Pakistan </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ikramdin Ujjan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anwar Ali Akhund <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abdul Mannan Baig <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ghulam Shah Nizamani </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 17, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000133  x------>
                                <!------ICST.1000134------>
                                <div class="article">
                                  <h4><a href="Extrinsic-targeting-strategies-against-acute-myeloid-leukemia-stem-cells.php"> Extrinsic targeting strategies against acute myeloid leukemic stem cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noureldien H. E. Darwish <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaker A. Mousa </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000134 x------>
                                <!------ICST.1000135------>
                                <div class="article">
                                  <h4><a href="Intrinsic-targeting-strategies-against-acute-myeloid-leukemic-stem-cells.php"> Intrinsic targeting strategies against acute myeloid leukemic stem cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Noureldien H. E. Darwish <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shaker A. Mousa </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000135 x------>
                                <!------ICST.1000136 ------>
                                <div class="article">
                                  <h4><a href="Navigating-commercial-approval-of-therapeutics-The-evolution-of-the-United-States-Food-and-Drug-Administration.php"> Navigating commercial approval of therapeutics: The evolution of the United States Food and Drug Administration </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dillon Jarrell <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Megan Hamrick <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michaela Good <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anna Chopp <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mark Brown </p>
                                  <p class="mb5">Commentry-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> June 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000136  x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-paneld">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000137------>
                                <div class="article">
                                  <h4><a href="Aeroplysinin-1-inhibits-Akt-and-Erk-pathway-signaling-selectively-in-endothelial-cells.php"> (+)-Aeroplysinin-1 inhibits Akt and Erk pathway signaling selectively in endothelial cells </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Beatriz Martínez-Poveda <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ana R. Quesada <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Miguel A. Medina </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 06, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000137 x------>
                                <!------ICST.1000138 ------>
                                <div class="article">
                                  <h4><a href="PKGI-mediates-the-growth-inhibitory-effects-of-cGMP-signaling-in-human-breast-cancer-cells-independent-of-catenin.php"> PKGI mediates the growth inhibitory effects of cGMP signaling in human breast cancer cells independent of β-catenin </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Perrin F. Windham <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Paul S. Rodriguez <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Nicholas J. Rivers <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Heather N. Tinsley </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> July 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000138  x------>
                                <!------ICST.1000139 ------>
                                <div class="article">
                                  <h4><a href="Blockade-of-extracellular-NM23-or-its-endothelial-target-slows-breast-cancer-growth-and-metastasis.php"> Blockade of extracellular NM23 or its endothelial target slows breast cancer growth and metastasis </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nucharee Yokdang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Senny Nordmeier <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Katie Speirs <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Heather R. Burkin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Iain L. O. Buxton </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 04, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000139  x------>
                                <!------ICST.1000140------>
                                <div class="article">
                                  <h4><a href="The-scopes-and-falls-of-molecular-targeted-therapy-in-gastric-cancer.php"> The scopes and falls of molecular targeted therapy in gastric cancer </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Vinod Vijay Subhash <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Wei Peng Yong </p>
                                  <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 20, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000140 x------>
                                <!------ICST.1000141 ------>
                                <div class="article">
                                  <h4><a href="Single-dose-bevacizumab-as-treatment-for-symptomatic-corticosteroid-refractory-pseudoprogression-after-chemoradiation-therapy-for-newly-diagnosed-glioblastoma-A-case-report.php"> Single dose bevacizumab as treatment for symptomatic, corticosteroid-refractory pseudoprogression after chemoradiation therapy for newly diagnosed glioblastoma: A case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Taylor M. Morris <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Isabelle Vallieres <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Andrew Attwell <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>R Petter Tonseth <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Natella Jafarova <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>David L. Saltman </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 24, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000141  x------>
                                <!------ICST.1000142------>
                                <div class="article">
                                  <h4><a href="Pituitary-gland-metastases-from-renal-cell-carcinoma-A-case-report-and-literature-update.php"> Pituitary gland metastases from renal cell carcinoma: A case report and literature update </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Domenico Murrone <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Francesco Amedeo Abbate <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Danilo De Paulis <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Renato Juan Galzio </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> August 27, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000142 x------>
                              </div>
                            </div>
                          </div>
                          <div class="content" id="v2-panele">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <!------ICST.1000144------>
                                <div class="article">
                                  <h4><a href="Pleurodesis-in-malignant-pleural-effusions-Outcome-and-predictors-of-success.php"> Pleurodesis in malignant pleural effusions: Outcome and predictors of success </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Hind Rafei <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Suha Jabak <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alain Mina <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arafat Tfayli </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 18, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000144 x------>
                                <!------ICST.1000145------>
                                <div class="article">
                                  <h4><a href="A-case-of-unresectable-small-cell-lung-cancermore-than-7-years-of-survival-treated-with-cryoablation-and-low-dose-chemotherapy.php"> A case of unresectable small cell lung cancermore than 7 years of survival treated with cryoablation and low dose chemotherapy </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Quanwang Li <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tian Zhou <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Min Jiang <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kaiwen Hu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Franco Lugnani <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antonio Cueto </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 19, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000145 x------>
                                <!------ICST.1000146 ------>
                                <div class="article">
                                  <h4><a href="Communicating-with-prostate-cancer-patients-Psychosocial-profile-and-determinants-of-seeking-psychosocial-care.php"> Communicating with prostate cancer patients: Psychosocial profile and determinants of seeking psychosocial care </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Laura Daeter <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adriaan Visser <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Chantal van Harten <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bert Voerman </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 21, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000146  x------>
                                <!------ICST.1000147------>
                                <div class="article">
                                  <h4><a href="Epiphrenic-esophageal-diverticulum-within-a-leiomyoma-A-rare-cause-of-dysphagia.php"> Epiphrenic esophageal diverticulum within a leiomyoma: A rare cause of dysphagia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Omar Abdel-lah Fernández <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Alberto Alvarez Delgado <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Felipe Carlos Parreño <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Antonio Rodriguez </p>
                                  <p class="mb5">Case Report-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 22, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000147 x------>
                                <!------ICST.1000148------>
                                <div class="article">
                                  <h4><a href="5-Fluorouracil-radiation-recall-dermatitis-A-case-report.php"> 5-Fluorouracil-ınduced radiation recall dermatitis: A case report </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Gonca Hanedan Uslu <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Emine Canyilmaz <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Savas Yayli <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Sevdegul Aydin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ahmet Yasar Zengin <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Adnan Yoney </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 25, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000148 x------>
                                <!------ICST.1000149------>
                                <div class="article">
                                  <h4><a href="BCL2L11-BIM-deletion-polymorphism-rather-than-BIM-SNP-is-an-effective-predictor-of-early-molecular-relapse-after-ABL-tyrosine-kinase-inhibitor-discontinuation-in-patients-with-chronic-myeloid-leukemia.php"> <em>BCL2L11 (BIM)</em> deletion polymorphism, rather than <em>BIM</em> SNP, is an effective predictor of early molecular relapse after ABL tyrosine kinase inhibitor discontinuation in patients with chronic myeloid leukemia </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Seiichiro Katagiri <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Tetsuzo Tauchi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kenichi Tadokoro <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Junko H. Ohyashiki <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kazuma Ohyashiki </p>
                                  <p class="mb5">Letter to Editor-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> September 28, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000149 x------>
                                <!------ICST.1000150------>
                                <div class="article">
                                  <h4><a href="Cytotoxicity-of-5-fluorouracil-loaded-pH-sensitive-liposomal-nanoparticles-in-colorectal-cancer-cell-lines.php"> Cytotoxicity of 5-fluorouracil-loaded pH-sensitive liposomal nanoparticles in colorectal cancer cell lines </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ofonime Udofot <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kevin Affram <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Bridg’ette Israel <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Edward Agyare </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 09, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000150 x------>
                                <!------ICST.1000151------>
                                <div class="article">
                                  <h4><a href="Lysophosphatidic-acid-induces-ME180-cell-migration-via-its-putative-receptor-GPR87.php"> Lysophosphatidic acid induces ME180 cell migration via its putative receptor GPR87 </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Ryo Saito <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Daisuke Furuta <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shunsuke Nakajima <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takuya Watanabe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shoichi Ochiai <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takashi Fujita <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Norihisa Fujita </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000151 x------>
                                <!------ICST.1000152 ------>
                                <div class="article">
                                  <h4><a href="The-unsuspected-intrinsic-property-of-melanin-to-transform-light-energy-into-chemical-energy-and-the-Warburg-effect-Connotations-in-cancer-biochemistry.php"> The unsuspected intrinsic property of melanin to transform light energy into chemical energy and the Warburg effect:  Connotations in cancer biochemistry </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Arturo Solis Herrera </p>
                                  <p class="mb5">Mini Review-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 12, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000152  x------>
                                <!------ICST.1000153 ------>
                                <div class="article">
                                  <h4><a href="ABT-737-and-Roscovitine-induces-apoptosis-in-a-synergistic-fashion-in-mast-cells-carrying-the-D816V-KIT-mutation.php"> ABT-737 and Roscovitine induces apoptosis in a synergistic fashion in mast cells carrying the D816V KIT mutation </a></h4>
                                  <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Katarina Lyberg<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Gunnar Nilsson<i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Christine Möller Westerberg </p>
                                  <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                  <em>
                                  <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i> October 16, 2015 </p>
                                  </em>
                                  <hr/>
                                </div>
                                <!------ICST.1000153  x------>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                  <dd> <a href="#panel1b" class="accordian-active">Volume 1 <span class="right career-accordion"> <img src="img/minus.png"/></span></a>
                    <div class="content" id="panel1b">
                      <div class="hr-tabs">
                        <ul class="tabs" data-tab>
                          <li class="tab-title active"><a href="#panelc"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i>December 2014</span> <span class="mt3 grey-text"> Issue 3</span> </a></li>
                          <li class="tab-title"><a href="#panelb"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> October 2014</span> <span class="mt3 grey-text"> Issue 2</span> </a></li>
                          <li class="tab-title "><a href="#panela"> <span><i class="fa small-font pr5 fa-calendar lite-text"></i> August 2014</span> <span class="mt3 grey-text">Issue 1</span> </a></li>
                        </ul>
                        <div class="tabs-content">
                          <div class="content " id="panela">
                            <div class="row pt10">
                              <div class="small-12 columns">
                                <h4><a href="Potential_significance_of_the_regucalcin_gene_in_human_carcinoma.php">Potential significance of the regucalcin gene in human carcinoma</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr10 small-font text-lite-green"></i>Masayoshi Yamaguchi</p>
                                <p class="mb5">Editorial-Integrative Cancer Science and Therapeutics (ICST)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr10 fa-calendar lite-text"></i>Aug 01, 2014</p>
                                </em>
                                <hr/>
                                <h4><a href="suppression_of_regucalcin_gene_expression_may_lead_to_hepatocarcinogenesis.php">The suppression of regucalcin gene expression may lead to hepatocarcinogenesis</a></h4>
                                <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr10 small-font text-lite-green"></i>Masayoshi Yamaguchi</p>
                                <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                                <em>
                                <p class="mb5 lite-text"><i class="fa small-font pr10 fa-calendar lite-text"></i>Aug 03, 2014</p>
                                </em> </div>
                            </div>
                          </div>
                          <div class="content" id="panelb">
                            <h4><a href="Soluble-CD30-and-peripheral-blood-or-pulmonary-involvement-of-adult-T-cell-leukemia-lymphoma.php">Soluble CD30 and peripheral blood or pulmonary involvement of adult T-cell leukemia/lymphoma</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shigeki Takemoto <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Ratiorn Pornkuna </p>
                            <p class="mb5">Short Communication-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 02, 2014</p>
                            <hr/>
                            <h4><a href="Prognostic-value-of-bone-marker-beta-crosslaps-in-patients-with-breast-carcinoma.php">Prognostic value of bone marker beta-crosslaps in patients with breast carcinoma</a></h4>
                            <p class="pt5 mb10"> <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green"></i> Nicole Zulauf <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Ingo Marzi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerhard M Oremek </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 03, 2014</p>
                            <hr/>
                            <h4><a href="Disrupting-cancer-cell-function-by-targeting-mitochondria.php">Disrupting cancer cell function by targeting mitochondria</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Emily Ann Carlson <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Shirley ShiDu Yan </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 04, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="chemotherapy-BISIC-for-the-peritoneal-metastasis-from-gastric-cancer-in-neoadjuvant-setting.php"> A new bidirectional intraperitoneal and systemic induction chemotherapy (BISIC) for the peritoneal metastasis from gastric cancer in neoadjuvant setting</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Yonemura Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Canbay <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Endou Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ishibashi H <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mizumoto A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Li Y <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takeshita K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Ichinose M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Takao N <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Noguchi K <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Hirano M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Glehen O <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Brűcher B <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Sugarbaker P </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 07, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Assessing-potential-herb-drug-interactions-A-clinical-decision-making-guideline-to-mitigate-patient-risk.php">Assessing potential herb-drug interactions: A clinical decision-making guideline to mitigate patient risk</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Shauna Birdsall <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i> Glen J Weiss </p>
                            <p class="mb5">Review Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>October 15, 2014</p>
                            </em> </div>
                          <div class="content active" id="panelc">
                            <h4><a href="Tartrate-resistant-acid-phosphatase-5b-as-a-diagnostic-marker-of-bone-metastases-in-patients-with-renal-cell-carcinoma.php">Tartrate-resistant acid phosphatase 5b as a diagnostic marker of bone metastases in patients with renal cell carcinoma</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Robert Preussner <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Hildegund Sauer-Eppe <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Gerhard Maximilian Oremek </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 03, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Cancer-as-a-mafia-within-the-body-A-proposition-of-conceptual-approach-that-seems-congruent-to-the-complex-biology-of-the-disease.php">Cancer as a “mafia” within the body: A proposition of conceptual approach that seems congruent to the complex biology of the disease</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Dariusz Adamek <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Anastazja Stoj </p>
                            <p class="mb5">Letter to Editor-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 09, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="small-molecule-inhibitors-as-emerging-cancer-therapeutics.php">Small molecule inhibitors as emerging cancer therapeutics</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Lavanya V <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohamed Adil A.A <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Neesar Ahmed <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Arun K.Rishi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Shazia Jamal </p>
                            <p class="mb5">Review  Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 09, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Weak-burst-firing-magnetic-fields-that-produce-analgesia-equivalent-to-morphine-do-not-initiate-activation-of-proliferation-pathways-in-human-breast-cells-in-culture.php">Weak burst-firing magnetic fields that produce analgesia equivalent to morphine do not initiate activation of proliferation pathways in human breast cells in culture</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i>Nirosha J. Murugan <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Lukasz M <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>M. Karbowski <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Michael A. Persinger </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 16, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="No-association-between-TTTA-short-tandem-repeat-STR-of-the-CYP19-gene-and-prostate-cancer-risk-in-Iranian-population-A-case-control-study.php">No association between TTTA short tandem repeat (STR) of the CYP19 gene and prostate cancer risk in Iranian population: A case control study</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Kunihide Tanaka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farah Farzaneh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Karimpur-zahmatkesh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Jalil Hosseini <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Farkhondeh Pouresmaeili <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Abolfazl Movafagh <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Eznollah Azarghashb <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Mohammad Yaghoobi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Reza Mirfakhraie </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>November 18, 2014</p>
                            </em>
                            <hr/>
                            <h4><a href="Topical-administration-of-a-newly-devised-anticancer-agent-A-novel-concept-of-cancer-chemotherapy.php">Topical administration of a newly devised anticancer agent: A novel concept of cancer chemotherapy</a></h4>
                            <p class="pt5 mb10"><i class="fa  fa-dot-circle-o pr5 small-font text-lite-green "></i> Akio Sugitachi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Naoko Takahashi <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Kohei Kume <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yukimi Ohmori <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Satoshi Nishizuka <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Yusuke Kimura <i class="fa  fa-dot-circle-o pr5 small-font text-lite-green ml10"></i>Go Wakabayashi </p>
                            <p class="mb5">Research Article-Integrative Cancer Science and Therapeutics (ICST)</p>
                            <em>
                            <p class="mb5 lite-text"><i class="fa small-font pr5 fa-calendar lite-text"></i>December 15, 2014</p>
                            </em> </div>
                        </div>
                      </div>
                    </div>
                  </dd>
                </dl>
              </div>
            </div>
            <div id="External_Databases_Indexes" class="content">
              <h2>External Databases & Indexes</h2>
              <hr>
              <p>
              <ul>
                <li>ROAD</li>
                <li>SciCurve Open</li>
                <li>Refseek</li>
                <li>WorldCat</li>
                <li>Crossref</li>
                <li>Google scholar</li>
                <li>Pubmed*</li>
                <li>SciLit</li>
                <li>Electronic Journals Library (EZB)</li>
                <li>German Journal Database (ZDB)</li>
                <li>Scientific Indexing Services (SIS)</li>
              </ul>
              </p>
              <p>All published articles are provided with DOI and are permanently archived at Open Access Text website in PDF and HTML formats.</p>
              <h1></h1>
              <hr/>
              <p>*Pubmed: Only selected publications will be indexed.</p>
            </div>
            <div id="Submit_Manuscript" class="content">
              <h2>Submit Manuscript</h2>
              <hr>
              <p> IOD welcomes direct submissions of manuscripts from authors. You can submit your manuscript to: <a href="mailto:submissions@oatext.com">submissions@oatext.com</a> ; alternatively to: <a href="mailto:masayoshi@oatext.com">masayoshi@oatext.com</a></p>
            </div>
            <div id="Publication_Charges" class="content">
              <h2>Publication Charges</h2>
              <hr/>
              <h4 class="mt10">Article Publication Charges (APC)</h4>
              <p>OA Text journals are open access, we do not charge the end user when accessing a manuscript or any article. This allows the scientific community to view, download, distribution of an article in any medium, provided that the original work is properly cited, under the term of<a target="_blank" href="http://creativecommons.org/licenses/by/4.0/">"Creative Commons Attribution License"</a>. In line with other open access journals we provide a flat fee submission structure (dependent upon the journal) on the acceptance of a peer-reviewed article which covers in part the entirety of the publication pathway (the article processing charge). The process includes our maintenance, submission and peer review systems and international editing, publication and submission to global indexing and tracking organisations and archiving to allow instant access to the whole article and associated supplementary documents. We also have to ensure enough investment to secure a sustainable model which ethically, legally and financially stable.</p>
              <ul>
                <li><a href="#set-F1">1. What are your publication charges?</a></li>
                <li><a href="#set-F2">2. Why are your charges set at these levels?</a></li>
                <li><a href="#set-F3">3. When do I pay Article Publication Charges?</a></li>
              </ul>
              <div id="set-F1">
                <h4>1. What are your publication charges?</h4>
                <p>The costs of producing and maintaining OA Text are recovered by charging a publication fee to authors or research sponsors for each article that they publish.</p>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tbody>
                    <tr>
                      <td style="width:319px;"><p><strong>Article type</strong></p></td>
                      <td style="width:123px;"><p><strong>Publication Fee (GBP / USD)</strong></p></td>
                    </tr>
                    <tr>
                      <td style="width:319px;"><p>Research, reviews and other types of articles</p></td>
                      <td style="width:123px;"><p>&pound;920 / $1300</p></td>
                    </tr>
                    <tr>
                      <td style="width:319px;"><p>Special issue articles</p></td>
                      <td style="width:123px;"><p>&pound;690 / $890</p></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div id="set-F2">
                <h4>2. Why are your charges set at these levels?</h4>
                <p>All articles published in OA Text are open access. Open Access publishing implies that all readers, anywhere in the world, are allowed unrestricted to full text of articles, immediately on publication in OA Text Journals. The Article Publication Charges pay for the editorial and production costs of the journal, for hosting the website, publishing articles online, preparing HTML , PDF and XML versions of the articles and submitting the articles in electronic citation database like CrossRef. </p>
                <p>Our financial goals are to:</p>
                <ul>
                  <li> Recover capitalization costs;</li>
                  <li> Produce sufficient revenue to allow for a sustainable and scalable publishing program, under continuous development;</li>
                  <li> Bend the publication-charge cost downward over time.</li>
                </ul>
              </div>
              <div id="set-F3">
                <h4>3. When do I pay Article Publication Charges?</h4>
                <p>Article Publication Charges will have to be paid after the manuscript has been peer reviewed and accepted for publication. The corresponding author will be responsible for arranging the payments. </p>
              </div>
            </div>
            <div id="Special_Issues" class="content">
              <h2>Special Issues</h2>
              <hr/>
              <ul>
                <li>
                  <div>
                    <h3 class="mb10 blue-text" style="top:-5px; position:relative;">Lung Cancer</h3>
                    <p class="mb10"><span class="f15 blue-text">Editor Affiliation:</span><br>
                      Kazuya Fukuoka, MD, PhD,<br>
                      Department of Medical Oncology<br>
                      Sakai Hospital, Kinki University Faculty of Medicine<br>
                      Japan</p>
                    <p class="mb10">Submission date: March 15, 2017 <br>
                      Publication date: April 02, 2017</p>
                    <p>Description of the Special Issue: The special issue focuses on all areas of lung cancer from basic biology to advanced treatment, prevention, epidemiology and etiology. Original articles, research, reviews, mini reviews, cases, commentaries, short communications, letters, opinions are accepted for publication.</p>
                    <p><b>Submit manuscript to: </b><a href="mailto:editor.icst@oatext.com">editor.icst@oatext.com</a> </p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h3 class="mb10 blue-text" style="top:-5px; position:relative;">Breast cancer</h3>
                    <p class="mb10"><span class="f15 blue-text">Editor Affiliation:</span><br>
                      Masakazu Toi, M.D. Ph.D,<br>
                      Professor<br>
                      Breast Surgery, Kyoto University<br>
                      Japan</p>
                    <p class="mb10">Submission date: Feb 25, 2017<br>
                      Publication date: March 20, 2017</p>
                    <p>Description of the Special Issue: The special issue is interested in all areas of biology and medicine related to breast cancer, including basic biology, genetics, causes, diagnosis, treatment etc., with focus on latest advances in breast cancer therapy. All types of articles are accepted for publication.</p>
                    <p><b>Submit manuscript to: </b><a href="mailto:editor.icst@oatext.com">editor.icst@oatext.com</a> </p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h3 class="mb10 blue-text" style="top:-5px; position:relative;">Brain tumor</h3>
                    <p class="mb10"><span class="f15 blue-text">Editor Affiliation:</span><br>
                      Damir Nizamutdinov<br>
                      Associate Director of Neuro-Oncology Research<br>
                      Department of Neurosurgery<br>
                      Baylor Scott and White Health<br>
                      Assistant Professor of Surgery<br>
                      Texas A&M University<br>
                      USA</p>
                    <br>
                    <p>Ekokobe Fonkem<br>
                      Neuro-Oncologist<br>
                      Baylor Scott & White Health<br>
                      USA</p>
                    <p class="mb10">Submission date: Feb 27, 2017<br>
                      Publication date: March 20, 2017</p>
                    <p>Description of the Special Issue: The special issue accepts articles on all aspects related to the research in brain tumor and neuro oncology. Clinical and experimental results in tumor pathology, reviews, case reports on brain tumor, short communications, opinions, commentaries etc., are welcome for publication under this section.</p>
                    <p><b>Submit manuscript to: </b><a href="mailto:editor.icst@oatext.com">editor.icst@oatext.com</a> </p>
                  </div>
                </li>
                <hr/>
                <li>
                  <div>
                    <h3 class="mb10 blue-text" style="top:-5px; position:relative;">Cancer Surgery</h3>
                    <p class="mb10"><span class="f15 blue-text">Editor Affiliation:</span><br>
                      Johnson Miller Denise<br>
                      Director <br>
                      Hackensack Meridian Health<br>
                      NJ, USA</p>
                    <p class="mb10">Submission date: March 05, 2017<br>
                      Publication date: March 30, 2017</p>
                    <p>The special issue focus on the surgical treatment of different cancer types like Breast Cancer, Lung Cancer, Kidney Cancer, Gastro intestinal cancer, gynaecological cancer, Thyroid Cancer, Prostate Cancer, neuro oncology, etc. All types of articles along with surgical images are suitable for publication.</p>
                    <p><b>Submit manuscript to: </b><a href="mailto:editor.icst@oatext.com">editor.icst@oatext.com</a> </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--CONTENT X -->
<a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
<!--****************************************************** FOOTER *****************************************************-->
<div class="wrapper bottom-footer">
  <div class="row">
    <div class="large-6 columns">
      <ul>
        <li><a href="PrivacyPolicy.php">Privacy Policy</a></li>
        <li><a href="Terms- conditions.php">Terms & conditions</a></li>
      </ul>
    </div>
    <div class="large-6 columns">
      <p class="text-right white-text">© 2017 Copyright OAT. All rights reserved.</p>
    </div>
  </div>
</div>
<!--****************************************************** FOOTER-X *****************************************************-->
<!--home content s -->
<!--<script src="js/vendor/jquery.js"></script> -->
<script src="js/foundation-latest.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
<script src="js/scroll.js"></script>
<script>
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
		</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53268443-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
